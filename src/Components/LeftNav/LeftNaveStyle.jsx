import styled from 'styled-components';

const LeftNevStyle = styled.div`
height: 100%;
&
.left-side-part {
    background: #00356a;
    height: 100% !important;
}
.left-side-part ul li a {
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-start;
    color: #fff;
    padding: 0px;
    width: 100%;
    align-items: center;
    &:hover{
        text-decoration:none;
    }
}
 
.left-side-part span.text-left {
    text-align: center;
    text-transform: capitalize;
    max-width: calc(100% - 35px);
    text-align: left;
    font-weight: 400; 
    padding-left:10px;
}
.left-side-part ul li {
    /* text-align: center; */
    /* display: flex; */
    padding: 10px 15px;
    list-style:none;
	position: relative;
    border-bottom:1px solid rgba(255, 255, 255, 0.3);
}
span.icon-lft-now {
    max-width: 25px;
    flex: 0 0 25px;
}
span.icon-lft-now img{
    max-width:25px;
    max-height:25px;
    object-fit:contain;
}
.left-side-part li a i {
    font-size: 22px;
    opacity:0.5;
} 
.left-side-part li a  img{
    opacity:0.5;
}

.left-side-part li.active,.left-side-part li:hover {
    /* background: rgba(255, 255, 255, 0.9); */
    background: #5286a9;
}
.left-side-part li.active a i, .left-side-part li.active a img,.left-side-part li:hover i, .left-side-part li:hover img {
    opacity:1;
}
.left-side-part li.active a,.left-side-part li:hover a{
    color: #00356a;
}
.left-side-part ul li:after {
    content: "";
    position: absolute;
    left: 50%;
    width: 160px;
    background: rgb(255,255,255);
    background: radial-gradient(circle, rgba(255,255,255,1) 50%, rgba(255,255,255,0) 100%);
    height: 1px;
    transform: translateX(-50%);
    top: 0;
    display:none;
}
img.order_second_image,
img.autoship_second_image {
    display: none;
}
/* li.nav-item.active .order_first_image,li.nav-item:hover .order_first_image{
    display: none;
} */
li.nav-item.active .order_second_image,li.nav-item:hover .order_second_image  {
    display: block;
    text-align: center;
    margin: 0 auto;
}
/* li.nav-item.active .autoship_first_image,li.nav-item:hover .autoship_first_image {
    display: none;
} */
li.nav-item.active .autoship_second_image, li.nav-item:hover .autoship_second_image{
    display: block;
    text-align: center;
    margin: 0 auto;
}
`;

export default LeftNevStyle;