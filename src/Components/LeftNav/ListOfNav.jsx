import Link from "next/link";
import { useRouter } from "next/router";

const TextHtml = ({ title, icon, status, classes }) => (
    <>
        <span className="icon-lft-now">
            {status === "icon" ? <i className={icon} aria-hidden="true"></i> : <img src={icon} className={classes ? classes : ""} />}
        </span>
        <span className="text-left">{title}</span>
    </>
)

const ListOfNav = ({ customerCheck }) => {
    const router = useRouter();
    return (
        <>
            <ul>
                <li className={`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/dashboard/' ? 'active' : ''}`}>
                    <Link className="nav-link" href={`/${router?.query?.page}/user/dashboard`}>
                        <a> <span className="icon-lft-now">
                            <img src="/dashboard.png" className="order_first_image" />
                        </span>
                            <span className="text-left">Dashboard</span></a>
                    </Link>
                </li>
                <li className={`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/profile/' ? 'active' : ''}`}>
                    <Link className="nav-link" exact href={`/${router?.query?.page}/user/profile`}>
                        <a> <span className="icon-lft-now">
                            <img src="/profileicon.png" className="order_first_image" />
                        </span>
                            <span className="text-left">Profile</span></a>
                    </Link>
                </li>
                <li className={`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/order/' || router?.query?.orderid > 0 ? 'active' : ''}`}>
                    <Link className="nav-link" exact href={`/${router?.query?.page}/user/order`}>
                        <a>
                            <span className="icon-lft-now">
                                <img src="/order.png" className="order_first_image" />
                            </span>
                            <span className="text-left">Orders</span>
                        </a>
                    </Link>
                </li>
                <li className={`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/smartshiporder/' ? 'active' : ''}`}>
                    <Link className="nav-link" exact href={`/${router?.query?.page}/user/smartshiporder`}>
                        <a>
                            <span className="icon-lft-now">
                                <img src="/smartship.png" className=" autoship_first_image" />
                            </span>
                            <span className="text-left"> SmartShip Orders</span>
                        </a>
                    </Link>
                </li>
                <li className={`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/address/' ? 'active' : ''}`}>
                    <Link className="nav-link" exact href={`/${router?.query?.page}/user/address`}>
                        <a>
                            <span className="icon-lft-now">
                                <img src="/address.png" className=" autoship_first_image" />
                            </span>
                            <span className="text-left">Addresses</span>
                        </a>
                    </Link>
                </li>
                <li className={`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/documents/' ? 'active' : ''}`}>
                    <Link className="nav-link" exact href={`/${router?.query?.page}/user/documents`}>
                        <a>
                            <span className="icon-lft-now">
                                <img src="/documents.png" className=" autoship_first_image" />
                            </span>
                            <span className="text-left">Documents</span>
                        </a>
                    </Link>
                </li>
                <li className={`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/savedcards/' ? 'active' : ''}`}>
                    <Link className="nav-link" exact href={`/${router?.query?.page}/user/savedcards`}>
                        <a> <span className="icon-lft-now">
                            <img src="/payments.png" className=" autoship_first_image" />
                        </span>
                            <span className="text-left">Payments</span></a>
                    </Link>
                </li>
                {customerCheck > 2 &&
                    <li className={`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/invitation_link' ? 'active' : ''}`}>
                        <Link className="nav-link" exact href={`/${router?.query?.page}/user/invitation_link`}>
                            <a>
                                <TextHtml title="Marketing" icon="fa fa-credit-card" status="icon" />
                            </a>
                        </Link>
                    </li>
                }
                {customerCheck > 2 &&
                    <>
                        <li className={`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/downline/' ? 'active' : ''}`}>
                            <Link className="nav-link" href={`/${router?.query?.page}/user/downline`}>
                                <a>
                                    <span className="icon-lft-now">
                                        <img src="/downline.png" className=" autoship_first_image" />
                                    </span>
                                    <span className="text-left">Downline</span>
                                </a>
                            </Link>
                        </li>
                        <li className={`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/commissions/' ? 'active' : ''}`}>
                            <Link className="nav-link" href={`/${router?.query?.page}/user/commissions`}>
                                <a> <span className="icon-lft-now">
                                    <img src="/commissions.png" className=" autoship_first_image" />
                                </span>
                                    <span className="text-left">Commissions</span></a>
                            </Link>
                        </li>
                        <li className={`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/reports/' ? 'active' : ''}`}>
                            <Link className="nav-link" exact href={`/${router?.query?.page}/user/reports`}>
                                <a><span className="icon-lft-now">
                                    <img src="/reports.png" className=" autoship_first_image" />
                                </span>
                                    <span className="text-left">Business Info</span></a>
                            </Link>
                        </li>
                        <li className={`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/refundreport/' ? 'active' : ''}`}>
                            <Link className="nav-link" href={`/${router?.query?.page}/user/refundreport`}>
                                <a><span className="icon-lft-now">
                                    <img src="/reports.png" className=" autoship_first_image" />
                                </span>
                                    <span className="text-left"> Refund Reports</span></a>
                            </Link>
                        </li>
                        <li className={`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/active_smartships/' ? 'active' : ''}`}>
                            <Link className="nav-link" exact href={`/${router?.query?.page}/user/active_smartships/`}>
                                <a> <span className="icon-lft-now">
                                    <img src="/smartship.png" className=" autoship_first_image" />
                                </span>
                                    <span className="text-left"> Downline Active SmartShip </span></a>
                            </Link>
                        </li>
                        <li className={`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/coupon_list/' ? 'active' : '' || router?.query?.orderid > 0 ? 'active' : ''}`}>
                            <Link className="nav-link" exact href={`/${router?.query?.page}/user/coupon_list`}>
                                <a><span className="icon-lft-now">
                                    <img src="/couponlist.png" className=" autoship_first_image" />
                                </span>
                                    <span className="text-left">Coupon List</span></a>
                            </Link>
                        </li>
                        <li className=
                            {`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/wallet/'
                                || router?.asPath === '/' + router?.query?.page + '/user/wallet/?logtype=kairecash'
                                || router?.asPath === '/' + router?.query?.page + '/user/wallet/?logtype=Commissions'
                                ? 'active' : ''}
                    `}>
                            <Link className="nav-link" href={`/${router?.query?.page}/user/wallet`}>
                                <a><span className="icon-lft-now">
                                    <img src="/wallet.png" className=" autoship_first_image" />
                                </span>
                                    <span className="text-left">Wallet </span></a>
                            </Link>
                        </li>
                    </>
                }

                {/* <li className={`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/promoter_packs/' ? 'active' : '' || router?.query?.orderid > 0 ? 'active' : ''}`}>
                        <Link className="nav-link" exact href={`/${router?.query?.page}/user/promoter_packs`}>
                            <a><TextHtml title="Promoter Packs" icon="fa fa-folder-open" status="icon" /></a>
                        </Link>
                </li>*/}

                <li className={`nav-item ${router?.asPath === '/' + router?.query?.page + '/user/customer_service/' ? 'active' : '' || router?.query?.orderid > 0 ? 'active' : ''}`}>
                    <Link className="nav-link" exact href={`/${router?.query?.page}/user/customer_service`}>
                        <a><span className="icon-lft-now">
                            <img src="/customerservice.png" className=" autoship_first_image" />
                        </span>
                            <span className="text-left">Customer Service </span></a>
                    </Link>
                </li>
            </ul >
        </>
    )
}
export default ListOfNav;