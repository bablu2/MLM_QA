import Link from "next/link";
import { useRouter } from "next/router";
import LeftNevStyle from "./LeftNaveStyle";
import { AiFillCreditCard } from 'react-icons/ai';
import ListOfNav from "./ListOfNav";



const LeftNav = (props) => {
    return (
        <div className="col-md-2">
            <LeftNevStyle>
                <div className="left-side-part">
                    <ListOfNav {...props} />
                </div>
            </LeftNevStyle>
        </div>

    )
}
export default LeftNav;