import React, { useState } from "react";
import { Toolbar, Box, IconButton, Drawer, Typography } from "@material-ui/core";
import ListOfNav from './ListOfNav';
import { Close, Menu } from "@material-ui/icons";
import styled from 'styled-components';

const DrawerStyle = styled.div`


`;
export default function MobileLeftNav({ customerCheck, getDetails }) {
    const [open, setOpen] = useState(false);

    const toggleSlider = () => {
        setOpen(!open);
    };
    return (
        <DrawerStyle>
            <div className="container_of_slider">
                <Box component="nav">
                    <Toolbar>
                        <IconButton onClick={toggleSlider}>
                            <Menu />
                        </IconButton>
                        <Typography>Menu</Typography>
                        <Drawer open={open} anchor="left" onClose={toggleSlider} classes={{ paper: "drawer_main_class" }}>
                            <Box className="manu_slider_container" component="div">
                                <Close onClick={() => toggleSlider()} />
                                <ListOfNav customerCheck={customerCheck} getDetails={getDetails} />
                            </Box>
                        </Drawer>
                    </Toolbar>
                </Box>
            </div>
        </DrawerStyle>
    );
}
