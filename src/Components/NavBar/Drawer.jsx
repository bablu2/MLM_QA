import * as React from "react";
import { Box, Drawer, Button, List, Divider, ListItem, ListItemButton, ListItemIcon, ListItemText, } from "@material-ui/core";

export default function TemporaryDrawer({ state, toggleDrawer, data, section, getData }) {
  const list = (anchor) => (
    <Box sx={{ width: 300 }} role="presentation" onKeyDown={() => toggleDrawer(section)}>
      <h2 onClick={() => toggleDrawer(section)}> <span>X</span> Select {section}</h2>
      <List>
        {data.map((text, index) => (
          <ListItem key={text} disablePadding onClick={() => {
            getData(section, text);
          }}>
            <ListItemButton>
              <ListItemText primary={text} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
      <Divider />
    </Box>
  );

  return (
    <div>
      <React.Fragment>
        <Drawer className="cstm-right-drawer" anchor={"right"} open={state} onClose={() => toggleDrawer(section)}>
          {list("right")}
        </Drawer>
      </React.Fragment>
    </div>
  );
}
