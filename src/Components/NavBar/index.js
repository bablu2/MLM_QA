import Link from "next/link";
import React, { useEffect, useState } from "react";
import Image from 'next/image'
// import img from '../Images/logo.jpg'
import { useRouter } from "next/router";
import 'react-toastify/dist/ReactToastify.css';
import { FaShoppingCart, FaHeart, FaUser, FaSignOutAlt, FaBell } from "react-icons/fa";
import api from '@api/Apis'
import NavBarStyle from "./NavBarStyle";
import classNames from 'classnames';
import NotFication, { WishListNotFication, LogoutNav, MenuDropDownPop } from "./NotFicationLogout";
import NumberFormatComp from "@Components/Common/NumberFormatComp";
import ViewCartPopup from "@Components/ViewCartPopup";
import TemporaryDrawer from "./Drawer";
import { Button } from "@material-ui/core";
import SignUpRefferalPopUp from "src/pages/[page]/signup/ReviewPage/SignUpRefferalPopUp";
import LoginPopup from "@Components/Common/LoginPopup";

const NavBar = props => {
    const [allcategory, setallcategory] = useState();
    const [AddClass, setAddClass] = useState(false);
    const [menuData, setMenuData] = useState([]);
    const [token, setToken] = useState('')
    const [open, setOpen] = useState(false);
    const popRef = React.useRef(null);
    const [getDomain, setDomain] = useState()
    const router = useRouter();
    const [isActiveMobile, setIsActiveMobile] = useState("");
    const [openLoginPopup, setOpenLoginPopup] = useState(false)
    const store = (router?.query?.page) ? router?.query?.page : 'us';

    useEffect(() => {
        api.getAllCategory(store).then(res => {
            setallcategory(res?.data?.categories)
        });
        _menuSectionFunction();
    }, [])

    useEffect(() => {
        const data = localStorage.getItem("Token")
        setToken(data)
        const dataref = JSON.parse(localStorage.getItem('correctRefferal'))
        setDomain(dataref ? props?.correctRefferal?.data?.name : dataref?.data?.name)
    }, [props?.correctRefferal, router]);

    const _menuSectionFunction = async () => {
        await api.MenuSectionOption().then((response) => {
            if (response?.status === 200 && response?.data?.code === 1) {
                setMenuData(response?.data?.menu_data)
            }
        }).catch((err) => console.log(err))
    }
    const showlogin = () => {
        setAddClass(false);
        props.setoldpath(router.asPath)
        router.push(`/us/login`)
        if (router.push(`/us/login`)) {
            setTimeout(() => {
                setOpenLoginPopup(true)
            }, 2000);
        }

    }
    // const Validatelogin = (e) => {
    //     if (getDomain) {
    //         // setOpen(true);
    //         router.push(`us/signup/?d=distributer`)
    //     }
    //     else {
    //         popRef.current.setRefferalPopup(true);
    //         popRef.current.setGoToPromoterPack(true);
    //         // router.push(`/${router.query.page}/signup/?d=distributor`)
    //         popRef.current.setCorrectRefferal({ status: false, data: {} });
    //         setOpen(true);
    //     }
    // }
    const [countryState, setCountryState] = React.useState({ popUp: false, value: "USA" });
    const [languageState, setLanguageState] = React.useState({ popUp: false, value: "ENG" });

    const toggleDrawer = (section, value) => {
        if (section === "Country") {
            setCountryState({ ...countryState, popUp: true });
            return;
        }
        setLanguageState({ ...languageState, popUp: true });
    };

    function _setValues(section, value) {
        if (section === "Country") {
            setCountryState({ value: value, popUp: false });
            return;
        }
        setLanguageState({ value: value, popUp: false });
    }
    return (
        <>
            <NavBarStyle image="/header.png">
                <div className="header-section">
                    <div className="upper_lower_div">
                        <div className="d-flex">
                            <div className="broughtby">
                                {getDomain &&
                                    <span>Brought to you By:{getDomain}</span>}
                            </div>
                            <div className="usa_country">
                                <button onClick={() => toggleDrawer("Country")}>{countryState?.value}</button>
                                <button onClick={() => toggleDrawer("Language")}>{languageState?.value}</button>
                            </div>
                        </div>
                    </div>
                    <nav className="navbar navbar-default">
                        <div className="navbar-header">
                            {AddClass ?
                                <button type="button" className="close" data-toggle="collapse" onClick={() => setAddClass(false)}><i className="fa fa-times" aria-hidden="true"></i></button>
                                :
                                <button type="button" className="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbar-collapse-1" onClick={() => setAddClass(true)}>
                                    <span className="sr-only">Toggle navigation</span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                            }
                            <Link className="nav-link" exact="true" href="/" >
                                <a className="navbar-brand">
                                    <img src="/images/nav_logo.png" />
                                </a>
                            </Link>
                        </div>

                        <div className={classNames("collapse navbar-collapse ", { 'open_collapse_nav': AddClass, "hide": !AddClass })} id="navbar-collapse-1">
                            <ul className={`nav navbar-nav navbar-left`}>
                                {
                                    menuData?.map(({ title, data, url, check }, index) => (
                                        <MenuDropDownPop title={title} url={url} innerData={data} key={index + 1} check={check}
                                            setAddClass={setAddClass} setIsActive={setIsActiveMobile} isActive={isActiveMobile} />
                                    ))
                                }
                            </ul>
                            <div className="upper_lower_div">
                                <div className="d-flex">
                                    <div className="broughtby">
                                        {getDomain &&
                                            <span>Brought to you By: {getDomain}</span>}
                                    </div>
                                    <div className="usa_country">
                                        <button onClick={() => toggleDrawer("Country")}>{countryState?.value}</button>
                                        <button onClick={() => toggleDrawer("Language")}>{languageState?.value}</button>
                                    </div>
                                </div>
                                <ul className="nav navbar-nav navbar-right" onClick={() => { props?.updatecartdata(true) }}>
                                    {/* my account section with pop up html */}
                                    {/*   {
                                        (props.isLogin === true || token !== null) &&
                                        <li className="share-kaire">   <a href="https://kaireaffilate-eox300hfq-mlm-e-comm.vercel.app/" className="font_change" target="_blank">ShareKaire</a>
                                        </li>
                                    }*/}
                                    <li>
                                        <>
                                            {(props.isLogin === true || token !== null) ?
                                                <>
                                                    <div className="dashboard_div">
                                                        <Link className="nav-link" exact="true" href={`/${store}/user/dashboard`}>
                                                            <a onClick={() => setAddClass(false)}>
                                                                <div className="account_title font_change">My Account</div>
                                                                <div className="account_icon font_change"><FaUser />
                                                                </div>
                                                            </a>
                                                        </Link>
                                                        <LogoutNav store={store} section="from_navBar_section"
                                                            {...props} key="NavbarSection3" token={token} />
                                                    </div>
                                                </>
                                                :
                                                <div className="login_signin">
                                                    {/* <a type="button" className="Register_cls" onClick={(e) => { Validatelogin(e) }} >Register  as distributor</a>*/}
                                                    {/*     <a type="button" className="Register_cls" href={`/${store}/signup/?d=distributor`} >Register as distributer</a>*/}
                                                    <a onClick={showlogin} style={{ cursor: "pointer", marginTop: '2px' }} className="font_change">Login</a>
                                                </div>
                                            }
                                        </>
                                    </li>

                                    {/* view Cart with pop up html */}
                                    <li className="view_cart_section" onClick={() => setAddClass(false)}>
                                        <Link className="" exact="true" href={`/${store}/cart/viewCart`}>
                                            <div className="dashboard_div">
                                                <a>
                                                    <div className="cart_title font_change" style={{
                                                        cursor: 'pointer'
                                                    }}>
                                                        View Cart
                                                    </div>
                                                    <div className="icon-box-cart">
                                                        <FaShoppingCart />
                                                        {props?.cartdata?.products?.length > 0 && props?.counts &&
                                                            <span>{props?.counts ? props?.counts : 0}</span>
                                                        }
                                                    </div>
                                                </a>
                                            </div>
                                        </Link>

                                        <ul className="dropdown-popup_viewCart">
                                            {props?.cartdata?.products
                                                ?
                                                <>
                                                    {props?.cartdata?.products?.map((productdata, index) => {
                                                        return (
                                                            <li className="" key={index + 1} >
                                                                <div className="view_cart_div">
                                                                    {productdata?.product?.product_images[0]?.image
                                                                        ?
                                                                        <img src={`${process.env.API_URL}${productdata.product?.product_images[0]?.image}`} />
                                                                        :
                                                                        <img src="/images/no-image.jpg" />
                                                                    }
                                                                </div>
                                                                <div className="txt" >
                                                                    {productdata?.variant === null ?
                                                                        <>
                                                                            {productdata?.product.name} <br />
                                                                            Qty:{productdata?.quantity} <br />

                                                                            <NumberFormatComp value={parseFloat(props?.cartdata?.products?.[0]?.is_autoship === 'True' ?
                                                                                productdata?.product?.autoship_cost_price * productdata?.quantity
                                                                                :
                                                                                productdata?.product?.cost_price * productdata?.quantity).toFixed(2)} />
                                                                        </>
                                                                        :
                                                                        <>
                                                                            {productdata?.product.name}- {productdata?.variant?.name} <br />
                                                                            Qty:{productdata?.quantity} <br />
                                                                            <NumberFormatComp value={parseFloat(props?.cartdata?.products?.[0]?.is_autoship === 'True' ?
                                                                                productdata?.variant?.autoship_cost_price * productdata?.quantity
                                                                                :
                                                                                productdata?.variant?.cost_price * productdata?.quantity).toFixed(2)} />
                                                                        </>
                                                                    }
                                                                </div>

                                                            </li>
                                                        )
                                                    })}
                                                    <div className="view_cart_action">
                                                        <Button onClick={() => router.push(`/${store}/cart/viewCart`)}>View Cart</Button>
                                                    </div>
                                                </>

                                                :
                                                <p className="cart_empty">Cart is empty</p>
                                            }
                                        </ul>
                                    </li>
                                    {/* <NotFication store={store} section="from_navBar_section" {...props} key="NavbarSection1" /> */}
                                    <WishListNotFication store={store} section="from_navBar_section" {...props} key="NavbarSection2" />
                                </ul>
                            </div>
                        </div >
                    </nav >
                </div>
            </NavBarStyle >
            <TemporaryDrawer state={countryState?.popUp}
                getData={(section, value) => _setValues(section, value)}
                toggleDrawer={() => setCountryState({ ...countryState, popUp: false })}
                data={['USA', 'Canada', 'Australia', ' New Zealand']} section="Country" />

            <TemporaryDrawer state={languageState?.popUp}
                getData={(section, value) => _setValues(section, value)}
                toggleDrawer={() => setLanguageState({ ...languageState, popUp: false })}
                data={['English']} section="Language" />

            <ViewCartPopup ref={popRef} content={{
                open: open,
                setOpen: setOpen,
                ...props
            }} />
            <LoginPopup openLoginPopup={openLoginPopup}
                setOpenLoginPopup={setOpenLoginPopup} />

        </>


    );
}
export default NavBar;