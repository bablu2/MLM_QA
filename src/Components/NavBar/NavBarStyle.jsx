import styled from "styled-components";

const NavBarStyle = styled.div`
  @font-face {
    font-family: "Graphik";
    src: url("./fonts/Graphik-Regular.woff2") format("woff2"),
      url("./fonts/Graphik-Regular.woff") format("woff");
    font-weight: normal;
    font-style: normal;
    font-display: swap;
  }
  @font-face {
    font-family: "Graphik-bold";
    src: url("./fonts/Graphik-Semibold.woff2") format("woff2"),
      url("/public/fonts/Graphik-Semibold.woff") format("woff");
    font-weight: 500;
    font-style: normal;
    font-display: swap;
  }
  .upper_lower_div {
    gap: 15px;
    ul li {
      .cart_title.font_change,
      & * {
        font-family: var(--common-font-bd);
        font-size: 13px;
        cursor: pointer;
      }
    }
    .d-flex {
      align-items: center;
      color: #fff;
      font-family: "Graphik-bold";
    }
  }
  li.products_section_class,
  li.the.original.pycnogenol_section_class {
    & > div {
      position: relative;
      padding-right: 20px;
      &:after {
        content: "";
        display: block;
        box-sizing: border-box;
        position: absolute;
        width: 10px;
        height: 10px;
        border-bottom: 2px solid;
        border-right: 2px solid;
        transform: rotate(45deg);
        right: 4px;
        top: 3px;
        border-color: #fff;
      }
    }
  }
  .header-section {
    background-image: ${(props) => `url(${props.image})`};
  }
  .dropdown-popup_viewCart p.cart_empty,
  .wishlist_empty {
    text-align: center;
    margin: 0 auto;
    font-size: 15px;
  }
  .navbar-left li.active_current {
    text-decoration: none;
  }
  ul.dropdown-popup_viewCart li {
    padding: 0 4px 10px;
    border-bottom: 1px dotted #e1e1e1;
    margin-bottom: 10px;
    align-items: center;
    &:last-child {
      margin-bottom: 0;
      padding-bottom: 0;
      border: none;
    }
  }
  ul.dropdown-popup_viewCart {
    box-shadow: 0px 0px 10px 0px #ddd;
    border-radius: 0px;
  }
  .header-section ul.nav.navbar-nav.navbar-left {
    margin: 0;
    padding: 0;
  }

  .header-section ul.nav.navbar-nav.navbar-left li div {
    color: #fff;
    font-size: 16px;
    line-height: normal;
    cursor: pointer;
    span {
      svg {
        width: 15px;
        position: relative;
        top: 4px;
        margin-left: 4px;
      }
    }
  }
  .header-section ul.nav.navbar-nav.navbar-left li a:hover {
    text-decoration: none;
    outline: none;
  }
  ul.nav.navbar-nav.navbar-left li ul {
    list-style: none;
    margin: 0;
    padding: 0;
    background: #00356a;
    -webkit-border-radius: 10px;
    border-radius: 0;
    padding: 15px 0 30px;
    position: absolute;
    left: 0;
    top: 95px;
    min-width: 200px;
    display: none;
    z-index: -1;
    overflow: hidden;
    /* border-bottom: 6px solid #e3f2f7 */
  }
  ul.nav.navbar-nav.navbar-left li ul li {
    padding: 5px 20px;
    cursor: pointer;
    color: #fff;
  }
  ul.nav.navbar-nav.navbar-left {
    & > li {
      padding: 40px 0;
      @media (max-width: 1024px) {
        padding: 0;
      }
      :not(:last-child) {
        margin-right: 40px;
        @media (max-width: 1024px) {
          margin: 0;
        }
      }
    }
  }
  ul.nav.navbar-nav.navbar-left > li:hover ul {
    display: block;
  }
  .header-section .navbar {
    padding: 0px 15px;
  }

  ul.nav.navbar-nav.navbar-left li ul li:hover {
    color: #eed7d7;
    text-decoration: none;
  }

  .Toastify__toast-container--top-right {
    top: 7em;
  }
  ul.nav.navbar-nav.navbar-left li {
    a:hover,
    span:hover {
      color: #eed7d7;
    }
  }
  // 17-01-2022
  li.wishlist_section .icon-box svg {
    color: #fff;
  }

  .upper_lower_div {
    float: right;
    display: flex;
    flex-direction: column;
    max-width: 380px;
    width: 100%;
    align-items: flex-end;
    ul {
      &.navbar-top {
        margin-bottom: 7px;
        li {
          display: flex;
          &:first-child {
            a {
              padding-left: 5px;
              padding-bottom: 5px;
              @media (max-width: 1024px) {
                padding: 0;
              }
            }
          }
          a {
            padding-top: 5px;
            padding-bottom: 5px;
            color: #fff;
            /* &:hover {
                        text-decoration: underline;
                    } */
          }
          span {
            color: #fff;
            padding-top: 9px;
            @media (max-width: 1024px) {
              padding: 0;
              margin: 0 10px;
            }
          }
        }
      }
    }
  }

  /* 08-03-2022 */
  .navbar-brand {
    height: auto;
    padding: 0 10px 0 0;
    img {
      max-width: 180px;
      width: 100%;
    }
  }

  .upper_lower_div {
    li.wishlist_section.from_navBar_section {
      a {
        padding: 0;
      }
    }
  }

  nav.navbar.navbar-default {
    display: flex;
    align-items: center;
    width: 100%;
    height: 99px;
  }

  ul.products_pop_class:before {
    content: "";
    height: 5px;
    width: 100%;
    position: absolute;
    top: 0;
    filter: blur(5px);
    backdrop-filter: blur(5px);
    z-index: -1;
    background: #000000ad;
  }
  .collapse.navbar-collapse {
    max-width: calc(100% - 58px);
    flex: calc(100% - 58px);
    width: 100%;
    justify-content: flex-end;
    align-items: center;
    padding-right: 0;
    @media (min-width: 1025px) {
      display: inline-flex !important;
    }
  }

  ul.nav.navbar-nav.navbar-left {
    max-width: calc(100% - 380px);
    flex: calc(100% - 380px);
    width: 100%;
  }
  ul.nav.navbar-nav.navbar-right > li a.font_change {
    padding: 0;
    color: #fff;
    line-height: normal;
  }

  ul.nav.navbar-nav.navbar-right {
    float: none !important;
  }

  ul.nav.navbar-nav.navbar-right > li .dashboard_div {
    line-height: normal;
  }

  .upper_lower_div ul.nav.navbar-nav.navbar-right li.share-kaire:after {
    content: "";
    height: 100%;
    width: 2px;
    background: #fff;
    position: absolute;
    right: 0;
    top: 0;
    @media (max-width: 1024px){
      display: none;
    }
  }

  .upper_lower_div ul.nav.navbar-nav.navbar-right li.share-kaire {
    padding-right: 10px;
    margin-right: 5px;
  }

  ul.nav.navbar-nav.navbar-left li span {
    font-family: "Graphik";
  }
  .header-section .nav li a {
    align-items: center;
  }
  ul.nav.navbar-nav.navbar-right li .account_icon.font_change,
  ul.nav.navbar-nav.navbar-right li .icon-box-cart {
    padding-left: 8px;
  }

  /* 23-03-2022 */
  ul ul.products_pop_class li a.font_change {
    font-family: "Graphik";
    font-size: 16px;
  }

  .dropdown-popup_viewCart {
    position: absolute;
    right: 0;
    top: 20px;
  }
  /* 24-03-2022 */
  .upper_lower_div ul li a:hover {
    color: #eed7d7;
  }
  ul.nav.navbar-nav.navbar-right li svg:hover * {
    fill: #eed7d7;
  }

  .dashboard_div {
    .logout_icon.from_navBar_section {
      background: #fff;
      position: absolute;
      width: 100%;
      right: 0;
      border-radius: 0;
      color: #000;
      padding: 0px;
      display: flex;
      visibility: hidden;
      font-size: 12px;
      justify-content: center;
      @media (max-width: 1024px) {
        position: unset;
        color: #fff;
        background: none;
        margin-top: 10px;
        button {
          color: #fff;
        }
      }
      svg {
        fill: #003a65;
        margin-left: 5px;
        @media (max-width: 1024px) {
          fill: #fff;
        }
      }
    }
    &:hover {
      .logout_icon.from_navBar_section {
        visibility: visible;
      }
    }
    @media (max-width: 991px) {
      .logout_icon.from_navBar_section {
        visibility: visible;
      }
    }

    .logout_icon.from_navBar_section .MuiButtonBase-root.MuiIconButton-root {
      background: none;
    }
  }

  @media (max-width: 1320px) {
    .header-section ul.nav.navbar-nav.navbar-left {
      padding-left: 0;
    }
    .header-section .navbar-left li a {
      font-size: 16px;
    }
    .navbar-brand > img {
      max-width: 180px;
      width: 100%;
    }
  }
  @media (max-width: 1100px) {
    .navbar-brand {
      padding: 15px 1px;
    }
  }
  @media (max-width: 1024px) {
    button.close {
      color: #fff;
      position: absolute;
      opacity: 1;
      right: 10px;
      top: 50%;
      transform: translateY(-50%);
    }
    .navbar-brand {
      padding: 8px 1px;
    }
    ul.navbar-right li:nth-last-child(-n + 3) {
      width: auto;
    }
    .navbar-toggle {
      display: block;
    }
    .header-section .navbar {
      background: transparent;
      border: none;
      border-radius: 0 !important;
      padding: 8px 15px;
      margin-bottom: 0;
    }

    .navbar-header {
      float: none;
    }
    .collapse {
      display: none !important;
    }
    .collapse.open_collapse_nav {
      display: block !important;
    }
    /* .navbar-nav > li {
        float: none;
        width: 100%;
    } */
    .navbar-right {
      float: none !important;
    }
    .login_signin {
      display: block;
      width: 100%;
    }
    .navbar-left {
      float: none !important;
      width: 100%;
    }

    .login_signin a {
      text-align: left;
      margin: 0;
      float: left;
      width: 100%;
    }
    .navbar-nav > li {
      float: left;
      width: 100%;
    }
    .dashboard_div {
      margin-left: 10px;
    }
    .icon-box-cart span {
      top: -10px;
      left: 13px;
    }
    .notifications {
      margin-left: 10px;
    }
    .logout_icon {
      margin-top: 0px;

      .header-section ul.nav.navbar-nav.navbar-left {
        margin: 5px 0 0;
        padding: 0;
      }
      ul.nav.navbar-nav.navbar-left > li {
        position: relative;
      }

      ul.nav.navbar-nav.navbar-left > li:hover ul {
        display: block;
      }
    }
 
    span#quick-notifications-menu {
      line-height: 14px;
    }
    li.view_cart_section:hover .dropdown-popup_viewCart,
    li.notifications:hover .dropdown-menu-for-cart,
    li.wishlist_section:hover .dropdown-menu-wishlist {
      display: none;
    }

    .navbar-default .navbar-toggle {
      border-color: #fff;
    }
    .navbar-default .navbar-toggle .icon-bar {
      background-color: #fff;
    }
    .navbar-default .navbar-toggle:focus,
    .navbar-default .navbar-toggle:hover {
      background-color: transparent !important;
    }
   
    ul.nav.navbar-nav.navbar-left li ul {
      /* position: static; */
      // margin: 5px 0 5px;
      width: 100%;
    }

    ul.nav.navbar-nav.navbar-right .dashboard_div {
      margin-left: 0;
    }
    ul.nav.navbar-nav.navbar-right {
      margin: 0;
    }
    // 13-01-2022
    .header-section .navbar-left li a {
      font-size: 16px;
      padding-top: 0;
    }
    .collapse.open_collapse_nav {
      position: absolute;
      top: 100px;
      background: #06356a;
      width: 100%;
      left: 0;
      padding: 0 30px;
      border-top: 0;
      max-width: 100%;
      flex: 100%;
    }

    .navbar-header {
      display: flex;
      align-items: center;
    }
    .navbar-default .navbar-toggle {
      position: absolute;
      right: 15px;
      top: 50%;
      transform: translateY(-50%);
      margin: 0;
    }
    section ul.nav.navbar-nav.navbar-right li a,
    .header-section .navbar-right li,
    .login_signin a {
      padding: 0;
    }
    .header-section ul.nav.navbar-nav.navbar-right li .login_signin a,
    .header-section li .dashboard_div a {
      padding: 10px 0;
    }

    ul.nav.navbar-nav li *,
    .header-section ul.nav.navbar-nav.navbar-right li a {
      font-size: 16px;
      line-height: normal;
      font-family: "Graphik";
      @media (max-width: 1024px) {
        margin: 0;
        padding: 0;
      }
    }
    .header-section li.wishlist_section.from_navBar_section {
      @media (max-width: 1024px) {
        margin: 0 10px;
      }
    }
    ul.nav.navbar-nav.navbar-top li {
      justify-content: flex-start;
    }
    ul.nav.navbar-nav.navbar-left {
      flex: 100%;
      max-width: 100%;
    }
    .upper_lower_div {
      width: 100%;
      max-width: 100%;
    }
    li.wishlist_section.from_navBar_section > * {
      margin: 10px 15px 0;
    }
    li.logout_icon.from_navBar_section > * {
      margin: 10px 0 0;
    }
    .icon-box svg {
      margin: 0;
    }
    .collapse.open_collapse_nav ul li {
      padding: 10px 0 !important;
    }
    ul.nav.navbar-nav.navbar-top {
      margin: 0;
    }
  
  
  }


  @media (min-width: 1024px){
    .header-section {
       &>.upper_lower_div .d-flex {
    display: none;
}
}
  }
  @media (max-width: 768px) { 
    .collapse.open_collapse_nav {
      padding: 0 20px;
    }
  }

  @media (max-width: 480px) {
    button.close {
      color: #fff;
      opacity: 1;
      padding-top: 11px;
    }
    .navbar-brand {
      padding: 0;
    }
  }

  /* New Code CSS */
  @media only screen and (max-width: 1024px) {
    .collapse.open_collapse_nav {
      top: 74px;
    }
    ul.nav.navbar-nav.navbar-left li{ 
      ul {
        position: relative;
        z-index: 9;
        top: auto;
        left: auto;
        background: none;
        box-shadow: none;
        padding: 0;
        // border: 1px solid #fff;
        margin-top: 0px;  
      }
      .products_pop_class{
        margin: 0;
      } 
    } 
    ul.nav.navbar-nav.navbar-left li ul::before {
      display: none;
    }
    ul.nav.navbar-nav.navbar-left li ul li {
      padding: 10px !important;
    }
    .navbar-brand > img {
      max-width: 120px;
    }
    nav.navbar.navbar-default {
      height: 75px;
    }
    button.close {
      padding-top: 0;
    } 
  
    .collapse.open_collapse_nav ul.nav.navbar-nav.navbar-right li:not(.view_cart_section) {
      padding: 0px !important;
      margin-bottom: 15px;
      width: 100%;
      justify-content: flex-start;
    } 
    .upper_lower_div {
    padding-top: 10px; 
    .d-flex {
    justify-content: center;
    width: 100%;
  }

  }
.upper_lower_div ul.nav.navbar-nav.navbar-right {
    margin-right: 0;
    width: 100%;
    li.view_cart_section{
      padding: 0 !important;
    }
}
.navbar-collapse{
  .upper_lower_div {
    .d-flex{ display: none;}
    }
} 
 
li ul.products_pop_class {
    display: block !important; 
    max-height: 0; 
    margin-bottom: 0;
    transition: 0.3s ease all; 
    li.view_cart_section{
      padding-top: 0;
    }  
} 
 .toggle_active {
  .products_pop_class {
    max-height: 20rem !important;
  }
}
.collapse.open_collapse_nav ul.nav.navbar-nav.navbar-right li.logout_icon.from_navBar_section {
  margin-bottom: 0;
}
ul.products_pop_class {
  position: relative;
  :after {
    content: "";
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    height: 130px;
    width: 1px;
    border-left: 0.5px dashed #ddd;
    left: 0;  
    @media (max-width: 767px){
      height: 108px;
    }
  } 
} 
li.pycnogenol_section_class.toggle_close {
  padding: 5px 0!important;
}
[class*="MinCartStyle-sc"] {
  display: none;
} 
  } 
  @media (max-width: 767px){
    ul ul.products_pop_class li a.font_change{
      font-size: 14px;
    }
    ul.nav.navbar-nav.navbar-left li ul li {
      padding: 7px 10px !important;
  } 
  div#navbar-collapse-1>.upper_lower_div {
    padding-top: 0;
}
ul.nav.navbar-nav.navbar-right>li:first-child {
    margin-bottom: 5px !important;
}
  }
`;

export default NavBarStyle;
