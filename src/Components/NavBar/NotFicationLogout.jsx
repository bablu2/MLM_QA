import { FaHeart, FaSignOutAlt, FaBell } from "react-icons/fa";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import api from "@api/Apis";
import NumberFormatComp from "@Components/Common/NumberFormatComp";
import { IconButton } from "@material-ui/core";

export function MenuDropDownPop({ setAddClass, setIsActive, isActive, ...props }) {

    const { title, innerData, url, check } = props;
    const router = useRouter();
    const store = (router?.query?.page) ? router?.query?.page : 'us';

    return <li className={`${title.toLowerCase()}_section_class ${(isActive === title) ? "toggle_active" : "toggle_close"}`} onClick={() => {
        if (isActive === title) {
            setIsActive(isActive ? "" : title);
        } else {
            setIsActive(title);
        }
    }} > {/* Notfication icon with pop up  */}
        <div >

            <a href={(url)?.trim() === "/" ? "/" : `/${store}${url}`} target={check ? "_blank" : ""}>
                <span className="" id="">{title}</span>
            </a>
        </div>
        {((innerData?.length > 0) && title !== "Home") &&
            <ul className={`${title.toLowerCase()}_pop_class products_pop_class`} >
                {innerData?.map(({ title, url, check }, index) => <li key={index + 1} className="nav-item" >
                    {/* <Link href={(url)?.trim() === "/" ? "/" : `/${store}${url}`} >
                        <a target={check ? "_blank" : ""} className="font_change">
                            {title}
                        </a>
</Link>*/}
                    <a href={(url)?.trim() === "/" ? "/" : `/${store}${url}`} target={check ? "_blank" : ""} className="font_change">
                        {title}
                    </a>

                </li>
                )}
            </ul>
        }
    </li >;
}

const NotFication = ({ store, section, setAddClass, ...props }) => {
    const router = useRouter();
    const { usernotifications, setUsernotification } = props?.notificationSection
    useEffect(() => {
        if (props?.isLogin) {
            let token = localStorage.getItem('Token');
            api.getUserNotifications(token).then(res => {
                if (res?.data?.code === 1) {
                    setUsernotification(res?.data?.notifications)
                }
            })
        }
    }, [props?.isLogin])

    return <li className={`notifications ${section}`} > {/* Notfication icon with pop up  */}
        {props?.isLogin === true &&
            <div>
                <div className="">
                    <FaBell className="font_change" />
                    {usernotifications?.length > 0 &&
                        <span className="count label label-warning" id="quick-notifications-menu">
                            {usernotifications?.length > 0 ? usernotifications?.length : 0}
                        </span>
                    }
                </div>
                {/*pop for notification */}
                {usernotifications?.length > 0 &&
                    <ul className="dropdown-menu-for-cart">
                        {usernotifications?.map((notification, index) => {
                            return notification?.table_name === "Order" ?
                                <li key={index + 1} className="nav-item" onClick={() => {
                                    if (notification?.public_order_id !== null) {
                                        router.push({
                                            pathname: `/${store}/autoshipdetail`,
                                            query: { orderid: notification?.instance_id },
                                        })
                                    }
                                }}>
                                    {notification?.action_performed}
                                </li>
                                :
                                <li key={index + 1}>
                                    {notification?.action_performed}
                                </li>
                        })}
                    </ul>
                }
            </div>
        }
    </li >
}

const WishListNotFication = ({ store, setAddClass, section, ...props }) => {
    return <li className={`wishlist_section ${section}`}> {/* wishlist icon with popup html */}
        {
            props.isLogin === true &&
            <>
                {/* <Link className="" exact="true" href={`/${store}/wishlist`}>
                    <a>
                        <div className="icon-box">
                            <FaHeart className="font_change" />
                            {props?.counts?.wishlist?.length > 0 &&
                                <span>{props?.counts?.wishlist?.length > 0 ? props?.counts?.wishlist?.length : 0}</span>
                            }
                        </div>
                    </a>
                        </Link>*/}

                {/* pop of wishlist */}
                <ul className="dropdown-menu-wishlist dropdown-popup_viewCart">
                    {props?.counts?.wishlist?.length > 0 ? props?.counts?.wishlist?.map((productdata, index) => {
                        return (
                            <li className="" key={index + 1}>
                                <div className="view_cart_div">
                                    {productdata?.product?.product_images[0]?.image
                                        ?
                                        <img src={`${process.env.API_URL}${productdata.product?.product_images[0]?.image}`} />
                                        :
                                        <img src="/images/no-image.jpg" />
                                    }
                                </div>
                                <div className="txt" >
                                    {productdata?.variant === null ?
                                        <>
                                            {productdata?.product.name} <br />
                                            Qty:{productdata?.product?.quantity} <br />
                                            <NumberFormatComp value={props?.counts?.is_autoship_user === 'True' ?
                                                productdata?.product?.autoship_cost_price * productdata?.quantity
                                                :
                                                productdata?.product?.cost_price * productdata?.quantity} />
                                        </>
                                        :
                                        <>
                                            {productdata?.product.name}- {productdata?.variant?.name} <br />
                                            Qty:{productdata?.variant?.quantity} <br />
                                            <NumberFormatComp value={props?.counts?.is_autoship_user === 'True' ?
                                                productdata?.variant?.autoship_cost_price * productdata?.quantity
                                                :
                                                productdata?.variant?.cost_price * productdata?.quantity} />
                                        </>}
                                </div>
                            </li>
                        )
                    })
                        : ""}
                </ul>
            </>
        }
    </li>
}

export const LogoutNav = ({ store, section, setAddClass, token, ...props }) => {
    return <li className={`logout_icon ${section}`} > {/* LOgout icon with pop up  */}
        {(props.isLogin === true || token !== null) &&
            <IconButton onClick={props.handleLogout}>
                Log out
                <FaSignOutAlt />
            </IconButton>
        }
    </li>
}

export default NotFication;
export { WishListNotFication }