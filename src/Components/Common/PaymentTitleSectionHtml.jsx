import NumberFormat from "react-number-format";

const PaymentTitleSectionHtml = ({ text, numberValue, colSpan }) => (
  <>
    <td
      className="cstmsub-total-sub"
      style={{ textAlign: "right" }}
      colSpan={colSpan ? colSpan : "2"}
    >
      {text}:
    </td>
    <td className="cstmsub-total">
      <NumberFormat
        value={parseFloat(numberValue).toFixed(2)}
        displayType={"text"}
        thousandSeparator={true}
        prefix={"$"}
        renderText={(value) => (
          <div className="price_amount details_price">{value} USD</div>
        )}
      />
    </td>
  </>
);
export default PaymentTitleSectionHtml;
