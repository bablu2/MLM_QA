
import { useRouter } from "next/router";
import { toast } from 'react-toastify';
import { useState } from 'react';
import PaymentForm from './PaymentForm';
import PaymentFormStyle from './PaymentFormStyle';
import AddressSelect from './AddressSelect';
import api from '@api/Apis';

const CardPaymentSection = (props) => {
    const { placerorder, section, setshowloader, setPaySection, setInnerLoader, orderData } = props;
    const [nextButton, setNextButton] = useState(false);
    const [cardSectionData, setCardSectionData] = useState({});
    const [expanded, setExpanded] = useState("existing");
    const [selectedAddress, setSelectedAddress] = useState(null);
    const [addressListData, setAddressListData] = useState([]);
    const [sameAddressmsg, setSameAddressMsg] = useState('')

    const router = useRouter();

    // async function savedNewAddress(formData) {

    //     await api.saveAddress(formData).then(res => {
    //         if (res?.data?.code === 1) {
    //             toast.success('Address added successfully', {
    //                 duration: 1
    //             });
    //             AddressList(formData?.token)
    //             setExpanded("panel1");
    //         }
    //     })
    // }

    const AddressList = async (token) => {
        // setInnerLoader(true)
        await api.manageAddress(token).then(res => {
            if (res?.data?.code === 1) {
                // setInnerLoader(false)
                setAddressListData(res?.data?.addresses)
                setSameAddressMsg('')
            }
            // if (res?.data?.code === 0) {
            //     setSameAddressMsg(res?.data?.code)
            // }
        })
    }

    async function PlaceOrderAndSaveCard(data) {
        let formData = data !== null ? { billing_details: data, payment_details: cardSectionData } : { payment_details: cardSectionData };
        const result = await placerorder(formData);
        if (result?.status === 200 && result?.data?.message) {
            if (section === "checkout") {
                // toast.success(result?.data?.message)
                localStorage.removeItem('coupon_name')
                localStorage.removeItem('shippingAddress')
                localStorage.removeItem('billingAddress')
                setPaySection(false);
                router.push({
                    pathname: `/${router.query.page}/order/thankYou`,
                    query: { orderid: result?.data.order_id }
                });

            }
            if (section === "Autoship" || section === "savedCard") {
                // toast.success(result?.data?.message)
                setPaySection(false);
            }

            setNextButton(false);
        } else {
            // toast.error(result?.data?.message);
            // setshowloader(false);
            // setInnerLoader(false)
        }
        // .catch ((error) => console.log(error))

    }
    return (
        <PaymentFormStyle>
            {nextButton ?
                <AddressSelect section={section}
                    PlaceOrderAndSaveCard={PlaceOrderAndSaveCard}
                    setNextButton={setNextButton}
                    cardSectionData={cardSectionData}
                    setCardSectionData={setCardSectionData}
                    selectedAddress={selectedAddress}
                    setSelectedAddress={setSelectedAddress}
                    expanded={expanded}
                    setExpanded={setExpanded}
                    setPaySection={setPaySection}
                    //savedNewAddress={savedNewAddress}
                    AddressList={AddressList}
                    addressListData={addressListData}
                />
                :
                <PaymentForm setNextButton={setNextButton}
                    PlaceOrderAndSaveCard={PlaceOrderAndSaveCard}
                    setCardSectionData={setCardSectionData}
                    cardSectionData={cardSectionData}
                    {...props} />
            }

        </PaymentFormStyle>
    )
}
export default CardPaymentSection;