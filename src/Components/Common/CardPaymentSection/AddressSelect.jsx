import * as React from "react";
import { Typography, Accordion, Radio, RadioGroup, FormControlLabel, FormControl, FormLabel, Box } from "@material-ui/core";
import { ArrowForwardIosSharp } from "@material-ui/icons";
import MuiAccordionSummary from "@material-ui/core/AccordionSummary";
import MuiAccordionDetails from "@material-ui/core/AccordionDetails";
import ExistingAddresses from "./ExistingAddresses";
import NewAddressForm from "./NewAddressForm";
import { styled } from "@material-ui/styles";
import { FormProvider, useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers/yup';
import { toast } from "react-toastify";

export default function CustomizedAccordions(props) {
    const { expanded, setExpanded, selectedAddress, PlaceOrderAndSaveCard, setPaySection } = props;
    const [state, setState] = React.useState({
        country_name: "",
        region_name: "",
        error_country: "",
        error_state: ""
    });
    const [addressError, setAddressError] = React.useState('')
    // const handleChange = (panel) => (event, newExpanded) => {
    //     setExpanded(newExpanded ? panel : false);
    // };
    const handleChange = (event) => {
        setExpanded(event?.target?.value);
    };
    const validationSchecm = yup.object().shape({
        first_name: yup.string()
            .required('"This field is required"'),
        last_name: yup.string()
            .required("This field is required"),
        phone_number: yup.string()
            .required("This field is required")
            .matches(/^[0-9]/, "Please Enter valid phone number")
            .test('type', "Must be a number", value => !isNaN(value))
            .min(5, "Enter minimum 5 digit")
            .max(10, "Phone number not longer then 10 digit"),
        postal_code: yup.string()
            .required("This field is required")
            .matches(/^[0-9]/, "Enter only number.")
            .min(5, "Enter minimum 5 digit")
            .max(10, "Posttal code not longer then 8 digit"),
        street_address_1: yup.string()
            .required("This field is required"),
        city: yup.string()
            .required('This field is required')
    });

    const methods = useForm({
        mode: "all",
        resolver: yupResolver(validationSchecm),
    });
    const onSubmit = data => {
        setPaySection(false)
        let logintoken = localStorage.getItem('Token');
        if (state?.country_name !== "") {
            if (state?.region_name !== "") {
                data = {
                    ...data,
                    // street_address_2: null,
                    country: state?.country_name,
                    state: state?.region_name
                };
                //const formData = { data: data, token: logintoken };
                if (logintoken) {
                    PlaceOrderAndSaveCard(data);
                } else {
                    PlaceOrderAndSaveCard(data);
                }
            } else {
                setState({ ...state, error_state: "please select a state." })
            }
        } else {
            setState({ ...state, error_country: "please select a country." })
        }
    }

    function handleOnChangeExisting() {
        if (selectedAddress) {
            localStorage.setItem("billingAddress", selectedAddress)
            PlaceOrderAndSaveCard(null)
            setPaySection(false)
            setAddressError('')
        }

        else {
            setAddressError("Please choose a address.")
        }
    }

    return (
        <div className="choose_address_section">
            <div className="container main_payment_form_class">
                <FormControl component="fieldset">
                    <FormProvider {...methods}>
                        <form onSubmit={methods?.handleSubmit(onSubmit)}>
                            <FormLabel component="legend">Choose your Address</FormLabel>
                            <RadioGroup
                                aria-label="gender"
                                name="controlled-radio-buttons-group"
                                value={expanded}
                                onChange={handleChange}
                            >
                                <FormControlLabel value="existing" control={<Radio />} className="existing_radios radio_for_css" label="Existing Address" classes={{ root: "radio_button_existing" }} />
                                {expanded === "existing" &&
                                    <Box classes={{ root: "box_existing" }}>
                                        <ExistingAddresses {...props} />
                                    </Box>}

                                <FormControlLabel value="new" control={<Radio />} label="Add New Address" className="new_radios radio_for_css" classes={{ root: "radio_button_new" }} />
                                {expanded === "new" &&
                                    <Box>
                                        <NewAddressForm state={state} setState={setState} {...props} />
                                    </Box>}
                            </RadioGroup>
                            <div className="action_button_section">
                                {addressError && <span className="error">{addressError}</span>}
                                <button onClick={() => props?.setNextButton(false)}>Go back to card detail</button>
                                {expanded === "new" &&
                                    <button type="submit" className="Save-address">{["savedCard", "Autoship"].includes(props?.section) ? "Add your card" : "Order Now"}</button>
                                }
                                {expanded === "existing" &&
                                    <button type="button" className="Save-address" onClick={() => handleOnChangeExisting()}>{props?.section === "Autoship" ? "Add your card" : "Save"}</button>
                                }
                            </div>
                        </form>
                    </FormProvider>
                </FormControl>
            </div>
        </div >
    );
}
