import { Checkbox } from "@material-ui/core";

const { useEffect, useState } = require("react")

const ExistingAddresses = (props) => {

    const { setSelectedAddress, selectedAddress, AddressList, addressListData } = props;
    useEffect(() => {
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        AddressList(token)
    }, []);

    function handleChangeCheckbox(id) {
        setSelectedAddress(id);
    }

    return (
        <div className="row ">
            <table>
                <tbody>
                    {addressListData?.length > 0 ? addressListData?.map((data, index) => (
                        <tr className="savedClass" key={index + 1} >
                            <td >
                                <div className="selection_Area">
                                    <Checkbox
                                        checked={+selectedAddress === +data?.id}
                                        onChange={() => handleChangeCheckbox(data?.id)}
                                        className="checkbox_for_addresses"
                                        inputProps={{ 'aria-label': 'controlled' }}
                                    />
                                </div>
                            </td>
                            <td>
                                <div>
                                    {data?.first_name + " " + data?.last_name + "," + `${data?.company_name ? data?.company_name + "," : ""}` + data?.street_address_1 + "," + `${data?.company_name ? data?.company_name + "," : ""}` + data?.city + ", " + data?.state + ", " + data?.postal_code + ", " + data?.country + ", " + data?.phone_number}
                                </div>
                            </td>
                        </tr>
                    )) :
                        <tr>
                            <td colSpan="2" className="not_found">No addresses found.</td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
        // <>{JSON.stringify(state?.addressListData)}</>
    )

}

export default ExistingAddresses;