
import { useForm } from 'react-hook-form';
import { useRouter } from "next/router";
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers/yup';
import CardType from 'credit-card-type';
import { toast } from 'react-toastify';
import { useRef, useState } from 'react';

const PaymentForm = ({ placerorder, setPaySection, section, setshowloader, setCardSectionData, cardSectionData, setNextButton, ...props }) => {
    const [cardType, setCardType] = useState();

    const inputRef = useRef();
    const router = useRouter();
    const schema = yup.object().shape({
        card_owner_name: yup.string()
            .required('Please enter your card name.'),
        card_expiry_month: yup.number()
            .typeError("Must be a number")
            .required("Enter your expiry date."),
        card_expiry_year: yup.number()
            .typeError("Must be a number")
            .required("Enter your expiry Year."),
        card_number: yup.string()
            .required("Please enter your card number.")
            .matches(/^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/, "Please Enter Valid card Number")
            // .positive("number")
            // .integer("Please enter valid Number")
            .test('type', "Must be a number", value => {
                if (!isNaN(value)) {
                    let cardType = CardType(value ? value : null);
                    setCardType(cardType[0]?.type);
                } else {
                    setCardType();
                }
                return !isNaN(value)
            }),
        security_code: yup.string()
            .required("Enter CVV.")
            .matches(/^[0-9]{3,4}$/, "Enter valid CVV.")
    });

    const { register, handleSubmit, errors, formState, setError, getValues } = useForm({
        mode: 'onChange',
        resolver: yupResolver(schema),
    })


    var date = new Date();
    var currentYear = date.getFullYear();

    const onSubmit = async (data) => {
        let cardType = CardType(data?.card_number);
        var currentMonth = date.getMonth() + 1;
        // var currentYear = date.getFullYear();
        if (currentYear === data?.card_expiry_year) {
            if (currentMonth <= data?.card_expiry_month) {
                data['card_type'] = cardType[0]?.type;
                // await PlaceOrderAndSaveCard(data);
                setCardSectionData(data);
                setNextButton(true);
            } else {
                setError("card_expiry_month", { type: "manual", message: "Enter valid month." });
            }
        }
        else {
            data['card_type'] = cardType[0]?.type;
            //await PlaceOrderAndSaveCard(data);
            setCardSectionData(data);
            setNextButton(true);
        }
    }

    const MonthYear = ({ start, end, name, type, values, label }) => {
        return (
            <>
                <label htmlFor={name}>{label}</label>
                <select name={name} defaultValue={values} ref={register}>
                    <option value="" disabled={true}>{type}</option>
                    {(() => {
                        const options = [];
                        for (let i = start; i <= end; i++) {
                            options.push(i)
                        }
                        return options?.map((value, i) => <option value={value} key={value}>{value}</option>)
                    })()}
                </select>
                {errors?.[name] && <p className="error">{errors?.[name]?.message}</p>}
            </>
        );
    }
    return (
        <div className="main_payment_form_class">
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="payment_fields">
                    <div className="row">
                        <div className="col-md-12">
                            <label htmlFor="card_owner_name">Card Holder Name</label>
                            <input name="card_owner_name"
                                defaultValue={cardSectionData?.card_owner_name}
                                ref={e => {
                                    inputRef.current = e
                                    register(e)
                                }} type="text" className="input" />
                            <p className="error">{errors?.card_owner_name && errors?.card_owner_name?.message}</p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="card_images_section">
                                {['visalogonew', 'mastercardlogo', 'americanexplogo', 'discoverlogo']?.map((img, index) => (
                                    <img src={`/images/${img}.png`} key={index} className={cardType !== "undefined" && (img === cardType) ? "active" : ""} />

                                ))}
                            </div>
                            <label htmlFor="card_number">Card Number</label>
                            <input name="card_number"
                                defaultValue={cardSectionData?.card_number}
                                ref={e => {
                                    inputRef.current = e
                                    register(e)
                                }} type="text" className="input" />
                            <p className="error">{errors?.card_number && errors?.card_number?.message}</p>
                        </div>
                    </div>
                    <div className="row card_cvv_section">
                        <div className="col-md-3 extra">
                            <MonthYear start={1} end={12} name="card_expiry_month" type="MM" values={getValues('card_expiry_month') ? getValues('card_expiry_month') : cardSectionData?.card_expiry_month} label="Month" />
                        </div>
                        <div className="col-md-3 extra">
                            <MonthYear start={currentYear} end={2050} name="card_expiry_year" type="YY" values={getValues('card_expiry_year') ? getValues('card_expiry_year') : cardSectionData?.card_expiry_year} label="Year" />
                        </div>
                        <div className="col-md-3 extra">
                            <label htmlFor="security_code">CVV</label>
                            <input name="security_code"
                                defaultValue={cardSectionData?.security_code}
                                ref={e => {
                                    inputRef.current = e
                                    register(e)
                                }} type="text" className="input" />
                            <p className="error">{errors?.security_code && errors?.security_code?.message}</p>
                        </div>
                    </div>
                    <div className='save-credit-check'>
                        <input type="checkbox"
                            ref={register({ required: false })}
                            className="form-check-input"
                            id="save_credit_card"
                            name="save_credit_card"
                        />  Save Credit Card
                    </div>
                    <div className="action_button">
                        <button type="button" onClick={() => setPaySection(false)}>Back</button>
                        <button type="submit" >Next</button>
                    </div>
                </div>
            </form>
        </div>
    )
}
export default PaymentForm;