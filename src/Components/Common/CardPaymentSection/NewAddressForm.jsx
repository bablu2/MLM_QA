import Head from 'next/head'
import { useState } from 'react';
import * as yup from "yup";
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, FormProvider, useFormContext } from 'react-hook-form';
import api from '../../../api/Apis';
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';

export default function NewAddressForm(props) {

    const { savedNewAddress, state, setState } = props;
    const { register, errors } = useFormContext();


    return <>
        {/* <div className="container main_payment_form_class">
            <form onSubmit={handleSubmit(onSubmit)}> */}
        <div className="row">
            <div className="col-md-6">
                <div className="md-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">First Name</label>
                    <input type="text" className="form-control" name="first_name" id="first_name" ref={register} />
                    {errors.last_name && <span className="error">{errors.last_name?.message}</span>}
                </div>
            </div>

            <div className="col-md-6">
                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">Last Name</label>
                    <input type="text" className="form-control" name="last_name" id="l_name" ref={register} />
                    {errors.last_name && <span className="error">{errors.last_name?.message}</span>}
                </div>
            </div>
        </div>

        <div className="row">
            <div className="col-md-12 company_space">
                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">Company Name</label>
                    <input type="text" className="form-control" name="company_name"
                        ref={register({ required: false })} />
                </div>
            </div>
            <div className="row">
                <div className="col-md-6">
                    <div className="mb-3 address_field_class">
                        <label htmlFor="exampleInputEmail1" className="form-label">Address 1</label>
                        <textarea type="textarea" className="form-control" name="street_address_1" id="billing_address" ref={register} />
                        {errors.street_address_1 && <span className="error">{errors.street_address_1?.message}</span>}
                    </div>
                </div>
                <div className="col-md-6">
                    <div className="mb-3 address_field_class">
                        <label htmlFor="exampleInputEmail1" className="form-label">Address 2</label>
                        <textarea type="textarea" className="form-control" name="street_address_2" id="billing_address"
                            ref={register({ required: false })} />

                    </div>
                </div>
            </div>

            <div className="col-md-6">
                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">Postal Code</label>
                    <input type="text" className="form-control" name="postal_code" ref={register} />
                    {errors.postal_code && <span className="error">{errors.postal_code?.message}</span>}
                </div>
            </div>

            <div className="col-md-6">
                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">Country</label>
                    <div className="select">
                        <CountryDropdown
                            value={state?.country_name}
                            name="country"
                            valueType="short"
                            priorityOptions={['US', 'CA']}
                            onChange={(val) => { setState({ ...state, country_name: val }) }} />
                    </div>
                    {state?.error_country && <span className="error">{state?.error_country}</span>}
                </div>
            </div>
            <div className="col-md-6">
                <div className="mb-3">
                    {state?.country_name &&
                        <>
                            <label htmlFor="exampleInputEmail1" className="form-label">State</label>
                            <div className="select">
                                <RegionDropdown
                                    country={state?.country_name}
                                    value={state?.region_name}
                                    name="state"
                                    valueType="full"
                                    defaultOptionLabel="Select State"
                                    countryValueType="short"
                                    onChange={(val) => setState({ ...state, region_name: val })} />
                            </div>
                            {state?.error_state && <span className="error">{state?.error_state}</span>}
                        </>}
                </div>

            </div>
            <div className="col-md-6">
                <div className="mb-3 validation_extra_class">
                    <label htmlFor="exampleInputEmail1" className="form-label">City</label>
                    <input type="text" name="city" placeholder="please enter your city" ref={register} />
                    {errors.city && <span className="error extra_class">{errors.city?.message}</span>}
                </div>
            </div>
        </div>

        <div className="row">

            <div className="col-md-12">
                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">Phone</label>
                    <input type="text" className="form-control" name="phone_number" ref={register} />
                    {errors.phone_number && <span className="error">{errors.phone_number?.message}</span>}
                </div>
            </div>
        </div>

    </>
}