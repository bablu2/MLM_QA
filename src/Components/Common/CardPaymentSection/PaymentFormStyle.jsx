import styled from 'styled-components';

const PaymentFormStyle = styled.div`
&
.main_payment_form_class {
    form {
        .col-md-6 {
            margin-bottom: 12px;
        }
        .action_button{
            button {
              
                font-size: 14px;
                font-weight:600;
                text-transform:uppercase;
                padding:10px 25px;
                color:#FFF;
                border-radius:50px;
                margin-right:20px;
                margin-top:15px;
                border:2px solid #06356a;
                &:hover{
                    background:none;
                    color:#06356a;
                }
                
            }
        }
        .payment_fields {
            p.error {
                left: 0;
                margin: 0;
                padding-left: 16px;
                bottom: -5px;
            }
            .card_images_section{
                margin-bottom: 15px;
                img {
                    width: 100%;
                    max-width:35px;
                    margin: 0 5px;
                    &.disabled {
                        opacity: 0.2;
                    }
                }
            }
            .card_cvv_section {
                .extra {
                    margin: 0;
                    width: 100%;
                    max-width: calc(100%/3);
                    @media (max-width: 767px){
                        max-width: 100%;
                    }
                }
            }
            
        }
    } 
}
.container.main_payment_form_class{
    tr.savedClass { 
    display: flex;
    padding-bottom: 5px;
}
}
.choose_address_section {
    .MuiCheckbox-colorPrimary {
                    padding: 0 10px 0 0;
                }
    fieldset {
        form {
            legend {
                font-size: 18px !important;
                color: #000;
            }
            .selection_Area {
                margin-top: 0px;
            }
            .MuiBox-root{
                margin: 0 37px;
                table {
                    td {
                        padding-bottom: 0px;
                    }
                } 
                input[type = "text"] {
                    margin-bottom: 10px !important;
                }
                input[type=checkbox]{
                    margin: 0;
                }
                .validation_extra_class {
                    position: relative;
                    .extra_class {
                        position: absolute;
                        left: 0;
                    }
                } 
            }
            .action_button_section {
                display: flex;
                margin-left: 0;
                margin-top: 9px;
                justify-content: center;
                button {
                    color: #fff;
                    border-radius: 30px;
                    text-transform: uppercase;
                }
            }
            .address_field_class{
                position: relative;
                margin-bottom: 14px;
                padding-left: 15px;
                 span {
                    bottom: -19px;
                }
            }
        }
        
        .radio_for_css {
            .MuiFormControlLabel-label{
                font-size:19px;
            }
            svg {
                font-size:22px;
            }
        }
    }
    .mb-3:empty{display: none}
    .extra_class{
        position: unset !important;
    }
}
.payment_form_section button {
    color: #fff !important;
}
button.Save-address {
    margin-left: 20px;
}
.row.card_cvv_section {
    .extra:first-child .error {
    bottom: -16px;
}
    .row.card_cvv_section p.error {
    margin: 0 auto 0;
    top: auto;
} 
}
.payment_form_section .main_payment_form_class{
    .error {
    height: auto;
    left: 17px;
    line-height: normal;
    bottom: -6px;
    max-width: calc(100% - 10px);
}
}
    .add_address_from_CheckoutPage_main_dialog 
    { 
        min-width: auto !important; 
        .form-check {
        display: flex;
        align-items: center;
    }
    input[type="checkbox"] {
        height: auto;
        line-height: normal; 
    } 
    .row{
        margin: 0;
    } 
} 
`;

export default PaymentFormStyle;