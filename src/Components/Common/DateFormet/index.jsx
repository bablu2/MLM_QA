import moment from "moment";

const DateFormat = ({ date }) => {
    // var dateObj = new Date(date);
    // var d = moment(d).format('MM-DD-YYYY')
    // var momentObj = moment(d, 'ddd MMM DD YYYY HH:mm:ssZ').format('MM-DD-YYYY');
    const local = moment.utc(date, 'MM-DD-YYYY HH:mm:ss').local();
    const momentFormat = moment(local, 'YYYY-MM-DD HH:mm:ss').format('MM-DD-YYYY');
    return momentFormat;
}
export default DateFormat;