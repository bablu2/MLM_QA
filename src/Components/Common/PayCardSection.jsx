import { useEffect, useState } from "react";
import CardPaymentSection from "./CardPaymentSection";
import SaveCardSection from "./SaveCardSection";
import styled from "styled-components";
import DialogComponent from "./DialogComponent";
import { toast } from "react-toastify";
import { useRouter } from "next/router";
import { DialogSectionCss } from "@PagesComponent/ActiveSmartShip/ActiveStyleComp";

const SaveCardStyle = styled.div`
  & .main_saved_comp {
    /* margin: 30px auto 25px;
        border:1px solid #ddd; */
    width: 100%;
    .savedClass {
      width: calc(100% / 3);
      border: 1px solid #ddd;
      box-shadow: none;
      @media (max-width: 767px) {
        width: 100%;
      }
    }
    .label {
      position: relative;
      background: #fff;
      padding: 5px 10px;
      border: 1px solid #ddd;
      border-radius: 5px;
      line-height: 1;
      label {
        margin: 0px;
      }
    }
    button {
      text-transform: uppercase;
      margin: 0px 0px 0 15px;
      border-radius: 6px;
      font-size: 14px;
      &:hover {
        color: #00356a !important;
        background: transparent !important;
      }
    }
    button[disabled] {
      background: #999 !important;
      border-color: #999 !important;
      cursor: not-allowed;
      &:hover {
        color: #999 !important;
        background: transparent !important;
      }
    }
    .continue-coupn {
      text-align: center;
    }
  }

  .savedCard_detail {
    .main_saved_comp {
      .row.saved-nmain {
        flex-wrap: wrap;
        display: flex;
        .col-md-6.savedClass {
          margin-bottom: 15px;
          max-width: calc(100% / 3 - 30px);
          width: 100%;
        }
      }
    }
  }
`;

//component for autoship and checkout section

const SaveCardComp = ({
  compFunction,
  orderData,
  section,
  savedCardId,
  setSavedCardId,
  UpdateDefaultCard,
  setshowloader,
  setInnerLoader,
  showContinue,
  showErrorPayment,
  product,
  walletCheck,
  buttonDis,
}) => {
  const [paymentSection, setPaySection] = useState(false);
  const [noCardSavedUser, setNoCardSavedUser] = useState(false);
  const [shippingError, setShippingError] = useState("");
  const [open, setOpen] = useState(false);
  const Router = useRouter();

  return (
    <SaveCardStyle>
      <div className="main_saved_comp">
        <div className="saved_card_section">
          {((showContinue !== null && walletCheck !== true) ||
            (product?.length === 1 &&
              product[0]?.product?.product_type === "coupon")) && (
              <SaveCardSection
                setSavedCardId={setSavedCardId}
                paymentSection={paymentSection}
                showContinue={
                  showContinue ||
                  (product?.length === 1 &&
                    product[0]?.product?.product_type === "coupon")
                }
                savedCardId={savedCardId}
                UpdateDefaultCard={UpdateDefaultCard}
                setNoCardSavedUser={setNoCardSavedUser}
                section={section}
              />
            )}
        </div>

        {section === "checkout" && (
          <button onClick={() => Router.back()}> Back</button>
        )}
        {section === "checkout" &&
          paymentSection === false &&
          noCardSavedUser &&
          !walletCheck && (
            <button
              disabled={!savedCardId || buttonDis}
              onClick={() => compFunction(null)}
            >
              Order Now
            </button>
          )}
        {!walletCheck && (
          <button
            className="add_new_card"
            onClick={() => {
              if (section !== "savedCard") {
                if (showContinue !== null) {
                  setPaySection(true);
                } else {
                  /* toast.warn("Please select shipping order."); */
                  setShippingError("Please select shipping order");
                  setTimeout(() => {
                    setShippingError("");
                  }, 2000);
                }
              } else {
                setPaySection(true);
              }
              if (section === "checkout") {
                setSavedCardId(null);
              }
            }}
          >
            {!noCardSavedUser && section !== "savedCard"
              ? "Order Now"
              : "Add New"}
          </button>
        )}
        {section === "checkout" && walletCheck === true && (
          <button onClick={() => compFunction(null)}>Order Now</button>
        )}
        {showErrorPayment && <span className="error">{showErrorPayment}</span>}

        {shippingError && <span className="error">{shippingError}</span>}
        {paymentSection && (
          <DialogComponent
            opend={paymentSection}
            handleClose={setPaySection}
            title="Card Details"
            classFor="paymentform_from_savedCardComp"
          >
            <div className="payment_form_section">
              <CardPaymentSection
                placerorder={(value) => compFunction(value)}
                orderData={(value) => orderData(value)}
                setPaySection={setPaySection}
                section={section}
                setshowloader={setshowloader}
                setInnerLoader={setInnerLoader}
              />
            </div>
          </DialogComponent>
        )}
      </div>
    </SaveCardStyle>
  );
};

const PayCardSection = ({
  product,
  orderData,
  compFunction,
  section,
  setSavedCardId,
  showContinue,
  saveCardId,
  buttonDis,
  showErrorPayment,
  setshowloader,
  ...props
}) => {
  return (
    <SaveCardStyle>
      <div className="main_saved_comp">
        <div className="payment_section">
          <SaveCardComp
            compFunction={(value) => compFunction(value)}
            section={section}
            orderData={(value) => orderData(value)}
            setSavedCardId={setSavedCardId}
            savedCardId={saveCardId}
            setshowloader={setshowloader}
            showContinue={showContinue}
            product={product}
            showErrorPayment={showErrorPayment}
            buttonDis={buttonDis}
            {...props}
          />
        </div>
      </div>
    </SaveCardStyle>
  );
};
export default PayCardSection;
export { SaveCardComp };
