import { DialogSectionCss } from "@PagesComponent/ActiveSmartShip/ActiveStyleComp";
import DialogComponent from "./DialogComponent";


const DialogDeleteComponent = ({ openForDelete, setOpenForDelete, title, deleteConfirm }) => {
    return (
        <div>
            <DialogComponent opend={openForDelete} handleClose={() => setOpenForDelete({ open: false, id: null })} title={title}>
                <DialogSectionCss className='Delete_pop'>
                    <p> Are You Sure You want to Delete this?</p>
                    <div className="popup">
                        <button onClick={() => setOpenForDelete({ open: false, id: null })}>cancel</button>
                        <button onClick={() => deleteConfirm(openForDelete?.id)}> OK </button>
                    </div>
                </DialogSectionCss>
            </DialogComponent>
        </div>
    );
}

export default DialogDeleteComponent;
