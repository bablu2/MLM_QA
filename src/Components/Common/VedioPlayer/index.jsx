import React from 'react'

// Lazy load the YouTube player
const VedioPlayer = ({ url }) => {
    return <iframe src={url} width="1000" height="800" frameBorder="0" allow="autoplay; fullscreen" allowFullScreen ></iframe>
}

export default VedioPlayer;
