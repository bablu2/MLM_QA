import React from 'react'
import styled from 'styled-components';
import Link from 'next/link';

const MaritimeSectionStyle = styled.div`
.martin_prime_section{
        max-width: 500px;
        margin: 40px auto 0px;
        border: 20px solid rgb(22 53 106);
        padding: 30px;
        text-align: center;
        img {
            width: 300px;
            margin: 0 auto;
            display: block;
        }
        a {
            background: rgb(0,53,106);
            color: rgb(255,255,255);
            border-radius: 25px;
            width: 100%;
            font-size: 18px;
            height: 40px;
            width: 100%;
            text-transform: none; text-decoration: none;
            padding: 10px;
            margin: 0 auto;
            display: inline-block;
            text-align: center;
            line-height: 1;
            max-width: 150px;transition: 0.3s ease all;
            border: 2px solid rgb(0 53 106);
            &:hover{
                color: rgb(0 53 106);
                background: #fff;
            }
        } 
        h2 {
            color: rgb(22 53 106) !important;
            margin-bottom: 20px;
            text-align: center !important;
            font-size: 50px !important;
        }
}
`;
function MaritimePrimeSection({ content }) {
    const { product_image, productName, btnText, btnLink } = content;
    return (
        <MaritimeSectionStyle>
            <div className="martin_prime_section">
                <div className="image_section">
                    <img src={product_image ? product_image : "/images/maritime_prime_pair.png"} />
                </div>
                {productName ? productName : <h2>Maritime Prime</h2>}
                <Link href={btnLink ? btnLink : "/us/allProduct"}><a>{btnText ? btnText : "Order Now!"}</a></Link>
            </div>
        </MaritimeSectionStyle>
    )
}
export default MaritimePrimeSection;
