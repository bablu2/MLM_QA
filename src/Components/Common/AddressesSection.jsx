const AddressesSection = ({ addressdata, type }) => {
  return (
    <table className="table inner-table-shop">
      <tbody>
        <tr>
          <th>First Name</th>
          <td data-value="First Name"> {addressdata?.[type]?.first_name}</td>
        </tr>
        <tr>
          <th>Last Name</th>
          <td data-value="Last Name">{addressdata?.[type]?.last_name}</td>
        </tr>
        <tr>
          <th>Address 1</th>
          <td data-value="Address 1">
            {addressdata?.[type]?.street_address_1}
          </td>
        </tr>
        {addressdata?.[type]?.street_address_2 && (
          <tr>
            <th>Address 2</th>
            <td data-value="Address 2">{`${addressdata?.[type]?.street_address_2 || ""
              }`}</td>
          </tr>
        )}
        {addressdata?.[type]?.company_name && (
          <tr>
            <th>Company Name</th>
            <td data-value="Company Name">{`${addressdata?.[type]?.company_name || ""
              }`}</td>
          </tr>
        )}
        <tr>
          <th>City</th>
          <td data-value="City">{addressdata?.[type]?.city}</td>
        </tr>
        <tr>
          <th>State</th>
          <td data-value="State">{addressdata?.[type]?.state}</td>
        </tr>
        <tr>
          <th>Zip Code</th>
          <td data-value="Zip Code">{addressdata?.[type]?.postal_code}</td>
        </tr>
        <tr>
          <th>Country</th>
          <td data-value="Country">{addressdata?.[type]?.country}</td>
        </tr>
        <tr>
          <th>Phone</th>
          <td data-value="Phone">{addressdata?.[type]?.phone_number}</td>
        </tr>
      </tbody>
    </table>
  );
};
export default AddressesSection;
