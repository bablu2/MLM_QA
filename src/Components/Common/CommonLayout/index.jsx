import DBOP from '@Components/Common/DBOP';
import React, { useEffect } from 'react';
import LeftNav from '@Components/LeftNav';
import MobileLeftNav from '@Components/LeftNav/MobileLeftNav';
import _ from 'lodash';
import Head from 'next/head';
import SvgAnimationBackground from '@Components/Common/SvgAnimationBackground';

const CommonLayout = ({ children, attributes, ...props }) => {
    const { section, title, showprotectedpage, getDetails, customerCheck, validateauth, } = attributes;
    useEffect(() => {
        validateauth();
    }, [])

    return (
        <>
            <Head>
                <title>{title}</title>
            </Head>
            <DBOP>
                {showprotectedpage === true ?
                    <div className={`mainorder-detail-sec main_${section}_section_class`}>
                        <div className="profile-detail-section">
                            <div className="container">
                                <div className="row">
                                    <div className="dashboard_main_div main_class_responsive">
                                        <LeftNav customerCheck={customerCheck} getDetails={getDetails} />
                                        <div className="col-md-10">
                                            <MobileLeftNav customerCheck={customerCheck} getDetails={getDetails} />
                                            <div className="container order-detail-page p-4">
                                                <div className="heading-top-main right-address">
                                                    <div className="heading-main-text">
                                                        <h1>{title}</h1>
                                                    </div>
                                                </div>
                                                {children}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    :
                    <div className="empty_div_section"></div>
                }
            </DBOP>
            <SvgAnimationBackground />
        </>
    )
}
export default CommonLayout;