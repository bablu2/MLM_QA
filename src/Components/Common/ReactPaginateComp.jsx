import ReactPaginate from "react-paginate";
import { ChevronLeft, ChevronRight } from "@material-ui/icons";
import styled from "styled-components";

const ReactPaginateStyle = styled.div`
  .main_pagination_section_class {
    .order_pagination {
      text-align: center;
      width: 100%;
      max-width: 100%;
      margin: 0 auto 0;
      display: flex;
      justify-content: center;
      ul {
        &.pagination {
          width: auto;
          float: left;
          justify-content: center;
          display: flex;
          background: #00356a;
          margin-left: 28px;
          margin: 20px 0;
          padding: 7px 10px;
          border-radius: 25px;
          @media (max-width: 767px){ 
            margin-bottom: 0 !important;
            margin-top: 30px !important; 
            gap: 10px;
          }
          li {
            border-radius: 50%;
            width: 30px;
            height: 30px;
            margin-bottom: 0;
            &.active {
              background: #fff;
              color: #00356a;
              a {
                color: #00356a;
              }
            }
            a {
              padding: 5px 10px;
              display: block;
              background: none;
              color: #fff;
              border: none;
              text-transform: capitalize;
              width: 100%;
            }
            &.previous,
            &.next {
              &.disabled {
                display: block !important;
                background-color: #dddd;
                cursor: not-allowed;
                &.svg {
                  cursor: not-allowed;
                }
              }
              background: #fff;
              a {
                color: #06356a;
                border-radius: 50%;
                height: 100%;
                width: 100%;
                padding: 8px 5px 7px 7px;
              }
            }
          }
        }
      }
    }
  }
`;
const ReactpaginateComp = ({ handlePageClick, pageCount }) => {
  return (
    <ReactPaginateStyle>
      <div className="main_pagination_section_class">
        <div className="order_pagination">
          <ReactPaginate
            previousLabel={<ChevronLeft />}
            nextLabel={<ChevronRight />}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={pageCount}
            marginPagesDisplayed={1}
            pageRangeDisplayed={5}
            onPageChange={handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages_pagination"}
            activeClassName={"active"}
          />
        </div>
      </div>
    </ReactPaginateStyle>
  );
};
export default ReactpaginateComp;
