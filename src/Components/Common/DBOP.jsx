import styled from "styled-components";

const DBOP = styled.div`

   .coupon-para h5 {
    font-size: 18px;
    margin-bottom: 20px; 
    line-height: normal;
    margin-top: 0;
}
  .main_dashboard_section_class .collapse_arrow_class {
    top: unset !important;
  }
  .main_dashboard_section_class span.membership_title_class {
    padding-left: 20px;
    width: 100%;
  }
  .active-block > div,
  .not-check,
  .MuiInput-root.MuiInput-underline svg.MuiSvgIcon-root {
    position: relative;
  }
  .not-check svg.not-check_tick {
    width: 20px;
    height: 20px;
    color: green;
  }
  .main_order_section_class
    .main_order_detail_class
    .orer-details-tables
    tr
    td:first-child {
    text-align: left;
  }
  .main_order_section_class
    .main_order_detail_class
    .orer-details-tables
    tr
    th:last-child,
  .main_order_section_class
    .main_order_detail_class
    .orer-details-tables
    tr
    td:last-child {
    text-align: right;
  }
  .not-check {
    display: flex;
    margin-bottom: 6px;
  }
  .check + .not-check {
    margin-bottom: 0;
    span.eye_icon_main_class {
      right: 0;
      svg {
        transform: translateY(6px);
      }
    }
  }
  .mainorder-detail-sec .container.order-detail-page {
    .row.saved-nmain {
      flex-wrap: wrap;
      display: flex;
    }
    .row.autoshipPayment_with_button {
      margin-bottom: 20px;
    }
  }
  /* 13-09-2022 */
  .total_sale + .check {
    padding-left: 7px;
    padding-bottom: 10px;
  }
  /* 12-10-2022 */
  .container.order-detail-page .table-profile-details .profile-img-left {
    padding-bottom: 15px;
  }
  .main_dashboard_section_class {
    .active-block {
      .check span.eye_icon_main_class {
        right: 0;
        bottom: 0;
      }
    }

    @media (max-width: 1024px) {
      padding-right: 0;
    }
  }

  @media (max-width: 1600px) {
    .main_autoship_section_class .autoship_order_detail .savedClass {
      width: 47%;
    }
    .main_saved_card_class .savedClass .card_details {
      min-width: auto;
    }
  }
  @media (max-width: 1440px) {
    .main_dashboard_section_class .dt-jned span {
      width: auto !important;
    }

    .main_saved_card_class .savedClass .cardType {
      min-width: 71px !important;
    }
    .main_order_section_class
      .main_order_detail_class
      .orer-details-tables
      tr
      td
      .details_price {
      right: 13px !important;
    }
    form.refund {
      margin: 0 15px;
    }
    .main_order_section_class
      .main_order_detail_class
      .orer-details-tables
      tr
      td
      .details_price {
      right: 77px !important;
      padding-right: 0 !important;
    }
    .main_refund_section_class
      .refund_detail_section
      table
      tbody
      tr
      td
      .price_amount {
      right: 86px !important;
    }
    .main_saved_card_class .savedClass {
      width: 29% !important;
    }
    .main_dashboard_section_class .dt-jned span {
      width: auto !important;
      padding-right: 5px;
    }

    .main_dashboard_section_class .newdata {
      display: block !important;
    }
    .main_dashboard_section_class strong {
      min-width: 100% !important;
      float: left;
    }
    .newsdata.DownlineStatic {
      min-height: 70px;
      .documents-downloads + .newdata,
      .documents-downloads + .newdata + .newdata {
        display: flex !important;
      }
    }
    .Toal-commission,
    .Toal-Sale {
      display: block;
    }
  }
  @media (max-width: 1366px) {
    .main_order_section_class
      .main_order_detail_class
      .orer-details-tables
      tr
      td
      .details_price {
      right: 70px !important;
    }
    .main_refund_section_class
      .refund_detail_section
      table
      tbody
      tr
      td
      .price_amount {
      right: 79px !important;
    }
    .autoship_details_table .main_saved_card_class .col-md-6.savedClass {
      width: 45% !important;
    }
  }
  @media (max-width: 1280px) {
    .main_order_section_class
      .main_order_detail_class
      .orer-details-tables
      tr
      td
      .details_price {
      right: 67px !important;
    }
    .main_refund_section_class
      .refund_detail_section
      table
      tbody
      tr
      td
      .price_amount {
      right: 72px !important;
    }
    .main_dashboard_section_class strong {
      width: 100px !important;
    }
  }
  @media (max-width: 1199px) {
    .main_dashboard_section_class .leftsec {
      padding: 10px;
    }
  }
  @media (max-width: 1024px) {
    .container.order-detail-page {
      margin-left: 0px !important;
    }
    .address_class_responsive {
      padding-right: 0 !important;
      .container.order-detail-page {
        margin-left: 0px !important;
      }
    }
    .table-profile-details {
      flex-wrap: wrap;
    }
    .main_profile_section_class .profile-img-left {
      flex: 0 0 95% !important;
      max-width: 95% !important;
    }
    .main_profile_section_class .profile-text-right {
      max-width: 95% !important;
      flex: 0 0 95% !important;
      margin: 20px 19px 40px !important;
    }
    .main_profile_section_class .profile-text-right table tr :where(th, td) {
      max-width: 50%;
    }
    .main_dashboard_section_class .deatis-info {
      border-top: 1px dashed #00356a;
      padding: 20px 0px 0 !important;
    }
    .statis_datas .comistion-table-data.dashboard-table-commsion th {
      font-size: 13px !important;
      white-space: nowrap !important;
    }
    .current-part {
      padding: 10px 0 !important;
    }
    .active-block {
      padding-left: 0 !important;
    }
    .active-block h3 {
      text-align: left;
      font-size: 12px;
      font-weight: 600;
      line-height: 18px;
    }
    .mainorder-detail-sec .col-md-3 {
      max-width: 100%;
      flex: 0 0 100%;
      /* padding-right: 0; */
    }
    .mainorder-detail-sec .col-md-9 {
      max-width: 100%;
      flex: 0 0 100%;
      padding: 0 15px;
    }
   
    .main_saved_card_class .savedClass .cardType {
      min-width: 100px !important;
    }
    .upload__image-wrapper.profile-section-now button {
      margin: 20px auto !important;
    }

    .dashboard-table-commsion
      tr.MuiTableRow-root.MuiTableRow-head.css-1q1u3t4-MuiTableRow-root
      th {
      padding-left: 10px !important;
    }
    .review_section .title_of_review {
      font-size: 13px !important;
    }
    .order_table th {
      width: 93px !important;
    }
    .main_refund_section_class
      .refund_detail_section
      table
      tbody
      tr
      td
      .price_amount {
      right: 51px !important;
    }
    .main_order_section_class
      .main_order_detail_class
      .orer-details-tables
      tr
      td
      .details_price {
      right: 46px !important;
    }
    .check + .check span.eye_icon_main_class {
      right: 0;
    }
    .Toal-commission span {
      right: 2px;
    } 

  }
  form.invitation-link-main{
    input[type="text"]{
    margin-bottom: 10px;}
     .field-class a {
    font-weight: 600;
    color: #06356a;
    cursor: pointer;
    text-decoration: underline;
    text-underline-offset: 4px; 
}

}
.order_details_main_dialog h3 {
    margin-top: 0; 
}
.container_for_load_data h5 {
    font-size: 20px;
    color: #000;
    margin-top: 0;
}
  @media (max-width: 991px) {
    .col-md-10 {
      max-width: 78%;
      flex: 0 0 78%;
    }
  }
  @media (max-width: 768px) {
    .main_dashboard_section_class .leftsec {
      margin: 0px auto !important;
    }
    .dashboard_main_div {
      display: block;
      padding: 20px;
    }

    .main_autoship_section_class {
      .autoship_order_detail table td {
        &.ship_text {
          padding-right: 0px !important;
        }
      }
      .auto-ship-order-table {
        td.autoship-action {
          display: flex !important;
        }
      }
      .autoshipPayment_with_button {
        display: flex;
        flex-wrap: wrap;
        margin-bottom: 20px;
        .shipping_auto_md {
          order: 2 !important;
          width: 100%;
          max-width: 100%;
          flex: 0 0 100%;
          .autoshipping_system {
            margin-bottom: 35px;
            @media (max-width: 767px) {
              margin-bottom: 0;
            }
          }
        }
        .changeAdd_auto_md {
          order: 1;
          max-width: 100%;
          flex: 0 0 100%;
          .changeadress-button {
          
          }
        }
      }
    }

    .main_saved_card_class .savedClass .card_details {
      min-width: 67%;
    }

    .order_table table th {
      white-space: nowrap;
    }
    .statis_datas .date_range_filter input {
      margin: 1px 1px 0 !important;
      width: 10px !important;
      height: 10px !important;
    }
    .mainorder-detail-sec.dashboad-deails.Dashboard_bg.main_dashboard_section_class
      .col-md-10.dashboard-main {
      padding-left: 0 !important;
      padding-right: 0 !important;
    }
 

    .container.order-detail-page {
      margin-left: 0px !important;
    }
    .container.order-detail-page td.action-btn {
      display: flex;
      padding-top: 60px !important;
      padding-left: 0 !important;
    }
    .main_order_section_class
      .main_order_detail_class
      .orer-details-tables
      tr
      td
      .details_price {
      right: 26px !important;
    }
    .main_refund_section_class
      .refund_detail_section
      table
      tbody
      tr
      td
      .price_amount {
      right: 29px !important;
    }
    .autoship_details_table .main_saved_card_class .col-md-6.savedClass {
      width: 55% !important;
    }
    .graphical-rep-commission canvas {
      width: 100% !important;
    }
    .main_profile_section_class .profile-text-right,
    .main_profile_section_class .profile-img-left {
      flex: 0 0 100% !important;
      max-width: 100% !important;
    } 
  }
  @media (max-width: 767px) { 
    .main_profile_section_class .profile-text-right,
    .main_profile_section_class .profile-img-left {
      flex: 0 0 100% !important;
      max-width: 100% !important;
    } 
    .mainorder-detail-sec.dashboad-deails.Dashboard_bg.main_dashboard_section_class
      .profile-detail-section
      > .container {
      padding: 0;
    }
    .address_class_responsive {
      padding-right: 15px !important;
    }
    .order-detail-page.main_address_page_listing .button-top {
      padding-right: 15px;
    }
    div.dashboard_main_div {
      flex-wrap: wrap !important;
      padding: 0;
      justify-content: center;
      .col-md-2 {
        max-width: 100%;
        flex: 0 0 100%;
      }
      .col-md-10 {
        max-width: 100%;
        flex: 0 0 100%;
      }
      .dashboard-main .container.order-detail-page {
        margin-left: 0px !important;
        margin-top: 40px;
      }
    }
    .row.addres-page.dashboard_main_div .col-md-2 {
      padding-right: 0 !important;
    }
    .row.addres-page.dashboard_main_div .col-md-10 {
      max-width: 100% !important;
      flex: 0 0 100% !important;
    }
    .container.order-detail-page.main_address_page_listing {
      margin-top: 30px;
    }
    .main_address_page_listing form .address_form_field {
      max-width: 100% !important;
      margin: 40px 10px !important;
    }
    .container.order-detail-page {
      margin-left: 0px !important;
      margin-top: 0px !important;
    }
    .upload__image-wrapper.profile-section-now button {
      margin-top: 10px !important;
    }
    .profile-text-right table {
      width: 91%;
    }
    .main_saved_card_class .savedClass {
      justify-content: space-around;
    }
    .add_address_class_from_autoship,
    .add_address_from_CheckoutPage,
    .change_passsword_from_profile {
      min-width: 90% !important;
    }
    .mainorder-detail-sec.dashboad-deails.Dashboard_bg.main_dashboard_section_class
      .col-md-10.dashboard-main {
      max-width: 100% !important;
      width: 100% !important;
    }
    .main_order_section_class
      .main_order_detail_class
      .orer-details-tables
      tr
      td
      .details_price {
      right: 36px !important;
    }
    .main_refund_section_class
      .refund_detail_section
      table
      tbody
      tr
      td
      .price_amount {
      right: 46px !important;
    }
    form.refund table th:last-child {
      text-align: right;
    }
    form.refund table td:last-child {
      text-align: right;
    }
    .main_refund_section_class
      .refund_detail_section
      table
      tbody
      tr
      td
      .price_amount {
      right: 0 !important;
      padding-right: 16px;
    }
    .mainorder-detail-sec.dashboad-deails.Dashboard_bg.main_dashboard_section_class
      .row {
      margin-right: 0;
      margin-left: 0;
    }
    .container_of_slider
      .MuiToolbar-root.MuiToolbar-gutters.MuiToolbar-regular {
      top: 0px;
      left: -10px;
      padding-left: 0;
    }
    .bcbzuD .main_dashboard_section_class .top-head-commission {
      font-size: 12px;
    }
    .main-top .date_range_filter {
      align-items: center;
    }
    .main_dashboard_section_class .date_range_filter {
      font-size: 12px !important;
      align-items: center !important;
    }
    .main_dashboard_section_class .top-head-commission {
      font-size: 12px !important;
    }
    .show-email-check {
      left: 60px;
    }
    .show-phone-check {
      left: 60px;
    }
    .lables {
      font-size: 12px;
    }

    .main_profile_section_class .profile-img-left table tr th {
      padding: 10px !important;
      font-size: 10px !important;
    }
    .main_profile_section_class .profile-text-right .update-details {
      box-shadow: none !important;
    }
    .main_profile_section_class .profile-text-right .update-details form {
      padding: 0px !important;
    }
    .main_profile_section_class .profile-text-right table tr th {
      font-size: 12px;
    }
    .Cart_product.order-detail-sec:empty {
      display: none;
    }

    .main_autoship_section_class .autoship_details_table .table-responsive {
      border: none;
      padding: 0 15px;
    }
  }
  @media (max-width: 667px) {
    .main_order_section_class
      .main_order_detail_class
      .orer-details-tables
      tr
      td
      .details_price {
      right: 28px !important;
    }
    canvas {
      width: 100% !important;
    }
  }

  @media (max-width: 480px) {
    .main_order_section_class {
      .order_pagination ul.pagination li.previous a,
      .order_pagination ul.pagination li.next a {
        padding-top: 6px;
      }
    }
    .dashboard_main_div .col-md-10 {
      max-width: 97%;
      flex: 0 0 97%;
    }
    .mainorder-detail-sec .col-md-3 {
      max-width: 100%;
      flex: 0 0 100%;
    }

    .dashboard-table-commsion
      tr.MuiTableRow-root.MuiTableRow-head.css-1q1u3t4-MuiTableRow-root
      th {
      padding-left: 9px !important;
    }
    .main_profile_section_class .profile-img-left {
      margin: 20px 0 0 0 !important;
    }
    .main_profile_section_class .profile-text-right {
      margin: 20px auto 30px !important;
    }
 
    .order_table.address-list.order-list {
      overflow-y: scroll;
      @media (max-width: 767px) {
        overflow: visible;
      }
    }

    .order_table.address-list.order-list {
      border: none;
    }
    .order_table.address-list.order-list th {
      min-width: 150px;
    }
    .main_pagination_section_class .order_pagination ul.pagination li {
      width: 24px !important;
      height: 24px !important;
      line-height: 20px;
    }
    .main_pagination_section_class .order_pagination ul.pagination li a {
      padding: 2px 4px !important;
    }
    ul.pagination li {
      margin: 0 1px !important;
    }
    .main_pagination_section_class
      .order_pagination
      ul.pagination
      li.previous
      a,
    .main_pagination_section_class .order_pagination ul.pagination li.next a {
      padding-top: 5px !important;
    }
    .main_pagination_section_class .order_pagination ul.pagination {
      margin-left: 0 !important;
    }
    .orer-details-tables {
      width: auto !important; 
    }
    .main_order_section_class
      .main_order_detail_class
      .orer-details-tables
      tr
      td
      .details_price {
      padding-right: 0px !important;
    } 
    .review_section .product_name,
    .review_section .rating_section {
      width: 100%;
    }
    .title_of_review {
      width: 100%;
      max-width: 100%;
      flex: 0 0 100%;
    }
    .mainorder-detail-sec .container.order-detail-page button {
      margin-bottom: 20px !important;
    }
    .main_saved_card_class .savedClass .cardType {
      min-width: 58px !important;
    }
    .main_order_detail_class {
      padding: 0 !important;
    }
    .main_saved_card_class .savedClass .cardType img {
      width: 100% !important;
      max-width: 31px !important;
      height: 31px !important;
      object-fit: contain !important;
    }
    .main_saved_card_class .savedClass .card_details {
      min-width: auto !important;
    }
    .changeadress-button {
      padding: 0 15px;
      margin: 0 0 20px;
      width: 100%;
    }

    .autoship_details_table table {
      td.autoship-action {
        display: flex;
      }
    }
    .changeadress-button button {
      margin: 0 !important;
    }
    .main_autoship_section_class .autoship_order_detail table td.ship_text {
      padding-right: 8px !important;
    }
    .main_document_section .document-table table tbody td:first-child {
      min-width: 100% !important;
    }
    .main_document_section .document-table table tbody td .download-now {
      display: flex;
    }
    .order_table.address-list.order-list.auto-ship-order-table button {
      margin-bottom: 0 !important;
    }

    .mainorder-detail-sec .container.order-detail-page button {
      margin: 0px auto !important;
    }
    .mainorder-detail-sec .container.order-detail-page .re-order {
      margin-bottom: 20px !important;
    }
    .orer-details-tables th {
      min-width: 150px;
    }
    .mainorder-detail-sec.dashboad-deails.Dashboard_bg.main_dashboard_section_class
      .col-md-10 {
      max-width: 96% !important;
    }
    .mainorder-detail-sec {
      padding: 26px 0 85px;
    }
    .main_pagination_section_class
      .order_pagination
      ul.pagination
      li.previous
      a,
    .main_pagination_section_class .order_pagination ul.pagination li.next a {
      padding-top: 5px !important;
    }
    .main_pagination_section_class .order_pagination ul.pagination {
      margin-left: 0 !important;
    }

    .mainorder-detail-sec.main_autoship_section_class
      .changeadress-button
      .back-button {
      margin-bottom: 10px !important;
      height: auto;
    }
    .mainorder-detail-sec.main_autoship_section_class button.add_new_card {
      margin-bottom: 10px !important;
    }
    .main_order_section_class
      .main_order_detail_class
      .orer-details-tables
      tr
      td
      .details_price {
      right: 55px !important;
    }
    .autoship_details_table .main_saved_card_class .col-md-6.savedClass {
      width: 90% !important;
    }
    .container.order-detail-page.saved-card button.add_new_card {
      margin-top: 0 !important;
  }
  }

  @media (max-width: 375px) {
    .main_document_section .document-table table tbody td:first-child {
      min-width: 100% !important;
    }
  }
  @media (max-width: 360px) {
    .main_document_section .document-table table tbody td .download-now {
      padding: 6px 13px !important;
      font-size: 12px;
    }
  }

 
/* commmon style is return hear */
div.dashboard_main_div {
    display: flex;
    padding: 0px 15px;
    min-height: 820px;
    @media (max-width: 767px) {
      min-height: auto;
      justify-content: flex-start;
    }
    .col-md-10 {
      margin-right: 0 !important;
      padding: 0 !important;

      @media only screen and (min-width: 1200px) {
        width: calc(100% - 250px);
      }
    }
    .col-md-2 {
      padding: 0 !important;
      position: relative;
      z-index: 1;
      @media only screen and (min-width: 1200px) {
        width: 250px;
      }
    }
  }
  .heading-top-main.right-address {
    text-align: left;
    width: 100%;
    display: inline-block;
    box-shadow: 6px 6px 17px -12px #3a3232;
    margin-bottom: 25px;
    background: rgba(0, 53, 106, 0.92);
    /*background:#00356a;
    background-image: url(/images/header.jpg);
    background-size:cover;*/
  }

  .heading-top-main h1 {
    margin: 0;
    padding: 14px 17px;
    font-size: 24px;
    white-space: nowrap;
    font-weight: 600;
    text-transform: capitalize;
    color: #fff;
    @media (max-width: 768px) {
      font-size: 20px;
    }
  }
  .container.order-detail-page {
    margin-left: 0px;
    background: #fff;
    box-shadow: 7px 4px 31px -5px #c5bfbf;
    height: 100%;
    min-height: 100%;
    /* max-height: 560px; */
    padding: 0px;
    margin-bottom: 15px;
    @media (max-width: 767px) {
      height: auto;
      min-height: auto;
    }
  }
  .address-list th {
    background: #00356a;
    padding: 13px;
    color: #fff;
    z-index: 9;
    text-transform: capitalize;
    border: none !important; 
    font-family: var(--common-fontmd);
    text-align: center;
  }
  .address-list {
    padding: 0;
    border: 0px solid #ddd;
    margin: 0px 24px 30px;
    border-radius: 0px;
    .Cart_product {
      border: 1px solid #ddd;
    }
  }
  tr.even-tr {
    background: #fbfbfb !important;
    width: 100%;
  }
  tr.even-td {
    width: 100%;
  }

  .address-list tr td {
    padding: 15px;
    text-align: center;
    height: auto;
    border: none;
  }
  table.commission-table th:last-child {
    text-align: center !important;
  }
  table.commission-table thead {
    background: #06356a;
    color: #fff !important;
  }
  td.action-btn {
    border: none !important;
  }
  .commission-table tbody tr td:last-child div {
    padding: 0;
  }

  .main_order_detail_class {
    margin: 0 25px;
    padding: 0 0px;
  }

  /* //***************************************** Dashoard page styles is here ****************************************************************** */
  .main_dashboard_section_class {
    .leftsec {
      border: none;
      margin: 0 17px;
      @media (max-width: 1024px) {
        margin: 0;
      }
      .usericon-a {
        margin: 0 auto;
        text-align: center;
        h3 {
          margin-top: 0px;
          margin-bottom: 0px;
        }
        .icons {
          margin-top: 10px;
          a {
            color: #fff;
            margin: 0 5px;
            background: #00356a;
            width: 30px;
            display: inline-flex;
            height: 30px;
            align-items: center;
            justify-content: center;
            border-radius: 30px;
            svg {
              width: 15px !important;
              margin: 0px;
              height: 14px;
            }
          }
        }
      }
    }

    .deatis-info {
      border-top: 1px dashed #d9d9d9;
      padding: 20px 10px 0;
    }

    .dt-jned {
      padding-bottom: 7px;
      clear: both;
      display: flex;
      justify-content: space-between;
      :where(:nth-child(5), :nth-child(6)) span:last-child {
        flex: 1;
        max-width: 100%;
        padding: 0;
      }
      span {
        width: 137px;
        float: left;
        svg {
          font-size: 14px;
          color: #06356a;
        }
      }
    }
    .current-part {
      border-top: 1px dashed #d9d9d9;
    }
    .Distributor.row {
      text-align: center;
      margin: 8px auto;
      width: 100%;
      border-top: 1px dashed #00356a;
      padding: 20px 0;
      border-bottom: 1px dashed #00356a;
    }
    .total_sale {
      padding: 0 0px 15px 10px;
      text-align: left;
      width: 100%;
      @media (max-width: 1024px) {
        padding: 0 0 15px;
        position: relative;
      }
      @media (max-width: 991px) {
        padding-right: 0;
      }
    }
    .active-block {
      border-top: 1px dashed #d9d9d9;
      margin: 0px 0 0;
      padding-bottom: 10px;
    }
    .Toal-Sale {
      padding-bottom: 10px;
      padding-right: 27px;
    }
    .DownlineStatic {
      border-radius: 0px;
      margin: 25px 17px;
      border: 0px solid #00356a;
      padding: 0;
      background: rgba(0, 0, 0, 0.03);
      @media (max-width: 1199px) {
        margin: 30px 0;
      }
      h3 {
        margin: 0;
        background: #00356a;
        color: #fff;
        padding: 10px;
        line-height: 1;
        border-radius: 0;
        font-weight: 600;
      }
      .documents-downloads {
        a {
          float: right;
          color: #fff;
          font-size: 13px;
          font-weight: normal;
        }
      }
    }
    .total-main-now {
      border-bottom: 1px solid #ddd;
      padding: 11px;
      font-size: 13px;
    }
    .newdata {
      height: auto;
      padding: 8px;
      display: -webkit-box;
      display: flex;
      strong {
        color: #000;
      }
    }
    strong {
      min-width: 120px;
      float: left;
    }
    .container.order-detail-page .row {
      border-bottom: none;
      margin: 0;
    }
    .leftsec { 
      background: rgba(0, 0, 0, 0.03);
      border-radius: 0px;
      .check svg.check_tick {
        font-size: 24px;
        font-weight: 800;
        color: #13ad13;
        @media (max-width: 1024px) {
          font-size: 14px;
        }
      }
    }
    .dataContainer {
      max-height: 230px;
      overflow-y: scroll;
    }
    /* // *****************************20 OCTOBER POOJA MAM****************** */
    .heading-top-main.right-address { 
      margin-bottom: 25px;
    }

    .statis_datas .top-head-commission {
      margin: 0px 0px 20px !important;
    }
    .top-head-commission {
      font-size: 18px;
      font-weight: 600;
    }
    .common-static-right a,
    .right-data-dashboard a {
      font-size: 13px;
      text-decoration: none;
    }
    .date_range_filter {
      font-size: 14px;
      font-weight: 600;
      display: flex;
      flex-direction: row;
      gap: 12px;
      select {
        padding: 5px;
        font-family: var(--common-font);
      }
    }
    .check {
      display: flex;
      align-items: center;
      position: relative;
      justify-content: flex-start;
      position: relative;
    }
    .currentrank h3 {
      font-size: 13px;
      margin-bottom: 0px;
      margin-top: 12px;
    }
    .leftsec .active-block h3 {
      text-align: left;
      font-size: 15px;
      margin-top: 0;
    }
    .leftsec .active-block h3.title_a {
      margin-top: 15px;
    }
    .leftsec .active-block .rank_select_requirememt {
      margin-bottom: 10px;
    }
    .select_rank {
      font-size: 15px;
      padding: 0px;
      margin-top: 0;
      border: 0px solid #000;
      max-width: 100%;
      width: 100%;
      margin-left: auto;
    }
    .leftsec .active-block {
      padding-right: 10px;
      @media (max-width: 1024px) {
        padding-right: 0;
      }
    }
    .leftsec .active-block label {
      display: none;
    }
    .filleddata {
      padding: 10px 20px;
      @media screen and (max-width: 1365px) {
        padding: 10px;
      }
    }
    .total-main-now div {
      padding: 10px;
      border: none;
      width: 100%;
      max-width: 50%;
      flex: 0 0 50%;
      text-align: center;
    }
    .total-main-now div:first-child {
      border-right: 1px solid rgba(0, 0, 0, 0.2);
    }
    .filleddata .total-main-now {
      border-bottom: none;
      padding: 0;
      font-size: 15px;
    }
    .filleddata .total-main-now:first-child {
      border-bottom: 1px solid rgba(0, 0, 0, 0.2) !important;
    }
    .pieChart_main_div svg {
      width: 20px;
      height: 20px;
    }
    /*.comistion-table-data{
        overflow-y: auto;
    }*/
    span.eye_icon_main_class {
      position: absolute;
      right: 10px;
      font-size: 12px;
      @media (max-width: 1024px) {
        right: 0;
      }
      @media (max-width: 991px) {
        right: 2px;
      }
    }
    span.membership_title_class {
      padding-left: 8px;
      width: 215px;
      @media (max-width: 1500px) {
        padding-left: 8px;
        font-size: 14px;
      }
    }
    .pieChart_main_div {
      float: revert;
      position: absolute;
      right: 40px;
    }
    .collapse_arrow_class {
      position: absolute;
      right: 0px;
      top: 124px;
      border: transparent !important;
      background-color: transparent !important;
    }
    .collapse_arrow_class svg {
      color: #00356a;
    }
    .comistion-table-data table {
      border: 1px solid #ddd;
      position: relative;
      th {
        font-size: 15px;
        font-weight: 500;
        padding: 15px;
        font-family: var(--common-font);
      }
      td {
        border-bottom: 1px solid #ddd;
        font-size: 14px;
        padding: 15px;
        font-family: var(--common-font);
      }
      td[colspan="9"] {
        border: 0px;
        .css-yuob64 {
          margin: 0px;
        }
      }
    }
    .comistion-table-data .inner-table {
      border: transparent !important;
      border-bottom: transparent !important;
    }
    .css-1k51tf5-MuiTooltip-tooltip {
      font-size: 14px !important;
    }
    .statis_datas .commsion-weekly-dashboard {
      float: left;
      width: 100%;
    }
    .dashboard-commsion-header {
      padding: 0 0 10px 0;
      margin-bottom: 10px;
      @media (max-width: 767px) {
        padding: 10px;
      }
    }
  }
  .statis_datas {
    padding: 0 35px 0px 25px;

    .top-head-commission {
      border: 1px solid #00356a;
      margin: 20px 0;
      justify-content: space-between;
      padding: 10px;
      border-radius: 5px;
      padding: 0;
      padding-bottom: 0px;
      border: 0;
      border-bottom-color: currentcolor;
      border-bottom-style: none;
      border-bottom-width: 0px;
      border-bottom: 1px solid #ddd;
      border-radius: 0;
      padding-bottom: 10px;
    }
    .main-top {
      /*border: 1px solid #00356a;*/
      margin: 20px 0;
      border-radius: 0px;
      margin: 20px 0;
      border-radius: 0px;
      background: rgba(0, 0, 0, 0.03);
      padding: 15px;
    }
    .commsion-weekly-dashboard {
      border: 0px solid #00356a;
      margin: 0px 0 20px;
      border-radius: 0px;
      background: rgba(0, 0, 0, 0.03);
      padding: 15px;
      .dashboard-table-commsion {
        table {
          td:first-child,
          th:first-child {
            width: 50px;
          }
          thead {
            display: block;
            tr {
              width: 100%;
              display: table;
              th {
                width: calc(100% / 6);
              }
            }
          }
          tbody {
            max-height: 400px;
            overflow-y: auto;
            position: relative;
            display: block;
            tr {
              display: table;
              width: 100%;
              td {
                position: relative;
                width: calc(100% / 6);
                button[type="button"] {
                  padding: 0 !important;
                  position: absolute;
                  right: 20px;
                  top: 50% !important;
                  transform: translateY(-50%);
                  margin: 0 40px 0 0;
                  * {
                    background: none;
                  }
                }
              }
            }
          }
        }
      }
      .common-static-left h3 {
        margin: 0;
        font-weight: 600;
      }

      .common-static-right a,
      .right-data-dashboard a {
        color: #00356a;
        text-decoration: none;
        font-weight: 600;
      }
      .date_range_filter input {
        margin: 2px 0 0;
        vertical-align: top;
      }
      .get_commision button {
        font-size: 12px;
      }
      .comistion-table-data.dashboard-table-commsion th {
        font-size: 14px;
        font-weight: 600;
        background: #00356a;
        color: #fff;
        text-transform: uppercase;
      }
      /* 7-dec-2021 */

      @media (max-width: 1024px) {
        padding: 0;
      }
    }

    /* //***************************************** Order Page Style is here *********************************************************************** */

    .main_order_section_class {
      .refund_history_class {
        margin-top: 25px;
        padding-left: 0;
      }
      button {
        font-size: 13px !important;
      }
      .main_order_detail_class {
        br {
          display: none;
        }
        .refund_button_class {
          float: right;
        }
        table.inner-table-shop {
          tr {
            th {
              text-align: left;
            }
            td {
            }
          }
        }
        .orer-details-tables {
          tr {
            th {
              border-right: none;
            }
            td {
              position: relative;
              border-right: none;
              input.product-qty {
                border: none;
                text-align: center;
              }
              .details_price {
                text-align: right;
              }
            }
          }
        }
      }
      .refund_button_class {
        background: #06356a;
        border: 1px solid #06356a;
        color: #fff;
        padding: 15px 40px;
        font-size: 18px;
        line-height: 1;
        display: table;
        margin: 0 auto;
        text-transform: uppercase;
        font-weight: 600;
        letter-spacing: 0.5px;
        border-radius: 6px;
        margin-top: 17px;
        &.disabled {
          background: #aaa7a7 !important;
          border: 2px solid #aaa7a7 !important;
          &:hover {
            background: none;
            color: #aaa7a7;
          }
        }
      }

      button.MuiButton-root.MuiButton-text.MuiButton-textPrimary.MuiButton-sizeMedium.MuiButton-textSizeMedium.MuiButtonBase-root.css-1e6y48t-MuiButtonBase-root-MuiButton-root {
        background: none;
        color: #000;
        border: none;
        width: auto;
      }
      td.action-btn button {
        font-size: 12px;
        height: 34px;
        width: 92px;
        margin: 0 10px;
      }
      button.disable {
        background: #aaa7a7 !important;
        border: 2px solid #aaa7a7 !important;
        color: #fff;
        &:hover {
          background: none !important;
          color: #aaa7a7 !important;
        }
      }

      .Cart_product.order-detail-sec {
        background: #fff;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        padding: 0;
        box-shadow: none;
        margin-top: 25px;
        @media (max-width: 767px) {
          min-width: auto;
        }
        h4 {
          margin-bottom: 0;
          border-top-left-radius: 0px;
          border-top-right-radius: 0px;
          font-size: 15px;
          font-weight: bold;
          padding: 15px;
        }
        tr {
          &:last-child {
            td {
              border: 0 none;
            }
          }
        }
      }
      button.re-order {
        float: left;
      }

      table {
        th {
          text-align: center;
        }
        td {
          text-align: center;
        }
        .title12 {
          width: 58px;
        }
      }
    }
  }
  /* //***************************************** Document page style is here ********************************************************************** */
  .main_document_section {
    .document-table {
      width: 100%;
      max-width: calc(100% - 30px);
      margin: 0px 15px;
      box-shadow: 0px 5px 31px -5px #e7e7e7;
      border: 1px solid #efefef;
      border-radius: 6px;
      @media (max-width: 767px) {
        margin-bottom: 20px;
       tr {
          display: flex;
          flex-wrap: wrap;
          justify-content: center;
      }
      }
      table {
        width: 100%;
        tbody {
          tr {
            border-bottom: 1px solid #ddd;
            &:last-child {
              border-bottom: 0px;
            }
          }
          td {
            padding: 20px;
            &:first-child {
              min-width: 220px;
              svg {
                color: #00356a;
                font-size: 35px;
                padding-right: 7px;
                vertical-align: sub;
              }
            }
            .icon-pdf {
              display: flex;
            }
            .download-now {
              background: #00356a;
              color: #fff;
              padding: 0 15px !important;
              text-align: center;
              border-radius: 50px;
              border: 2px solid #00356a;
              transition: background 0.3s ease;
              max-width: 150px;
              float: right;
              font-size: 14px;
              width: 100%;
              &:hover {
                background: #fff;
                color: #00356a;
                button {
                  background: #fff;
                  color: #00356a;
                }
                svg {
                  fill: #00356a;
                }
              }
              svg {
                color: #fff;
                font-size: 14px;
                margin-right: 10px;
                min-width: 20px;
              }
              button {
                color: #fff;
                background: #06356a;
                border: none;
                transition: background 0.3s ease;
                margin: 0;
              }
            }
            div.psd-text {
              font-weight: 600;
            }
            .psd-text {
              vertical-align: sub;
              padding-top: 6px;
              a {
                color: #000 !important;
              }
            }
          }
        }
      }
    }
  }

  /* //***************************************** Address Page Style is here *********************************************************************** */
  .main_address_page_listing {
    table {
      border: 1px solid #ddd;
    }
    table.commission-table thead {
      background: #06356a;
      color: #fff !important;
    }
    .document_page_section {
      background: red;
    }
    .row {
      margin-left: 30px;
      margin-right: 30px;
      margin-top: 0px;
      margin-bottom: 0;
      padding-bottom: 0;
    }
    form {
      .address_form_field {
        margin: 0px auto 40px;
        max-width: calc(100% - 40px);
        border: 1px solid #e7e7e7;
        border-radius: 0px;
        padding: 15px;
        background: #fbfbfb;
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        @media (max-width: 767px) {
          margin: 0 20px 20px !important;
          .cstm-btns-sec {
            display: flex;
            gap: 10px;
          }
        }
        .mb-3 {
          width: calc(50% - 10px);
          @media (max-width: 767px) {
            width: 100%;
          }
        }
        input {
          margin-bottom: 12px;
          height: 40px;
          box-shadow: none;
          text-transform: capitalize;
        }
        select {
          margin-bottom: 12px;
          height: 40px;
          border: 1px solid #ddd;
          background: #fff;
          text-transform: capitalize;
        }
        textarea {
          margin-bottom: 12px;
          height: 70px;
          padding: 10px;
        }
        .cstm-btns-sec {
          text-align: right;
          width: 100%;
          flex: 0 0 100%;
          button {
            margin-left: 20px;
          }
        }
      }
    }
    .edit_address {
      margin: 0 0 0 13px;
      button {
        background: transparent !important;
        border: none !important;
        color: red !important;
        padding-right: 17px !important;
        font-size: 16px;
      }
    }
    .order_table {
      &.address-list {
        &.order-list {
          table {
            width: 100%;

            &.addressTable-table {
              th {
                &:last-child {
                  text-align: center;
                }
              }
              td {
                &:last-child {
                  text-align: center !important;
                  float: none;
                }
                button {
                  text-transform: uppercase;
                  + svg {
                    float: none;
                    margin-left: auto;
                    margin-right: 0;
                    cursor: pointer;
                    color: #06356a;
                    width: 20px;
                    height: 20px;
                    vertical-align: top;
                  }
                }
                .edit_address {
                  > svg {
                    fill: var(--blue);
                    transform: translateY(3px);
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  /* //***************************************** Autoship Page Style is here *********************************************************************** */

  .main_autoship_section_class {
    .autoship_details_table {
      border: 1px solid #ddd;
      border-radius: 0px;
      width: 100%;
      margin-bottom: 25px;
      /* @media (max-width: 767px){
            border: none;
        } */
    }
    table {
      tr {
        @media (max-width: 767px) {
          border: none;
        }
        td {
          text-align: center;
          @media (max-width: 767px) {
            border: none;
          }
          svg {
            &:first-child {
              font-size: 17px;
              margin-top: 3px;
              color: #06356a;
            }
            &:last-child {
              font-size: 15px;
              color: #f60707;
              margin-top: 4px;
              margin-right: 0;
            }
          }

          .main-qty-sec {
            margin: 13px 0;
            display: flex;
            justify-content: center;
            .box {
              padding: 0;
            }
          }
          #qty {
            border: 1px solid #ddd;
            width: auto;
            flex: 0 0 auto !important;
            display: flex;
            height: 34px;
            min-width: 130px;
            button {
              width: 32px;
              position: relative;
              &:hover {
                background: #ddd;
              }
            }
            input {
              background: #fff;
              width: calc(120px - 64px) !important;
              height: 32px !important;
              border-left: 1px solid #ddd;
              border-right: 1px solid #ddd;
            }
            .sub {
              left: auto;
              top: auto !important;
              position: relative;
              margin: 0;
              font-size: 22px;
              height: 32px;
              bottom: auto;
              border-radius: 0px;
              display: flex;
              @media (max-width: 767px) {
                padding: 0 !important;
              }
            }
            .add {
              top: auto !important;
              position: relative;
              margin: 0;
              border-radius: 0;
              font-size: 14px;
              height: 32px;
              @media (max-width: 767px) {
                padding: 0 !important;
              }
            }
          }
        }
      }
    }
    .autoship_address_section {
      button[disabled] {
        background: #cbc8c8 !important;
        color: #ddd;
        border-color: #ddd;
      }
    }
    .thn-lft.raido-addres-sel {
      background: #fff;
      margin-top: 0px;
      padding: 0;
      border: 1px solid #ddd;
      margin-bottom: 25px;
      box-shadow: none;
      border-radius: 0;
      h2 {
        font-size: 15px;
        background: #06356a;
        color: #fff;
        margin: 0;
        padding: 15px 20px;
      }

      .radio-bnt {
        padding: 0 10px;
        margin: 10px !important;
        height: 45px;
        background: #fff;
        input {
          margin-top: 2px;
        }
        .checkout-addrees {
          padding: 18px 0 0 !important;
          @media (max-width: 767px) {
            padding: 0 0 !important;
            max-width: calc(100% - 10px);
            word-break: break-word;
          }
        }
        @media (max-width: 767px) {
          height: auto;
        }
        h5 {
          margin: 0;
        }
      }
    }
    .autoship_order_detail {
      h4 {
        padding: 15px 0;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        font-size: 15px;
        font-family: "Lato";
      }
      table {
        width: 100%;
        tr {
          border-bottom: 1px solid #ddd;
          @media (max-width: 767px) {
            border-bottom: none;
          }
          &:last-child {
            border-bottom: none;
          }
        }
        th {
          background: #06356a;
          color: #fff;
          text-align: left;
          padding: 10px;
          &:first-child {
            border-top-left-radius: 0px;
          }
          &:last-child {
            border-top-right-radius: 0px;
            text-align: center;
          }
        }
        td {
          text-align: left;
          padding: 10px;
          &.ship_amout {
            margin: 30px;
            padding: 15px;
            @media (max-width: 767px) {
              margin: 0;
            }
          }
          &.ship_text {
            text-align: center;
            padding-right: 0px;
            vertical-align: top;
            padding-top: 15px;
            padding-bottom: 15px;
          }
          &.ship_amout {
            margin-bottom: 10px !important;
            padding-bottom: 14px;
            @media (max-width: 767px) {
              display: none;
            }
          }
          .cart-product-details {
            justify-content: flex-start;
            flex-wrap: nowrap;
          }
          .payment_with_delete {
            display: flex;
            text-align: center;
            justify-content: center;
          }
          .delete {
            margin: 0 0 0 10px;
          }
        }
      }
      .add_product_button {
        width: 100%;
        text-align: right;
        margin-bottom: 24px;
        button {
          margin-bottom: 17px;
          text-transform: uppercase;
          margin-bottom: 0;
          height: 40px;
        }
      }
      .savedClass {
        max-width: calc(100% / 3 - 30px);
        width: 100%;
        margin-bottom: 15px;
        flex: 100%;
        @media (max-width: 991px) {
          max-width: 100%;
        }
        .card_delete_and_type_container {
          justify-content: flex-end;
          padding-right: 10px;
        }
      }
    }
    button.back-button {
      margin: 40px 0;
    }
    .changeadress-button button {
      margin-left: 10px !important;
    }
  }

  /* //***************************************** Profile Page Style is here *********************************************************************** */
  .main_profile_section_class {
    .table-profile-details {
      display: flex;
      margin: 0px 0 0;
      padding: 0 30px;
      gap: 20px;
      @media (max-width: 767px) {
        padding: 0 15px;
      }
    }
    .profile-img-left {
      padding: 0;
      border: 1px solid #ddd;
      border-radius: 0px;
      flex: 0 0 calc(100% / 2 - 20px);
      max-width: calc(100% / 2 - 20px);
      height: 100%;
      h4 {
        background: #00356a;
        margin: 0;
        color: #fff;
        text-align: center;
        padding: 15px 0;
        border-top-left-radius: 0px;
        border-top-right-radius: 0px;
        font-weight: 600;
        font-size: 15px;
      }
      table {
        width: 100%;
        tr {
          width: 100%;
          th {
            padding: 14px;
            font-size: 14px;
            text-align: left;
          }
          td{
            padding: 14px;
          }
        }
      }
    } 
    .profile-text-right {
      max-width: calc(100% / 2 - 20px);
      flex: 0 0 calc(100% / 2 - 20px);
      /*box-shadow: 0 0 10px #ddd;*/
      border: 1px solid #ddd;
      margin: 0px 0 40px;
      border-radius: 0px;
      table {
        margin: 30px auto;
        width: 100%;
        max-width: calc(100% - 30px);
        tr {
          th {
            padding: 14px 10px;
            width: 50%;
          }
          td {
            width: 50%;
          }
        }
      }
      .profile-section-now {
        text-align: center;
        img {
          width: 180px;
          height: 180px;
        }
      }
      .profile-btn {
        a {
          cursor: pointer;
          background: #00356a;
          margin: 0 5px;
          padding: 7px 15px;
          color: #fff;
          border-radius: 45px;
          font-size: 14px;
          border: 2px solid #00356a;
          transition: all 0.5s;
          font-weight: 600;
          text-transform: capitalize;
          &.disabled {
            pointer-events: none;
          }
          i {
            padding: 0px 5px 0 0;
          }
          &:hover {
            background: none;
            color: #00356a;
            text-decoration: none;
          }
        }
      }
      .update-details {
        box-shadow: 0 0 10px #ddd;
        float: left;
        h4 {
          background: #00356a;
          margin: 0;
          color: #fff;
          text-align: center;
          padding: 15px 0;
          border-top-left-radius: 0px;
          border-top-right-radius: 0px;
          font-weight: 600;
          font-size: 15px;
          text-transform: uppercase;
        }
        form {
          border: none;
          padding: 20px;
          border-radius: 5px;
          button.row {
            border: unset !important;
          }
          input {
            border: none;
            box-shadow: none;
            outline: none;
            border-bottom: 1px solid #ddd;
            border-radius: 0;
            height: 30px;
            padding: 0 10px;
            margin-right: 7px;
            margin-top: 1px;
          }
          .check_box_align {
            margin: -5px 0px 0px -1px;
          }
          label {
            padding-top: 8px;
          }
        }
      }
      button {
        margin-left: 20px !important;
      }
    }
    .upload__image-wrapper.profile-section-now button {
      margin-bottom: 20px !important;
    }
    .profile-btn {
      margin: 20px 0 0;
    }
    .image-section {
      margin-top: 20px;
      margin-bottom: 15px;
      img {
        object-fit: cover;
      }
    }
    .image-item__btn-wrapper {
      display: flex;
    }
    .profile-btn {
      display: flex;
      justify-content: center;
    }
    .change_password {
      padding-top: 0;
    }
    .edit-image button {
      padding: 5px;
      margin: 0;
    }
    .image-item__btn-wrapper {
      display: flex;
      justify-content: center;
    }
    .image_delete_div {
      padding-top: 10px;
      padding-left: 15px;color: #f00;
    }
    .profile-text-right {
      padding-bottom: 30px; 
      tr {
        border-bottom: 1px solid #ddd;
      }
    }
    .profile-img-left {
      tr {
        border-bottom: 1px solid #ddd;
        &:last-child {
          border-bottom: none;
        }
      }
      @media (max-width: 767px) {
        margin-top: 0;
      }
    }

    //***********************************Saved card**********************

    @media (max-width: 768px) {
      .saved_card_page {
        .saved_card {
          max-width: 71%;
          flex: 0 0 70%;
        }
      }
    }
  }
  
  [class*="DnaReportstyle__DnaReport"] div{
    max-width: 100%;
    width: 100%; 
    table {
    border: 1px solid #ddd;
    } 
}
`;

export default DBOP;
