import api from "@api/Apis";
const { useEffect, useState, useCallback } = require("react");
import InputLabel from '@material-ui/core/InputLabel';
import { Select, Radio, FormControl, FormControlLabel, Button, Input, RadioGroup } from '@material-ui/core'
import { toast } from "react-toastify";
import styled from 'styled-components';
import _ from "lodash";

const ShippingSystemStyle = styled.div`
&
.main_shipping_method_class{
    .select_area_for_shipping_charge {
        padding-left:0px;
    }
    .radio_section_for_shipping_cost_gsn {
        padding-left:0px;
        .MuiRadio-root{
            padding:0px 10px;
        }
        span.MuiTypography-root.MuiTypography-body1.MuiFormControlLabel-label.css-ahj2mt-MuiTypography-root p {
            font-size: 14px;
            padding-top: 0px;
            margin:0px;
        }
        .radio_option {
            width: 100%;
            margin-bottom: 0px;
            label{
                max-width:900px !important;
            }
        }
    }
    .select_area_for_shipping_charge select {
        padding: 10px;
        width: 100%;
        min-width: 347px;
        font-size:14px !important;
    }
    .choose_label {
        font-size: 16px;
        display: block !important;
        overflow: unset;
        white-space: normal;
        color: #000;
        padding-left:0;
        font-weight:600;
        font-family:var(--common-font);
        .css-wgai2y-MuiFormLabel-asterisk {
            color: red;
        }
    }
}
`;

const ShippingSystem = ({ shippingaddress, shippingWithoutLogin, freeShipping, Datashow, setShowContinue, section, setShippingChargesAmount, AutoshipSelected, updateShippingPayment, product, ...props }) => {
    // const [selectState, setSelectState] = useState("");

    const [shippingData, setShippingData] = useState([]);
    const [selectedCheck, setSelectedCheck] = useState(null);
    const [showShippingAmount, setShowShippingAmount] = useState(true);

    useEffect(() => {
        let token = localStorage.getItem('Token');
        if (shippingaddress && Datashow?.weight !== undefined) availableShippingModules(token);
    }, [shippingaddress, Datashow, shippingWithoutLogin])

    useEffect(() => {
        if (section === "Autoship") {
            const { id, amount } = AutoshipSelected;
            setSelectedCheck(id);
            setShippingChargesAmount(amount);
        }
    }, [AutoshipSelected, Datashow]);

    const availableShippingModules = async (token) => {
        const payload = {
            shipping_address_id: parseInt(shippingaddress),
            cart_items: Datashow?.productsCount,
            cart_weight: +Datashow?.weight,
            cart_total: Datashow?.subamount
        }
        if (section === "checkout") {
            if (shippingaddress === "null") {
                payload['shipping_address_detail'] = !token ? shippingWithoutLogin : {};
            }
            const data = localStorage.getItem('packProduct');
            const jsonPraseData = data ? JSON.parse(data) : {};
            if (jsonPraseData && jsonPraseData?.customPack) {
                payload['cart_items'] = (jsonPraseData?.products?.length > 0) ? payload['cart_items'] + 1 : payload['cart_items'];
            }
        }
        await api.availableShippingModules(token, payload).then((response) => {
            if (response.status === 200 && response?.data) {
                setShippingData(response?.data);
                let data = [];
                Object.entries(response?.data).forEach(([key, value]) => {
                    if (key !== "code") { data = [...data, ...value] }
                });
                let shippingArray = _.map(data, 'id');
                if (shippingArray?.length > 0 && shippingArray?.indexOf(selectedCheck) <= -1) {
                    setSelectedCheck(null);
                    setShowContinue(null);
                }

            }
        }).catch((err) => console.log(err))
    };
    // function handleChange(event) {
    //     setSelectState(event.target.value);
    // }
    async function handleClick(value) {
        setSelectedCheck(value);
        setShowContinue(value);
        let token = localStorage.getItem('Token');
        let payload = {
            Token: token,
            shipping_id: value
        }


        if (value !== null) {
            if (section === "Autoship") {
                updateShippingPayment(value)
            }
            await api.calculateShipping(token, payload).then((response) => {
                if (response?.status === 200 && response?.data) {
                    let freeshipAmount = props?.discount?.discount_amount1;
                    if (freeShipping && props?.discount?.discount_amount1) {
                        freeshipAmount = 0;
                        props?.discount?.setdiscount_amount(0);
                    }
                    setShippingChargesAmount(response?.data?.shipping_charge)
                    /* only perform for checkout page */
                    if (!!(props?.discount?.coupon?.name && freeshipAmount === 0)) {
                        props?.discount?.setdiscount_amount(response?.data?.shipping_charge);
                        const FreeShippingDiscount = response?.data?.shipping_charge;
                        props?.changeAmountOnCouponBased(response?.data?.shipping_charge, FreeShippingDiscount)
                    }
                }
                // else {
                //     toast.error('Somthing went wrong.')
                // }
            }).catch((error) => console.log(error))
        }
    }

    let datas = [];
    Object.entries(shippingData).forEach(([key, value]) => {
        if (value) {
            if (key !== "code") { datas = [...datas, ...value] }
        }
    });

    return (
        <ShippingSystemStyle>
            <div className="main_shipping_method_class">
                <div>
                    {
                        datas?.length > 0 &&
                        <>
                            <InputLabel htmlFor="filled-age-native-simple" className="choose_label" required>Choose a Shipping method:</InputLabel>
                        </>
                    }
                </div>
                <div>
                    <div className="radio_section_for_shipping_cost_gsn">
                        {datas?.length > 0 ? datas?.map((data, index) => {
                            return (
                                <FormControl key={index + 1} className="radio_option">
                                    <FormControlLabel
                                        value="end"
                                        control={
                                            <Radio
                                                value={data?.shipping_cost}
                                                checked={selectedCheck === data?.id}
                                                onChange={() => handleClick(data?.id)}
                                                name="radio-button-demo"
                                                inputProps={{ 'aria-label': 'A' }}
                                            />}
                                        label={<p>{`${data?.name}: $${data?.shipping_cost}`}</p>} />
                                </FormControl>
                            )
                        })
                            :
                            <span>No shipping method on this address.</span>
                        }

                    </div>
                </div>
            </div>
        </ShippingSystemStyle>
    )
}

export default ShippingSystem;