import api from "@api/Apis";
import { useEffect, useState } from "react";
import styled from "styled-components";
import Radio from "@material-ui/core/Radio";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import { toast } from "react-toastify";
import { InputLabel } from "@material-ui/core";

export const SaveCardCss = styled.div`
  .main_saved_card_class {
    display: flex !important;
    flex-wrap: wrap;
    position: relative;
    .row {
      width: 100%;
      margin-top: 25px;
    }
    .label {
      position: absolute;
      top: 0px;
      left: 12px;
      border: none;
      label {
        font-size: 16px;
        font-family: var(--common-font);
        font-weight: 600;
        color: #000;
        .css-wgai2y-MuiFormLabel-asterisk {
          color: red;
        }
      }
    }
    .savedClass {
      width: 24%;
      border: 1px solid #06356a;
      margin: 0 0 30px;
      box-shadow: 0 0 10px #ddd;
      display: flex;
      margin: 0 15px 20px;
      background: #f3f3f3;
      border-radius: 6px;
      height: 75px;
      padding: 0px 0;
      align-items: center;
      .selection_Area {
        span.css-1i4cj80 {
          margin: -8px 0 0;
        }
      }
      .cardType {
        min-width: 70px;
        margin-right: 15px;
        img {
          width: 100%;
          max-width: 40px;
          object-fit: contain;
        }
        h3 {
          font-size: 15px;
          margin: 0;
          padding: 0 0 7px;
        }
      }
      .card_details {
        min-width: 184px;
        h5 {
          padding-bottom: 5px;
          margin: 3px 0 0;
          font-size: 16px;
        }
      }
      .delete_section {
        svg {
          color: red;
          font-size: 17px;
        }
      }
      .radio_and_card_type_container {
        display: flex;
        align-items: center;
      }
      .card_delete_and_type_container {
        display: flex;
        align-items: center;
        width: 100%;
      }
    }
  }
  @media (max-width: 1440px) {
    .main_saved_card_class .savedClass {
      width: 31%;
    }
  }
  @media (max-width: 1366px) {
    .main_saved_card_class .savedClass {
      width: 46%;
    }
  }
  @media (max-width: 1024px) {
    .main_saved_card_class .savedClass {
      width: 46%;
    }
  }
  @media (max-width: 768px) {
    .main_saved_card_class {
      .savedClass {
        width: 94%;
        .radio_and_card_type_container {
          min-width: 137px;
        }
        .card_delete_and_type_container {
          width: 100%;
          .card_details {
            margin: 0 auto;
          }
        }
      }
    }
    .main_saved_card_class .savedClass {
      width: 65%;
    }
  }

  @media (max-width: 480px) {
    .main_saved_card_class .savedClass {
      width: 90%;
    }
  }
`;
export const ImagesForCard = ({ type }) => {
  let imageUrl = "";
  if (type) {
    switch (type) {
      case "Visa":
        imageUrl = "/images/visalogonew.png";
        break;
      case "Mastercard":
        imageUrl = "/images/mastercardlogo.png";
        break;
      case "American Express":
        imageUrl = "/images/americanexplogo.png";
        break;
      case "Discover":
        imageUrl = "/images/discoverlogo.png";
        break;
    }
  }
  return <img src={imageUrl} height="50" width="70" />;
};
const SaveCardSection = ({
  setSavedCardId,
  paymentSection,
  savedCardId,
  UpdateDefaultCard,
  setNoCardSavedUser,
  section,
  showContinue,
}) => {
  const [savedCard, setSavedCard] = useState([]);
  const [check, setChecked] = useState();
  const [selected, setSelected] = useState([]);

  useEffect(() => {
    let token = localStorage.getItem("Token");
    setChecked(savedCardId);
    savedCardFunction(token);
  }, [paymentSection, savedCardId]);

  const savedCardFunction = async function (token) {
    await api
      .getSavedCards(token)
      .then((res) => {
        if (res?.data?.code === 1) {
          setSavedCard(res?.data?.saved_cards);
          if (res?.data?.saved_cards?.length > 0) {
            setNoCardSavedUser(true);
          }
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleDeleteCard = async (deleteId = null) => {
    let token = localStorage.getItem("Token");
    await api.deleteCard(token, { card_id: deleteId }).then((res) => {
      if (res?.data?.code === 1) {
        // setSavedCard(res?.data?.saved_cards);
        // toast.success(res?.data?.message);
        savedCardFunction(token);
      }
    })
      .catch((error) => {
        console.log(error);
      });

  };
  return (
    <SaveCardCss>
      <div className="main_saved_card_class">
        {showContinue !== null && (
          <>
            {savedCard?.length > 0 && (
              <div className="label">
                <InputLabel
                  htmlFor="filled-age-native-simple"
                  className="choose_label"
                  required
                >
                  Choose a Payment method:
                </InputLabel>
              </div>
            )}

            <div className="row saved-nmain">
              {savedCard?.map((data, index) => (
                <div className="col-md-6 savedClass" key={index + 1}>
                  <div className="radio_and_card_type_container">
                    <div className="selection_Area">
                      <Radio
                        value=""
                        checked={check === data?.id}
                        className="radio_for_card"
                        name="savedCard"
                        inputProps={{ "aria-label": "B" }}
                        onChange={() => {
                          setChecked(data?.id);
                          setSavedCardId(data?.id);
                          if (section === "Autoship") {
                            UpdateDefaultCard(data?.id);
                          }
                        }}
                      />
                    </div>
                    <div className="cardType">
                      <ImagesForCard type={data?.card_type} />
                    </div>
                  </div>
                  <div className="card_delete_and_type_container">
                    <div className="card_details">
                      <h5>{data?.card_number}</h5>
                    </div>
                    <div
                      className="delete_section"
                      style={{ cursor: "pointer" }}
                    >
                      {!(check === data?.id) && (
                        <DeleteForeverIcon
                          fontSize="8rem"
                          onClick={() => handleDeleteCard(data?.id)}
                        />
                      )}
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </>
        )}
      </div>
    </SaveCardCss>
  );
};
export default SaveCardSection;
