import moment from "moment";
import React, { useEffect, useState } from "react";


const CountDownPeriod = ({ deadline }) => {
    const [days, setDays] = useState(0);
    const [hours, setHours] = useState(0);
    const [minutes, setMinutes] = useState(0);
    const [seconds, setSeconds] = useState(0);

    const leading0 = (num) => {
        return num < 10 ? "0" + num : num;
    };

    const getTimeUntil = (deadline) => {
        const time = Date.parse(deadline) - Date.parse(new Date());
        if (time < 0) {
            setDays(0);
            setHours(0);
            setMinutes(0);
            setSeconds(0);
        } else {
            setDays(Math.floor(time / (1000 * 60 * 60 * 24)));
            setHours(Math.floor((time / (1000 * 60 * 60)) % 24));
            setMinutes(Math.floor((time / 1000 / 60) % 60));
            setSeconds(Math.floor((time / 1000) % 60));
        }
    };

    useEffect(() => {
        setInterval(() => getTimeUntil(deadline), 1000);

        return () => getTimeUntil(deadline);
    }, [deadline]);


    return (
        <div className="container_week">
            <div className="time_period_main_div">
                <h3>Current Period Ends In:</h3>
                <div className="time-container">
                    <div className="Clock-days">{leading0(days)}d</div>
                    <div className="Clock-hours">{leading0(hours)}h</div>
                    <div className="Clock-minutes">{leading0(minutes)}m</div>
                    <div className="Clock-seconds">{leading0(seconds)}s</div>
                </div>
            </div>
        </div>
    );
};

export default CountDownPeriod;