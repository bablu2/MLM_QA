import { DialogSectionCss } from '@PagesComponent/ActiveSmartShip/ActiveStyleComp'
import React, { useState } from 'react'
import DialogComponent from './DialogComponent'

const LoginPopup = ({ openLoginPopup, setOpenLoginPopup }) => {

    return (
        <>
            <DialogComponent opend={openLoginPopup} handleClose={() => setOpenLoginPopup(false)} title="Welcome to Kaire’s New Website!">
                <DialogSectionCss>
                    <div className='login-pop'>
                        Welcome to Kaire’s New Website! <br />
                        <br />

                        For security purposes, we were unable to transfer your full login details over to our new website.
                        Please follow the steps to reset your password.<br />
                        <br />

                        We apologize for any inconvenience this may cause and thank you for your understanding. <br />

                        <br />

                        Live Better, Longer!
                    </div>
                </DialogSectionCss>
            </DialogComponent>
        </>
    )
}

export default LoginPopup