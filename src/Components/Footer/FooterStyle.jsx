import styled from "styled-components";

const FooterStyle = styled.div`
  & .extra_footer_section {
    display: none;
  }

  /* .blackLoader{
    background: #000;
}     */

  .fotter_section {
    background: ${(props) => `url(${props.image})`};
    padding: 25px 0 0px; 
    margin-top: auto;
    /* margin-bottom: 75px; */
    position: absolute;
    bottom: 0;
    z-index: 100;
    background-repeat: repeat-y;
    background-size: 100%;
    .fotter-logo {
      width: 50%;
      img {
        width: 100%;
        max-width: 180px;
      }
      margin: 0 auto;
    }
    @media (max-width: 991px) {
      background-size: contain;
    }
    @media (max-width: 767px) {
      padding-bottom: 10px;
      background-repeat-y: repeat;
      margin-bottom: 0;
    }
    .main_div {
      padding-bottom: 230px;
      @media (max-width: 767px) {
        padding-bottom: 350px;
      }
    }
    @media (max-width: 767px) {
      position: unset;
    }
  }

  .fotter-listdetail li {
    a {
      pointer-events: none;
      span {
        pointer-events: none;
      }
      &:hover {
        text-decoration: none;
      }
    }
  }
  @media (max-width: 768px) {
    .fotter-listdetail li {
      padding: 0 34px !important;
    }
    .fotter-listdetail ul {
      display: block !important;
      justify-content: center;
      height: auto;
      @media (max-width: 767px) {
        height: auto;
      }
    }
    .extra_footer_section {
      display: block;
    }
    .extra_footer_section ul li {
      display: block;
      position: relative;
      padding-bottom: 10px;
      &.wishlist_section.from_fooTer_section {
        .icon-box span {
          position: absolute;
          top: -8px !important;
          right: -9px !important;
        }
      }
      &.notifications.from_fooTer_section {
        #quick-notifications-menu {
          left: 5px;
          top: 23px;
        }
      }
    }
    .extra_footer_section li.logout_icon svg {
      color: #fff !important;
      margin: 2px 0 0;
    }

    span#quick-notifications-menu {
      line-height: 15px;
      font-size: 9px;
      padding-left: 2px;
    }
    li.wishlist_section.from_fooTer_section a {
      color: #fff;
    }

    .extra_footer_section {
      display: block;
      background: rgb(6, 53, 106);
      position: fixed;
      bottom: -134px;
      right: 0px;
      width: 100%;
      padding: 10px;
      z-index: 999;
      border-top-left-radius: 6px;
      border-bottom-left-radius: 6px;
      border-top: 1px solid #fff;
      height: 200px;
    }

    .extra_footer_section ul {
      display: flex;
      width: 100%;
      flex-wrap: nowrap;
      justify-content: center;
    }

    .extra_footer_section
      ul
      li.wishlist_section.from_fooTer_section
      .icon-box
      span {
      position: absolute;
      top: -8px !important;
      right: 24px;
    }

    li.wishlist_section.from_fooTer_section {
      margin: 22px 0 0;
    }

    li.logout_icon.from_fooTer_section {
      margin-top: 20px;
    }
  }

  @media (max-width: 480px) {
    .fotter-list ul li a {
      padding: 0 5px;
      width: 100%;
      display: block;
      font-size: 13px;
    }
  }
`;
export default FooterStyle;
