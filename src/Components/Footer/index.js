
import React, { useEffect, useState } from "react";
import Image from 'next/image'
import FooterStyle from './FooterStyle';
import NotFication, { WishListNotFication, LogoutNav } from "@Components/NavBar/NotFicationLogout";
import { useRouter } from "next/router";
import Link from 'next/link';
import ViewCartPopup from "@Components/ViewCartPopup";
import api from "@api/Apis";

const Footer = ({ showloader, isLoginToken, innerLoader, subDomain, ...props }) => {

    const [open, setOpen] = useState(false);
    const popRef = React.useRef(null);
    const router = useRouter();
    const store = router?.query?.page ? router?.query?.page : 'us'

    const Validatelogin = (e) => {

        if (subDomain) {
            // setOpen(true);
            router.push(`/us/signup/?d=member`)
        }
        else {
            popRef.current.setRefferalPopup(true);
            popRef.current.setGoToPromoterPack(true);
            popRef.current.setCorrectRefferal({ status: false, data: {} });
            setOpen(true);
        }
    }

    useEffect(() => {
        if (innerLoader === true) {
            document.body.classList.add('bg-footer');
        }
        else {
            document.body.classList.remove('bg-footer');
        }
        if (router?.asPath === "/us/signup/?d=member&r=packs") {
            props?.setActiveStep(1)
        }
    }, [router])

    return (
        <>
            <FooterStyle image="/footer.png">
                <div className="extra_footer_section">
                    <ul>
                        {/* <NotFication store={store} section="from_fooTer_section" key="footerSection1" {...props} /> */}
                        <WishListNotFication store={store} section="from_fooTer_section" key="footerSection2" {...props} />
                        <LogoutNav store={store} section="from_fooTer_section" key="footerSection3" {...props} />
                    </ul>
                </div>
                <div className="fotter_section">
                    <div className="container_footer">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="fotter-logo">
                                    <img src="/images/nav_logo.png" />
                                </div>
                                <div className="fotter-list">
                                    <ul>
                                        <li>
                                            <a className="privacy_style" target='_blank' href='https://admin.shopkaire.com/media/pdf/Kaire_ReturnPolicy.pdf'>Return Policy</a>
                                        </li>
                                        <li>
                                            <a className="privacy_style" target='_blank' href='https://admin.shopkaire.com/media/pdf/Kaire_PrivacyPolicy.pdf'> Privacy Policy</a>
                                        </li>
                                        <li>
                                            <a className="privacy_style" target='_blank' href='https://admin.shopkaire.com/media/pdf/Kaire_TermsConditions.pdf'> Terms & Conditions </a>
                                        </li>
                                        {/*  {props?.isLogin !== true &&
                                            <li>
                                                <a type="button" className="Register_cls" style={{ cursor: 'pointer' }} onClick={(e) => { Validatelogin(e) }} >Join Now</a>
                                            </li>
                                        }*/}
                                    </ul>
                                </div>
                                <div className="logo">
                                    <img src="/images/creditcard.png/" />
                                </div>

                                <div className="footer_text">
                                    These statements have not been evaluated by the Food and Drug Administration. Kaire products are not intended to diagnose prevent treat or cure any disease. If you are under medical supervision for any allergy, disease, taking prescription medications or you are breastfeeding contact your medical
                                    provider before adding any new supplements to your daily regimen.
                                </div>
                                <div className="fotter-listdetail">
                                    <ul>
                                        <li>
                                            <a href="#">North America Toll Free Phone & Fax
                                                <span className="loc1">1-877-603-1710 (Toll-Free)</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">New Zealand Toll Free Phone & Fax
                                                <span className="loc1"> 0800-451353</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">Australia Toll Free Phone & Fax
                                                <span className="loc1">1-800-737-643</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {
                    showloader === true &&
                    <div className="loader-back">
                        <div className="Loader loader-main" data-text="Loading...">
                        </div>
                    </div>
                }
                {
                    innerLoader === true &&
                    <div className="flexbox">
                        <div className="">
                            <div className="bt-spinner"></div>
                        </div>
                    </div>
                }

            </FooterStyle >
            <ViewCartPopup ref={popRef} content={{
                open: open,
                setOpen: setOpen,
                ...props
            }} />
        </>

    );
}
export default Footer