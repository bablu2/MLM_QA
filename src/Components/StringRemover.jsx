const StringRemover = ({ content = "" }) => {

    return (content).replace(/(<([^>]+)>)/ig, '')
}
export default StringRemover;