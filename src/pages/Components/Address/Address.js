import Head from 'next/head'
import Link from 'next/link'
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import api from '../../../api/Apis';
import { toast } from 'react-toastify';
import csc from 'country-state-city'
import { ICountry, IState, ICity } from 'country-state-city'
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';

export default function AddressData({ logintoken, setaddressData, addressDetails, Setshowaddress, setInnerLoader, setaddressDetails }) {
    const { register, handleSubmit, watch, errors } = useForm();

    const [countryError, setCountryError] = useState()
    const [stateError, setStateError] = useState()
    const [country, setCountry] = useState();
    const [state, setState] = useState()
    const [sameAddressmsg, setSameAddressMsg] = useState('')

    if (addressDetails?.country && addressDetails?.state && country === undefined && state === undefined) {
        setCountry(addressDetails.country);
        setState(addressDetails?.state)
    }

    const onSubmit = data => {
        //update_address
        if (country !== undefined && state !== undefined) {
            data.country = country
            data.state = state
            // data['street_address_2'] = null;
            if (+data?.address_id > 0) {
                const formData = { data: data, token: logintoken };
                setInnerLoader(true)
                api.updateAddress(formData).then(res => {
                    if (res?.data?.code === 1) {
                        setInnerLoader(false)
                        setSameAddressMsg('')
                        // toast.success('Address updated successfully', {
                        //     duration: 1
                        // })
                        api.manageAddress(logintoken).then(res => {
                            setaddressData(res?.data?.addresses)
                            setaddressDetails()
                            Setshowaddress(false)
                        })
                    }
                    else {
                        setInnerLoader(false)
                    }
                    if (res?.data?.code === 0) {
                        setSameAddressMsg(res?.data?.message)
                        setInnerLoader(false)
                    }
                })
            }
            else {
                // data['street_address_2'] = null;
                const formData = { data: data, token: logintoken };
                setInnerLoader(true)
                api.saveAddress(formData).then(res => {
                    if (res?.data?.code === 1) {
                        setInnerLoader(false)
                        setSameAddressMsg("")
                        // toast.success('Address added successfully', {
                        //     duration: 1
                        // })
                        api.manageAddress(logintoken).then(res => {
                            setaddressData(res?.data?.addresses)
                            setaddressDetails()
                            Setshowaddress(false)
                        })
                    }
                    else {
                        setInnerLoader(false)
                    }
                    if (res?.data?.code === 0) {
                        setSameAddressMsg(res?.data?.message)
                        setInnerLoader(false)
                    }
                })
            }
        } else {
            if (country === undefined && countryError === undefined) {
                setCountryError("Please Select The Country")
            }
            if (state === undefined && stateError === undefined) {
                setStateError("Please Select The State")
            }
        }

    }

    return <>
        <Head>
            <title>Address setup</title>
        </Head>
        {/* <div className="container"> */}
        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="heading-top-main right-address">
                <div className="heading-main-text">
                    <h1>Addresses List</h1>
                </div>
            </div>
            <div className="address_form_field">
                {addressDetails?.id &&
                    <input type="hidden" className="form-control" name="address_id" id="address_id" aria-describedby="nameHelp"
                        ref={register({ required: "This field is required" })}
                        value={addressDetails?.id}
                    />
                }
                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">First Name</label>
                    <input type="text" className="form-control" name="first_name" id="first_name" aria-describedby="nameHelp"
                        ref={register({ required: "This field is required" })}
                        defaultValue={addressDetails?.first_name}
                    />
                    {errors.first_name && <span className="error">{errors.first_name?.message}</span>}
                </div>
                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">Last Name</label>
                    <input type="text" className="form-control" name="last_name" id="l_name" aria-describedby="nameHelp"
                        ref={register({ required: "This field is required" })}
                        defaultValue={addressDetails?.last_name}
                    />
                    {errors.last_name && <span className="error">{errors.last_name?.message}</span>}
                </div>
                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">Company Name</label>
                    <input type="text" className="form-control" name="company_name"
                        ref={register({ required: false })}
                        defaultValue={addressDetails?.company_name}
                    />
                    {errors.company_name && <span className="error">{errors.company_name?.message}</span>}
                </div>

                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">Address 1</label>
                    <textarea type="textarea" className="form-control" name="street_address_1" id="billing_address"
                        defaultValue={addressDetails?.street_address_1}
                        aria-describedby="billing_addressHelp"
                        ref={register({ required: "This field is required" })}
                    />
                    {errors.street_address_1 && <span className="error">{errors.street_address_1?.message}</span>}
                </div>
                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">Address 2</label>
                    <textarea type="textarea" className="form-control" name="street_address_2" id="billing_address"
                        defaultValue={addressDetails?.street_address_2}
                        aria-describedby="billing_addressHelp"
                        ref={register({ required: false })}
                    />

                </div>
                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">Postal Code</label>
                    <input type="text" className="form-control" name="postal_code"
                        defaultValue={addressDetails?.postal_code}
                        ref={
                            register({
                                required: "This field is required",
                                pattern: {
                                    // value: /^[0-9]/,
                                    // message: "Enter only number"
                                },
                                minLength: {
                                    value: 4,
                                    message: "Enter minimum 4 digit"
                                },

                            })
                        } />
                    {errors.postal_code && <span className="error">{errors.postal_code?.message}</span>}
                </div>
                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">Country</label>
                    <div className="select">
                        <CountryDropdown
                            value={country}
                            priorityOptions={['US', 'CA']}
                            name="country"
                            valueType="short"
                            onChange={(val) => setCountry(val)} />
                    </div>
                    {country === undefined && <span className="error">{countryError}</span>}
                </div>

                {country && <><div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">State</label>
                    <div className="select">
                        <RegionDropdown
                            country={country}
                            value={state}
                            name="state"
                            valueType="full"
                            defaultOptionLabel="Select State"
                            countryValueType="short"
                            onChange={(val) => setState(val)} />
                    </div>
                    {state === undefined && <span className="error">{stateError}</span>}
                </div>
                </>}

                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">City</label>
                    <input
                        type="text"
                        name="city"
                        className="form-control"
                        defaultValue={addressDetails?.city}
                        placeholder="please enter your city"
                        ref={register({ required: "This field is required" })}
                    />
                    {errors.city && <span className="error">{errors.city?.message}</span>}
                </div>


                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label">Phone </label>
                    <input type="text" className="form-control" name="phone_number"
                        defaultValue={addressDetails?.phone_number?.replace(/[`~!@#$%^&*()_|+\-=?;:'" " ",.<>\{\}\[\]\\\/]/gi, '')}
                        ref={
                            register({
                                required: "This field is required",
                                pattern: {
                                    value: /^[0-9]/,
                                    message: "Enter only number"
                                },
                                maxLength: {
                                    value: 10,
                                    message: "Phone number not longer then 10 digit"
                                },
                            })}
                    />
                    {errors.phone_number && <span className="error">{errors.phone_number?.message}</span>}
                </div>
                <div className="cstm-btns-sec">
                    <button type="submit" className="Save-address">Save</button>
                    <button type="button" className="Save-back" onClick={() => {
                        setaddressDetails()
                        Setshowaddress(false)
                    }} >Back</button>
                </div>
            </div>
            {sameAddressmsg && <span className="error deletemsg" style={{ fontSize: "16px", display: "block", width: '100%', textAlign: "center" }}>{sameAddressmsg}</span>}
        </form>
        {/* </div> */}

    </>
}