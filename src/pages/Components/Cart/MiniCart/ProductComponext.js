import NumberFormat from 'react-number-format';
import { AiFillDelete } from 'react-icons/ai';
import { IconButton } from '@material-ui/core';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import _ from 'lodash';

const Product = ({ smartShipCheck, minCartAutoshipCheck, Cart, deleteproduct, index, register, Add, Sub, updateqty, setcosttype, costtype, swithtoautoship }) => {
  //check product is selected to autoship or normal

  return (
    <>
      <div className="mini-cart" key={index}>
        <div className="min-cat-uppper">

          <div className="cart-image">
            {Cart?.product?.product_images[0]?.image
              ?
              <img src={`${process.env.API_URL}${Cart?.product.product_images[0]?.image}`} />
              :
              <img src="/images/no-image.jpg" />
            }
          </div>
          <div className="mini-cart-des">
            <div className="cart-product-details">
              {Cart?.product?.name}
            </div>

            <div className="main-qty-sec">
              <div className="box">
                {Cart?.qty_display &&
                  <div id="qty">
                    <button type="button"
                      name={`${Cart?.product?.id},${null}`}
                      disabled={Cart?.quantity === "1"}
                      id={Cart?.product?.id}
                      className="sub"
                      value={Cart?.quantity}
                      onClick={(e) => { Sub(e, Cart?.id) }}>-</button>
                    <input
                      name={`${Cart?.product?.id},${null}`}
                      type="text"
                      readOnly
                      ref={register &&
                        register({
                          valueAsNumber: true,
                        })
                      }
                      value={+(updateqty?.id) === +(Cart?.product?.id) ? +(updateqty.value) : Cart?.quantity}
                    />
                    <button type="button"
                      name={`${Cart?.product?.id},${null}`}
                      id={Cart?.product?.id} data-value={Cart?.product?.quantity}
                      className="add"
                      value={Cart?.quantity}
                      onClick={(e) => { Add(e, Cart?.id) }}>+</button>
                  </div>
                }
              </div>
            </div>

            <div className="cart-product-details">
              <p className="title">
                <NumberFormat value={`${parseFloat(Cart?.product?.cost_price * Cart?.quantity).toFixed(2)}`}
                  displayType={'text'}
                  thousandSeparator={true}
                  prefix={'$'}
                  renderText={value => <div>{
                    // (Cart?.is_autoship !== 'True' && minCartAutoshipCheck !== "True" && )
                    (minCartAutoshipCheck !== "True" && smartShipCheck !== true) ? value + " " + "USD" :
                      `$${parseFloat(Cart?.product?.[`${(minCartAutoshipCheck === "True" || smartShipCheck) ? "autoship_cost_price" : "cost_price"}`] * Cart?.quantity).toFixed(2)} ` + " " + "USD" + (Cart?.is_autoship === "True" ? ' / $' + parseFloat(Cart?.product?.autoship_cost_price * Cart?.quantity).toFixed(2) + " " + "USD" : "") + " " + (Cart?.is_autoship === "True" ? 'SmartShip' : "")}</div>} />
              </p>
            </div>

            <div className="cart-product-details">
              <div className="box">
                {(Cart?.product?.product_type === "Product" || Cart?.product?.product_type === "Bogo") &&
                  <div className="select">
                    <select className="form-select form-select-lg mb-3" aria-label=".form-select-lg example"
                      value={Cart?.is_autoship === 'True' ? "AutoShip" : "Normal"}
                      data-product-id={Cart?.product?.id}
                      data-product-qty={Cart?.quantity}
                      data-variant-id={null}
                      onChange={(e) => {
                        const found = costtype.data.some(el => el.id === Cart?.product?.id);
                        if (!found) {
                          setcosttype({ ...costtype, data: [...costtype.data, { "id": Cart?.product?.id, "cartId": Cart?.id, "value": e.target.value, "variant_id": null }] })

                        }
                        else {
                          const objIndex = costtype.data.findIndex((obj => obj.id == Cart?.product?.id));
                          costtype.data.splice(objIndex, 1);

                          setcosttype({ ...costtype, data: [...costtype.data, { "id": Cart?.product?.id, "cartId": Cart?.id, "value": e.target.value, "variant_id": null }] })
                        }
                        swithtoautoship(e, Cart?.id)

                      }}
                    >
                      <option value="Normal" >One-time Only</option>
                      <option value="AutoShip">SmartShip</option>
                      {/* <option value="AutoShip">AutoShip</option> */}

                    </select>
                  </div>
                }
              </div>
            </div>


          </div>
        </div>

        <div className="cart-product-qty-del">
          <button><DeleteOutlineIcon id={Cart?.product?.id} data-value={Cart?.variant?.id} onClick={(e) => { deleteproduct(e, Cart?.id, Cart?.is_autoship) }} className="dlt" /></button>
        </div>

      </div >
    </>)
}
export default Product;