import React, { useEffect, useState } from "react";
import api from "@api/Apis";
import { MailOutlineTwoTone, Call } from '@material-ui/icons';
import { MailComponent, TelComponent } from "@PagesComponent/Reports/CommonComponent";
import _ from "lodash";

const useReportPageHook = (props) => {

    const [leftSideData, setLeftSideData] = useState([]);
    const [rightSideData, setRightSideData] = useState([]);
    const [customer, setCustomer] = useState(null);
    const [firstOrderBonus, setFirstOrderBonus] = useState(null);
    const [firstOrderBonusMatch, setFirstOrderBonusMatch] = useState(null);
    const [retailCustomerBonus, setRetailCustomerBonus] = useState(null);
    const [unilevelCommission, setUnilevelCommission] = useState(null);
    const [unilevelCommissionMatch, setUnilevelCommissionMatch] = useState(null);
    const [rankData, setrankData] = useState([])

    useEffect(() => {
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        if (token) {
            getKairePageDetails(token);
            getKaireUsersFunction(token);
        }
    }, []);


    //Active and Inactive customer structure
    let activeCustomer = [{ customers: "Active Customers", orderValue: "", runDate: "", actions: " " }];
    let inActiveCustomer = [{ customers: "Inactive Customers", orderValue: "", runDate: "", actions: " " }];

    function ActiveInactiveCustomer(data, key) {
        data[key]?.forEach(({ public_first_name, public_email, public_phone_number, order_amount, next_shipping_date }, index) => {
            if (key === "active_users") {
                activeCustomer.push({
                    customers: public_first_name || "N/A",
                    orderValue: `$${order_amount}` || "N/A",
                    runDate: next_shipping_date || "N/A",
                    actions: (
                        <>
                            <MailComponent email={public_email}><MailOutlineTwoTone /></MailComponent>
                            <TelComponent phone={public_phone_number}><Call /></TelComponent>
                        </>)
                });
            } else {
                inActiveCustomer.push({
                    customers: public_first_name,
                    orderValue: order_amount || "N/A",
                    runDate: next_shipping_date || "N/A",
                    actions: (
                        <>
                            <MailComponent email={public_email}><MailOutlineTwoTone /></MailComponent>
                            <TelComponent phone={public_phone_number}><Call /></TelComponent>
                        </>)
                });
            }
        })
        if (key === "active_users") {

            setCustomer({ title: data?.kaire_cash_heading, data: activeCustomer });
        } else {
            setCustomer({ title: data?.kaire_cash_heading, data: [...activeCustomer, ...inActiveCustomer] })
        }
    }

    const changeKeyFunction = (keyValue, dataValue, onSetValue) => {
        const values = _.map(dataValue?.table_info, (row) => {
            return ({
                Level: `${row?.level}`, [keyValue]: row['percent'] + '%',
            });
        });
        onSetValue({ title: dataValue?.table_heading, data: values });
    }


    const getKaireUsersFunction = async (token) => {
        await api.getKaireUsers(token).then((res) => {
            if (res?.data?.code === 1) {
                setRightSideData(res?.data);
                ["active_users", "inactive_users"].forEach((value) => ActiveInactiveCustomer(res?.data, value));
            }
        }).catch((err) => console.log(err))
    }

    const getKairePageDetails = async (token) => {
        await api.getKairePageDetails(token).then((res) => {
            if (res?.data?.code === 1) {
                changeKeyFunction("firstOrder", res?.data?.commission_percentages?.first_order_bonus, setFirstOrderBonus);
                changeKeyFunction("firstOrderMatch", res?.data?.commission_percentages?.first_order_bonus_match, setFirstOrderBonusMatch);
                changeKeyFunction("retailBonus", res?.data?.commission_percentages?.retail_customer_bonus, setRetailCustomerBonus);
                setUnilevelCommission({
                    title: res?.data?.commission_percentages?.unilevel_commission?.table_heading,
                    data: res?.data?.commission_percentages?.unilevel_commission?.table_info
                });
                setUnilevelCommissionMatch({
                    title: res?.data?.commission_percentages?.unilevel_commission_match?.table_heading,
                    data: res?.data?.commission_percentages?.unilevel_commission_match?.table_info
                })
                // changeResultFunction(res?.data?.commission_percentages?.unilevel_commission, setUnilevelCommission, res?.data?.ranks);
                setLeftSideData(res?.data?.user_data?.userdetails?.[0]);
                console.log("res?.data", res?.data)
                setrankData(res?.data?.ranks)
            }
        }).catch((err) => console.log(err))
    }


    return [
        leftSideData,
        rankData,
        rightSideData,
        customer,
        firstOrderBonus,
        firstOrderBonusMatch,
        retailCustomerBonus,
        unilevelCommission,
        unilevelCommissionMatch
    ]

}
export default useReportPageHook;


