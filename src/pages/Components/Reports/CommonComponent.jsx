import { PieChart } from 'react-minimal-pie-chart';
import { Check, Clear } from '@material-ui/icons';
import { Tooltip } from '@material-ui/core';
import parse from 'html-react-parser';
import { CircularProgressbar } from 'react-circular-progressbar';
// title with checked 
const CheckedSection = (props) => {
    const { title, checked, status, pieData, pieChart } = props;
    return (
        <>
            <div className={`${title?.toLowerCase()}_section`}>
                <span>
                    {checked ?
                        <Check style={{ color: "green" }} />
                        :
                        <Clear style={{ color: "red" }} />
                    }
                </span>
                <span>{title}</span>
                <span>{status}</span>
                {
                    pieChart === true &&
                    <Tooltip classes={{ popper: "pie_cahrt_pop" }} arrow title={
                        <div className="dataContainer">
                            <>
                                <p>{(pieData?.data >= pieData?.totalValue) ? "100 %" : `${parseFloat(pieData?.data * 100 / pieData?.totalValue).toFixed(2)}%`}</p>
                                <div className="achieved_section tooltip_section">
                                    <p className="text">{`${pieData?.data} Order and ${pieData?.data} New customer `}</p>
                                </div>
                                <br /><br />
                            </>
                            {/* <p> Detail will be visible on mouseover</p> */}
                        </div>
                    } >

                        <div className="report-svg">
                            {pieData !== undefined &&
                                <CircularProgressbar
                                    data={[{ title: "", value: pieData?.data, color: '#E38627' }]}
                                    totalValue={(pieData?.totalValue) ? pieData?.totalValue : 2}
                                    background="gray"
                                    labelStyle={{ display: "none" }}
                                    viewBoxSize={[100, 100]}
                                />
                            }
                        </div>
                    </Tooltip>
                }
            </div>

        </>
    )
}


// telephone link section
export const TelComponent = ({ children, phone }) => {
    return <a href={`tel:${phone}`}>{children}</a>
}

// mail link section
export const MailComponent = ({ children, email }) => (
    <a href={`mailto:${email}`}>{children}</a>
)

//table dynmic section
export const SectionContainer = ({ children, ...props }) => {
    const { section, paidRankBold, firstTitle, secondTitle, leftData, rightContent, tableData, pieData } = props;

    let theadKey = Object.keys(tableData?.thead)?.length > 0 ? Object.keys(tableData?.thead) : [];
    return (
        <div className={`${section}_conatiner common_section_class`}>
            <div className="row heading_row">
                <div className="col-md-6">{firstTitle}</div>
                <div className="col-md-6">{secondTitle}</div>
            </div>
            <div className="row report_all_in_one">
                <div className="col-md-6 left-side-contents">
                    {leftData?.map(({ name, checked, pieChart }, index) => (
                        <CheckedSection title={name} checked={checked} status={checked ? "Yes" : "No"} key={index + 1} pieData={pieData} pieChart={pieChart} />
                    ))}
                </div>
                <div className="col-md-6 right-side-contents">
                    <>
                        {/* {rightContent?.map((value, index) => (
                            <p key={index + 1}>{value}</p>
                        ))} */}
                        {parse(String(rightContent))}
                        <div className="table-wrap">
                            <table>
                                <thead>
                                    <tr>
                                        {theadKey?.map((value, index) => (
                                            <th key={index + 1}>{(paidRankBold === tableData?.thead[value]) ? <strong>{tableData?.thead[value]}</strong> : tableData?.thead[value]}</th>
                                        ))}
                                    </tr>
                                </thead>
                                <tbody>
                                    {tableData?.tbody?.map((data, index) => (
                                        <tr key={index + 1}>
                                            {theadKey?.map((value, i) => (
                                                <td key={index + i + 1} className={String(data[value])?.match(/Active Customers|InActive Customers/g) ? "active_bhoot" : "non_active"}>
                                                    {(paidRankBold === tableData?.thead[value]) ?
                                                        <strong>{data[value]}</strong>
                                                        : data[value]
                                                    }
                                                </td>


                                            ))}
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </>
                </div>
            </div>
        </div >
    )
}
export default CheckedSection;