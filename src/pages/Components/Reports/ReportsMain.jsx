import React, { useEffect } from "react";
import CountDownPeriod from "@Components/Common/CountDownPeriod";
import moment from "moment";
import { SectionContainer } from "@PagesComponent/Reports/CommonComponent";
import ReportPageStyle from "src/pages/Components/Reports/Reports.style";
import useReportPageHook from "./ReportMain.hook";
import _ from 'lodash';

export default function ReportsMain(props) {
    const [leftSideData, rankData, rightSideData, customer, firstOrderBonus, firstOrderBonusMatch, retailCustomerBonus, unilevelCommission,
        unilevelCommissionMatch
    ] = useReportPageHook(props);


    useEffect(() => {
        document.body.classList.add('dashboard');
    }, [])
    return (
        <ReportPageStyle>
            <div className="table_container ">
                <h3>Current Period Reports</h3>
                <div className="address-list">
                    <div className="week_sections">
                        <div className="week_sections_main">
                            <h5>Weekly Rewards</h5>
                            <CountDownPeriod deadline={moment().add(1, 'weeks').startOf("week")} />
                        </div>
                        <SectionContainer section="weekly"
                            firstTitle="Unilevel Commission"
                            paidRankBold={_.find(rankData, { id: +leftSideData?.paid_as_rank }) ? _.find(rankData, { id: +leftSideData?.paid_as_rank })?.rank_name : ''}
                            secondTitle={`Estimated Earnings: $${parseFloat(rightSideData?.weekly_commission?.commission__sum).toFixed(2)}`}
                            leftData={[
                                { name: "Membership", checked: leftSideData?.membership === "True" ? true : false, pieChart: false },
                                { name: "Active SmartShip", checked: leftSideData?.is_autoship_user === "True" ? true : false, pieChart: false },
                                { name: "Rank as Member or above", checked: leftSideData?.paid_as_rank >= 3 ? true : false, pieChart: false }
                            ]}
                            rightContent={unilevelCommission?.title}
                            tableData={
                                {
                                    thead: _.zipObject(_.keys(unilevelCommission?.data?.[0]), _.keys(unilevelCommission?.data?.[0])),
                                    tbody: unilevelCommission ? unilevelCommission?.data : []
                                }
                            }
                        />
                        <SectionContainer section="weekly"
                            firstTitle="Unilevel Commission Match"
                            paidRankBold={_.find(rankData, { id: +leftSideData?.paid_as_rank }) ? _.find(rankData, { id: +leftSideData?.paid_as_rank })?.rank_name : ''}
                            secondTitle={`Estimated Earnings: $${parseFloat(rightSideData?.weekly_commission?.commission__sum).toFixed(2)}`}
                            leftData={[
                                { name: "Membership", checked: leftSideData?.membership === "True" ? true : false, pieChart: false },
                                { name: "Active SmartShip", checked: leftSideData?.is_autoship_user === "True" ? true : false, pieChart: false },
                                { name: "Rank as Member or above", checked: leftSideData?.paid_as_rank >= 3 ? true : false, pieChart: false }
                            ]}
                            rightContent={unilevelCommissionMatch?.title}
                            tableData={
                                {
                                    thead: _.zipObject(_.keys(unilevelCommission?.data?.[0]), _.keys(unilevelCommission?.data?.[0])),
                                    tbody: unilevelCommissionMatch ? unilevelCommissionMatch?.data : []
                                }
                            }
                        />
                        <SectionContainer section="weekly"
                            firstTitle="First Order Bonus"
                            secondTitle={`Estimated Earnings: $${parseFloat(rightSideData?.month_commission?.commission__sum).toFixed(2)}`}
                            leftData={[
                                { name: "Bought a Member pack", checked: leftSideData?.is_promoter_pack, pieChart: true },
                                { name: "Active Autoship", checked: leftSideData?.is_autoship_user === "True" ? true : false, pieChart: false },
                                { name: "Rank as Member or above", checked: leftSideData?.paid_as_rank >= 3, pieChart: false }
                            ]}
                            rightContent={firstOrderBonus?.title}
                            tableData={
                                {
                                    thead: { Level: "Level", firstOrder: "First Order Bonus" },
                                    tbody: firstOrderBonus ? firstOrderBonus?.data : []
                                }
                            }
                        />
                        <SectionContainer section="weekly"
                            firstTitle="First Order Bonus Match"
                            secondTitle={`Estimated Earnings: $${parseFloat(rightSideData?.month_commission?.commission__sum).toFixed(2)}`}
                            leftData={[
                                { name: "Bought a Member pack", checked: leftSideData?.is_promoter_pack, pieChart: false },
                                { name: "Active Autoship", checked: leftSideData?.is_autoship_user === "True" ? true : false, pieChart: false },
                                { name: "Rank as Member or above", checked: leftSideData?.paid_as_rank >= 3, pieChart: false },
                                { name: "2 Member in downline", checked: leftSideData?.my_sponsered_promoters >= 2, pieChart: true }
                            ]}
                            rightContent={firstOrderBonusMatch?.title}
                            tableData={
                                {
                                    thead: { Level: "Level", firstOrderMatch: "First Order Bonus Match" },
                                    tbody: firstOrderBonusMatch ? firstOrderBonusMatch?.data : []
                                }
                            }
                            pieData={{ totalValue: 2, data: leftSideData?.my_sponsered_promoters }}
                        />
                        <SectionContainer section="weekly"
                            firstTitle="Retail Customer Bonus"
                            secondTitle={`Estimated Earnings: $${parseFloat(rightSideData?.month_commission?.commission__sum).toFixed(2)}`}
                            leftData={[
                                { name: "Membership", checked: leftSideData?.membership === "True" ? true : false, pieChart: false },
                                { name: "Active Autoship", checked: leftSideData?.is_autoship_user === "True" ? true : false, pieChart: false },
                                { name: "Rank as Member", checked: leftSideData?.paid_as_rank >= 3, pieChart: false },
                            ]}
                            rightContent={retailCustomerBonus?.title}
                            tableData={
                                {
                                    thead: { Level: "Level", retailBonus: "Member" },
                                    tbody: retailCustomerBonus ? retailCustomerBonus?.data : []
                                }
                            }
                        />
                    </div>
                    <div className="month_kaire_sections">
                        <div>

                            <div className="week_sections_main">
                                <h5>Monthly Rewards</h5>
                                <CountDownPeriod deadline={moment().add(1, 'months').startOf("month")} />
                            </div>
                            <SectionContainer section="kaire"
                                firstTitle="Kaire Cash"
                                secondTitle={`Estimated Earnings: $${parseFloat(rightSideData?.month_commission?.commission__sum).toFixed(2)}`}
                                leftData={[
                                    // { name: "Membership", checked: leftSideData?.membership === "True" ? true : false, pieChart: false },
                                    { name: "Active SmartShip", checked: leftSideData?.is_autoship_user === "True" ? true : false, pieChart: false },
                                    { name: "2 New Customers on Autoship", checked: rightSideData?.new_customers_in_autoship >= 2 ? true : false, pieChart: true }
                                ]}
                                rightContent={customer?.title}
                                tableData={
                                    {
                                        thead: { customers: "Customers", orderValue: "Order Value", runDate: "Run Date", actions: "actions" },
                                        tbody: customer ? customer?.data : []
                                    }
                                }
                                pieData={{ totalValue: 2, data: rightSideData?.new_customers_in_autoship }}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </ReportPageStyle>
    );
}

