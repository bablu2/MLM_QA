import styled from "styled-components";

const ReportPageStyle = styled.div`
  && h2 {
    font-size: 25px;
  }
  .main_class_responsive {
    .saved_card {
      max-width: 70%;
      flex: 0 0 70%;
    }
  }

  .address-list {
    border: unset;
    margin: 16px 24px;
    .common_section_class {
      border: 1px solid #dddddd;
      margin-top: 16px;
    }
    .month_kaire_sections {
      .common_section_class {
        margin-top: 10px;
      }
    }
    .right-side-contents {
      p {
        font-size: 14px;
        white-space: inherit;
        line-height: normal;
        &:not(:last-child) {
          margin-bottom: 10px;
        }
      }
      tr td.active_bhoot {
        font-size: 14px;
        font-weight: 600;
        text-transform: capitalize;
      }
    }
    b, strong {
    font-weight: 900;
    color: #000;
}
    .heading_row {
      margin: 10px auto 10px 0px !important;
      border-bottom: 1px solid #dddd;
      padding: 0px !important;
    }
    .report_card_table {
      width: 100%;
      tr {
        td {
          text-align: center;
          .edit_delete_option {
            font-size: 23px;
            color: red;
          }
        }
      }
    }
  }
  // -----21-oct.-2021-----
  /* .main_reports_page_section_class .heading-top-main.right-address {
        box-shadow: none;
    }
    .main_reports_page_section_class .heading-top-main h1{
        text-align: left !important;
        display: block;
    } */
  .time-container {
    display: flex;
    font-size: 20px;
  }
  .time_period_main_div {
    display: flex;
    align-items: center;
    justify-content: end;
  }
  .Clock-minutes {
    margin: 0 10px;
  }
  .Clock-days {
    margin: 0px 10px;
  }
  .time_period_main_div h3 {
    font-size: 20px;
    margin: 0;
    font-family: var(--common-font);
  }
  .weekly_containers {
    display: flex;
  }
  .weekly_containers h5 {
    font-size: 20px;
    max-width: 400px;
    width: 100%;
  }
  .address-list th {
    font-size: 14px;
    font-weight: 600;
  }
  td {
    font-size: 14px;
  }
  .heading_row .col-md-6 {
    color: #fff;
    font-family: var(--common-font-bd);
    font-size: 14px;
  }
  .address-list .heading_row {
    padding: 10px !important;
  }
  .row.heading_row {
    background: #00356a !important;
    margin: 0 !important;
    padding: 10px !important;
  }
  .row.heading_row .col-md-6:last-child {
    text-align: end;
  }
  .report_all_in_one table {
    background-color: transparent;
    margin-left: auto;
    border: 1px solid #00356a;
    width: 100%;
  }
  .report_all_in_one .right-side-contents {
    text-align: right;
  }
  .report_all_in_one {
    padding: 20px;
  }
  .address-list th:last-child {
    border-top-right-radius: 0;
  }
  .address-list th:first-child {
    border-top-left-radius: 0;
  }
  .right-side-contents p {
    text-align: left;
  }
  .new.customer_section {
    display: flex;
    align-items: center;
  }

  .address-list tr td {
    text-align: center;
  }
  .report-svg svg {
    width: 20px;
    margin-left: 20px;
  }
  .week_sections_main h5 {
    width: 400px;
    font-size: 20px;
    margin: 0;
  }
  .week_sections_main {
    justify-content: space-between;
    display: flex;
    margin: 20px 0px;
    padding: 0 15px;
    align-items: center;
  }
  .active.autoship_section {
    margin: 11px 0px;
  }
  .left-side-contents svg {
    margin-right: 10px;
  }
  .address-list tr td {
    padding: 10px;
  }
  .table_container {
    margin: 0px 20px;
  }
  .main_class_responsive.container.order-detail-page {
    padding: 0 !important;
  }
  .address-list {
    margin: 16px 0px;
  }
  .right-side-contents tr:nth-child(odd) {
    background: #eeeeee;
  }
  .left-side-contents span:nth-child(2) {
    padding-right: 25px;
    max-width: 230px;
    width: 100%;
    display: inline-block;
  }
  .active.autoship_section {
    display: flex;
    align-items: center;
  }
  .active.autoship_section span {
    line-height: normal;
    height: 20px;
  }
  .customers.on.autoship_section,
  .downline_section {
    position: relative;
  }
  .customers.on.autoship_section .report-svg,
  .downline_section .report-svg {
    position: absolute;
    top: 0;
    left: 270px;
  }
  .downline_section .report-svg {
    left: auto;
    right: 30px;
  }

  /* 26-04-2022 */
  .row.report_all_in_one {
    .active.autoship_section {
      margin: 0;
      span {
        height: auto;
      }
      .report-svg {
        svg {
          width: 17px;
        }
      }
    }
  }
  .main-table {
    height: 265px;
    overflow-y: auto;
    thead {
      position: sticky;
      top: 0;
    }
  }

  // media query
  @media (max-width: 1400px) {
    .dashboard_main_div.main_class_responsive .container {
      width: 100% !important;
      max-width: 100% !important;
    }
  }
  @media (max-width: 1199px) {
    .week_sections_main h5 {
      width: 300px;
    }
    .week_sections_main {
      display: flex;
    }
    .time_period_main_div {
      justify-content: flex-start;
    }
  }
  @media (max-width: 991px) {
    .week_sections_main h5 {
      width: 180px;
      font-size: 14px;
    }
    .time_period_main_div h3 {
      font-size: 14px;
    }
    .time-container {
      font-size: 14px;
    }
    .report_all_in_one .right-side-contents {
      text-align: right;
      margin-bottom: 20px;
    }
    .address-list th {
      font-size: 12px;
      padding: 10px;
    }
    .mainorder-detail-sec .container.order-detail-page .row,
    .row.heading_row {
      display: flex;
    }
    .right-side-contents {
      margin-top: 10px;
    }
  }

  @media (max-width: 767px) {
    .time_period_main_div h3 {
      font-size: 12px;
    }
    .time-container {
      font-size: 12px;
    }
    .week_sections_main h5 {
      width: 180px;
      font-size: 12px;
    }
    .report_all_in_one table {
      width: 150vw;
    }
  }
  @media (max-width: 576px) {
    .week_sections_main {
      display: block;
    }
    .row.heading_row {
      padding: 10px 0px !important;
    }
    .address-list .heading_row .col-md-6 {
      font-size: 12px;
    }
  }
`;

export default ReportPageStyle;
