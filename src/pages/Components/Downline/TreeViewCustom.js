import { FaUser, FaEnvelopeOpen, FaPhoneAlt, FaCalendarCheck, FaTags, FaStore } from "react-icons/fa";
import { useEffect, useState } from 'react';
import NumberFormat from "react-number-format";
// import Moment from "react-moment";
import moment from "moment"
import { index } from "d3-array";
import Hover from "./hovercomponent";
import Downline from "src/pages/[page]/user/downline";

const TreeViewCustom = ({ Downline_data }) => {
    const [shownode, setShownode] = useState(false)
    const displaynode = (Downline_data_display) => {
        return <div className="box">

            {/* <Hover
                 data={ `
                 <div>
                 
                     <div className="user_rank">
                         <h3>${Downline_data_display?.attributes?.user_rank}</h3>
                     </div>
                     <div className="icons">
                         ${Downline_data_display?.attributes?.show_email === "True" && <FaEnvelopeOpen />}
                         ${Downline_data_display?.attributes?.show_phone_no === "True" && <FaPhoneAlt />}
                     </div>
                     <div className="data"> <FaCalendarCheck />Date Joined : <strong>
                         ${moment(Downline_data_display?.attributes?.date_joined).format(`${process.env.Date_Format}`)}
     
                     </strong></div>
                     <div className="data"><FaStore />Total Order : <strong>${Downline_data_display?.attributes?.orders_count}</strong></div>
                     <div className="data"><FaTags />Total Sales : <strong>
                         ${+Downline_data_display?.attributes?.total_sales > 0 ?
                             <NumberFormat
                                 value={parseFloat(+Downline_data_display?.attributes?.total_sales).toFixed(2)}
                                 displayType={'text'} thousandSeparator={true} prefix={'$'}
                                 renderText={value => <div> {value} </div>} />
                             :
                             <NumberFormat
                                 value={parseFloat(0).toFixed(2)}
                                 displayType={'text'} thousandSeparator={true} prefix={'$'}
                                 renderText={value => <div> {value} </div>} />
                         }
     
                     </strong></div>
                 </div>`}
            image={Downline_data_display?.attributes?.user_profile?.image ?
                <img
                    src={`${process.env.DOC_URL}/media/${Downline_data_display?.attributes?.user_profile?.image}`} alt="" width="100" />
                :
                <FaUser />}
            /> */}

            <div className="default_info">
                <div className="profile-img-d">
                    {Downline_data_display?.attributes?.user_profile?.image ?
                        <>
                            <img
                                src={`${process.env.DOC_URL}/media/${Downline_data_display?.attributes?.user_profile?.image}`} alt="" width="100" />
                            {Downline_data_display?.attributes?.user_rank === "Customer" &&
                                <div className="customer-rank">
                                    <span>C</span>
                                </div>
                            }
                            {Downline_data_display?.attributes?.user_rank === "Member" &&
                                <div className="Member-rank">
                                    <span>M</span>
                                </div>
                            }
                        </>
                        :
                        <FaUser />

                    }

                </div>
                <div className={`first_name ${Downline_data_display?.sectionClass ? Downline_data_display?.sectionClass : ""}`}>
                    <h3>{Downline_data_display?.name}</h3>
                </div>
                <div className="usercount-d">
                    <span className="count-val">{Downline_data_display?.count ? Downline_data_display?.count : 0}</span>
                </div>
            </div>



            {/* <div className="first_name">
                <h3>{Downline_data_display?.name} ({Downline_data_display?.count ? Downline_data_display?.count : 0})</h3>
            </div> */}
            <div className="show_on_hover">
                <div className="tree_top_details">
                    <div className="user_rank">
                        <h3>{Downline_data_display?.attributes?.user_rank}</h3>
                    </div>
                    <div className="icons">
                        {Downline_data_display?.attributes?.show_email === "True" && <a href={`mailto:${Downline_data_display?.attributes?.email}`}><FaEnvelopeOpen /></a>}
                        {Downline_data_display?.attributes?.show_phone_no === "True" && <a href={`tel:${Downline_data_display?.attributes?.public_phone_no}`}><FaPhoneAlt /></a>}
                    </div>
                </div>
                <div className="data"> <FaCalendarCheck />Date Joined:&nbsp; <strong>
                    {moment(Downline_data_display?.attributes?.date_joined, "ddd MMM DD YYYY HH:mm:ssZ").format(`${process.env.Date_Format}`)}

                </strong></div>
                <div className="data"><FaStore />Personal Orders:&nbsp; <strong>{Downline_data_display?.attributes?.orders_count}</strong></div>
                <div className="data"><FaTags />Total Sales:&nbsp; <strong>
                    {+Downline_data_display?.attributes?.total_sales > 0 ?
                        <NumberFormat
                            value={parseFloat(+Downline_data_display?.attributes?.total_sales).toFixed(2)}
                            displayType={'text'} thousandSeparator={true} prefix={'$'}
                            renderText={value => <div> {value} </div>} />
                        :
                        <NumberFormat
                            value={parseFloat(0).toFixed(2)}
                            displayType={'text'} thousandSeparator={true} prefix={'$'}
                            renderText={value => <div> {value} </div>} />
                    }

                </strong></div>
            </div>
        </div>
    }

    const AddclassData = (e, level) => {
        if (level === 0) {
            let node = e.currentTarget?.parentNode?.querySelectorAll('.level0');
            let data_var = node['0']?.className.split(' ')
            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level0')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level0')?.classList?.add("hide")
            }
        }
        if (level === 1) {
            let node = e.currentTarget?.parentNode?.querySelectorAll('.level1');
            let data_var = node['0']?.className.split(' ')
            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level1')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level1')?.classList?.add("hide")
            }
        }
        else if (level === 2) {

            let node = e.currentTarget?.parentNode?.querySelectorAll('.level2');
            let data_var = node['0']?.className.split(' ')
            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level2')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level2')?.classList?.add("hide")
            }
        }
        else if (level === 3) {

            let node = e.currentTarget?.parentNode?.querySelectorAll('.level3');
            let data_var = node['0']?.className.split(' ')
            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level3')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level3')?.classList?.add("hide")
            }
        }
        else if (level === 4) {
            let node = e.currentTarget?.parentNode?.querySelectorAll('.level4');
            let data_var = node['0']?.className.split(' ')
            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level4')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level4')?.classList?.add("hide")
            }
        }
        else if (level === 5) {
            let node = e.currentTarget?.parentNode?.querySelectorAll('.level5');
            let data_var = node['0']?.className.split(' ')
            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level5')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level5')?.classList?.add("hide")
            }
        }
    }
    return (
        <>
            <div className="tree">
                <ul className="main-lv-0">
                    <li>
                        <div className="boxshape" onClick={(e) => {
                            AddclassData(e, 0)
                        }}>
                            {/* {Downline_data?.attributes.email} */}
                            {displaynode(Downline_data)}
                        </div>
                        {Downline_data?.children?.length > 0 &&
                            <ul className="showing-node level0 main-lv-1 hide">
                                {Downline_data?.children?.map((childData) => {
                                    return (
                                        <>
                                            <li >
                                                <div className="boxshape" onClick={(e) => {
                                                    AddclassData(e, 1)
                                                }}>
                                                    {displaynode(childData)}

                                                </div>
                                                {childData?.children?.length > 0 &&
                                                    <ul className="showing-node level1 main-lv-2"  >
                                                        {childData?.children?.map((childData1) => {
                                                            return (<li >
                                                                <div className="boxshape" onClick={(e) => {
                                                                    AddclassData(e, 2)
                                                                }}>

                                                                    {displaynode(childData1)}

                                                                </div>

                                                                {childData1?.children?.length > 0 &&

                                                                    <ul className="showing-node level2 main-lv-3" >
                                                                        {childData1?.children?.map((childData2) => {
                                                                            return (<li >
                                                                                <div className="boxshape" onClick={(e) => {
                                                                                    AddclassData(e, 3)
                                                                                }}>
                                                                                    {displaynode(childData2)}

                                                                                </div>

                                                                                {childData2?.children?.length > 0 &&

                                                                                    <ul className="showing-node level3 main-lv-4">
                                                                                        {childData2?.children?.map((childData3) => {
                                                                                            return (<li onClick={(e) => {
                                                                                                AddclassData(e, 4)
                                                                                            }}>
                                                                                                <div className="boxshape">
                                                                                                    {displaynode(childData3)}

                                                                                                </div>
                                                                                                {childData3?.children?.length > 0 &&

                                                                                                    <ul className="showing-node level4 main-lv-5">
                                                                                                        {childData3?.children?.map((childData4) => {
                                                                                                            return (<li onClick={(e) => {
                                                                                                                AddclassData(e, 5)
                                                                                                            }}>
                                                                                                                <div className="boxshape">
                                                                                                                    {displaynode(childData4)}

                                                                                                                </div>
                                                                                                            </li>)
                                                                                                        })
                                                                                                        }
                                                                                                    </ul>
                                                                                                }

                                                                                            </li>)
                                                                                        })
                                                                                        }
                                                                                    </ul>
                                                                                }
                                                                            </li>)
                                                                        })
                                                                        }
                                                                    </ul>
                                                                }
                                                            </li>)
                                                        })}
                                                    </ul>
                                                }
                                            </li>
                                        </>
                                    )
                                })

                                }




                            </ul>
                        }
                    </li>
                </ul>
            </div>
        </>
    )

}
export default TreeViewCustom;