import styled from "styled-components";

const DownlinePageStyle = styled.div`
  && .downline_custom_style {
    margin-top: 0px;
    /* .Level .box svg:first-child {
        position: unset;
        left: unset;
        top: unset;
    } */
    /* .icon-name {
        width: 126px;
        flex: 126px;
    } */
    .icon-count .name_d0 {
      background: transparent;
      width: 59px;
      justify-content: center;
      display: flex;
    }
    .icon-count {
      & > {
        svg {
          display: none;
        }
      }
      .name_d0 svg {
        position: unset;
        left: unset;
        top: unset;
      }
    }
    .details-list-section {
      height: 165px;
      background: #fff;
      border-top-left-radius: 0px;
      border-top-right-radius: 0px;
      overflow: hidden;
      margin-bottom: 0;
      .hovershow {
        padding: 5px 10px;
        background: #00356a;
      }
    }
    .user_rank h3 {
      margin: 0;
    }
    .hovershow {
      align-items: center;
      justify-content: space-between;
      .icons {
        gap: 10px;
      }
      svg {
        width: 15px;
        color: #fff;
      }
    }
    .user_rank h3 {
      font-size: 15px;
      font-weight: 600;
      color: #fff;
    }
    .icons a {
      color: #000;
    }
    .groups.tabs {
      width: 100%;
      float: left;
      overflow-x: unset;
    }
    .tree {
      width: 100%;
      overflow: auto;
      height: 700px;
    }
    ul.main-lv-0 {
      width: 100vw;
      margin-top: 130px;
    }
    .lavel-box {
      border: transparent;
    }
    .lavel-box {
      .row {
        margin: 0 0px;
      }
      & > .row {
        .col-md-3 {
          max-width: 300px;
          flex: 300px;
          min-width: 300px;
          @media (max-width: 767px) {
            max-width: 100%;
            min-width: 100%;
            width: 100%;
          }
        }
      }
      @media (max-width: 767px) {
        padding: 0;
      }
    }

    span.counts_user-d {
      position: unset;
      right: unset;
      top: unset;
      width: auto;
      padding-top: 0;
      font-size: 22px;
      line-height: 28px;
      color: #06356a;
      font-weight: 700;
    }
    .show_on_hover {
      background: #fff;
    }
  }
  .main_parent {
    h3 {
      color: black;
      font-weight: 600;
    }
  }
  .downline_custom_style {
    .tree {
      > ul.main-lv-0 {
        /* width: 100vw; */
        margin-top: 175px;
        margin-left: 55px;
        li:first-child {
          /* width: calc(100% - 450px); */
          .show_on_hover {
            position: absolute;
            top: -190px;
            left: 50%;
            transform: translateX(-50%);
          }
        }
        svg {
          color: #fff;
        }
        .tree_top_details {
          .icons {
            max-width: 20px;
          }
        }
      }
    }
  }
  @media (max-width: 1199px) {
    .lavel-box .row .col-md-3:first-child {
      margin-bottom: 30px;
    }
  }
`;

export default DownlinePageStyle;
