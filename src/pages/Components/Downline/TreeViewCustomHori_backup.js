import { FaUser, FaEnvelope, FaPhoneAlt } from "react-icons/fa";
import { useEffect, useState } from 'react';
import NumberFormat from "react-number-format";
// import Moment from "react-moment";
import moment from "moment"
import { index } from "d3-array";

const TreeViewCustomHori = ({ Downline_data }) => {
    const [shownode, setShownode] = useState(false)
    const displaynode = (Downline_data_display) => {
        return <div className="box">
            <FaUser />
            <div className="icons">
                {Downline_data_display?.attributes?.show_email === "True" && <FaEnvelope />}
                {Downline_data_display?.attributes?.show_phone_no === "True" && <FaPhoneAlt />}


            </div>

            <div className="first_name">
                <h3>{Downline_data_display?.name} ({Downline_data_display?.count ? Downline_data_display?.count : 0})</h3>
            </div>
            <div className="user_rank">
                <h3>{Downline_data_display?.attributes?.user_rank}</h3>
            </div>
            <div className="data"><span className="date_title">Date Joined:</span><span className="date_data"><strong>
                {moment(Downline_data_display?.attributes?.date_joined, "ddd MMM DD YYYY HH:mm:ssZ").format(`${process.env.Date_Format}`)}

            </strong></span></div>
            <div className="data"><span className="total_order_title">Personal Orders:</span> <span className="total_order_data">
                <strong>{Downline_data_display?.attributes?.orders_count}</strong>
            </span></div>
            <div className="data"><span className="sale_title">Total Sales:</span><span className="sale_data">
                <strong>
                    {+Downline_data_display?.attributes?.total_sales > 0 ?
                        <NumberFormat
                            value={parseFloat(+Downline_data_display?.attributes?.total_sales).toFixed(2)}
                            displayType={'text'} thousandSeparator={true} prefix={'$'}
                            renderText={value => <div> {value} </div>} />
                        :
                        <NumberFormat
                            value={parseFloat(0).toFixed(2)}
                            displayType={'text'} thousandSeparator={true} prefix={'$'}
                            renderText={value => <div> {value} </div>} />
                    }

                </strong>
            </span> </div>

        </div>
    }

    const AddclassData = (e, level) => {
        if (level === 0) {
            let node = e.currentTarget?.parentNode?.querySelectorAll('.level0');
            let data_var = node['0']?.className.split(' ')

            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level0')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level0')?.classList?.add("hide")
            }
        }
        if (level === 1) {
            let node = e.currentTarget?.parentNode?.querySelectorAll('.level1');
            let data_var = node['0']?.className.split(' ')

            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level1')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level1')?.classList?.add("hide")
            }
        }
        else if (level === 2) {

            let node = e.currentTarget?.parentNode?.querySelectorAll('.level2');
            let data_var = node['0']?.className.split(' ')
            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level2')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level2')?.classList?.add("hide")
            }
        }
        else if (level === 3) {

            let node = e.currentTarget?.parentNode?.querySelectorAll('.level3');
            let data_var = node['0']?.className.split(' ')
            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level3')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level3')?.classList?.add("hide")
            }
        }
        else if (level === 4) {
            let node = e.currentTarget?.parentNode?.querySelectorAll('.level4');
            let data_var = node['0']?.className.split(' ')
            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level4')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level4')?.classList?.add("hide")
            }
        }
        else if (level === 5) {
            let node = e.currentTarget?.parentNode?.querySelectorAll('.level5');
            let data_var = node['0']?.className.split(' ')
            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level5')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level5')?.classList?.add("hide")
            }
        }



    }
    return (
        <>


            <div id="wrapper"><span className="label"
                onClick={(e) => {
                    AddclassData(e, 0)
                }}
            > <div className="level">{displaynode(Downline_data)}</div>
            </span>
                {Downline_data?.children?.length > 0 &&
                    <div className="branch lv1 level0">
                        {Downline_data?.children?.map((childData, index) => {
                            return (
                                <div className={`entry level_${index}`}><span className="label"

                                    onClick={(e) => {
                                        AddclassData(e, 1)
                                    }}
                                >{displaynode(childData)}</span>
                                    {childData?.children?.length > 0 &&

                                        <div className="branch lv2 level1">
                                            {childData?.children?.map((childData1, index1) => {
                                                return (<div className={`entry level_${index1 + 8 + index}_1`}>
                                                    <span className="label"
                                                        onClick={(e) => {
                                                            AddclassData(e, 2)
                                                        }}
                                                    >{displaynode(childData1)}</span>
                                                    {childData1?.children?.length > 0 &&

                                                        <div className="branch lv3 level2">
                                                            {childData1?.children?.map((childData2, index2) => {

                                                                return <div className={`entry level_${index2 + 13 + index}_2`}><span className="label"
                                                                    onClick={(e) => {
                                                                        AddclassData(e, 3)
                                                                    }}
                                                                >{displaynode(childData2)}</span>

                                                                    {childData2?.children?.length > 0 &&

                                                                        <div className="branch lv4 level3">
                                                                            {childData2?.children?.map((childData3, index3) => {

                                                                                return <div className={`entry level_${index3 + 30 + index}_3`}><span className="label"
                                                                                    onClick={(e) => {
                                                                                        AddclassData(e, 4)
                                                                                    }}
                                                                                >{displaynode(childData3)}</span>
                                                                                    {childData3?.children?.length > 0 &&

                                                                                        <div className="branch lv4 level4">
                                                                                            {childData3?.children?.map((childData4, index4) => {

                                                                                                return <div className={`entry level_${index4 + 50 + index}_4`}><span className="label">{displaynode(childData4)}</span>



                                                                                                </div>
                                                                                            })}
                                                                                        </div>
                                                                                    }

                                                                                </div>
                                                                            })}
                                                                        </div>
                                                                    }
                                                                </div>

                                                            })}
                                                        </div>




                                                    }

                                                </div>

                                                )
                                            })}



                                        </div>
                                    }
                                </div>
                            )
                        })
                        }
                    </div>


                }
            </div>
        </>
    )

}
export default TreeViewCustomHori;