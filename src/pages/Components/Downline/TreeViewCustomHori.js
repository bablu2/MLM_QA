import { FaUser, FaEnvelopeOpen, FaPhoneAlt, FaCalendarCheck, FaTags, FaStore } from "react-icons/fa";
import { useEffect, useState } from 'react';
import NumberFormat from "react-number-format";
// import Moment from "react-moment";
import moment from "moment"
import { index } from "d3-array";
import { BsArrowsExpand, BsFillPeopleFill } from "react-icons/bs";
import { HiDotsHorizontal } from "react-icons/hi";

const TreeViewCustomHori = ({ Downline_data }) => {
    const [shownode, setShownode] = useState(false)
    const displaynode = (Downline_data_display) => {
        return <>
            <div className="box">
                <div className="groupicon-dd">
                    <div className="box-inner">
                        <div className="defaultshow-down">
                            <div className="icon">
                                {Downline_data_display?.attributes?.user_profile?.image ?
                                    <img
                                        src={`${process.env.DOC_URL}/media/${Downline_data_display?.attributes?.user_profile?.image}`} alt="" width="100" />
                                    :
                                    <FaUser />

                                }
                            </div>
                            <div className="icon-name">
                                <span className="name_d0">{Downline_data_display?.name}</span>
                            </div>
                            <div className="icon-count">
                                <HiDotsHorizontal />
                                <span className="name_d0"><BsFillPeopleFill onClick={(e) => { ShowHideBox(e) }} /> <span className="counts_user-d">{Downline_data_display?.count ? Downline_data_display?.count : 0}</span></span>


                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div className="details-list-section hide">
                <div className="hovershow">
                    <div className="user_rank">
                        <h3>{Downline_data_display?.attributes?.user_rank}</h3>
                    </div>
                    <div className="icons">

                        {Downline_data_display?.attributes?.show_email === "True" && <a href={`mailto:${Downline_data_display?.attributes?.email}`}><FaEnvelopeOpen /></a>}
                        {Downline_data_display?.attributes?.show_phone_no === "True" && <a href={`tel:${Downline_data_display?.attributes?.public_phone_no}`}><FaPhoneAlt /></a>}

                    </div>
                </div>

                <div className="data"><span className="date_title"><FaCalendarCheck />Date Joined: </span><span className="date_data"><strong>
                    {moment(Downline_data_display?.attributes?.date_joined, "ddd MMM DD YYYY HH:mm:ssZ").format(`${process.env.Date_Format}`)}

                </strong></span></div>
                <div className="data"><span className="total_order_title"><FaStore />Personal Orders:</span> <span className="total_order_data">
                    <strong>{Downline_data_display?.attributes?.orders_count}</strong>
                </span></div>
                <div className="data"><span className="sale_title"><FaTags />Total Sales:</span><span className="sale_data">
                    <strong>
                        {+Downline_data_display?.attributes?.total_sales > 0 ?
                            <NumberFormat
                                value={parseFloat(+Downline_data_display?.attributes?.total_sales).toFixed(2)}
                                displayType={'text'} thousandSeparator={true} prefix={'$'}
                                renderText={value => <div> {value} </div>} />
                            :
                            <NumberFormat
                                value={parseFloat(0).toFixed(2)}
                                displayType={'text'} thousandSeparator={true} prefix={'$'}
                                renderText={value => <div> {value} </div>} />
                        }

                    </strong>
                </span>
                </div>
            </div></>

    }

    const AddclassData = (e, level) => {
        if (level === 0) {
            let node = e.currentTarget?.parentNode?.querySelectorAll('.level0');
            let data_var = node['0']?.className.split(' ')

            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level0')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level0')?.classList?.add("hide")
            }
        }
        if (level === 1) {
            let node = e.currentTarget?.parentNode?.querySelectorAll('.level1');
            let data_var = node['0']?.className.split(' ')

            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level1')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level1')?.classList?.add("hide")
            }
        }
        else if (level === 2) {

            let node = e.currentTarget?.parentNode?.querySelectorAll('.level2');
            let data_var = node['0']?.className.split(' ')
            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level2')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level2')?.classList?.add("hide")
            }
        }
        else if (level === 3) {

            let node = e.currentTarget?.parentNode?.querySelectorAll('.level3');
            let data_var = node['0']?.className.split(' ')
            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level3')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level3')?.classList?.add("hide")
            }
        }
        else if (level === 4) {
            let node = e.currentTarget?.parentNode?.querySelectorAll('.level4');
            let data_var = node['0']?.className.split(' ')
            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level4')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level4')?.classList?.add("hide")
            }
        }
        else if (level === 5) {
            let node = e.currentTarget?.parentNode?.querySelectorAll('.level5');
            let data_var = node['0']?.className.split(' ')
            if (data_var?.length === 4) {
                e.currentTarget?.parentNode?.querySelector('.level5')?.classList?.remove("hide")
            }
            else {
                e.currentTarget?.parentNode?.querySelector('.level5')?.classList?.add("hide")
            }
        }



    }

    const ShowHideBox = (e) => {
        e.stopPropagation();

        let node = e.currentTarget?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.querySelectorAll('.details-list-section');
        let data_var = node['0']?.className.split(' ')
        if (data_var.length === 1) {
            e.currentTarget?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.querySelector('.details-list-section')?.classList?.add("hide")

        }
        else {
            e.currentTarget?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.querySelector('.details-list-section')?.classList?.remove("hide")

        }

    }
    return (
        <>
            <div id="wrapper"><span className="label"
                onClick={(e) => {
                    AddclassData(e, 0)
                }}
            > <div className="level">{displaynode(Downline_data)}</div>
            </span>
                {Downline_data?.children?.length > 0 &&
                    <div className="branch lv1 level0">
                        {Downline_data?.children?.map((childData, index) => {
                            return (
                                <div className={`entry ${Downline_data?.children?.length < 2 ? 'sole' : ''} firstlevel`}><span className="label lable-translate"

                                    onClick={(e) => {
                                        AddclassData(e, 1)
                                    }}
                                >{displaynode(childData)}</span>
                                    {childData?.children?.length > 0 &&

                                        <div className="branch lv2 level1 hide">
                                            {childData?.children?.map((childData1, index1) => {
                                                return (<div className={`entry ${childData?.children?.length < 2 ? 'sole' : ''}`}>
                                                    <span className="label lable-translate"
                                                        onClick={(e) => {
                                                            AddclassData(e, 2)
                                                        }}
                                                    >{displaynode(childData1)}</span>
                                                    {childData1?.children?.length > 0 &&

                                                        <div className="branch lv3 level2 hide">
                                                            {childData1?.children?.map((childData2, index2) => {

                                                                return <div className={`entry ${childData1?.children?.length < 2 ? 'sole' : ''}`}><span className="label lable-translate"
                                                                    onClick={(e) => {
                                                                        AddclassData(e, 3)
                                                                    }}
                                                                >{displaynode(childData2)}</span>

                                                                    {childData2?.children?.length > 0 &&

                                                                        <div className="branch lv4 level3 hide">
                                                                            {childData2?.children?.map((childData3, index3) => {

                                                                                return <div className={`entry ${childData2?.children?.length < 2 ? 'sole' : ''}`}><span className="label lable-translate"
                                                                                    onClick={(e) => {
                                                                                        AddclassData(e, 4)
                                                                                    }}
                                                                                >{displaynode(childData3)}</span>
                                                                                    {childData3?.children?.length > 0 &&

                                                                                        <div className="branch lv4 level4 hide">
                                                                                            {childData3?.children?.map((childData4, index4) => {

                                                                                                return <div className={`entry ${childData3?.children?.length < 2 ? 'sole' : ''}`}><span className="label lable-translate">{displaynode(childData4)}</span>



                                                                                                </div>
                                                                                            })}
                                                                                        </div>
                                                                                    }

                                                                                </div>
                                                                            })}
                                                                        </div>
                                                                    }
                                                                </div>

                                                            })}
                                                        </div>




                                                    }

                                                </div>

                                                )
                                            })}



                                        </div>
                                    }
                                </div>
                            )
                        })
                        }
                    </div>


                }
            </div>
        </>
    )

}
export default TreeViewCustomHori;