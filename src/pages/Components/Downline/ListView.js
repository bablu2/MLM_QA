import { FaUser, FaEnvelopeOpen, FaPhoneAlt, FaCalendarCheck, FaTags, FaStore } from "react-icons/fa";
import { useEffect, useState } from 'react';
import NumberFormat from "react-number-format";
// import Moment from "react-moment";
import { index } from "d3-array";
import { BsArrowsExpand, BsFillPeopleFill } from "react-icons/bs";
import { HiDotsHorizontal } from "react-icons/hi";
import DateFormat from "@Components/Common/DateFormet";

const TreeViewCustom = ({ state, setState }) => {
    const [level2, setLevel2] = useState()
    const [level3, setLevel3] = useState()
    const [level4, setLevel4] = useState()
    const [level5, setLevel5] = useState()
    const [level6, setLevel6] = useState()
    const [level7, setLevel7] = useState()
    const [level8, setLevel8] = useState()
    const [level9, setLevel9] = useState()
    const [level10, setLevel10] = useState()
    const [level11, setLevel11] = useState()
    const [level12, setLevel12] = useState()
    const [level13, setLevel13] = useState()
    const [level14, setLevel14] = useState()
    const [level15, setLevel15] = useState()
    const [hide, setHide] = useState(false)

    let count_dats = []

    const convert = (names) => {
        const res = {};
        names?.forEach((obj) => {
            const key = `${obj.parent}`;
            if (!res[key]) {
                res[key] = { ...obj, count: 0 };
            };
            res[key].count += 1;
        });
        return Object.values(res);
    };


    for (let i = 2; i <= 15; i++) {
        const names = state && state?.level2 && state[`level${i}`]

        let result = convert(names)
        result.map((filterdata) => {
            count_dats.push({ "parant_id": filterdata.parent, "count": filterdata.count })
        })
    }

    const ShowNextLevel = (id, level) => {

        if (level === 1) {
            setLevel2()
            setLevel3()
            setLevel4()
            setLevel5()
            setLevel6()
            setLevel7()
            setLevel8()
            setLevel9()
            setLevel10()
            setLevel11()
            setLevel12()
            setLevel13()
            setLevel14()
            setLevel15()
        }
        if (level === 2) {
            setLevel3()
            setLevel4()
            setLevel5()
            setLevel6()
            setLevel7()
            setLevel8()
            setLevel9()
            setLevel10()
            setLevel11()
            setLevel12()
            setLevel13()
            setLevel14()
            setLevel15()
        }
        if (level === 3) {
            setLevel4()
            setLevel5()
            setLevel6()
            setLevel7()
            setLevel8()
            setLevel9()
            setLevel10()
            setLevel11()
            setLevel12()
            setLevel13()
            setLevel14()
            setLevel15()
        }
        if (level === 4) {
            setLevel5()
            setLevel6()
            setLevel7()
            setLevel8()
            setLevel9()
            setLevel10()
            setLevel11()
            setLevel12()
            setLevel13()
            setLevel14()
            setLevel15()
        }
        if (level === 5) {
            setLevel6()
            setLevel7()
            setLevel8()
            setLevel9()
            setLevel10()
            setLevel11()
            setLevel12()
            setLevel13()
            setLevel14()
            setLevel15()
        }
        if (level === 6) {
            setLevel7()
            setLevel8()
            setLevel9()
            setLevel10()
            setLevel11()
            setLevel12()
            setLevel13()
            setLevel14()
            setLevel15()
        }
        if (level === 7) {
            setLevel8()
            setLevel9()
            setLevel10()
            setLevel11()
            setLevel12()
            setLevel13()
            setLevel14()
            setLevel15()
        }
        if (level === 8) {
            setLevel9()
            setLevel10()
            setLevel11()
            setLevel12()
            setLevel13()
            setLevel14()
            setLevel15()
        } if (level === 9) {
            setLevel10()
            setLevel11()
            setLevel12()
            setLevel13()
            setLevel14()
            setLevel15()
        } if (level === 10) {
            setLevel11()
            setLevel12()
            setLevel13()
            setLevel14()
            setLevel15()
        } if (level === 11) {
            setLevel12()
            setLevel13()
            setLevel14()
            setLevel15()
        } if (level === 12) {
            setLevel13()
            setLevel14()
            setLevel15()
        } if (level === 13) {
            setLevel14()
            setLevel15()
        } if (level === 14) {
            setLevel15()
        }

        let setdata = true;
        if (+level === 1) {
            if (level2?.length > 0) {
                const objIndex = level2?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel2()
                }

            }

        }
        if (+level === 2) {
            if (level3?.length > 0) {
                const objIndex = level3?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel3()
                }
            }

        }
        if (+level === 3) {
            if (level4?.length > 0) {
                const objIndex = level4?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel4()
                }
            }

        }
        if (+level === 4) {
            if (level5?.length > 0) {
                const objIndex = level5?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel5()
                }
            }

        }
        if (+level === 5) {
            if (level6?.length > 0) {
                const objIndex = level6?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel6()
                }
            }

        }
        if (+level === 6) {
            if (level7?.length > 0) {
                const objIndex = level7?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel7()
                }
            }

        }
        if (+level === 7) {
            if (level8?.length > 0) {
                const objIndex = level8?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel8()
                }
            }

        }
        if (+level === 8) {
            if (level9?.length > 0) {
                const objIndex = level9?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel9()
                }
            }

        } if (+level === 9) {
            if (level10?.length > 0) {
                const objIndex = level10?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel10()
                }
            }

        } if (+level === 10) {
            if (level11?.length > 0) {
                const objIndex = level11?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel11()
                }
            }

        } if (+level === 11) {
            if (level12?.length > 0) {
                const objIndex = level12?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel12()
                }
            }

        }
        if (+level === 12) {
            if (level13?.length > 0) {
                const objIndex = level13?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel13()
                }
            }
        }
        if (+level === 13) {
            if (level14?.length > 0) {
                const objIndex = level14?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel14()
                }
            }
        } if (+level === 14) {
            if (level15?.length > 0) {
                const objIndex = level15?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel15()
                }
            }
        }

        let nextlevelData = [];

        {
            setdata === true &&

                state[`level${level + 1}`]?.map((downlineList, index) => {
                    if (+id === +downlineList?.parent) {
                        nextlevelData.push(downlineList)


                        if (+level === 1)
                            setLevel2(nextlevelData)
                        if (+level === 2)
                            setLevel3(nextlevelData)
                        if (+level === 3)
                            setLevel4(nextlevelData)
                        if (+level === 4)
                            setLevel5(nextlevelData)
                        if (+level === 5)
                            setLevel6(nextlevelData)
                        if (+level === 6)
                            setLevel7(nextlevelData)
                        if (+level === 7)
                            setLevel8(nextlevelData)
                        if (+level === 8)
                            setLevel9(nextlevelData)
                        if (+level === 9)
                            setLevel10(nextlevelData)
                        if (+level === 10)
                            setLevel11(nextlevelData)
                        if (+level === 11)
                            setLevel12(nextlevelData)
                        if (+level === 12)
                            setLevel13(nextlevelData)
                        if (+level === 13)
                            setLevel14(nextlevelData)
                        if (+level === 14)
                            setLevel15(nextlevelData)


                    }
                })

        }

    }

    const listviewdata = (downlineList, level) => {

        //  const date = Moment(downlineList?.user?.date_joined).format(`${process.env.Date_Format}`);
        const objIndex = count_dats?.findIndex((obj => Number(obj.parant_id) == Number(downlineList?.id)));

        return (
            <div className="row"
                onClick={() => { ShowNextLevel(downlineList?.id, level) }}
            >
                <div className="col-md-6 istviewsection">
                    <div className="box">
                        <div className="groupicon-dd" >

                            <div className="box-inner">
                                <div className="defaultshow-down">
                                    <div className="icon">
                                        {downlineList?.user?.user_profile?.image ?
                                            <img
                                                src={`${process.env.DOC_URL}/media/${downlineList?.user?.user_profile?.image}`} alt="" width="100" />
                                            :
                                            <FaUser />

                                        }
                                    </div>
                                    <div className="icon-name">
                                        <span className="name_d0"> <HiDotsHorizontal onClick={(e) => { ShowHideBox(e) }} /> {downlineList?.user?.first_name} {downlineList?.user?.last_name}</span>
                                    </div>
                                    <div className="icon-count">
                                        <HiDotsHorizontal />
                                        <span className="name_d0"><BsFillPeopleFill onClick={(e) => { (e) }} /> <span className="counts_user-d">{count_dats[objIndex]?.count ? count_dats[objIndex]?.count : 0}</span></span>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="details-list-section  hide">
                        <div className="hovershow">
                            <div className="user_rank">
                                <h3>{downlineList?.user?.user_rank}</h3>
                            </div>
                            <div className="icons" style={{ display: 'flex' }}>
                                {downlineList?.user?.show_email === "True" && <a href={`mailto:${downlineList?.user?.email}`}><FaEnvelopeOpen /></a>}
                                {downlineList?.user?.show_phone_no === "True" && <a href={`tel:${downlineList?.user?.public_phone_no}`}><FaPhoneAlt /></a>}

                            </div>
                        </div>

                        <div className="data"><span className="date_title"><FaCalendarCheck />Date Joined: </span><span className="date_data"><strong>
                            <DateFormat date={downlineList?.user?.date_joined} />

                        </strong></span></div>
                        <div className="data"><span className="date_title"><FaCalendarCheck />Email:</span><span className="date_data">
                            <strong>{downlineList?.user?.email} </strong>
                        </span></div>

                        <div className="data"><span className="total_order_title"><FaStore />Personal Orders:&nbsp; </span>
                            <strong>{downlineList?.user?.orders_count}</strong>
                        </div>
                        <div className="data"><span className="sale_title"><FaTags />Total Sales:&nbsp; </span><span className="sale_data">
                            <strong>
                                {+downlineList?.user?.total_sales > 0 ?
                                    <NumberFormat
                                        value={parseFloat(+downlineList?.user?.total_sales).toFixed(2)}
                                        displayType={'text'} thousandSeparator={true} prefix={'$'}
                                        renderText={value => <div> {value} </div>} />
                                    :
                                    <NumberFormat
                                        value={parseFloat(0).toFixed(2)}
                                        displayType={'text'} thousandSeparator={true} prefix={'$'}
                                        renderText={value => <div> {value}
                                        </div>}
                                    />
                                }

                            </strong>
                        </span>
                        </div>

                    </div>
                </div>
            </div>

        )
    }

    const ShowHideBox = (e) => {
        e.stopPropagation();

        let node = e.currentTarget?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.querySelectorAll('.details-list-section');
        let data_var = node['0']?.className.split(' ')
        if (data_var.length === 1) {
            e.currentTarget?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.querySelector('.details-list-section')?.classList?.add("hide")

        }
        else {
            e.currentTarget?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.querySelector('.details-list-section')?.classList?.remove("hide")

        }

    }
    return (
        <>
            <div className="row">
                <div className="col-md-3">
                    <div className="Level">
                        <h2>Level 1</h2>
                        {state &&
                            state['level1']?.map((downlineList, index) => {
                                return (listviewdata(downlineList, 1))
                            })
                        }
                    </div>
                </div>
                <div className="downline_users" style={{ 'display': 'flex', 'overflow': 'overlay' }}>

                    {
                        level2 &&
                        <div className="col-md-3">
                            <div className="Level">
                                <h2>Level 2</h2>
                                <div className={`animations ${level2 === undefined ? 'hidden' : ''}`}>
                                    {
                                        level2?.map((downlineList, index) => {
                                            return (listviewdata(downlineList, 2))
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    }


                    {level3 &&
                        <div className="col-md-3">
                            <div className="Level">
                                <h2>Level 3</h2>
                                <div className={`animations ${level3 === undefined ? 'hidden' : ''}`}>
                                    {
                                        level3?.map((downlineList, index) => {
                                            return (listviewdata(downlineList, 3))
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    }


                    {level4 &&
                        <div className="col-md-3">
                            <div className="Level">
                                <h2>Level 4</h2>
                                <div className={`animations ${level4 === undefined ? 'hidden' : ''}`}>
                                    {
                                        level4?.map((downlineList, index) => {
                                            return (listviewdata(downlineList, 4))
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    }

                    {level5 &&
                        <div className="col-md-3">
                            <div className="Level">
                                <h2>Level 5</h2>
                                <div className={`animations ${level5 === undefined ? 'hidden' : ''}`}>
                                    {
                                        level5?.map((downlineList, index) => {
                                            return (listviewdata(downlineList, 5))
                                        })
                                    }
                                </div>

                            </div>
                        </div>}
                    {level6 &&
                        <div className="col-md-3">
                            <div className="Level">
                                <h2>Level 6</h2>
                                <div className={`animations ${level6 === undefined ? 'hidden' : ''}`}>

                                    {
                                        level6?.map((downlineList, index) => {
                                            return (listviewdata(downlineList, 6))
                                        })
                                    }
                                </div>

                            </div>
                        </div>}
                    {level7 &&
                        <div className="col-md-3">
                            <div className="Level">
                                <h2>Level 7</h2>
                                <div className={`animations ${level7 === undefined ? 'hidden' : ''}`}>
                                    {
                                        level7?.map((downlineList, index) => {
                                            return (listviewdata(downlineList, 7))
                                        })
                                    }
                                </div>
                            </div>
                        </div>}
                    {level8 &&
                        <div className="col-md-3">
                            <div className="Level">
                                <h2>Level 8</h2>
                                <div className={`animations ${level8 === undefined ? 'hidden' : ''}`}>

                                    {
                                        level8?.map((downlineList, index) => {
                                            return (listviewdata(downlineList, 8))
                                        })
                                    }
                                </div>
                            </div>
                        </div>}
                    {level9 &&
                        <div className="col-md-3">
                            <div className="Level">
                                <h2>Level 9</h2>
                                <div className={`animations ${level9 === undefined ? 'hidden' : ''}`}>

                                    {
                                        level9?.map((downlineList, index) => {
                                            return (listviewdata(downlineList, 9))
                                        })
                                    }
                                </div>

                            </div>
                        </div>}

                    {level10 &&
                        <div className="col-md-3">
                            <div className="Level">
                                <h2>Level 10</h2>
                                <div className={`animations ${level10 === undefined ? 'hidden' : ''}`}>

                                    {
                                        level10?.map((downlineList, index) => {
                                            return (listviewdata(downlineList, 10))
                                        })
                                    }
                                </div>
                            </div>
                        </div>}
                    {level11 &&
                        <div className="col-md-3">
                            <div className="Level">
                                <h2>Level 11</h2>
                                <div className={`animations ${level11 === undefined ? 'hidden' : ''}`}>

                                    {
                                        level11?.map((downlineList, index) => {
                                            return (listviewdata(downlineList, 11))
                                        })
                                    }
                                </div>

                            </div>
                        </div>}
                    {level12 &&
                        <div className="col-md-3">
                            <div className="Level">
                                <h2>Level 12</h2>
                                <div className={`animations ${level12 === undefined ? 'hidden' : ''}`}>

                                    {
                                        level12?.map((downlineList, index) => {
                                            return (listviewdata(downlineList, 12))
                                        })
                                    }
                                </div>

                            </div>
                        </div>}
                    {level13 &&
                        <div className="col-md-3">
                            <div className="Level">
                                <h2>Level 13</h2>
                                <div className={`animations ${level13 === undefined ? 'hidden' : ''}`}>

                                    {
                                        level13?.map((downlineList, index) => {
                                            return (listviewdata(downlineList, 13))
                                        })
                                    }
                                </div>

                            </div>
                        </div>}
                    {level14 &&
                        <div className="col-md-3">
                            <div className="Level">
                                <h2>Level 14</h2>
                                <div className={`animations ${level14 === undefined ? 'hidden' : ''}`}>

                                    {
                                        level14?.map((downlineList, index) => {
                                            return (listviewdata(downlineList, 14))
                                        })
                                    }
                                </div>

                            </div>
                        </div>}
                    {level15 &&
                        <div className="col-md-3">
                            <div className="Level">
                                <h2>Level 15</h2>
                                <div className={`animations ${level15 === undefined ? 'hidden' : ''}`}>

                                    {
                                        level15?.map((downlineList, index) => {
                                            return (listviewdata(downlineList, 16))
                                        })
                                    }
                                </div>

                            </div>
                        </div>}

                </div>

            </div>
        </>
    )

}
export default TreeViewCustom;