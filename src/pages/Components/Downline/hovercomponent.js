import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import { FaInfo } from "react-icons/fa";
import { Markup } from 'interweave';

const Hover = ({ data, image }) => {
    return (
        <Grid item>
            <Tooltip disableFocusListener disableTouchListener title={<Markup content={data} />} classes={{ tooltip: "tooltip_main_class" }}>

                <Button>{image}</Button>


            </Tooltip>
        </Grid>
    );
}

export default Hover;