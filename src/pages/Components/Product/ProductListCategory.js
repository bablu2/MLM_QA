import Link from "next/link";
import { useRouter } from "next/router";
import AllCategoryStyle from './AllCategoryStyle';
import _ from 'lodash';
import { AddCardButtonText, SecondSection } from "src/pages/[page]/product/common";

const ProductListCategory = ({
    smartShipCheck, minCartAutoshipCheck,
    addNotify, isauroshipuser, data, addToCart,
    setcosttype, costtype, quantityCheck, allcategory,
    categories, searchData, setSearchData, setAddNotify,
    handleClick, Add, Sub, refrence, isLogin, loading
}) => {
    const router = useRouter();
    const { id, name, page } = router.query;
    return (
        <>
            <AllCategoryStyle>
                <div className="main_all_category_section_class">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="right-side">
                                    <h3 key={1}>{categories?.replace(/_/g, " ")}</h3>
                                    <h4>{data?.length === 0 && 'No product Found'}{loading}</h4>
                                    <div className="cst-categorie-sec">
                                        <div className="row">
                                            {data && data?.map((productsData, index) => {
                                                let autoShiptValide = (costtype?.length > 0 && _.find(costtype, { id: productsData?.id })?.value === "AutoShip")
                                                let names = productsData.name.replaceAll('25%', '')

                                                let catname1 = names.replaceAll('\t', '')
                                                let catname = catname1.replaceAll(' ', '_')
                                                let checkQuantity = _.find(quantityCheck, { id: productsData?.id })
                                                return (
                                                    <div className="col-md-3" key={index}>
                                                        <div className="categories">
                                                            <table className="category_table">
                                                                <tbody>
                                                                    <tr>
                                                                        <td rowSpan="">
                                                                            <Link href={`/${router.query.page}/product/${catname}/${productsData.id}`}>
                                                                                <a> <img src={productsData?.product_images[0]?.image !== undefined ?
                                                                                    `${process.env.API_URL}/${productsData?.product_images[0]?.image}` : "/images/no-image.jpg"} /></a>
                                                                            </Link>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <Link href={`/${router.query.page}/product/${catname}/${productsData.id}`}>
                                                                                <a> <h1 className="title"> {productsData.name}</h1></a>
                                                                            </Link>
                                                                        </td>
                                                                    </tr>
                                                                    {isLogin &&
                                                                        <tr>
                                                                            <td>BV: {(minCartAutoshipCheck === "True" || autoShiptValide) ? productsData?.autoship_bonus_value.split()[0] : productsData?.bonus_value}</td>
                                                                        </tr>
                                                                    }
                                                                    <tr>
                                                                        <td>
                                                                            {productsData?.has_variants === 'False' &&
                                                                                <>
                                                                                    <SecondSection data={{
                                                                                        costtype,
                                                                                        productCost: productsData?.cost_price,
                                                                                        productSmartShip: productsData?.autoship_cost_price,
                                                                                        minCartAutoshipCheck,
                                                                                        setcosttype,
                                                                                        id: productsData.id,
                                                                                        smartShipCheck,
                                                                                        section: "allproduct"
                                                                                    }} />
                                                                                </>
                                                                            }
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            {(productsData?.has_variants === 'False') &&
                                                                                <div className="main-qty-sec">
                                                                                    <div className="box">
                                                                                        {(productsData?.qty_display) &&
                                                                                            <div id="qty">
                                                                                                <button type="button"
                                                                                                    name={`${productsData?.id},${null}`}
                                                                                                    id={productsData?.id}
                                                                                                    className="sub"
                                                                                                    value={(checkQuantity) ? checkQuantity.qty : 1}
                                                                                                    onClick={(e) => { Sub(e, productsData) }}>-</button>
                                                                                                <input
                                                                                                    name={`${productsData?.id},${null}`}
                                                                                                    type="text"
                                                                                                    className="get"
                                                                                                    value={(checkQuantity) ? checkQuantity?.qty : 1}
                                                                                                    list={productsData?.quantity}
                                                                                                    max={productsData?.quantity}
                                                                                                    readOnly
                                                                                                />
                                                                                                <button type="button"
                                                                                                    name={`${productsData?.id},${null}`}
                                                                                                    id={productsData?.id} className="add"
                                                                                                    value={(checkQuantity) ? checkQuantity.qty : 1}
                                                                                                    onClick={(e) => { Add(e, productsData) }}>+</button>
                                                                                            </div>
                                                                                        }
                                                                                    </div>
                                                                                </div>
                                                                            }
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div className="add-cart">
                                                                                {productsData?.has_variants === 'False' ?
                                                                                    <div className={`Add_to_cart ${productsData?.is_stock_available === 'True' ? 'abc' : 'out-of-stock'}`}>
                                                                                        <button disabled={productsData.is_stock_available === 'True' ? false : true}
                                                                                            type="button"
                                                                                            value={autoShiptValide ? true : false}
                                                                                            id={productsData.id}
                                                                                            ref={refrence}
                                                                                            onClick={(e) => addToCart(e, (checkQuantity) ? checkQuantity.qty : 1)}>
                                                                                            {AddCardButtonText(productsData?.is_stock_available, costtype, productsData.id)}
                                                                                        </button>
                                                                                        {addNotify?.id === productsData.id && <span className="error" style={{ color: 'green' }}>{addNotify?.message}</span>}
                                                                                    </div>
                                                                                    :
                                                                                    <div className="variant_exist">
                                                                                        <Link href={`/${router.query.page}/product/${catname}/${productsData.id}`}>
                                                                                            <a> <h1 className="title">view details</h1></a>
                                                                                        </Link>
                                                                                    </div>
                                                                                }
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>)
                                            })}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </AllCategoryStyle>
        </>)
}
export default ProductListCategory;