import styled from "styled-components";

const AllCategoryStyle = styled.div`
  & .main_all_category_section_class {
    .cst-categorie-sec .col-md-3 {
      margin-top: 0px;
    }
    .strickClass {
      text-decoration: line-through;
      font-size: 15px;
      color: #6a6a6a;
      text-align: right;
    }
    .right-side .categories {
      background: rgb(255 255 255 / 59%);
      box-shadow: none;
      border-radius: 0;
      box-shadow: none;
      min-height: 470px;
    }
    .right-side .categories h1.title {
      color: #000000;
      font-size: 30px;
      margin-top: 8px;
      margin-bottom: 5px;
      min-height: 70px;
      font-family: "Graphik-bold";
    }
    .title.pro-price {
      font-size: 15px;
      font-weight: 600;
    }
    .cst-categorie-sec .row {
      display: flex;
      flex-wrap: wrap;
      width: 100%;
    }
    .Add_to_cart.abc button {
      height: 45px;
      font-size: 12px;
      border-radius: 40px;
      border: 2px solid #06356a;
      transition: 0.3s ease all;
      &:hover {
        background: #fff;
        color: #06356a;
      }
    }
    .right-side h3 {
      font-size: 31px;
      font-family: "Graphik-bold";
      padding-bottom: 10px;
      text-transform: capitalize;
      margin: 0;
      padding: 0;

      @media (max-width: 767px) {
        font-size: 28px;
      }
    }
    /* .main_all_category_section_class .right-side h3 {
        font-size: 31px;
        padding-bottom: 10px;
    
    } */
    .product_cat .categories a:hover {
      text-decoration: none;
    }
    table.category_table .title.product-page-peice {
      font-size: 18px;
      font-weight: 400;
    }
    .title.product-page-peice b {
      font-weight: 500;
    }
    .cst-categorie-sec {
      .category_table {
        text-align: center;
        width: 100%;
        tr {
          td {
            .main-qty-sec {
              margin: 13px -15px 15px;
              display: flex;
              justify-content: center;
              .box {
                justify-content: center;
              }
            }
            #qty {
              display: flex;
              justify-content: center;
              width: 100%;
              border: 1px solid var(--blue);
              max-width: inherit;
              overflow: hidden;
              background: #fff;
              button {
                height: auto !important;
                flex: 1;
                background: #fff;
              }
              input {
                background: #fff;
                width: 30px !important;
                height: 30px !important;
                border-left: 1px solid var(--blue);
                border-right: 1px solid var(--blue);
              }
              .get {
                color: var(--blue);
              }
              @media (max-width: 767px){
                flex: auto;
                max-width: 90px;
              }
            }
            img {
              max-width: 198px;
              max-height: 170px;
              height: 229px;
              margin: 0 auto 10px;
            }
          }
        }
      }
      .col-md-3 {
        @media (max-width: 1279px) {
          width: calc(100% / 3);
        }
        p.green_code,
        span.discount_smartship {
          font-family: Lato;
        }
        @media (max-width: 1440px) {
          p.green_code,
          span.discount_smartship {
            font-size: 13px;
          }
        }
        @media (max-width: 1440px) {
          p.green_code,
          span.discount_smartship {
            font-size: 12px;
          }
        }
        @media (max-width: 991px) {
          width: 50%;
        }
        @media (max-width: 767px) {
          width: 100%;
          padding: 0;
        }
      }
    }
    .second_section {
      .radio_class {
        display: flex;
        align-items: center;
        border-bottom: 1px solid #969696;
        min-height: 75px;
        color: #000;
        padding-right: 15px;
        &.custom_radio {
          svg {
            path {
              fill: #00356a;
            }
          }
          width: 93%;
          margin: 0 auto;
        }
        &:first-child {
          border-top: 1px solid #969696;
        }
        &:hover {
          background: hsl(216deg 66% 24% / 7%);
          box-shadow: 0 0 5px 2px rgb(0 0 0 / 10%);
        }
        &.selected_class {
          background: hsl(216deg 66% 24% / 7%);
          // box-shadow: 0 0 5px 2px rgb(0 0 0 / 10%);
        }
        .allproduct_normal {
          font-weight: 600;
    color: #b2b2b2;
    font-size: 14px;
    width: 100%;
    max-width: fit-content;
        }
      }
      .active {
        background: hsl(216deg 66% 24% / 7%);
      }
      .title_section {
        width: 100%;
        padding-left: 10px;
        h3 {
          margin: 0 0 2px;
          font-size: 17px;
          font-family: var(--common-font);
        }
        p {
          margin-bottom: 0;
          font-size: 15px;
          line-height: normal;
          font-family: var(--common-font);
        }
      }
    }
  }

  table.category_table {
    justify-content: center;
    margin-bottom: 0;
    font-weight: 600;
    color: #b2b2b2;
    .second_section {
      .title_section {
        text-align: left;
        h3 {
          text-align: left;
          padding-top: 0;
        }
      }
      p.green_code,
      span.discount_smartship {
        font-size: 13px;
      }
    }
  }

  @media (max-width: 1024px) {
    .left-side ul li::before {
      left: 6px;
      top: 14px;
    }
  }

  @media (max-width: 991px) {
    .main_all_category_section_class .right-side .categories h1.title {
      color: #000000;
      font-size: 20px;
    }
    .cst-categorie-sec .row {
      margin-right: 0px;
      margin-left: 0;
    }
  }
  @media (max-width: 767px) {
    .productPage_banner {
      height: 310px;
      overflow: hidden;
    }
    .productPage_banner iframe {
      width: 520px;
      height: 300px;
    }
    .main_all_category_section_class .right-side .categories h1.title {
      min-height: auto;
      margin-bottom: 20px;
    }
  }
`;

export default AllCategoryStyle;
