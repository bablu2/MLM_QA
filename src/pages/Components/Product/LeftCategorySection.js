import Link from "next/link";

const LeftCategorySection = ({ setSearchData, searchData, allcategory, router, handleClick }) => {

    return (
        <div className="col-md-3">
            <div className="left-side">
                <div className="search">
                    <input type="text" placeholder="Search.." name="search" value={searchData} onChange={(e) => setSearchData(e)} />
                    <button onClick={handleClick}>Search</button>
                </div>
                <ul>
                    <li className={router?.query?.categories ? '' : 'active'}>
                        <Link href={`/${router?.query?.page}/allProduct`}>
                            <a>All Products</a>
                        </Link>
                    </li>
                    {allcategory?.map((category, index) => {
                        let names = category?.name.replace('25%', '')

                        let catname1 = names.replaceAll('\t', '')
                        let catname = catname1.replaceAll(' ', '_')

                        return (
                            <li className={`${router?.query?.categories === catname && 'active'}`} key={index}>
                                <Link href={`/${router?.query?.page}/product/productcategories/${catname}/${category?.id}`}>
                                    <a> {category?.name}</a>
                                </Link>
                            </li>
                        )
                    })}
                </ul>
            </div>
        </div>
    )
}

export default LeftCategorySection;
