import api from "@api/Apis";
import DialogComponent from "@Components/Common/DialogComponent";
import { FormLabel } from "@material-ui/core";
import { DialogSectionCss } from "@PagesComponent/ActiveSmartShip/ActiveStyleComp";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";

const InviteLinkPopup = ({ openLink, setOpenLink, setInviteLink }) => {
  const { register, handleSubmit, watch, errors, setError, getValues, reset } =
    useForm({
      mode: "all",
    });
  const [slug, setSlug] = useState();
  const [emailError, setEmailError] = useState('')
  useEffect(() => {
    document.body.classList.add("dashboard");
    getdata();
  }, []);

  function getdata() {
    api.invitationLink().then((res) => {
      setInviteLink(res?.data);
    });
  }
  async function onSubmit(linkdata) {
    linkdata.slug = slug;
    await api.invitationLink("post", linkdata).then((res) => {
      if (res?.data?.code === 1) {
        getdata();
        setOpenLink(false);
      }
      else {
        setEmailError("Email is already exist")
        setTimeout(() => {
          setEmailError('')
        }, 4000);
      }

    });
  }

  return (
    <div>
      <DialogComponent
        opend={openLink}
        handleClose={() => setOpenLink(false)}
        title="Create Link"
        classFor="order_details"
      >
        <DialogSectionCss>
          <form
            className="invitation-link-main"
            onSubmit={handleSubmit(onSubmit)}
          >
            <div className="mb-3 field-class">
              <label>Name</label>
              <input
                type="text"
                className="form-control"
                name="name"
                id="name"
                aria-describedby="nameHelp"
                placeholder="Name and Surname"
                ref={register({ required: false })}
              />
            </div>
            <div className="mb-3 field-class">
              <label>Email</label>
              <input
                type="text"
                className="form-control"
                name="email"
                id="email"
                aria-describedby="emailnoHelp"
                placeholder="Email Address"
                ref={register({ required: false })}
              />
            </div>
            <div className="mb-3 field-class">
              <label>Cell</label>
              <input
                type="text"
                className="form-control"
                name="phone_number"
                id="phone_number"
                aria-describedby="emailnoHelp"
                placeholder="Phone Number"
                ref={register({ required: false })}
              />
            </div>
            <div className="mb-3 field-class">
              <label>Country</label>
              <input
                type="text"
                className="form-control"
                name="country"
                id="country"
                aria-describedby="emailnoHelp"
                placeholder="Country"
                ref={register({ required: false })}
              />
            </div>
            <div className="mb-3 field-class">
              <label> Postal Code</label>
              <input
                type="text"
                name="postal_code"
                className="form-control"
                id="postal_code"
                placeholder="Zip Code"
                ref={register({
                  required: false,
                  minLength: {
                    value: 4,
                    message: "Enter minimum 4 digits",
                  },
                })}

              />
            </div>
            <div className="mb-3 field-class">
              <label>Choose Page</label>
              <select
                id="page"
                value={slug}
                onChange={(e) => setSlug(e.target.value)}
              >
                <option value="">Choose Page</option>
                <option value="/product/Super_Prime_120/8/">
                  Super Prime 120 Product
                </option>
                <option value="/product/Maritime_Prime_180/4/">
                  Maritime Prime 180 Product{" "}
                </option>
                <option value="/product/SilverKaire_(8oz)/7/">
                  SilverKaire Product
                </option>
                <option value="/product/BioticKaire+/35/">BioticKaire Product</option>
                <option value="allProduct">All Products</option>
              </select>
            </div>
            <div className="submit_button field-class">
              <button
                type="submit"
                className="btn btn-primary signup_up_button"
              >
                Next
              </button>
            </div>
            {emailError && <span className="error">{emailError}</span>}

          </form>
        </DialogSectionCss>
      </DialogComponent>
    </div>
  );
};

export default InviteLinkPopup;
