import UseCustomerServiceHook from "./CustomerServiceHook";
import ServiceStyle, { Heading, ChatBoxCss } from "./CustomerService.style";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import * as yup from 'yup';
import _ from "lodash";
import ReplyTableList from "./ReplyTableList";
import DialogComponent from "@Components/Common/DialogComponent";
import SendIcon from '@material-ui/icons/Send';
import CircularProgress from '@material-ui/core/CircularProgress';

const CustomerService = () => {
    const [
        onSubmit,
        tableData, open,
        setOpen, getSubDeskChat, getChatData,
        showloader, setLoader, currentChat, setCurrentChat,
        replyText, setReplyText, ticketDetail, chatLoader
    ] = UseCustomerServiceHook();

    const schema = yup
        .object()
        .shape({
            subject: yup.string().required("Please enter the subject"),
            description: yup.string().required("Please enter description"),
        });

    const { register, handleSubmit, reset, errors } = useForm({
        mode: "all",
        resolver: yupResolver(schema)
    });
    return (
        <>
            {showloader &&
                <CircularProgress sx={{
                    animation: "unset",
                    minWidth: "100%",
                    minHeight: "100%",
                    justifyContent: "center",
                    display: "flex",
                    position: "absolute",
                    top: 0,
                    left: 0,
                    background: "#f5f4f27a",
                    zIndex: 9,
                    '&>svg': {
                        minWidth: "100px",
                        maxWidth: "100px",
                    }
                }} />
            }
            <ServiceStyle>
                <Heading className='form_heading'>SUPPORT REQUEST FORM</Heading>
                <form className="customer_service_form" onSubmit={handleSubmit((data) => onSubmit(data, reset))}>
                    <div className="mb-3 field">
                        <label htmlFor="subject" className="form-label">Subject</label>
                        <input type="text" name="subject" className="form-control" id="subject" placeholder="subject" ref={register} />
                        {errors?.subject?.message && <span className="error">{errors?.subject?.message}</span>}
                    </div>
                    <div className="mb-3 field">
                        <label htmlFor="exampleFormControlTextarea1" className="form-label">Description</label>
                        <textarea className="form-control" name="description" id="exampleFormControlTextarea1" rows="5" ref={register} />
                        {errors?.description?.message && <span className="error">{errors?.description?.message}</span>}
                    </div>
                    <div className="mb-3 button_section field">
                        <button type="submit" id="customerService">Send</button>
                    </div>
                </form>
            </ServiceStyle>
            <ReplyTableList
                tableData={tableData}
                getSubDeskChat={(datas, method, section) => getSubDeskChat(datas, method, section)}>
            </ReplyTableList>
            <DialogComponent opend={open} handleClose={() => {
                setOpen(false);
                setLoader(false)
            }} title={<>{ticketDetail?.subject}<br /><span style={{ fontSize: "11px" }}>{ticketDetail?.description}</span></>} classFor="reply_chat_section" >
                <ChatBoxCss>
                    <div className="main_chat_container">
                        {(getChatData?.length > 0) &&
                            _.map(getChatData, (row, i) => (
                                <div className={`${row?.incoming ? "my_chat" : "client_chat"}`} key={i + 1}>{row?.body_text}</div>
                            ))}
                    </div>
                    <div className="text_fieldContainer">
                        <textarea rows={2} cols={20} placeholder="Enter your message" value={replyText} onChange={(e) => {
                            setCurrentChat({ ...currentChat, ...{ reply: e.target.value } });
                            setReplyText(e.target.value)
                        }} />
                        <button onClick={() => {
                            getSubDeskChat(currentChat, "post", null)
                        }}><SendIcon /></button>

                        {chatLoader &&
                            <CircularProgress
                                size={68}
                                sx={{
                                    color: "green",
                                    position: 'absolute',
                                    zIndex: 99,
                                    minWidth: "400px",
                                    position: "absolute",
                                    justifyContent: "center",
                                    display: "flex",
                                    top: "50% !important"
                                }}
                            />
                        }

                    </div>
                </ChatBoxCss>
            </DialogComponent>
        </>
    )
}
export default CustomerService;