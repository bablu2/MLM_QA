import styled from "styled-components";

const ServiceStyle = styled.div`
  padding: 10px 30px 20px;
  && .customer_service_form {
    max-width: 800px;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
    /* height: 450px; */
    @media (max-width: 767px) {
      height: auto;
    }
    .field {
      &:not(:last-child) {
        margin-bottom: 30px;
      }
      label {
        font-size: 18px;
      }
      input[type="text"] {
        padding: 8px 10px;
        font-size: 18px;
        font-family: var(--common-font);
        height: 40px;
      }
      textarea {
        padding: 18px 10px;
        font-size: 18px;
        font-family: var(--common-font);
      }
      &.button_section {
        max-width: 231px;
        width: 100%;
        margin: 0 auto;
           /* 11-10-2022 */
           @media (max-width: 767px){
          max-width: 100%;
        }
        /* end */
      }
      button {
        padding: 5px;
        text-align: center;
        width: 100%;
        border-radius: 25px;
        height: 43px;
        transition: 0.3s ease all;
        font-weight: 500;
        &:hover {
          color: #06356a;
          background: #fff;
        }
      }
      span.error {
        font-size: 15px;
      }
    }
  }
`;

const Heading = styled.div`
  width: 100%;
  text-align: center;
  margin-bottom: 20px;
  background: #06356a;
  color: #fff;
  padding: 15px 0px;
  font-size: 20px;
`;

const ReplyTableCss = styled.div`
  width: 100%;
  padding: 30px;
  .quary_constainer {
    position: relative;
    overflow-y: auto;
    max-height: 500px;
    padding-right: 10px;
    span.MuiCircularProgress-root {
      position: absolute;
      left: 0;
      top: 80px;
      background: rgba(255, 255, 255, 0.7);
      width: 100%;
      height: calc(100% - 50px);
      display: flex;
      align-items: center;
      justify-content: center;
      svg {
        width: 30px;
        height: 40px;
        animation: animation-61bdi0 1.4s linear infinite;
      }
    }
  }

  table {
    width: 100%;
    margin: 0 auto;
    thead {
      tr {
        th {
          border: 1px solid #ddd;
          padding: 15px 25px;
          background: #06356a;
          color: #fff;
          &:last-child {
            text-align: center;
          }
        }
      }
    }
    tbody {
      tr {
        td {
          border: 1px solid #ddd;
          padding: 9px 25px;
          &:last-child {
            text-align: center;
            button.conversation {
              border-radius: 25px;
              font-size: 12px;
              padding: 12px 25px;
              min-width: 100px;
            }
          }
        }
      }
    }
  }
`;

const ChatBoxCss = styled.div`
  .main_chat_container {
    min-width: 400px;
    max-height: 450px;
    margin: 5px;
    position: relative;
    min-height: 450px;
    overflow-y: auto;
    .client_chat {
      background: #9ca0a5;
      width: 50%;
      word-break: break-word;
      font-size: 15px;
      padding: 7px 10px;
      border-radius: 15px 15px 15px 0px;
      font-weight: 500;
      color: #fff;
      margin: 6px 0px;
    }
    .my_chat {
      background: #7489a1;
      width: 50%;
      word-break: break-word;
      font-size: 15px;
      padding: 7px 10px;
      border-radius: 15px 15px 0px 15px;
      font-weight: 500;
      color: #fff;
      margin: 5px 4px 5px auto;
    }
    ::-webkit-scrollbar {
      width: 5px;
      height: 5px;
    }
    ::-webkit-scrollbar-track {
      box-shadow: inset 0 0 5px grey;
      border-radius: 10px;
    }
    ::-webkit-scrollbar-thumb {
      background: gray;
      border-radius: 10px;
    }
    ::-webkit-scrollbar-thumb:hover {
      background: #38528e;
    }
  }
  .text_fieldContainer {
    display: flex;
    align-items: flex-end;
    padding: 10px 5px;
    border-top: 1px solid #f1f1f1;
    textarea {
      margin: 0;
      height: 65px;
      width: 100%;
      border-radius: 10px;
      padding: 14px 20px;
    }
    button {
      margin-left: 10px;
      color: #fff;
      padding: 9px 12px 2px;
      /* margin: 5px; */
      border-radius: 10px;
      transition: 0.3s ease all;
      svg {
        width: 100%;
        margin-left: 3px;
        font-size: 30px;
      }
      &:hover {
        color: #38528e;
        background: #fff;
      }
    }
  }
`;

export default ServiceStyle;
export { ReplyTableCss, Heading, ChatBoxCss };
