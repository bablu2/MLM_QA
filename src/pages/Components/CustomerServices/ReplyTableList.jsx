import React from 'react';
import PropTypes from 'prop-types';
import { Heading, ReplyTableCss } from "./CustomerService.style";
import moment from 'moment';
import _ from 'lodash';

function ReplyTableList({ children, tableData, getSubDeskChat }) {
    let theadKey = tableData?.length > 0 ? Object.keys(tableData[0]) : [];

    return (
        <ReplyTableCss>
            <div className="quary_constainer">
                <Heading className='table_heading'>Support Request Ticket</Heading>
                {children}
                <table>
                    <thead>
                        <tr>
                            {(theadKey?.length > 0) &&
                                <>
                                    {_.map(_.pull([...theadKey, "Action"], 'user_id'), (row, index) => {
                                        return (
                                            <th key={index + 1}>{row?.replace("_", " ")?.toUpperCase()}</th>
                                        )
                                    })}
                                </>
                            }
                        </tr>
                    </thead>
                    <tbody>
                        {tableData?.length > 0
                            ?
                            _.map(tableData, (value, index) => (
                                <tr key={index + 1}>
                                    {_.map(_.pull(theadKey, 'user_id'), (row, i) => (
                                        <td key={index + i + 1}>
                                            {row === "created_at" ? moment(value[row]).format('DD-MM-YYYY') : value[row]}
                                        </td>
                                    ))}
                                    <td>
                                        <button className="conversation" onClick={() => getSubDeskChat({ id: value.id, user_id: value.user_id }, "get", "getChat")}>Show</button>
                                    </td>
                                </tr>
                            ))
                            :
                            <tr><td colSpan="5">No record found</td></tr>
                        }
                    </tbody>
                </table>
            </div>
        </ReplyTableCss>
    )
}


export default ReplyTableList;
