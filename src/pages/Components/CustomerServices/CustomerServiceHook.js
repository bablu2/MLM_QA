import api from "@api/Apis";
import moment from "moment";
import { useEffect, useState } from 'react';
import _ from 'lodash';

export default function UseCustomerServiceHook() {

    const [tableData, setTableData] = useState([]);
    const [ticketDetail, setTicketDetail] = useState(null);
    const [callback, setCallback] = useState(false);
    const [open, setOpen] = useState(false);
    const [getChatData, setChatData] = useState([]);
    const [replyText, setReplyText] = useState("");
    const [currentChat, setCurrentChat] = useState(null);

    // for full page loader
    const [showloader, setLoader] = useState(false);

    // chat loader
    const [chatLoader, setChatLoader] = useState(false);

    useEffect(() => {
        subDeskFunction({}, 'get', 'tableData');
    }, [callback])

    const onSubmit = (data, reset) => {
        setLoader(true);
        subDeskFunction(data, "post", "save");
        reset({
            subject: "",
            description: ""
        });
    }

    const subDeskFunction = async (data, method, section) => {
        await api.supportDeskPost(data, method, section).then((res) => {
            const { data, status } = res;
            if (status === 200) {
                if (section === "tableData") {
                    setTableData(_.reverse(data?.data));
                    setLoader(false);
                }
                if (section === "save") {
                    setCallback(!callback);
                }
            }
        }).catch((err) => {
            console.log(err);
        });
    }

    const getSubDeskChat = async (datas, method, section) => {
        if (method === "get") {
            setLoader(true);
        } else {
            setChatLoader(true);
        }
        await api.supportDeskPost(datas, method, section).then((res) => {
            const { data, status } = res;
            const dataValue = data?.message;
            setReplyText('');
            if (status === 200) {
                if (section === 'getChat') {
                    setTicketDetail(data?.ticket_data)
                    setChatData(data?.message);
                    setCurrentChat(datas)
                    setOpen(true);
                    setLoader(false);
                    setChatLoader(false);
                } else {
                    if (getChatData?.length === data?.message?.length) {
                        dataValue.push({
                            body_text: datas?.reply,
                            created_at: moment().format('DD-MM-YYYY'),
                            incoming: true,
                            user_id: datas?.user_id
                        });
                        setChatLoader(false);
                        setChatData(dataValue);
                    } else {
                        setChatData(dataValue);
                    }
                    setLoader(false);
                    setChatLoader(false);
                }
            }
        }).catch((err) => {
            console.log(err);
        });
    }

    return [
        onSubmit,
        tableData, open,
        setOpen, getSubDeskChat, getChatData,
        showloader, setLoader, currentChat, setCurrentChat,
        replyText, setReplyText, ticketDetail, chatLoader];
}
