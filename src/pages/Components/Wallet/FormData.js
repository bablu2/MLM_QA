import NumberFormat from 'react-number-format';
import ReactPaginate from 'react-paginate';
import moment from "moment"
import CollapsibleTable from './TableData';
import ReactpaginateComp from '@Components/Common/ReactPaginateComp';

const FormDataKaieCash = ({ currentPageCommision, transactiondata, transactiondataError, handlePageClick, pageCount }) => {

    return (
        <>
            <CollapsibleTable transactiondata={transactiondata} />
            {transactiondataError === undefined &&
                <ReactpaginateComp pageCount={pageCount} handlePageClick={handlePageClick} />
            }
        </>
    )
}
export default FormDataKaieCash;