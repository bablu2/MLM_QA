import ReactPaginate from 'react-paginate';
import moment from "moment"
import { useState, useEffect } from 'react';
import _ from 'lodash';
import NumberFormatComp from '@Components/Common/NumberFormatComp';
import ReactpaginateComp from '@Components/Common/ReactPaginateComp';
import { TableCell, TableRow } from 'semantic-ui-react';
import { IconButton, TableHead } from '@material-ui/core';

const FormData = ({ currentPagekaire, transactiondata, transactiondataError, handlePageClick1, pageCount }) => {

    const [data, setData] = useState([]);

    useEffect(() => {
        let oldDate = '';
        let allTransetionData = [];
        transactiondata?.map((transaction, index) => {
            if (oldDate === transaction?.created_at) {
                const FIND_DATA = _.find(allTransetionData, { transDate: transaction?.created_at });
                if (FIND_DATA) {
                    FIND_DATA['transectionData'] = [
                        ...FIND_DATA?.transectionData,
                        {
                            kaire: +transaction?.kaire_amount,
                            status: transaction?.status,
                            wallet: transaction?.wallet_amount,
                            // available_kaire: transaction?.available_kaire_cash
                        }
                    ]
                }
            } else {
                oldDate = transaction?.created_at;
                allTransetionData.push({
                    transDate: transaction?.created_at,
                    transectionData: [{
                        kaire: +transaction?.kaire_amount,
                        status: transaction?.status,
                        wallet: transaction?.wallet_amount,
                        // available_kaire: +transaction?.available_kaire_cash
                    }]
                })
            }
        });
        setData(allTransetionData);
    }, [transactiondata])

    return (
        <>

            {/* <CollapsibleTable transactiondata={transactiondata} /> */}

            <table className="commission-table" style={{ '': '' }}>
                <thead>
                    <th align="right">DATE</th>
                    <th align="right">KAIRE AMOUNT</th>
                    <th align="right">STATUS</th>
                    <th align="right">AVAILABLE WALLET BALANCE</th>

                </thead>
                <tbody>
                    {data?.map((transaction, index) => {
                        return (<>
                            <tr className="date">
                                {transaction?.transDate}

                            </tr>
                            {_.map(transaction?.transectionData, (row) => (
                                <tr>
                                    <td> </td>
                                    <td>
                                        <NumberFormatComp value={row?.kaire} />
                                    </td>
                                    <td>{row?.status}</td>
                                    <td> {row?.wallet}</td>
                                    <td>
                                        <NumberFormatComp value={row?.available_kaire} />
                                    </td>

                                </tr>
                            ))}
                        </>)
                    })}
                    {data.length === 0 &&
                        <TableRow >
                            <TableCell colSpan="5" style={{ paddingLeft: '585px' }}>No record found</TableCell>
                        </TableRow>
                    }
                </tbody>

            </table>

            <ReactpaginateComp handlePageClick={handlePageClick1} pageCount={pageCount} />
        </>
    )
}
export default FormData;