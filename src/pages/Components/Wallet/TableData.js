import React from 'react';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

import moment from "moment"
import { useRouter } from 'next/router';


function Row(props) {
    const { row } = props;
    const [open, setOpen] = React.useState(false);
    const router = useRouter()
    return (
        <>
            <TableRow className="main">

                {/* <TableCell component="th" scope="row">
                    {row.name}
                </TableCell> */}
                <TableCell align="right">{row.Date}</TableCell>
                <TableCell align="right">{row.Commission_Amount === 0 ? "-" : "$" + (row.Commission_Amount?.toLocaleString(undefined, { minimumFractionDigits: 2 }))}</TableCell>
                <TableCell align="right">{row.deduction_Amount === 0 ? "-" : "$" + (row.deduction_Amount?.toLocaleString(undefined, { minimumFractionDigits: 2 }))}</TableCell>
                <TableCell align="right">{row?.Available_Wallet_Balance}</TableCell>
                <TableCell align="left">
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? "Hide Details" : "Show Details"}
                    </IconButton>
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box margin={1}>
                            <Typography variant="h6" gutterBottom component="div">History</Typography>
                            <Table size="small" aria-label="purchases">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>DATE</TableCell>
                                        <TableCell>ORDER ID</TableCell>
                                        <TableCell>CUSTOMER NAME</TableCell>
                                        <TableCell align="right">COMMISSION</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {row.history?.map((historyRow, index) => (
                                        <TableRow key={index + 1}>
                                            <TableCell scope="row">
                                                {historyRow.date}
                                            </TableCell>
                                            <TableCell
                                                onClick={() => {
                                                    router.push({
                                                        pathname: `/${router.query.page}/user/commissions/`,
                                                        query: { Commission_order_id: historyRow.OrderId?.id }
                                                    });
                                                }}

                                                className="clickableicon"

                                            >#{historyRow.OrderId?.public_order_id}</TableCell>
                                            <TableCell>{historyRow.OrderId?.user?.first_name} {historyRow.OrderId?.user?.last_name}</TableCell>

                                            <TableCell align="right">${(historyRow.status === "deduction" ? "-" : "") + historyRow.amount}</TableCell>

                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </>
    );
}

export default function CollapsibleTable({ transactiondata }) {
    return (
        <TableContainer component={Paper}>
            <Table aria-label="collapsible table">
                <TableHead>
                    <TableRow>
                        <TableCell align="right">DATE</TableCell>
                        <TableCell align="right">CREDIT</TableCell>
                        <TableCell align="right">DEBIT</TableCell>
                        <TableCell align="right">AVAILABLE WALLET BALANCE</TableCell>
                        <TableCell align="right">ACTIONS </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {transactiondata?.length > 0
                        ?
                        transactiondata?.map((row) => (
                            <Row key={row.name} row={row} />
                        ))
                        :
                        <TableRow>
                            <TableCell colSpan="5">No record found</TableCell>
                        </TableRow>
                    }
                </TableBody>
            </Table>
        </TableContainer>
    );
}
