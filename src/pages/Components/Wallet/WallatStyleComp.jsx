import styled from "styled-components";

const FormStyleWallet = styled.div`
  && form {
    width: 100%;
    margin: 27px auto;
    max-width: 1320px;
  }
  form.signupform.main-sign-frm {
    display: flex;
    align-items: center;
  }
  .Custom_filter {
    display: flex;
    align-items: center;
  }
  .date_range_filter {
    display: flex;
    align-items: center;
    font-size: 15px;
  }
  .date_range_filter input[type="radio"] {
    margin-right: 5px;
    margin-top: 0px !important;
  }
  .css-wne9d8 {
    margin-left: 10px;
  }
  @media (max-width: 1199px) {
    form.signupform.main-sign-frm {
      width: 100%;
    }
    .table-top {
      margin: 0 20px;
    }
    .transactions-data-table {
      padding: 0 15px;
    }
    .container.order-detail-page .groups.tabs ul.tabs {
      max-width: 100%;
    }
  }
  @media (max-width: 991px) {
    form.signupform.main-sign-frm {
      min-height: 110px;
    }
  }
  @media (max-width: 767px) {
    .date_range_filter {
      font-size: 14px;
      display: block;
    }
    form.signupform.main-sign-frm {
      min-height: auto;
      padding: 15px 10px;
      flex-wrap: wrap;
      margin-bottom: 0;
    }
    .transactions-data-table tr td {
      justify-content: flex-start;
    }
  }
`;

export const FormDataStyleWallet = styled.div`
  && .transactions-data-table {
    max-width: 1320px;
    margin: 0 auto;
    table {
      tr {
        th {
          z-index: 9999;
          background: #06356a;
        }
        td {
          .date {
            color: #fff;
          }
        }
      }
    }
  }

  .transactions-data-table {
    table {
      tr {
        th {
          color: #fff;
          background: #00356a;
          z-index: 0 !important;
          background: #04386c !important;
          font-size: 15px;
          padding: 15px;
        }
        td {
          font-size: 14px !important;
        }
      }
    }
  }
  @media (max-width: 991px) {
    .transactions-data-table {
      overflow-x: scroll;
      table {
        width: 1000px;
      }
    }
  }
  @media (max-width: 767px) {
    .transactions-data-table {
      overflow-x: visible;
      table {
        // width: 100%;
        border: none;
        tr {
          margin: 10px 0 0;
          td {
            justify-content: right;
            button {
              margin-right: 0 !important;
              margin-left: auto !important;
            }
          }
        }
      }
      & > div {
        border: none;
        box-shadow: none;
      }
    }
  }
`;

export const FormDataKaieCashStyle = styled.div`
  && .transactions-data-table {
    width: 100%;
    max-width: 1320px;
    margin: 0 auto;
    table {
      thead {
        th {
          background: #04386c !important;
          padding: 8px;
        }
        td {
          font-size: 14px !important;
        }
      }
    }
  }
  @media (max-width: 991px) {
    table.commission-table {
      width: 1000px;
    }
    .transactions-data-table {
      overflow-x: auto;
    }
  }
`;

export default FormStyleWallet;
