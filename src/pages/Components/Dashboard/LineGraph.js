import React, { useEffect, useState } from 'react';
import { Bar } from 'react-chartjs-2';
import _ from 'lodash';
import { legsStructure } from './FilterCommissionForm';

const LineGraph = ({ linedata, filter,
    bonusData, setDuplecateBonusData,
    setBonusFilter, bonusValueDataState,
    setBonusValueDataState,
    legsCommission, legsData,
    legsGraphStruture, setLegsGraphStructure,
    setLegCommission, setLegsFilter, dateOnly, setDateOnly, commissionValue, setCommissionValue
}) => {

    let newDate = new Date()
    function getLineGraphData(dateKey, keyName, section) {

        let dataComission = [];
        if (section === "year") {
            dataComission = [
                { month: "Jan", total_commission: 0 },
                { month: "Feb", total_commission: 0 },
                { month: "Mar", total_commission: 0 },
                { month: "Apr", total_commission: 0 },
                { month: "May", total_commission: 0 },
                { month: "Jun", total_commission: 0 },
                { month: "July", total_commission: 0 },
                { month: "Aug", total_commission: 0 },
                { month: "Sep", total_commission: 0 },
                { month: "Oct", total_commission: 0 },
                { month: "Nov", total_commission: 0 },
                { month: "Dec", total_commission: 0 }
            ]
        }
        linedata?.map((data, index) => {
            const values = _.find(dataComission, { [keyName]: data[dateKey] || "01-16-2022" });
            if (values) {
                values["total_commission"] += data?.bonus_value
            } else {
                if (data[dateKey] !== null) {
                    dataComission.push({ [keyName]: data[dateKey], total_commission: data?.bonus_value })
                } else {
                    dataComission.push({ [keyName]: "01-16-2022", total_commission: data?.bonus_value })
                }
            }
        });
        return dataComission;
    }

    function _bonusValuesFunction(dateKey, keyName, data, section = "bonus") {
        let bonusValueData = {}
        let arrayOption = [];
        if (section === "legs") {
            _.forEach(_.keys(data), (row) => {
                bonusValueData[row] = [];
            });
            arrayOption = _.keys(data);
        } else {
            bonusValueData = {
                'FOC': [], 'FO_match': [],
                'retail_customer_bonus': [], 'unilevel_commission': [],
                'unilevel_commission_match': []
            }
            arrayOption = ['FOC', 'FO_match', 'retail_customer_bonus', 'unilevel_commission', 'unilevel_commission_match']
        }
        _.forEach(arrayOption, (row) => {
            let allData = [
                { month: "Jan", total_commission: 0 },
                { month: "Feb", total_commission: 0 },
                { month: "Mar", total_commission: 0 },
                { month: "Apr", total_commission: 0 },
                { month: "May", total_commission: 0 },
                { month: "Jun", total_commission: 0 },
                { month: "July", total_commission: 0 },
                { month: "Aug", total_commission: 0 },
                { month: "Sep", total_commission: 0 },
                { month: "Oct", total_commission: 0 },
                { month: "Nov", total_commission: 0 },
                { month: "Dec", total_commission: 0 }
            ];
            _.map(data?.[row], (item) => {
                const values = _.find(allData, { [keyName]: item[dateKey] });
                if (values) {
                    values['total_commission'] += item?.total_commission;
                    if (item[dateKey] !== null) {
                        bonusValueData[row] = allData;
                    }
                }
            });
        });
        return bonusValueData;
    };

    const options = {
        responsive: true,
        barThickness: 30,
        plugins: {
            legend: {
                display: true,
                labels: {
                    filter: (item) => {
                        return !item?.text?.includes('leg')
                    }
                }
            },
            title: {
                display: true,
                text: 'Commission Chart',
            },
        },
        scales: {
            x: {
                stacked: true
            },
            y: {
                stacked: true
            }
        }
    };
    const data = {
        labels: dateOnly,
        datasets: [
            // {
            //     label: `Total ${filter} commission`,
            //     backgroundColor: 'rgba(75,192,192,0.4)',
            //     borderColor: 'rgba(75,192,192,1)',
            //     data: commissionValue?.newCommission
            // },

            {
                label: 'FOC',
                data: _.map(bonusValueDataState?.FOC, 'total_commission'),
                borderColor: '#9FE2BF',
                backgroundColor: '#9FE2BF',
                barThickness: 30,

            },
            {
                label: 'FO Match',
                data: _.map(bonusValueDataState?.FO_match, 'total_commission'),
                borderColor: '#40E0D0',
                backgroundColor: '#40E0D0',
                barThickness: 30,
            },
            {
                label: 'Unilevel',
                data: _.map(bonusValueDataState?.unilevel_commission, 'total_commission'),
                borderColor: '#FFBF00',
                backgroundColor: '#FFBF00',
                barThickness: 30,
            },
            {
                label: 'Unilevel Match',
                data: _.map(bonusValueDataState?.unilevel_commission_match, 'total_commission'),
                borderColor: '#FF7F50',
                backgroundColor: '#FF7F50',
                barThickness: 30,
            },
            {
                label: 'Retail Customer',
                data: _.map(bonusValueDataState?.retail_customer_bonus, 'total_commission'),
                borderColor: '#DE3163',
                backgroundColor: '#DE3163',
                barThickness: 30,
            }
        ].concat(legsGraphStruture)

    };
    useEffect(() => {

        if (filter === 'month' || filter === 'last_month') {
            setBonusFilter("all");
            setLegsFilter('all');
            const dataValues = getLineGraphData("updated_at", "date", "month");
            // setDateOnly(_.map(dataValues, "date"));
            setDateOnly(['week1', 'week2', 'week3', 'week4', 'week5'])
            const TotalCommission = _.map(dataValues, "total_commission");

            setCommissionValue({
                prevCommission: TotalCommission, newCommission: TotalCommission
            });

            //commission bonus state
            setBonusValueDataState(bonusData);
            setDuplecateBonusData(bonusData);

            //legs state
            const structure = legsStructure(legsData);
            setLegsGraphStructure([]);
            setLegCommission(legsData);
        }
        if (filter === 'year' || filter === 'last_year') {
            setBonusFilter("all");
            setLegsFilter('all');
            const dataValues = getLineGraphData("created_at", "month", "year");
            setDateOnly(_.map(dataValues, "month"));
            const TotalCommission = _.map(dataValues, "total_commission");
            setCommissionValue({
                prevCommission: TotalCommission, newCommission: TotalCommission
            });
            //commission bonus state
            const bonusValues = _bonusValuesFunction("date", "month", bonusData)
            setBonusValueDataState(bonusValues);
            setDuplecateBonusData(bonusValues);

            //legs state
            const legsValues = _bonusValuesFunction("date", "month", legsData, "legs")
            const structure = legsStructure(legsValues);
            setLegsGraphStructure([])
            setLegCommission(legsValues);
        }

    }, [filter, bonusData, legsData]);
    return (

        <Bar
            data={data}
            options={options}
            width={500}
            height={300}
        />

    )
}

export default LineGraph;