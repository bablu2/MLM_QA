
import * as React from 'react';
import TextField from '@material-ui/core/TextField';
import DateRangePicker from '@material-ui/lab/DateRangePicker';
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import Box from '@material-ui/core/Box';
import MobileDateRangePicker from '@material-ui/lab/MobileDateRangePicker';
import Link from 'next/link';
import _ from 'lodash';
import { toast } from 'react-toastify';

export function legsStructure(data) {
    const structureData = [];
    _.forEach(_.keys(data), (row, index) => {
        structureData.push({
            label: `${row}`,
            data: _.map(data[row], 'total_commission'),
            borderColor: `rgb(255, 99, 19${index})`,
            backgroundColor: `rgba(255, 99, 19${index}, 0.5)`,
        })
    });
    return structureData;
}

const SelectOption = ({ name, className, option, value, onChange }) => {
    return (
        <select className={className} name={name} value={value} onChange={(e) => {
            onChange(e.target?.value)
        }}>
            {_.map(option, (row) => <option value={row?.value} >{row?.title}</option>)}
        </select >
    )
};
const FilterCommissionForm = ({
    register,
    IsCustomdate,
    handleSubmit,
    commissionLevel,
    bonusFilter,
    setBonusFilter,
    onSubmit, setValue,
    iscustomdate, value,
    setBonusValueDataState, duplicateBonusData,
    setLegsGraphStructure,
    legsCommission,
    legsFilter,
    setLegsFilter,
    commissionValue,
    setCommissionValue

}) => {
    // function _handleChangeBonus(values) {
    //     if (values === 'all') {
    //         setBonusValueDataState(duplicateBonusData);
    //         setCommissionValue(pre => ({ ...pre, newCommission: pre?.prevCommission }))
    //     } else {
    //         _.forEach(_.entries(duplicateBonusData), ([key, value]) => {
    //             if (key === values) {
    //                 setBonusValueDataState({ [key]: value })
    //             }
    //         })
    //         setCommissionValue(pre => ({ ...pre, newCommission: [] }))
    //     };

    //     setBonusFilter(values);
    // }

    function _handleChangesLeg(values) {
        if (values === 'all') {
            const structure = legsStructure(legsCommission);
            setLegsGraphStructure([]);
        } else {
            _.forEach(_.entries(legsCommission), ([key, value]) => {
                if (key === values) {
                    const structure = legsStructure({ [key]: value });
                    setLegsGraphStructure(structure);
                    if (value?.length === 0) {
                        // toast.warn("No data of this user")
                    }
                }
            })

        }
        setLegsFilter(values);
    }
    return (
        <div className="headsection-graphical">
            <div className="commsion header">
                <div className="common-static-left">
                    <h3 className="title1">Commission Statistics</h3>
                </div>
                <div className="common-static-right">
                    <Link href={`/us/user/commissions`}>
                        <a> View All</a>
                    </Link>
                </div>
            </div>

            <form className="signupform main-sign-frm" onSubmit={handleSubmit && handleSubmit(onSubmit && onSubmit)}>
                <div className="Custom_filter">
                    <div className="date_range_filter" >
                        <SelectOption
                            name="monthAndYear"
                            className="monthAndYearSelection"
                            option={[
                                { value: "month", title: "This month" },
                                { value: "last_month", title: "Last month" },
                                { value: "year", title: "This year" },
                                { value: "last_year", title: "Last year" }
                            ]}
                            value={iscustomdate}
                            onChange={(value) => IsCustomdate(value)} />
                        {/* <SelectOption
                            name="commissionLevel"
                            className="commissionLevelSelection"
                            option={[
                                { value: "all", title: "All Commission Types" },
                                { value: "unilevel_commission", title: "Unilevel Bonus" },
                                { value: "unilevel_commission_match", title: "Unilevel Bonus Match" },
                                { value: "FOC", title: "First Order Bonus" },
                                { value: "FO_match", title: "First Order Bonus Match" },
                                { value: "retail_customer_bonus", title: "Retail Customer Bonus" }
                            ]}
                            value={bonusFilter}
                        onChange={(value) => _handleChangeBonus(value)} />*/}

                        <SelectOption
                            name="commissionLevel"
                            className="commissionLevelSelection"
                            option={[
                                { value: "all", title: "All Downline Users" },
                                ..._.map(_.keys(legsCommission), (row) => ({
                                    value: row, title: row?.split('_')?.[0]?.toUpperCase()
                                }))]}
                            value={legsFilter}
                            onChange={(value) => _handleChangesLeg(value)}

                        />
                    </div>
                    {iscustomdate === true &&
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <MobileDateRangePicker
                                startText="from"
                                endText="end"
                                value={value}
                                maxDate={new Date()}
                                onChange={(newValue, value) => {
                                    setValue(newValue);
                                }}
                                renderInput={(startProps, endProps) => (
                                    <React.Fragment>
                                        <TextField {...startProps} variant="standard" />
                                        <Box sx={{ mx: 2 }}> to </Box>
                                        <TextField {...endProps} variant="standard" />
                                    </React.Fragment>
                                )}
                            />
                        </LocalizationProvider>
                    }
                </div>
            </form>
        </div>
    )
}


export default FilterCommissionForm;