import Link from 'next/link';
import { useEffect, useState } from 'react';
import NumberFormat from 'react-number-format';
import ReactPaginate from 'react-paginate';
import api from '../../../api/Apis';
import CollapsibleTable from './TableData';
import Moment from 'moment';

const Commission = () => {
    const [offset, setOffset] = useState(0);
    const [data, setData] = useState();
    const [perPage] = useState(10);
    const [pageCount, setPageCount] = useState(0)
    const [commissionData, setcommissionData] = useState()
    const [commissionDataError, setcommissionDataError] = useState()


    useEffect(() => {
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        // api.getCommissionReport(token, 'week').then(res => {
        // api.GetCommissionsApproved(token, 'week').then(res => {
        api.getDashboardCommissions('week', token).then(res => {

            if (res?.data?.code === 1) {
                // const slice = res?.data?.commissions_data?.slice(offset, offset + perPage)
                // setPageCount(Math.ceil(res?.data?.commissions_data?.length / perPage))
                // setcommissionData(slice)
                settabledaa(res?.data?.commissions_data)
                //setcommissionData(res?.data?.commissions_data)

            }
            if (+res?.data?.commissions_data?.length === 0) {
                setcommissionDataError("No data found!")
            }
        })

    }, [offset]);

    let oldDate = '';

    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * perPage;
        setOffset(offset)
    };

    const settabledaa = (data) => {
        let oldDate = ''
        let test = [{ "name": "abc", "value": '12' }, { "name": "abc1", "value": '1212' }, { "name": "abc12", "value": '121' }]


        let tabledata = []
        data?.map((trans, index) => {

            tabledata.push({
                "date": trans?.created_at,
                "Order": trans?.for_order?.public_order_id,
                "name": `${trans?.for_order?.user?.first_name} ${trans?.for_order?.user?.last_name}`,
                "commission_percentage": trans?.commission_percentage,
                "history": [
                    {
                        name: `${trans?.for_order?.user?.first_name} ${trans?.for_order?.user?.last_name}`,
                        level: `${trans?.commission_level}`,
                        firstOrder: trans?.for_order?.is_first_order,
                        Product_info: trans?.user_commission_detail,
                        order_for: `${trans?.for_order?.public_order_id}`,
                        commission_percentage: trans?.commission_percentage,
                    },
                ],
            })

            if (oldDate === trans?.created_at) {
                const objIndex = tabledata?.findIndex((obj => obj.date === trans?.created_at));
                if (objIndex >= 0) {
                    tabledata[objIndex].history.push({
                        name: `${trans?.for_order?.user?.first_name} ${trans?.for_order?.user?.last_name}`,
                        level: `${trans?.commission_level}`,
                        firstOrder: trans?.for_order?.is_first_order,
                        order_for: `${trans?.for_order?.public_order_id}`,
                        Product_info: trans?.user_commission_detail,
                        commission_percentage: trans?.commission_percentage,

                    })
                }

            }
            test?.map((testdata) => {
                oldDate = trans?.created_at

            })

        })

        let array = tabledata
        let result = Object.values(array.reduce((a, { date, Order, history, firstOrder, commission_percentage, name }) => {
            a[date] = (a[date] || { date, Order, history, commission_percentage, firstOrder, name });
            // a[date].Commission_Amount = Number(a[date].Commission_Amount) + Number(Commission_Amount);
            return a;
        }, {}));



        // const slice = result?.slice(offset, offset + perPage)
        // setPageCount(Math.ceil(result?.length / perPage))
        setcommissionData(result)
    }

    return (
        <>
            <div className="dashboard-commsion-header">
                <div className="left-data-dashboard">
                    This Week's Commissions
                </div>
                <div className="right-data-dashboard">
                    <Link href={`/us/user/commissions`}>
                        <a> View All</a>
                    </Link>
                </div>
            </div>
            {/* <div className="headingData"> Commissions for this week</div> */}

            <div className="comistion-table-data dashboard-table-commsion">
                {commissionData !== undefined &&
                    <CollapsibleTable commissionData={commissionData} />
                }
            </div>
            {/* {commissionData?.length > 0 &&
                <ReactPaginate
                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={pageCount}
                    marginPagesDisplayed={10}
                    pageRangeDisplayed={10}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"} />
            } */}

        </>
    )

}
export default Commission;