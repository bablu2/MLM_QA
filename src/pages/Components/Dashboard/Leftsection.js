import * as React from 'react';
import Link from "next/link";
import { FaUser, FaEnvelope, FaPhoneAlt } from "react-icons/fa";
import { FiCopy } from "react-icons/fi";
import { BsEyeFill } from "react-icons/bs";
import { useRouter } from "next/router";
import DateFormat from "@Components/Common/DateFormet";
import { Tooltip } from "@material-ui/core";
import { Check } from '@material-ui/icons';
import { PieChart } from 'react-minimal-pie-chart';
import RankSelect from './RankSelect';
import NumberFormatComp from '@Components/Common/NumberFormatComp';
import CopyFunction from 'copy-to-clipboard';
import { CircularProgressbar } from 'react-circular-progressbar';
import "react-circular-progressbar/dist/styles.css";
var randomstring = require("randomstring");
const Leftsection = ({ rankOption, newsdata, profileData, documentdata, customerCheck, pqvgvData }) => {
    const router = useRouter()
    const [Rank, setRank] = React.useState(3);
    const [hostName, setHostName] = React.useState('shopkaire.com');

    React.useEffect(() => {
        setHostName(window.location.hostname);
    }, [])

    let detailInfo = [
        { title: "Referral Code", value: profileData?.user_data?.userdetails[0]?.referral_code, copy: true },
        { title: "Referral URL", value: profileData?.user_data?.userdetails[0]?.referral_code + '.' + hostName, copy: true },
        { title: "SmartShip Activated", value: profileData?.user_data?.userdetails[0]?.is_autoship_user, copy: false }
    ];

    if (customerCheck > 2) {
        detailInfo = [...detailInfo, { title: "Sponser Name", value: profileData?.sponser_name || "N/A", copy: false }]
    }

    const _rankDataFunction = (dataValue) => {
        return dataValue?.map(({ name, checkCondtion, tooltip, value, required, section, data }, index) => (
            <div className={checkCondtion ? 'check' : 'not-check'} key={index + 1}>
                <Check className={checkCondtion ? 'check_tick' : 'not-check_tick'} />
                <span className="membership_title_class">{name}</span>
                <Tooltip classes={{ popper: "pie_cahrt_pop" }} arrow title={
                    <div className="dataContainer" >
                        {tooltip &&
                            <>
                                <p>{checkCondtion ? "100%" : parseFloat(+value * 100 / required).toFixed(2) + "%"}</p>
                                <div className="achieved_section tooltip_section">
                                    <p className="text">Achieved:</p>
                                    <p className="value">{value?.toLocaleString()} BV</p>
                                </div>
                                <div className="required_section tooltip_section">
                                    <p className="text">Required: </p>
                                    <p className="value">{required?.toLocaleString()} BV</p>
                                </div>
                                <br /><br />
                            </>
                        }
                        {/* <p className="mouseover_class"> Detail will be visible on mouse hover</p> */}
                    </div>
                } >

                    <div className="pieChart_main_div">
                        {value ?
                            <CircularProgressbar
                                // data={[{ title: name, value: +value, color: '#E38627' }]}
                                // totalValue={+required}
                                // background="#eeeaea"
                                labelStyle={{ display: "none" }}
                                viewBoxSize={[100, 100]}
                            />
                            :
                            <>
                            </>
                        }
                    </div>
                </Tooltip>
                {(section === "pqv" || section === "gv") &&
                    <Tooltip className="eye_icon_main_class" arrow title={
                        <div className="dataContainer" style={{ maxHeight: "230px", overflowY: "scroll" }}>
                            {tooltip &&
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Order ID&nbsp;</th>
                                            <th>{section.toUpperCase()}</th>
                                            <th> Customer Name&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {data?.length > 0 ?
                                            data?.map((val, index) => (
                                                <tr key={index + 1}>
                                                    <td>{val.public_order_id}</td>
                                                    <td>{val['order_' + section] ? val['order_' + section] : "NA"}</td>
                                                    <td>{val?.customer_name}</td>
                                                </tr>
                                            ))
                                            :
                                            <tr>
                                                <td colSpan={2}>No record found</td>
                                            </tr>
                                        }
                                    </tbody>
                                </table>
                            }
                        </div>
                    }>
                        <span><BsEyeFill /></span>
                    </Tooltip>
                }
                {section === "eligible" &&
                    <span className="eye_icon_main_class"><BsEyeFill onClick={() => router.push('/us/user/active_smartships/')} /></span>
                }
            </div>
        ))
    }


    return (
        <div className="col-md-3">
            <div className="leftsec">
                <div className="usericon-a">
                    <div className="usericon">
                        {profileData?.user_data?.user_profile?.image
                            ?
                            <h3>
                                <img src={`${process.env.DOC_URL}/media/${profileData?.user_data?.user_profile?.image}?id=${randomstring.generate(16)}`} alt="" width="100" />
                            </h3>
                            :
                            <FaUser />}
                    </div>
                    <div className="user-Reser">
                        <h5 className="title">{profileData?.user_data?.first_name} {profileData?.user_data?.last_name}</h5>
                    </div>
                    <div className="icons">

                        <Link className="nav-link" href={`mailto:${profileData?.user_data?.email}`}>
                            <a><FaEnvelope /></a>
                        </Link>
                        <Link className="nav-link" href={`callto:${profileData?.user_data?.userdetails[0]?.phone_number}`}>
                            <a><FaPhoneAlt /></a>
                        </Link>

                    </div>
                </div>

                <div className="deatis-info">
                    <div className="dt-jned"><span>ID:</span>
                        {profileData?.user_data?.userdetails?.[0]?.public_user_id}

                    </div>
                    <div className="dt-jned"><span>Date Joined: </span>
                        {profileData?.user_data?.userdetails && <DateFormat date={profileData?.user_data?.date_joined} />}

                    </div>
                    {detailInfo?.map(({ title, value, copy }, index) => (
                        <div className="dt-jned" key={index + 1}>
                            <span>{title}:</span>
                            <span>{value}</span>
                            {copy && <span onClick={() => CopyFunction(value)}><FiCopy /></span>}
                        </div>
                    ))}

                </div>
                {customerCheck > 2 &&
                    <>
                        <div className="current-part">
                            <div className="currentrank">
                                <div className="current-rank-left">
                                    <h3 className="title1-heading">Paid as Rank:</h3>
                                    {
                                        profileData?.ranks?.map((ranks) => {
                                            return (
                                                +profileData?.user_data?.userdetails[0]?.paid_as_rank === ranks.id &&
                                                <h3 className="title1 " key={ranks?.rank_name}>{ranks?.rank_name}</h3>
                                            )
                                        })

                                    }
                                </div>
                                <div className="current-rank-right">
                                    <h3 className="title1-heading">Qualified as Rank:</h3>
                                    <h3 className="title1 ">
                                        {
                                            profileData?.ranks?.map((ranks) => {
                                                return (
                                                    +profileData?.user_data?.userdetails[0]?.qualified_as_rank === ranks.id &&
                                                    ranks?.rank_name
                                                )
                                            })

                                        }
                                    </h3>
                                </div>
                            </div>
                        </div>
                        {/* <div className="Distributor row"><GrDiamond /></div> */}
                        <br /><br />
                        <div className="total_sale">
                            <div className="Toal-Sale"><strong>Downline Sales: </strong>
                                <NumberFormatComp value={+profileData?.total_sales} />
                            </div>
                            <div className="Toal-commission"><strong>Total Commissions: </strong>
                                <NumberFormatComp value={+profileData?.total_commission} />
                                <span><BsEyeFill onClick={() => router.push('/us/user/commissions/ ')} /></span>
                            </div>
                            <div className="Toal-commission"><strong>Kaire Cash: </strong>
                                <NumberFormatComp value={+profileData?.total_kaire} />
                                <span><BsEyeFill onClick={() => router.push('/us/user/wallet/?for=kaire')} /></span>
                            </div>
                        </div>
                    </>
                }



                {(() => {
                    let eligible = { name: "Kaire Cash Eligible", checkCondtion: profileData?.customer_status, tooltip: false, section: "eligible" };
                    return _rankDataFunction([eligible]);
                })()}

                {
                    customerCheck > 2 &&
                    <div className="active-block">
                        <>
                            <h3 className="title_a">Rank Requirements</h3>
                            <RankSelect Rank={Rank} setRank={setRank} rankOption={rankOption} />
                        </>

                        {(() => {
                            let dataArray = [];
                            let member = { name: "Membership", checkCondtion: profileData?.user_data?.userdetails[0]?.membership === true, tooltip: false };
                            let smartActive = { name: "SmartShip Active", checkCondtion: profileData?.user_data?.userdetails[0]?.is_autoship_user === "True", tooltip: false };
                            let pqv = { name: "100 PQV", checkCondtion: +profileData?.pqv >= 100, tooltip: true, value: +profileData?.pqv, required: 100, section: "pqv", data: pqvgvData?.pqvData };
                            let gv = { name: "2500 GV", checkCondtion: +profileData?.gv >= 2500, tooltip: true, value: +profileData?.gv, required: 2500, section: "gv", data: pqvgvData?.gvData };
                            let kaire = { name: "Two Customers Added in Downline & on Smartship", checkCondtion: profileData?.kaire_status, tooltip: false };
                            let minActiveLag = { name: "", checkCondtion: false, tooltip: false, section: "eligible" }
                            if (Rank) {
                                switch (Rank) {
                                    case 1:
                                        dataArray = [];
                                        break;
                                    case 2:
                                        dataArray = [
                                            smartActive,
                                            kaire,
                                        ];
                                        break;
                                    case 3:
                                        dataArray = [member, smartActive,
                                            { ...pqv, name: "50 PQV", checkCondtion: +profileData?.pqv >= 50, required: 50 }
                                        ];
                                        break;
                                    case 4:
                                        dataArray = [member, smartActive, pqv, gv, { ...minActiveLag, name: "2 Active legs" }];
                                        break;
                                    case 5:
                                        dataArray = [
                                            member, smartActive,
                                            { ...pqv, name: "200 PQV", checkCondtion: +profileData?.pqv >= 200, required: 200 },
                                            { ...gv, name: "15000 GV", checkCondtion: +profileData?.pqv >= 15000, required: 15000 },
                                            { ...minActiveLag, name: "3 Active legs" }
                                        ];
                                        break;
                                    case 6:
                                        dataArray = [
                                            member, smartActive,
                                            { ...pqv, name: "200 PQV", checkCondtion: +profileData?.pqv >= 200, required: 200 },
                                            { ...gv, name: "30000 GV", checkCondtion: +profileData?.pqv >= 30000, required: 30000 },
                                            { ...minActiveLag, name: "4 Active legs" }];
                                        break;
                                    default:
                                        dataArray = [];
                                        break;
                                }
                            }
                            return _rankDataFunction(dataArray);
                        })()}
                    </div>
                }

            </div>
            {customerCheck > 2 &&
                <>
                    <div className="DownlineStatic">
                        <h3 className="title_1">Downline Statistics</h3>
                        {(() => {
                            return (
                                <div className="filleddata" >
                                    {['Total', 'Level 1', 'Level 2', "Level 3", "Level 4"]?.map((value, index) => (
                                        <div className="total-main-now" key={index + 1}>
                                            <div className="data-sts">{value} </div>

                                            <div className="data-sts"> {profileData?.downline_statics?.[0]?.[`${value?.replace(/\s/g, "")?.toLowerCase() + "_count"}`]} </div>
                                        </div>
                                    ))}
                                </div>)

                        })()}
                    </div>
                    <div className="newsdata DownlineStatic">
                        <h3 className="title_1">News </h3>

                        {newsdata &&
                            newsdata?.data?.map((news, index) => {
                                return (
                                    <div className="newdata" key={index}><strong>{news?.news_title}</strong> {news?.news_content}</div>
                                )
                            })
                        }
                    </div>

                    <div className="newsdata DownlineStatic" >
                        <div className="documents-downloads">
                            <h3 className="title_1" >Downloads
                                <Link href="/us/user/documents/">View all
                                </Link>
                            </h3>
                        </div>
                        {documentdata?.data?.map((docs) => {
                            return (
                                <div className="newdata" key={docs.document_name}>

                                    <Link className="nav-link" exact href={`/${router?.query?.page}/user/documents`}>
                                        <a><strong>{docs.document_name}</strong></a>
                                    </Link>
                                </div>
                            )
                        })
                        }
                    </div>
                </>
            }
        </div >
    )
}
export default Leftsection;