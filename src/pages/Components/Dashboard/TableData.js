import React from 'react';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

import moment from "moment"
import { useRouter } from 'next/router';
import DateFormat from '@Components/Common/DateFormet';


function Row(props) {
    const { row, rows, index, total_bv, total_price, total_commision } = props;
    const [open, setOpen] = React.useState(false);
    const router = useRouter()
    return (
        <React.Fragment>
            {index === 0 &&
                <TableRow className="main">
                    <TableCell align="right"></TableCell>
                    <TableCell align="right"><DateFormat date={rows.date} /></TableCell>
                    <TableCell align="right"></TableCell>
                    <TableCell align="right"></TableCell>
                    <TableCell align="right"></TableCell>
                    <TableCell align="right"></TableCell>
                    <TableCell align="right"></TableCell>


                </TableRow>
            }
            <TableRow className="main">
                {/* <TableCell>
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell> */}
                <TableCell align="right"></TableCell>

                <TableCell align="right">{row.name}<br />(Level {row.level})</TableCell>

                <TableCell align="right">{row.order_for}</TableCell>
                <TableCell align="right"></TableCell>
                <TableCell align="right">${parseFloat(total_price).toFixed(2)}</TableCell>
                <TableCell align="right">{parseFloat(total_bv).toFixed(2)}</TableCell>
                <TableCell align="right">${parseFloat(total_commision).toFixed(2)}
                    <IconButton aria-label="expand row" className="collapse_arrow_class" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={9}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box margin={3}>
                            <Table size="small" aria-label="purchases" className="inner-table">
                                <TableBody>
                                    {row?.Product_info?.map((historyRow1, index) => {
                                        return (<>
                                            <TableRow key={historyRow1}>
                                                <TableCell></TableCell>
                                                <TableCell></TableCell>
                                                <TableCell >{historyRow1?.product?.name}</TableCell>
                                                <TableCell align="right">${parseFloat(historyRow1?.price_per_unit * historyRow1?.product_quantity).toFixed(2)}</TableCell>
                                                <TableCell align="right">BV{parseFloat(historyRow1?.product_bonus_value).toFixed(2)}</TableCell>
                                                <TableCell align="right">${parseFloat(historyRow1?.commission).toFixed(2)} ({row?.commission_percentage}% {row?.firstOrder ? "F.O.C" : ""})</TableCell>
                                            </TableRow>
                                        </>)
                                    })}
                                </TableBody>
                            </Table>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}

export default function CollapsibleTable({ commissionData }) {
    return (
        <TableContainer component={Paper}>
            <Table aria-label="collapsible table">
                <TableHead>
                    <TableRow>
                        <TableCell />
                        <TableCell>NAME</TableCell>
                        <TableCell>ORDER INFO</TableCell>
                        <TableCell>PRODUCT INFO</TableCell>
                        <TableCell>PRICE</TableCell>
                        <TableCell>BONUS VALUE</TableCell>
                        <TableCell>COMMISSION</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {commissionData?.length > 0
                        ?
                        commissionData?.map((rows) => {


                            return rows?.history?.map((row1, index) => {
                                let total_price = 0
                                let total_bv = 0
                                let total_commision = 0
                                row1.Product_info?.map((caldata12, index) => {
                                    total_price += (caldata12.price_per_unit * caldata12.product_quantity)
                                    total_bv += caldata12.product_bonus_value
                                    total_commision += caldata12.commission

                                })
                                return (
                                    <Row key={row1.name + index} row={row1} rows={rows} index={index} total_price={total_price} total_bv={total_bv} total_commision={total_commision} />
                                )
                            })
                        })
                        :
                        <TableRow>
                            <TableCell colSpan={7} align="center">No Record Found</TableCell>
                        </TableRow>

                    }


                </TableBody>
            </Table>
        </TableContainer>
    );
}
