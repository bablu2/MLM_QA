import { PieChart } from 'react-minimal-pie-chart';
import { map, keys, filter, forEach } from 'lodash';
const PieCharts = ({ data, totalValue, bonusFilter }) => {

  const colors = ['#FFBF00', '#FF7F50', '#DE3163', '#9FE2BF', '#40E0D0']
  const pieData = map(keys(data), (row, index) => {

    return ({
      title: `${row?.replace('_', ' ').toUpperCase()} ($${data[row]})`,
      value: data[row],
      color: colors[index]
    })
  })

  let values = 0;
  forEach(map(pieData, 'value'), (row) => {
    values += Number(row)
  })

  const filterValue = `${bonusFilter?.replace('_', ' ').toUpperCase()} `;
  const filterData = (bonusFilter === "all") ? pieData : filter(pieData, (row) => {
    return row?.title == filterValue + "(" + '$' + row?.value + ")";
  })

  return (
    <>
      <PieChart
        data={filterData}
        label={(labelRenderProps) => <div className="chk">{`${Math.round(labelRenderProps?.dataEntry.percentage)}%`}</div>}
        totalValue={totalValue}
        labelStyle={{ color: "red" }}
        background="#dddd"
        animate={true}
        viewBoxSize={[100, 100]}
      />
      <div className="total_commissonValue">
        <div>Total Commission:</div>
        <div>{values.toLocaleString(undefined, { minimumFractionDigits: 2 })}</div>
      </div>
    </>
  )
}
export default PieCharts;