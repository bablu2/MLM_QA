import { Box, InputLabel, FormControl, NativeSelect } from '@material-ui/core';
import { useState } from 'react';

const RankSelect = ({ Rank, setRank, rankOption }) => {
    return (
        <Box sx={{ minWidth: 120 }} className="rank_select_requirememt">
            <FormControl fullWidth>
                <InputLabel variant="standard" htmlFor="uncontrolled-native">
                    Age
          </InputLabel>
                <NativeSelect
                    className="select_rank"
                    value={Rank}
                    inputProps={{
                        name: 'age',
                        id: 'uncontrolled-native',
                    }}
                    onChange={(e) => setRank(+e.target.value)}
                >
                    {rankOption?.map(({ id, rank_name }, index) => (
                        <option value={id} key={index + 1}>{rank_name}</option>
                    ))}
                </NativeSelect>
            </FormControl>
        </Box>
    )
}

export default RankSelect;