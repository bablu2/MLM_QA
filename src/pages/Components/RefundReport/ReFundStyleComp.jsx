import styled from "styled-components";

const RefundFilterFormStyle = styled.div`
  @media (max-width: 1199px) {
    overflow: auto;
  }
  && form {
    width: 100%;
    margin: 27px auto;
    max-width: 1320px;
  }
  .Custom_filter > div {
    align-items: center;
  }
  .Custom_filter {
    max-width: 100%;
    flex: 100%;
    width: 100%;
    justify-content: flex-start;
  }
  .order_id_filter {
    align-items: center;
  }
  .get_commision {
    align-items: center;
    width: auto;
  }
  .date_range_filter {
    font-size: 15px;
  }
  .date_range_filter input[type="radio"] {
    margin-top: 0;
    margin-right: 5px;
    margin-left: 5px;
  }
  .Custom_filter {
    flex: 0 0 100% !important;
    max-width: 100% !important;
    align-items: center;
  }
  .order_id_filter {
    font-size: 15px;
  }
  .order_id_filter input {
    height: 35px;
  }
  .css-wne9d8 {
    margin-left: 5px;
  }
  @media (max-width: 1199px) {
    form.signupform.main-sign-frm {
      width: 1200px;
    }
  }
  @media (max-width: 1023px) {
    .Custom_filter {
      flex-wrap: wrap;
    }
    .mainorder-detail-sec .get_commision {
      margin-top: 0px;
    }
    .get_commision {
      margin-top: 0px !important;
    }
  }
  @media (max-width: 767px) {
    .order_id_filter {
      font-size: 13px;
    }
    .date_range_filter {
      font-size: 10px;
    }
    .date_range_filter input[type="radio"] {
      margin-top: -4px;
      margin-right: 0;
    }
  }
`;

export const RefundDataStyle = styled.div`
  && .refund-report-data {
    max-width: 1320px;
    margin: 0 auto;
    table {
      tr {
        th {
          z-index: 9;
          background: #06356a;
          font-size: 15px;
          padding: 15px;
          font-weight: 600;
          line-height: 1;
        }
        td {
          font-size: 14px;
          padding: 15px;
          .date {
            color: #fff;
          }
        }
      }
    }
  }
  @media (max-width: 1023px) {
    .commission-table {
      width: 1200px;
      border: 0;
    }
    .refund-report-data {
      overflow-x: auto;
      width: 100%;
    }
  }
`;

export default RefundFilterFormStyle;
