import moment from "moment"
import NumberFormatComp from "@Components/Common/NumberFormatComp";

const RefundData = ({ setFirstDate, setLastDate, refundreportError, refundreport, CsvData }) => {
    refundreport?.map((comision, index) => {

        if (index === 0) {
            setFirstDate(moment(comision?.created_at, "ddd MMM DD YYYY HH:mm:ssZ").format(`${process.env.Date_Format}`))
        }
        setLastDate(moment(comision?.created_at, "ddd MMM DD YYYY HH:mm:ssZ").format(`${process.env.Date_Format}`))

        CsvData.push({
            "Refund Id": `RF${comision?.order_refund_id}`,
            "Date": moment(comision?.created_at, "ddd MMM DD YYYY HH:mm:ssZ").format(`${process.env.Date_Format}`),
            "Order Id": comision?.order_id,
            "Product Name": '',
            "Amount": '',
            "Status": '',

        })
        comision?.user_clawback_details.map((comisionprd, index1) => {
            CsvData.push({
                "Refund Id": '',
                "Date": '',
                "Order Id": '',
                "Product Name": comisionprd?.product?.name,
                "Amount": `$${parseFloat(+comisionprd?.clawback_amount).toFixed(2)}`,
                "Status": comisionprd?.status,

            })
        })


    })
    return (
        <div className="refund-report-data">
            <table className="commission-table">
                <thead>
                    <tr>
                        <th>REFUND ID</th>
                        <th>DATE</th>
                        <th>ORDER ID</th>
                        <th>PRODUCT NAME</th>
                        <th>AMOUNT</th>
                        <th>STATUS</th>
                    </tr>
                </thead>
                <tbody>

                    {/* { refundreportError !== '' ? <tr className="title_error"><td className="error-commision">{refundreportError}</td></tr> : */}
                    {refundreportError ? <tr className="title_error"><td colSpan="6" className="error-commision">{refundreportError}</td></tr> :

                        refundreport?.map((comision, index) => {
                            return (
                                <>
                                    <tr key={index}>
                                        <td>RF{comision?.order_refund_id}</td>
                                        <td>{moment(comision?.created_at, "ddd MMM DD YYYY HH:mm:ssZ").format(`${process.env.Date_Format}`)}</td>
                                        <td>{comision?.order_id}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    {
                                        comision?.user_clawback_details.map((comisionprd, index1) => {
                                            return (
                                                <>
                                                    <tr key={index1}>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>{comisionprd?.product?.name}</td>
                                                        <td><NumberFormatComp value={+comisionprd?.clawback_amount} /></td>
                                                        <td>{comisionprd?.status}</td>
                                                    </tr>
                                                </>
                                            )
                                        })
                                    }
                                </>)


                        })
                    }
                </tbody>
            </table>
        </div>
    )
}

export default RefundData;