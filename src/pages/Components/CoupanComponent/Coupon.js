import api from "@api/Apis";
import { toast } from "react-toastify";
import React, { useState } from 'react';
import _ from 'lodash';

const useCoupanHook = (props) => {

    const headerFirstOrder = ["COUPON NAME", "COUPON CODE", "AVAILABLE", "USED"];
    const headerDistribute = [" ORDER ID", "COUPON NAME", "USER NAME", "EMAIL", "USED ON"];

    const [popOpen, setPopOpen] = React.useState(false)
    const [selected, setSelected] = React.useState(null);
    const [sentMsg, setSentMsg] = React.useState("kuch bhi...");
    //for table
    const [firstCouponData, setFirstCouponData] = React.useState([]);
    const [distributedList, setDistributerList] = React.useState([]);

    React.useEffect(() => {
        CouponListFunction();
    }, [popOpen]);

    const CouponListFunction = async () => {
        let firstOrder = [];
        let distributeList = [];
        await api.CouponListFunction().then((res) => {
            if (res.status === 200 && res?.data?.code === 1) {
                //set template msg
                setSentMsg(res?.data?.message);

                //first order loop
                _.map(res?.data?.first_order_coupon, ({ id, coupon_name, coupon_code, available, distributed_qty }) => {
                    firstOrder.push({ id, coupon_name, coupon_name, coupon_code, available, distributed: distributed_qty });
                });
                setFirstCouponData(firstOrder);
                //distributed list loop
                _.map(res?.data?.coupons_distributed_to, ({ order_id, coupon_type, user_name, user_email, used_on }) => {
                    distributeList.push({ order_id, coupon_type, user_name, user_email, used_on });
                });
                setDistributerList(distributeList);

            } else {
            }
        }).catch((err) => {
            console.log(err);
        })
    }

    function SendCoupon() {
        SendCouponApi();
    }

    const SendCouponApi = async () => {
        await api.SendCouponFunction(selected).then((res) => {
            if (res?.status === 200 && res?.data?.code === 1) {
                _.forEach(["invalid_contact", "valid_contact", "contact_not_eligible"], (row) => {
                    if (res?.data[row]?.length > 0) {
                        _.forEach(res?.data[row], data => {
                            if (row === 'invalid_contact') {
                                // toast.error(`coupon not sent on ${data}`);
                            } else if (row === 'valid_contact') {
                                // toast.success(`coupon sent on ${data}`);
                            } else {
                                // toast.warn(`${data} not eligable for coupon`);
                            }
                        })
                    }
                })
                setPopOpen(false);
            } else if (res?.data?.code === 0) {
                // toast.error(res?.data?.message);
                setPopOpen(false);
            }
        }).catch((err) => {
            console.log(err);
        });
    }

    return [
        headerFirstOrder,
        headerDistribute,
        popOpen,
        selected,
        setSelected,
        firstCouponData,
        distributedList,
        SendCoupon,
        setPopOpen,
        sentMsg,
    ];
}
export default useCoupanHook;
