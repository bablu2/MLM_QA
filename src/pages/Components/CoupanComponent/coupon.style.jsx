import styled from "styled-components";

const CoupanStyle = styled.div`
  // 18-02-2022
  padding: 0 40px 40px;
 
  h3 {
    background: rgb(0, 53, 106);
    margin: 0px;
    color: rgb(255, 255, 255);
    text-align: center;
    padding: 15px 0px;
    border-top-left-radius: 0px;
    border-top-right-radius: 0px;
    font-weight: 600;
    font-size: 18px;
  }
  svg {
    font-size: 27px;
    &:not(:last-child) {
      margin-right: 10px;
    }
  }
  .distributed_customer {
    margin: 30px 0 0;
  }

  .coupon_list_table {
    .MuiPaper-root.MuiPaper-elevation.MuiPaper-rounded {
      border-radius: 0;
    }
    th {
      font-size: 15px;
      font-weight: 400;
      font-family: var(--common-font-bd);
    }

    td {
      font-size: 14px;
    }
    th {
      &.shareSection_class {
        display: flex;
        justify-content: space-evenly;
        align-items: baseline;
        justify-content: center;
        @media (max-width: 767px) {
          gap: 10px;
        }
      }
    }
  }
`;

const DialogInsideStyle = styled.div`
  && .autocompleteSection {
    display: flex;
    flex-direction: column;
    .MuiInput-underline {
      padding-right: 0px;
    }
    label {
      font-size: 18px;
    }
    input {
      margin-bottom: 0;
      padding: 12px;
      font-size: 16px;
    }
    button {
      margin-top: 20px;
      padding: 8px 10px;
      color: #fff;
      border-radius: 30px;
      max-width: 110px;
      border: 2px solid var(--blue);
      transition: 0.3s ease all;
      font-family: var(--common-font);
      :hover {
        background: #fff;
        color: var(--blue);
      }
    }

    button.disable {
      border: 2px solid #d3d3d3 !important;
      :hover {
        background: none !important;
        color: #d3d3d3;
      }
    }
    .MuiFormControl-root.MuiTextField-root {
      width: 100%;
    }
    @media (max-widht: 767px) {
      .coupan_list_table_main_div table {
        white-space: nowrap;
      }
    }
  }
`;

export default CoupanStyle;
export { DialogInsideStyle };
