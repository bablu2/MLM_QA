import React, { useState } from 'react';
import { Table, TextField, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, IconButton } from '@material-ui/core';
import { Email, Sms } from '@material-ui/icons';
import _ from 'lodash';
import DialogComponent from '@Components/Common/DialogComponent';
import CoupanStyle from './coupon.style';
import { DialogInsideStyle } from './coupon.style';
import {
    FacebookShareButton,
    TwitterShareButton,
    WhatsappShareButton,
} from "react-share";
import {
    FacebookIcon,
    TwitterIcon,
    WhatsappIcon,
} from "react-share";
import useCoupanHook from './Coupon';
import { DialogSectionCss } from '@PagesComponent/ActiveSmartShip/ActiveStyleComp';
import api from '@api/Apis';

function RowOfTable(props) {
    const { coupanList, share, setState, sentMsg, setOpen, setCouponDetails } = props;
    const CouponData = async (orderid) => {
        let payload = {
            token: localStorage.getItem('Token') ? localStorage.getItem('Token') : null,
            order_id: orderid
        }
        await api.getOrderDetailForRefund(payload).then(res => {
            if (res?.data.code === 1) {
                const { first_name, last_name, street_address_1, state, postal_code, country, phone_number } = res?.data?.orders?.billing_address;
                const ship = res?.data?.orders?.shipping_address;

                let data = {
                    billingAddress: `${first_name} ${last_name},${street_address_1},${state},${country},${phone_number},${postal_code}`,
                    shippingAddress: `${ship?.first_name} ${ship?.last_name},${ship?.street_address_1},${ship?.state},${ship?.country},${ship?.phone_number},${ship?.postal_code}`,
                    amount_paid: res?.data?.orders?.amount_paid,
                    couponData: res?.data?.orders?.order_details,
                }
                setCouponDetails(data);
                setOpen(true);
            }
        })
    }

    return coupanList?.map((value, index) => {
        const keyValue = _.values(_.omit(value, 'id'));
        return (
            <TableRow key={index}>
                {_.map(keyValue, (row, ind) => (
                    <TableCell onClick={() => (!share && ind === 0) ? CouponData(value?.order_id) : ''}
                        align="center" component="td" scope="row" className="td_block" key={ind + index + 1}>{(!share && ind === 0) ? <a style={{ cursor: "pointer", 'textDecorationLine': 'underline' }}>{row}</a> : row}</TableCell>
                ))}

                {share &&
                    <TableCell align="center" component="th" className="shareSection_class" scope="row">
                        <Email onClick={() => {
                            setState?.setSelected({
                                coupon_id: value?.id,
                                share_by: "email"
                            });
                            setState?.setPopOpen(true);
                        }} fontSize="large" />
                        <FacebookShareButton url={process.env.domainName} quote={sentMsg}>
                            <FacebookIcon size={28} round={true} />
                        </FacebookShareButton>
                        <WhatsappShareButton url={process.env.domainName} title={sentMsg}>
                            <WhatsappIcon size={28} round={true} />
                        </WhatsappShareButton>
                        <TwitterShareButton url={process.env.domainName} title={sentMsg}>
                            <TwitterIcon size={28} round={true} />
                        </TwitterShareButton>
                        <Sms onClick={() => {
                            setState?.setSelected({
                                coupon_id: value?.id,
                                share_by: "sms"
                            });
                            setState?.setPopOpen(true);
                        }} fontSize="large" />
                    </TableCell>
                }
            </TableRow >);
    });
}

const TableCoupon = ({ headerArray, coupanList, share, setState, sentMsg }) => {
    const [open, setOpen] = useState(false)
    const [couponDetails, setCouponDetails] = useState()
    return (
        <>
            < div className="coupon_list_table" >
                <TableContainer component={Paper}>
                    <div className="coupan_list_table_main_div">
                        <Table aria-label="table">
                            <TableHead>
                                <TableRow >
                                    {(() => {
                                        return headerArray?.map((value, index) => (
                                            <TableCell align="center" component="th" scope="row" key={index + 1}>{value}</TableCell>
                                        ))
                                    })()}
                                    {share &&
                                        <TableCell align="center" component="th" scope="row">SHARE</TableCell>
                                    }
                                </TableRow>
                            </TableHead>

                            <DialogComponent opend={open} handleClose={() => setOpen(false)} title="Coupon Details " classFor="order_details"
                            >
                                <DialogSectionCss>
                                    <label>Details</label>
                                    <div className="content_container">
                                        <div className="billing_address">
                                            <h6>Billing Address</h6>
                                            <span>{couponDetails?.billingAddress}</span>
                                        </div>
                                        <div className="shipping_address">
                                            <h6>Shipping Address</h6>
                                            <span>{couponDetails?.shippingAddress}</span>
                                        </div>
                                        <div className="shipping_address">
                                            <h6>Amount Paid</h6>
                                            <span>{couponDetails?.amount_paid}</span>
                                        </div>
                                    </div>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>PRODUCT NAME</th>
                                                <th>PRODUCT QUANTITY</th>
                                                <th>BV</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                couponDetails?.couponData?.map((items, index) => (
                                                    <tr key={index}>
                                                        <td>{items?.product_name}</td>
                                                        <td> {items?.product_quantity}</td>
                                                        <td> {items?.bonus_value}</td>
                                                    </tr>
                                                ))
                                            }
                                        </tbody>

                                    </table>
                                </DialogSectionCss>
                            </DialogComponent>

                            <TableBody className="active_amart_tbody">
                                {coupanList?.length > 0 ?
                                    <RowOfTable key="actve_smart_tabless" couponDetails={couponDetails} setOpen={setOpen} setCouponDetails={setCouponDetails} coupanList={coupanList} share={share} setState={setState} sentMsg={sentMsg} />
                                    :
                                    <TableRow>
                                        <TableCell align="center" component="td" scope="row" colSpan={9} className="year_block">No results found</TableCell>
                                    </TableRow>
                                }
                            </TableBody>
                        </Table>
                    </div>
                </TableContainer>
            </ div >
        </>

    )
}

function CouponComponent(props) {

    const [
        headerFirstOrder,
        headerDistribute,
        popOpen,
        selected,
        setSelected,
        firstCouponData,
        distributedList,
        SendCoupon,
        setPopOpen,
        sentMsg
    ] = useCoupanHook(props);

    React.useEffect(() => {
        document.body.classList.add('dashboard');
    }, [])

    return (
        <CoupanStyle>
            <div className="first_order_coupontable">
                <div className='coupon-para'>
                    <h5> Your Kaire Membership includes FREE coupons for you to share with your prospects.  This code can be used on checkout for their first order.
                        Using these coupons will connect prospects to your account even if they have not entered your
                        referral information or used your referral URL.</h5>
                </div>
                <h3>Coupons</h3>
                <TableCoupon
                    headerArray={headerFirstOrder}
                    coupanList={firstCouponData}
                    share={true}
                    setState={{ setPopOpen, setSelected }}
                    sentMsg={sentMsg}
                />
                <DialogComponent opend={popOpen} handleClose={setPopOpen} title="Share Coupon" classFor="share_coupan">
                    <DialogInsideStyle>
                        <div className="autocompleteSection">
                            <TextField
                                onChange={(event) => setSelected({ ...selected, customer_contact: event?.target?.value })}
                                size="medium"
                                id="disable-clearable"
                                sx={{ width: 300 }}
                                style={{ textTransform: "capitalize" }}
                                label={`Enter ${(selected?.share_by)?.replace('_', ' ')}`}
                                variant="standard"
                                InputLabelProps={{
                                    shrink: true,
                                }} />
                            <button onClick={() => SendCoupon()}>Send</button>
                        </div>
                    </DialogInsideStyle>
                </DialogComponent>
            </div>
            <div className="distributed_customer">
                <h3>Used Coupons</h3>
                <TableCoupon headerArray={headerDistribute} coupanList={distributedList} sentMsg={sentMsg} share={false} />
            </div>
        </CoupanStyle>
    )
}
export default CouponComponent;