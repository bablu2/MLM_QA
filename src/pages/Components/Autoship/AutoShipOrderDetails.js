import { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import NumberFormat from 'react-number-format';
import api from '../../../api/Apis';
import _ from 'lodash'
import React from 'react'
import { Dropdown } from 'semantic-ui-react'
import AutoshipAddress from "./AutoshipAddress";
import AddressAutoship from "./AddressAutoship";
import { toast } from 'react-toastify';
import DialogComponent from "@Components/Common/DialogComponent";
import { SaveCardComp } from "../../../Components/Common/PayCardSection";
import AutoshipProductVariantDetails from "./AutoShipProductVariantDetails";
import moment from "moment";
import ShippingSystem from "@Components/Common/ShippingSystem";

const AutoShipOrderDetails = ({ showdetails, paginateData, offset, orderData, setOrderData, setshowdetails }) => {

    const router = useRouter();
    const [data, setdata] = useState()
    // const [offset, setOffset] = useState(0);
    const [pageCount, setPageCount] = useState(0);
    const [perPage] = useState(10);
    const [addressdata, setaddressdata] = useState()
    const [alladdressData, setalladdressData] = useState();
    const [dataall, setdataall] = useState([])
    const [showproducts, addtolistshow] = useState(false)
    const [showaddressform, setshowaddressform] = useState(false)
    const [Logintoken, setLogintoken] = useState()
    const [savedCardId, setSavedCardId] = useState(null);
    const [billingAdrressId, setBillingAddressId] = useState();

    const [showDataValues, setShowDataValues] = useState({});
    const [shippingChargesAmout, setShippingChargesAmount] = useState(null);
    const [shippingaddress, setShippingAddress] = useState();
    const [billingAddressSelected, setBillingAddressSelected] = useState(null);
    const [autoShipSelected, setAutoShipSelected] = useState({});
    const [getSelectedShippinId, setSelectedShippinId] = useState(null);
    const [sameAddressMessage, setSameAddressMessage] = useState('')
    const [quantityArray, setQuantityArray] = useState([]);
    const [defualtShippingModel, setDefaultShippingModel] = useState()
    const [smartshipDetails, setSmartshipDetails] = useState()
    const [smartshipUpdateMsg, setSmartShipUpdateMsg] = useState('')
    const [errorShipping, setErrorShipping] = useState('')
    const [updateDate, setUpdateDate] = useState('')
    const [value, setValue] = useState(new Date());
    const [getNextShipDate, setNextShipDate] = useState()
    const [newShipDate, setNewShipDate] = useState()

    let Datashow = {};
    let total = 0;
    let AutoShipSelected = {};
    useEffect(() => {
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLogintoken(token)
        setshowdetails(showdetails)
        const update_data = {
            token: token,
            order_id: +showdetails,
            shipping_module_id: null,
        }
        OrderByIdApi(update_data);
        api.getAllProduct('us').then(res => {
            setdataall(res?.data?.products)
        })
    }, [showdetails]);

    //get next ship date

    //Get order detail using id
    const OrderByIdApi = async (update_data) => {
        await api.autoshipOrderById(update_data).then(res => {
            // product_quantity
            if (res?.data?.code === 1) {
                setNextShipDate(res?.data?.next_shipping_date)
                const dataArray = [];
                _.map(res?.data?.orders?.autoship_order_details, ({ product, product_quantity }) => (
                    dataArray.push({
                        id: product?.id,
                        image: product?.product_images[0]?.image,
                        name: product?.name,
                        weight: +product?.weight,
                        qty: +product_quantity,
                        stock: +product?.quantity,
                        variant: null,
                        normal_amount: product?.autoship_cost_price,
                        amount: product?.autoship_cost_price * +product_quantity
                    })
                ))
                setQuantityArray(dataArray);
                setdata(res?.data?.orders)
                commmonStateFunction(res, dataArray);
            }
        })
    };

    function commmonStateFunction(res, dataArray) {
        let weight = 0;
        setaddressdata(res?.data?.addresses[0]);
        setSavedCardId(res?.data?.addresses[0]?.['payment_card']?.id);
        setBillingAddressId(res?.data?.addresses[0]);
        setBillingAddressSelected(res?.data?.addresses[0]?.billing_address?.id);
        setShippingAddress(res?.data?.addresses[0]?.shipping_address?.id);

        setDefaultShippingModel(res?.data?.orders?.autoship_order);
        if (res?.data?.orders?.autoship_order?.length > 0) {
            _.map(res?.data?.orders?.autoship_order, (row) => {
                AutoShipSelected = {
                    name: row?.shipping_module?.shipping_type,
                    id: row?.shipping_module?.id,
                    amount: row?.shipping_module?.shipping_cost
                }
                setSelectedShippinId(row?.shipping_module?.id);
                return null;
            })
            setAutoShipSelected(AutoShipSelected);
        }
        CallAutoshipApi(dataArray)
    }

    function CallAutoshipApi(dataArray) {
        let weight = 0;
        if (dataArray?.length > 0) {
            dataArray?.map((value) => {
                weight += parseFloat(value?.qty) * parseFloat(value?.weight);
            })
            Datashow['subamount'] = _.sum(_.map(dataArray, 'amount'));
            Datashow['productsCount'] = _.sum(_.map(dataArray, 'qty'));
            Datashow['weight'] = weight;
            setShowDataValues(Datashow);
            if (getSelectedShippinId === null) {
                if (defualtShippingModel?.length > 0) {
                    _.map(defualtShippingModel, (row) => {
                        AutoShipSelected = {
                            name: row?.shipping_module?.shipping_type,
                            id: row?.shipping_module?.id,
                            amount: row?.shipping_module?.shipping_cost
                        }
                        setSelectedShippinId(row?.shipping_module?.id);
                        return null;
                    })
                    setAutoShipSelected(AutoShipSelected);
                }
            }

        }
    }
    // Quantity update section*************************************************

    const Add = (e) => {

        let product_id = e.currentTarget.getAttribute('data-product_id')
        let max_qty = e.currentTarget.getAttribute('data-max_qty')
        const findData = _.find(quantityArray, { id: product_id });
        if (findData?.qty > 0 && +findData?.qty < +max_qty) {
            let updateValue = [];
            if (findData) {
                const remainData = _.reject(quantityArray, findData);
                updateValue = [...remainData, {
                    ...findData,
                    qty: findData.qty + 1,
                    amount: findData.normal_amount * +(findData.qty + 1)
                }]
                setQuantityArray(updateValue);
            }
            CallAutoshipApi(updateValue)
        }
    }

    const Sub = (e) => {
        let product_id = e.currentTarget.getAttribute('data-product_id')
        const findData = _.find(quantityArray, { id: product_id });

        if (findData?.qty > 1) {
            let updateValue = [];

            if (findData) {
                const remainData = _.reject(quantityArray, findData);
                updateValue = [...remainData, {
                    ...findData,
                    qty: findData?.qty - 1,
                    amount: findData?.normal_amount * +(findData?.qty - 1)
                }]
                setQuantityArray(updateValue)
            }
            CallAutoshipApi(updateValue);
        }
    }

    //*****************************Add Products api********************************* */

    const addtolist = async (e, data) => {
        const data_add = data?.value[0]?.split(' ')
        // const update_data = {
        //     order_id: +showdetails,
        //     product_id: +data_add[0],
        //     variant_id: +data_add[1],
        //     quantity: 1
        // }
        const product = _.find(dataall, { id: data_add[0] });
        const checkIN = _.find(quantityArray, { id: data_add[0] });
        let Data = [];
        if (checkIN) {
            const removeData = _.reject(quantityArray, checkIN);
            Data = [{
                ...checkIN,
                qty: checkIN?.qty + 1,
                amount: checkIN?.normal_amount * (checkIN?.qty + 1)
            }, ...removeData];

        } else {
            Data = [...quantityArray, {
                id: product?.id,
                image: product?.product_images[0]?.image,
                name: product?.name,
                weight: +product?.weight,
                qty: 1,
                variant: null,
                stock: +product?.quantity,
                normal_amount: product?.autoship_cost_price,
                amount: product?.autoship_cost_price * 1
            }]
        }
        setQuantityArray(Data);
        CallAutoshipApi(Data);
        addtolistshow(false)
    }

    // delete from from autoship order **********************************/
    const deleteautoship = (product_id, variant_id) => {
        const update_data = {
            order_id: +showdetails,
            product_id: +product_id,
            variant_id: variant_id
        }
        if (quantityArray?.length > 1) {
            const removeData = _.reject(quantityArray, { id: product_id });
            setQuantityArray(removeData);
            CallAutoshipApi(removeData);
        }


    }
    //  display product in dropdown **************************************
    let stateOptions = []
    dataall?.map((state) => {
        if (state.variants?.length == 0) {
            stateOptions.push(
                {
                    key: state.id,
                    text: state.name,
                    value: `${state.id} ${null}`,
                    image: { avatar: true, src: `${process.env.API_URL}/${state?.product_images[0]?.image}` },
                })
        }
        else {
            state.variants.map((variantss) => {
                stateOptions.push(
                    {
                        key: variantss.id,
                        text: `${state.name} - ${variantss?.name} `,
                        value: `${state.id} ${+variantss.id}`,
                        image: { avatar: true, src: `${process.env.API_URL}/${variantss?.product_variant_images[0]?.image}` },

                    })
            })
        }
    });
    // onclik view address
    const showaddresslist = () => {
        api.manageAddress(Logintoken).then(res => {
            if (res?.data?.code === 1) {
                setalladdressData(res?.data)
            }
        })
    }

    // get autoship order by id
    const getautoshiporder = () => {
        const update_data = {
            token: Logintoken,
            order_id: +showdetails
        }
        api.autoshipOrderById(update_data).then(res => {
            setdata(res?.data?.orders)
            setaddressdata(res?.data?.addresses[0])
        })
    }

    //************New card save click ********************** */
    const Save = async (data = null) => {
        let token = localStorage.getItem('Token');
        let billing_address = parseFloat(billingAdrressId, 2) || null
        let payload = { payment_details: data !== null ? data?.payment_details : null, billing_address_id: billing_address }
        const result = await api.saveNewCard(token, payload).then((response) => {
            return response;
        }).catch((error) => {
            console.log(error);
        })
        return result;
    }

    async function UpdateDefaultCard(cardId) {
        let token = localStorage.getItem('Token');
        const payload = {
            token: token,
            order_id: +showdetails,
            card_id: +cardId
        }
        await api.autoshipCardUpdate(token, payload).then((res) => {
            if (res?.data?.code === 1) {
                // toast.success(res?.data?.message);
            } else {
                // toast.success(res?.data?.message);
            }
        }).catch((error) => {

        })
    }

    // async function UpdateAddresses(id = null) {
    //     if (id !== null) {
    //         apiCall({
    //             order_id: +showdetails,
    //             billing_address_id: +addressdata?.billing_address?.id,
    //             shipping_address_id: +addressdata?.shipping_address?.id,
    //             shipping_module_id: +id,
    //         });
    //     } else {
    //         await apiCall(updateShippingData);
    //     }
    // }

    // async function apiCall(update) {
    //     await api.addressUpdate(update, Logintoken).then(res => {
    //         if (res?.data?.code === 1) {
    //             if (section === "billing") {
    //                 setBillingAddressId(BillingState?.billingAddressSelected);
    //             }
    //             getautoshiporder();
    //             setalladdressData([]);
    //             //showaddresslist();
    //             toast.success(res?.data?.message);
    //         }
    //     })
    // }
    async function handleFunction() {
        if (getSelectedShippinId && savedCardId) {
            setErrorShipping('')
            const datas = {
                "public_order_id": data?.id,
                "billing_details": null,
                "shipping_address_id": shippingaddress,
                "billing_address_id": billingAddressSelected,
                "amount": _.sum(_.map(quantityArray, 'amount')),
                "tax_amount": 0,
                "shipping_amount": shippingChargesAmout,
                "gross_total": _.sum(_.map(quantityArray, 'amount')) + shippingChargesAmout,
                "amount_paid": _.sum(_.map(quantityArray, 'amount')) + shippingChargesAmout,
                "shipping_module_id": getSelectedShippinId,
                "saved_card_id": savedCardId,
                "next_shiping_date": getNextShipDate ? getNextShipDate : value,
                "products": _.map(quantityArray, (row) => {
                    const values = _.pick(row, ['id', 'qty'])
                    return ({
                        "product_id": values.id,
                        "quantity": values?.qty,
                        "variant_id": null,
                    })
                })
            };

            await api.EditSmartshipData(datas).then((res) => {
                if (res?.data?.code === 1) {
                    setSmartshipDetails(res?.data)
                    setSmartShipUpdateMsg(res?.data?.message)
                    setTimeout(() => {
                        setSmartShipUpdateMsg('')
                    }, 3000);
                }
            }).catch((err) => {
                console.log(err)
            })

        } else {
            setErrorShipping("please select shipping method")

        }
    }
    const disablePastDate = () => {
        const today = new Date();
        const dd = String(today.getDate() + 1).padStart(2, "0");
        const mm = String(today.getMonth() + 1).padStart(2, "0");
        const yyyy = today.getFullYear();
        return yyyy + "-" + mm + "-" + dd;
    };
    const handleBack = () => {
        paginateData(Logintoken, offset)
        setshowdetails()

    }

    function shipError(value) {
        if (getSelectedShippinId !== null) {
            setErrorShipping('')
        }
    }

    return (

        <div className="col-md-10" >
            <div className="container order-detail-page">
                <div className="Cart_product order-detail-sec">
                    <div className="heading-top-main right-address">
                        <div className="heading-main-text">
                            <h1>Order #{data?.public_order_id} </h1>
                        </div>
                    </div>
                    <div className="main_order_detail_class">
                        <AutoshipAddress getautoshiporder={getautoshiporder}
                            showdetails={showdetails}
                            Logintoken={Logintoken}
                            showaddresslist={showaddresslist}
                            addressdata={addressdata}
                            setshowdetails={setshowdetails}
                            alladdressData={alladdressData}
                            setalladdressData={setalladdressData}
                            setshowaddressform={setshowaddressform}
                            showaddressform={showaddressform}
                            BillingState={{ billingAddressSelected, setBillingAddressSelected, setBillingAddressId }}
                            ShippingState={{ shippingaddress, setShippingAddress }}
                            getSelectedShippinId={getSelectedShippinId}
                            shippingSystem={{
                                showContinue: getSelectedShippinId,
                                Datashow: showDataValues,
                                shippingaddress: shippingaddress,
                                setShippingChargesAmount: setShippingChargesAmount,
                                setShowContinue: setSelectedShippinId,
                                AutoshipSelected: autoShipSelected,
                                section: "Autoship",
                            }}

                        />

                        {/* Add Address popup */}
                        <DialogComponent opend={showaddressform} handleClose={(value) => setshowaddressform(value)} title="Address" classFor="add_address_class_from_autoship">
                            <AddressAutoship alladdressData={alladdressData}
                                setalladdressData={setalladdressData}
                                logintoken={Logintoken}
                                showdetails={showdetails}
                                showaddresslist={showaddresslist}
                                setshowaddressform={setshowaddressform}
                                getautoshiporder={getautoshiporder}
                            />
                        </DialogComponent>

                        <div className="autoship_order_detail SmartShip-Order-Details">
                            <h4 className="tittle">SmartShip Order Details</h4>
                            <div className="add_product_button">

                                <button className="add-product-autoshiporder" onClick={() => { addtolistshow(true) }}>Add product</button>
                                {showproducts === true &&
                                    stateOptions.length >= 1 &&
                                    <DialogComponent opend={showproducts} handleClose={(value) => addtolistshow(value)} title="Add Product" classFor="product_add_from_autoship">
                                        <Dropdown
                                            value={null}
                                            fluid
                                            multiple
                                            search
                                            selection
                                            onChange={addtolist}
                                            options={stateOptions}
                                            additionLabel="tetinggggg"
                                        />
                                    </DialogComponent>
                                }
                            </div>
                            <div className="autoship_details_table">
                                <div className="table-responsive">
                                    <table>
                                        <thead className="row">
                                            <tr>
                                                <th className="pro-name">IMAGE</th>
                                                <th className="pro-name">PRODUCT</th>
                                                <th className="pro-name">QUANTITY</th>
                                                <th className="pro-name">TOTAL</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {/* * +datas?.product_quantity */}
                                            {quantityArray?.map((datas, index) => {
                                                if (datas?.variant !== null) {
                                                    total = total + (+datas?.variant?.autoship_cost_price * +datas.product_quantity)
                                                } else {
                                                    total = total + datas?.amount;
                                                }
                                                return (
                                                    <tr className="" key={index}>
                                                        <AutoshipProductVariantDetails
                                                            deleteautoship={deleteautoship}
                                                            datas={datas}
                                                            // quantityArray={quantityArray}
                                                            Sub={Sub}
                                                            Add={Add}
                                                            data={data}
                                                            section={datas?.variant !== null ? "variant" : "product"} />
                                                    </tr>
                                                )
                                            })}
                                            <tr className="">
                                                <td className="ship_amout" >:</td>
                                                <td></td>
                                                <td className="ship_text" data-value="Subtotal:">
                                                    <NumberFormat
                                                        value={parseFloat(total).toFixed(2)} displayType={'text'}
                                                        thousandSeparator={true} prefix={'$'}
                                                        renderText={value => <div>{value} USD</div>} />
                                                </td>
                                            </tr>
                                            {data?.discount_amount > 0 &&
                                                <tr className="">
                                                    <td className="ship_amout">Discount {`${data?.coupon_name !== undefined ? (data.coupon_name) : ''}`}:</td>
                                                    <td></td>
                                                    <td className="ship_text" data-value="Discount:">
                                                        <NumberFormat value={parseFloat(data?.discount_amount).toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <div>{value} USD</div>} />
                                                    </td>
                                                </tr>
                                            }

                                            {shippingChargesAmout &&
                                                <tr className="">
                                                    <td className="ship_amout">Shipping Amount:</td>
                                                    <td></td>
                                                    <td className="ship_text" data-value="Shipping Amount:">
                                                        <NumberFormat value={parseFloat(shippingChargesAmout).toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <div>{value} USD</div>} />
                                                    </td>
                                                </tr>
                                            }

                                            {(() => {
                                                let totalAmount = parseFloat(total) + parseFloat((shippingChargesAmout) ? +shippingChargesAmout : 0);
                                                return (
                                                    <tr className="">
                                                        <td className="ship_amout" >Total:</td>
                                                        <td></td>
                                                        <td className="ship_text" data-value="Total:">
                                                            <NumberFormat value={parseFloat(totalAmount)?.toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <div>{value} USD</div>} />
                                                        </td>
                                                    </tr>

                                                )
                                            })()}
                                        </tbody>
                                    </table>
                                    <div className="next-sec">
                                        <label> Next Shipping Date</label>
                                        <div className="date-picker">
                                            <input type="date" min={disablePastDate()}
                                                defaultValue={getNextShipDate}
                                                onChange={(e) =>
                                                    setValue(moment(new Date(e.target.value)).format("YYYY-MM-DD"))
                                                } />
                                        </div>
                                    </div>
                                </div>

                                <div className="shipping_auto_md">
                                    <div className="autoshipping_system">
                                        <ShippingSystem {...{
                                            showContinue: getSelectedShippinId,
                                            Datashow: showDataValues,
                                            shippingaddress: shippingaddress,
                                            setShippingChargesAmount: setShippingChargesAmount,
                                            setShowContinue: setSelectedShippinId,
                                            AutoshipSelected: autoShipSelected,
                                            section: "Autoship",
                                        }}
                                            updateShippingPayment={(value) => shipError(value)}
                                        />
                                    </div>
                                </div>

                                <div className="savedCard_detail">
                                    <SaveCardComp
                                        compFunction={Save}
                                        section="Autoship"
                                        setSavedCardId={setSavedCardId}
                                        savedCardId={savedCardId}
                                        UpdateDefaultCard={UpdateDefaultCard} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="backbutton d-flex">
                    <button className="back-button" onClick={() => handleBack()}>Back</button>
                    <button className="back-button" onClick={() => handleFunction()}>Update</button>
                </div>
                {smartshipUpdateMsg && <span className="error deletemsg" style={{ bottom: '20px', transform: 'translateX(-50%)', left: '50%', position: 'absolute', color: 'green', fontSize: "16px", display: "block", width: '100%', textAlign: "center" }}>{smartshipUpdateMsg}</span>}
                {errorShipping && <p className="error">{errorShipping}</p>}

            </div>
        </div>
    )
}
export default AutoShipOrderDetails;

