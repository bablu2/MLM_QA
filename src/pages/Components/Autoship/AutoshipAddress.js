import AddressesSection from '@Components/Common/AddressesSection';
import ShippingSystem from '@Components/Common/ShippingSystem';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import api from '../../../api/Apis';

const AutoshipAddress = (props) => {
    const { showaddresslist, getautoshiporder, addressdata, setshowdetails, alladdressData, showdetails, setalladdressData,
        Logintoken, setshowaddressform, getSelectedShippinId, BillingState, shippingSystem, ShippingState } = props;

    // save shipping address  
    let section = null;
    const [updateShippingData, setShippingUpdateData] = useState(null);

    const [show, setShow] = useState(null);

    useEffect(() => { setShow(getSelectedShippinId) }, [getSelectedShippinId]);
    useEffect(() => {
        setShippingUpdateData({
            order_id: +showdetails,
            billing_address_id: +BillingState?.billingAddressSelected,
            shipping_address_id: +ShippingState?.shippingaddress,
            shipping_module_id: show ? show : null,
        });
    }, [BillingState?.billingAddressSelected, ShippingState?.shippingaddress]);

    const saveShippingAddress = (id, country = "", section = "") => {
        section = section;
        if (section === "shipping") {
            ShippingState?.setShippingAddress(id);
            if (country === "US") {
                setShow(getSelectedShippinId);
            } else {
                setShow(null);
            }
        } else {
            BillingState?.setBillingAddressSelected(id);
        }

    }
    async function UpdateAddresses(id = null) {
        // if (id !== null) {
        //     apiCall({
        //         order_id: +showdetails,
        //         billing_address_id: +addressdata?.billing_address?.id,
        //         shipping_address_id: +addressdata?.shipping_address?.id,
        //         shipping_module_id: +id,
        //     });
        // } else {
        //     await apiCall(updateShippingData);
        // }
    }

    async function apiCall(update) {
        await api.addressUpdate(update, Logintoken).then(res => {
            if (res?.data?.code === 1) {
                if (section === "billing") {
                    setBillingAddressId(BillingState?.billingAddressSelected);
                }
                getautoshiporder();
                setalladdressData([]);
                //showaddresslist();
                // toast.success(res?.data?.message);
            }
        })
    }
    return (
        <div className="autoship_address_section billing-arcstm">
            <div className="row order-cstm  addressesOption_with_change">
                <div className="col-md-6">
                    <div className="thn-lft raido-addres-sel">
                        <h2>BILLING ADDRESS</h2>
                        {alladdressData?.addresses?.length > 0 ?
                            alladdressData?.addresses?.map((address, index) => {
                                let checkedValue = +BillingState?.billingAddressSelected === +address?.id ? true : false;
                                return (
                                    <>
                                        <div className="radio-bnt" key={index}>
                                            <input type="radio" name="addressBilling" checked={checkedValue} value={address?.id} onChange={(e) => {
                                                if (!checkedValue) saveShippingAddress(+e.target.value, address?.country, "billing");
                                            }} />
                                            <div className="checkout-addrees">
                                                <h5 className="tittle">
                                                    {address?.first_name + " " + address?.last_name + "," + `${address?.company_name ? address?.company_name + "," : ""}` + " " + address?.street_address_1 + "," + `${address?.street_address_2 ? address?.street_address_2 + "," : ""}` + address?.city + ","
                                                        + address?.state + "," + address?.postal_code + "," + address?.country + "," + address?.phone_number}
                                                </h5>
                                            </div>

                                        </div>
                                    </>
                                )
                            })
                            :
                            <AddressesSection addressdata={addressdata} type="billing_address" />
                        }
                    </div>
                </div>
                <div className="col-md-6">
                    <div className="thn-lft thn-rgt raido-addres-sel">
                        <h2>SHIPPING ADDRESS </h2>
                        {alladdressData?.addresses?.length > 0 ?
                            alladdressData?.addresses?.map((address, index) => {
                                let checkedValue = +ShippingState?.shippingaddress === +address?.id ? true : false;
                                return (
                                    <>
                                        <div className="radio-bnt" key={index}>
                                            <input type="radio" name="addressShip" checked={checkedValue} value={address?.id} onChange={(e) => {
                                                if (!checkedValue) saveShippingAddress(+e.target.value, address?.country, "shipping");
                                            }} />
                                            <div className="checkout-addrees">
                                                <h5 className="tittle">
                                                    {address?.first_name + " " + address?.last_name + "," + `${address?.company_name ? address?.company_name + "," : ""}` + " " + address?.street_address_1 + ", " + `${address?.street_address_2 ? address?.street_address_2 + "," : ""}` + address?.city + ", " + address?.state + "," + address?.postal_code + "," + address?.country + "," + address?.phone_number}
                                                </h5>
                                            </div>
                                        </div>
                                    </>
                                )
                            })
                            :
                            <AddressesSection addressdata={addressdata} type="shipping_address" />
                        }
                    </div>
                </div>
            </div>
            <div className="row autoshipPayment_with_button">

                <div className="col-md-12 changeAdd_auto_md">
                    <div className="changeadress-button">

                        <button className="back-button" onClick={() => { showaddresslist() }}>Change address</button>
                        <button className="back-button" onClick={() => { setshowaddressform(true) }}>Add New</button>
                    </div>
                </div>

            </div>
        </div>
    )
}
export default AutoshipAddress;