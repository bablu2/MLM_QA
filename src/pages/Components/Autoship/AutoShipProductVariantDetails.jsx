import NumberFormat from "react-number-format";
import { FaTrash, FaEdit } from "react-icons/fa";
import NumberFormatComp from "@Components/Common/NumberFormatComp";

const AutoshipProductVariantDetails = ({ datas, Add, Sub, data, deleteautoship, section, }) => {
  return (
    <>
      <td className="" data-value="PRODUCT IMAGE">
        <div className="cart-product-details-img">
          <img
            src={`${process.env.API_URL}${datas?.image}`}
            height="100"
            width="90"
          />
        </div>
      </td>
      <td className="" data-value="NAME">
        <div className="cart-product-details">
          {datas?.name}
          {` ${section === "variant" && datas?.variant !== null
            ? ` - ${datas?.variant?.name}`
            : ""
            }`}
        </div>
      </td>
      <td className="qty-sec" data-value="QUANTITY">
        <div className="main-qty-sec">
          <div className="box">
            <div id="qty">
              <button
                type="button"
                data-qty={datas?.qty}
                data-product_id={datas?.id}
                data-variant_id={
                  section === "variant" ? +datas?.variant?.id : null
                }
                data-order_id={data?.id}
                className="sub"
                onClick={(e) => {
                  Sub(e, "sub");
                }}
              >
                -
              </button>
              <input name={section} type="text" value={datas?.qty} readOnly />
              <button
                type="button"
                data-qty={datas?.qty}
                data-product_id={datas?.id}
                data-variant_id={
                  section === "variant" ? +datas?.variant?.id : null
                }
                data-order_id={data?.id}
                data-max_qty={
                  section === "variant"
                    ? +datas?.variant?.quantity
                    : +datas?.stock
                }
                className="add"
                value={datas?.qty}
                onClick={(e) => {
                  Add(e, "add");
                }}
              >
                +
              </button>
            </div>
          </div>
        </div>
      </td>
      <td data-value="TOTAL">
        <div className="payment_with_delete">
          <div className="cart-product-details">
            <NumberFormatComp value={datas?.amount} />
          </div>
          {(() => {
            let defaultData = null;
            if (section === "variant") {
              defaultData = +datas?.variant?.id;
            }
            return (
              <div className="delete">
                <FaTrash
                  onClick={() => {
                    deleteautoship(datas?.id, defaultData);
                  }}
                />
              </div>
            );
          })()}
        </div>
      </td>
    </>
  );
};
export default AutoshipProductVariantDetails;
