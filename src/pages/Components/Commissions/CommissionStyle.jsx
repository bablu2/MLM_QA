import styled from "styled-components";

const FormStyle = styled.div`
  z-index: 9;
  @media (max-width: 1199px) {
    overflow: auto;
  }

  position: relative;
  && form {
    width: 100%;
    margin: 27px auto;
    max-width: 1320px;
  }
  .date_range_filter {
    font-size: 15px;
    vertical-align: top;
    display: flex;
    align-items: center;
  }
  .Custom_filter {
    display: flex;
    align-items: center;
  }
  .order_id_filter {
    font-size: 15px;
  }
  .Status {
    display: flex;
    font-size: 15px;
    align-items: center;
  }
  .Status select {
    height: 35px;
  }
  .order_id_filter input {
    height: 35px;
  }
  .order_id_filter {
    display: flex;
    align-items: center;
  }
  .custom-date-collection {
    top: 82px;
    left: 154px;
  }
  .MuiFormControl-root {
    background: #fff;
    margin-right: 10px;
    margin-left: 10px;
  }
  .date_range_filter input[type="radio"] {
    vertical-align: middle;
    margin-left: 10px !important;
    margin-right: 10px;
    margin-top: 1px;
  }
  .Custom_filter {
    max-width: 100% !important;
    flex: 0 0 100% !important;
    width: 100% !important;
  }
  .signupform {
    padding: 35px 10px;
  }
  .groups.tabs.commission-tab {
    z-index: 0 !important;
  }
  @media (max-width: 1365px) {
    .Custom_filter {
      justify-content: space-around;
    }
    .date_range_filter {
      font-size: 12px;
    }
    .order_id_filter {
      font-size: 12px;
    }
    .Status {
      font-size: 12p;
    }
    .Custom_filter {
      flex-wrap: wrap;
      justify-content: center !important;
    }
    .get_commision {
      margin-top: 3px;
    }
  }
  @media (max-width: 1199px) {
    form.signupform.main-sign-frm { 
      @media (max-width: 767px) {
        width: 100%;
      }
    }
  }

`;

export const FormDataStyle = styled.div`
  && .comission-data-table {
    max-width: 1320px;
    margin: 0 auto;
    table {
      tr {
        th {
          z-index: 9999;
          background: #06356a;
          font-size: 15px;
          line-height: 1;
          padding: 15px;
          font-weight: 600;
        }
        td {
          .date {
            color: #fff;
            font-size: 14px;
            line-height: 1;
            padding: 15px;
          }
        }
      }
    }
  }
  .commission-table tbody .date td {
    background: #f9f9f9 !important;
    color: #06356a;
    font-weight: 600;
  }
  .commission-table tbody tr td {
    height: 50px;
  }
  .comission-data-table table tr th {
    z-index: 1 !important;
  }
  @media (max-width: 991px) {
    .comission-data-table {
      overflow: auto;
    }
    .comission-data-table .commission-table {
      width: 1200px;
    }
  }
  @media (max-width: 767px) {
    width: 100%;
    .comission-data-table .commission-table {
      width: 100%;
      td {
        width: 100% !important;
      }
    }
  }
`;

export const GraphicalCommissionStyle = styled.div`
  && .graphical-rep-commission {
    width: 100%;
    max-width: 1320px;
    margin: 0 auto;
  }
  .graphical-rep-commission {
    padding: 0px 20px;
  }
  .col-md-10.dashboard-main {
    max-width: 100% !important;
    width: 100% !important;
  }
`;

export default FormStyle;
