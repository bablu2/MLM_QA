

import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react';
import api from '../../../api/Apis';
import NumberFormat from 'react-number-format';
import { useForm } from 'react-hook-form';
import NProgress from 'nprogress'
import { toast } from 'react-toastify';
import BundleRefund from './vieworder/BundleRefund';
import ProductRefund from './vieworder/ProductRefund';
import AddressesSection from '../../../Components/Common/AddressesSection';
import PaymentTitleSectionHtml from '@Components/Common/PaymentTitleSectionHtml';
// import RefundHistory from './refundhistory';
import { KeyboardArrowRight, KeyboardArrowDown } from '@material-ui/icons';

export default function ViewOrderDetails({ orderid, setorderid, setshowdetailsorder, setInnerLoader }) {
    const router = useRouter();
    const [data, setdata] = useState()
    const [refundHistoryData, setRefundHistoryData] = useState()
    const [Logintoken, setLogintoken] = useState()
    const [BundleData, setBundleData] = useState()
    const [expend, setExpend] = useState(false);

    useEffect(() => {
        // setInnerLoader(true)
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLogintoken(token)
        const update_data = {
            token: Logintoken,
            order_id: orderid
        }
        api.getOrderDetailForRefund(update_data).then(res => {
            if (res?.data.code === 1) {
                setdata(res?.data?.orders)
                setInnerLoader(false)
            }
        })

    }, [orderid]);
    const Backfromhere = (e) => {
        setorderid()
        setshowdetailsorder(false)
    }


    let product_qty;

    return (<>
        <Head>
            <title>Order Details</title>
        </Head>
        <div className="container">
            <h4 className="cstm-tittle">Order #{data?.public_order_id}</h4>
            <div className="row order-cstm">
                <div className="col-md-6">
                    <div className="thn-lft thn-rgt raido-addres-sel">
                        <h2>BILLING ADDRESS</h2>
                        <AddressesSection addressdata={data} type="billing_address" />
                    </div>
                </div>
                <div className="col-md-6">
                    <div className="thn-lft thn-rgt raido-addres-sel">
                        <h2>SHIPPING ADDRESS</h2>
                        <AddressesSection addressdata={data} type="shipping_address" />
                    </div>
                </div>
            </div>
            <br /><br />
            <div className="Cart_product order-detail-sec">
                <h4 className="tittle">Order details</h4>

                <div className="orer-details-tables TABLE-smart">
                    <table>
                        <thead>
                            <tr>
                                <th>PRODUCT</th>
                                <th>QTY</th>
                                <th>TOTAL</th>
                            </tr>
                        </thead>
                        <tbody>
                            {data?.order_details.map((datas, ind) => {
                                let is_variant;

                                try {
                                    is_variant = JSON.parse(datas?.metadata)
                                }
                                catch {
                                    is_variant = ''
                                }
                                const product_type = is_variant[0]?.fields?.product_type
                                if (product_type === 'Bundle') {
                                    api.getBundleProduct(datas?.product, Logintoken).then(res => {
                                        const datassss = res?.data?.bundle_data;
                                        setProductType(product_type);
                                        if (BundleData === undefined) {
                                            setBundleData(datassss);
                                        }
                                    })
                                }
                                return (
                                    <>
                                        {product_type === 'Bundle' ?
                                            <>
                                                <tr className="bundale_title">
                                                    <td data-value="PRODUCT">
                                                        <span>
                                                            {!expend ?
                                                                <KeyboardArrowRight sx={{ fontSize: 20, position: "relative", top: "5px" }} onClick={() => setExpend(true)} />
                                                                :
                                                                <KeyboardArrowDown sx={{ fontSize: 20, position: "relative", top: "5px" }} onClick={() => setExpend(false)} />
                                                            }
                                                        </span>
                                                        <span>{datas?.product_name}</span>
                                                    </td >
                                                    <td data-value="QTY"></td><td data-value="TOTAL">include</td>
                                                </tr>
                                                {BundleData?.map((data_bundle, index) => {
                                                    return <BundleRefund
                                                        BundleData={BundleData}
                                                        expend={expend}
                                                        data_bundle={data_bundle}
                                                        product_qty={data_bundle.quantity}
                                                        is_autoship={datas?.is_autoship}
                                                        key={ind + index + 1}
                                                    />
                                                })}
                                            </>
                                            :
                                            <ProductRefund datas={datas} product_qty={product_qty} key={ind + 1} />
                                        }
                                    </>)
                            })}
                            <tr>
                                <PaymentTitleSectionHtml text="SUBTOTAL" numberValue={data?.amount} /></tr>
                            {data?.discount_amount > 0 && <>
                                <tr>
                                    <PaymentTitleSectionHtml text={`Discount (${data?.coupon_name !== undefined ? (data.coupon_name) : ''})`}
                                        numberValue={data?.discount_amount}
                                    />
                                </tr>
                            </>}
                            {data?.shipping_amount > 0 && <>
                                <tr><PaymentTitleSectionHtml text="Shipping Amount" numberValue={data?.shipping_amount} /></tr>
                            </>}
                            <tr><PaymentTitleSectionHtml text="Total" numberValue={data?.gross_total} /></tr>
                        </tbody>
                    </table>
                </div>
            </div>
            {/* {refundHistoryData && <RefundHistory data={data} refundHistoryData={refundHistoryData} />} */}
            <div className="row">
                <div className="col-md-6">
                    <button className="re-order" onClick={(e) => {
                        Backfromhere()
                    }} >Back</button>
                </div>

            </div>
        </div>
    </>)
}