import NumberFormat from 'react-number-format';
import ReactPaginate from 'react-paginate';
import moment from "moment"
import NumberFormatComp from '@Components/Common/NumberFormatComp';
import ReactpaginateComp from '@Components/Common/ReactPaginateComp';

const FormData = ({ setFirstDate, setLastDate, allcommissiondata, commissionData, commissionDataError, handlePageClick, pageCount, ShoworderDetails, CsvData }) => {
    let oldDate = ''
    allcommissiondata?.map((comision, index) => {
        if (index === 0) {
            setFirstDate(moment(comision?.created_at, "ddd MMM DD YYYY HH:mm:ssZ").format(`${process.env.Date_Format}`))
        }
        setLastDate(moment(comision?.created_at, "ddd MMM DD YYYY HH:mm:ssZ").format(`${process.env.Date_Format}`))

        CsvData.push({
            "Name": moment(comision?.created_at, "ddd MMM DD YYYY HH:mm:ssZ").format(`${process.env.Date_Format}`),
            "Order Info": '',
            "Product Info": '',
            "Price": '',
            "Bonusable Volume": '',
            "Commission": '',
            "Status": ''

        })
        CsvData.push({
            "Name": `${comision?.for_order?.user?.first_name}  ${comision?.for_order?.user?.last_name}`,
            "Order Info": `ORD${comision?.for_order?.id}`,
            "Product Info": '',
            "Price": '',
            "Bonusable Volume": '',
            "Commission": '',
            "Status": ''

        })
        comision?.user_commission_detail.map((comisionprd, index1) => {
            CsvData.push({
                "Name": '',
                "Order Info": '',
                "Product Info": comisionprd?.product?.name,
                "Price": `$${parseFloat(+comisionprd?.price_per_unit * +comisionprd?.product_quantity).toFixed(2)}`,
                "Bonusable Volume": `BV $${parseFloat(comisionprd?.product_bonus_value).toFixed(2)}`,
                "Commission": `$${parseFloat(+comisionprd?.commission).toFixed(2)} - {(comision?.commission_percentage)} %`,
                "Status": `${comisionprd?.is_approved === false ?
                    'Pending' : 'Paid'
                    }`
            })
        })
    })
    return (
        <>
            <table className="commission-table">
                <thead>
                    <tr>
                        <th> DATE</th>
                        <th> NAME</th>
                        <th>ORDER INFO</th>
                        <th>PRODUCT INFO</th>
                        <th>PRICE</th>
                        <th>BONUSABLE VOLUME</th>
                        <th>COMMISSION</th>
                        <th>STATUS</th>
                    </tr>
                </thead>
                <tbody>
                    {commissionDataError &&
                        <tr className="title_error">
                            <td colSpan="8" className="error-commision">
                                {commissionDataError}</td>
                        </tr>}

                    {commissionData ?
                        commissionData?.map((comision, index) => {
                            return (
                                <>
                                    {oldDate !== comision?.created_at &&
                                        <tr className="date">
                                            <td data-value="DATE"> {comision?.created_at}</td>
                                            <td data-value="NAME">
                                                {`${comision?.for_order?.user?.first_name}  ${comision?.for_order?.user?.last_name}`}

                                                {` (L${comision?.commission_level})`}
                                            </td>
                                            <td data-value="ORDER INFO" className="clickable" onClick={() => {
                                                ShoworderDetails(comision?.for_order?.id)
                                            }}>
                                                {comision?.for_order?.public_order_id}
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    }

                                    {
                                        comision?.user_commission_detail?.map((comisionprd, index1) => {
                                            oldDate = comision?.created_at;
                                            return (
                                                <>
                                                    <tr>
                                                        {oldDate !== comision?.created_at &&
                                                            <tr className="date">
                                                                <td> {comision?.created_at}</td>
                                                                <td>
                                                                    {`${comision?.for_order?.user?.first_name}  ${comision?.for_order?.user?.last_name}`}
                                                                    <br />
                                                                    {`(Level${comision?.commission_level} )`}
                                                                </td>
                                                                <td className="clickable" onClick={() => {
                                                                    ShoworderDetails(comision?.for_order?.id)
                                                                }}>
                                                                    {comision?.for_order?.public_order_id}
                                                                </td>
                                                                <td data-value="PRODUCT INFO"></td>
                                                                <td data-value="PRICE"></td>
                                                                <td data-value="BONUSABLE VOLUME"></td>
                                                                <td data-value="COMMISSION"></td>
                                                                <td data-value="STATUS"></td>
                                                            </tr>
                                                        }
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>

                                                        <td data-value="PRODUCT INFO">{comisionprd?.product?.name}</td>
                                                        <td data-value="PRICE"> <NumberFormatComp value={+comisionprd?.price_per_unit * +comisionprd?.product_quantity} /></td>
                                                        <td data-value="BONUSABLE VOLUME"> <NumberFormatComp value={comisionprd?.product_bonus_value} /></td>
                                                        <td data-value="COMMISSION">

                                                            <NumberFormatComp value={+comisionprd?.commission} />{`- ${comision?.commission_percentage}%`}

                                                        </td>
                                                        <td data-value="STATUS">
                                                            {comisionprd?.is_approved === false ?
                                                                'Pending' : 'Paid'
                                                            }
                                                        </td>

                                                    </tr>
                                                </>
                                            )
                                        })

                                    }
                                </>)


                        })
                        :
                        <tr><td colSpan={8}>No record found</td></tr>
                    }
                </tbody>
            </table>
            {
                commissionDataError === undefined &&
                <ReactpaginateComp pageCount={pageCount} handlePageClick={handlePageClick} />
            }
        </>
    )
}
export default FormData;