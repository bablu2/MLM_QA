import styled from "styled-components";

const RefundOrderStyle = styled.div`
  && .main_refund_section_class {
    width: 100%;
    max-width: 1338px;
    margin: 0 auto;
    .Cart_product.order-detail-sec.refund_detail_section.cstm-refund {
      margin-top: 0;
      border: 1px solid #ddd;
      @media (max-width: 767px) {
        border: none;
        .row.order-cstm {
          padding: 0;
        }
      }
      @media (max-width: 767px) {
        tr td:empty,
        tr:empty {
          display: none;
        }
      }
    }
    .refund_detail_section {
      width: 100%;
      max-width: 100%;
      margin: 40px auto 0;
      table {
        width: 100%;
        border: none;
        thead {
          tr {
            th {
              z-index: 9;
              background: #06356a;
              font-size: 15px;
              padding: 15px;
              font-weight: 600;
              line-height: 1;
            }
          }
        }
        tbody {
          tr {
            /* border-bottom: 1px solid #ddd; */
            td {
              position: relative;
              font-size: 14px;
              padding: 15px;
              border: none;
              &:first-child {
                border: none;
                text-align: center;
              }
              .cart-product-details {
                display: unset;
              }
              .main-qty-sec {
                margin: 0;
                display: flex;
                justify-content: center;
              }
              #qty {
                border: 1px solid #ddd;
                width: auto;
                flex: 0 0 auto !important;
                display: flex;
                height: 34px;
                min-width: 130px;
                margin: 0 auto;
                justify-content: center;
                button {
                  width: 35px;
                  position: relative;
                  padding: 4px !important;
                }
                input {
                  background: #fff;
                  width: calc(120px - 64px) !important;
                  height: 32px !important;
                  border-left: 1px solid #ddd;
                  border-right: 1px solid #ddd;
                }
                .sub {
                  left: auto;
                  top: auto !important;
                  position: relative;
                  font-size: 20px;
                  bottom: auto;
                  border-radius: 0px;
                  @media (max-width: 767px) {
                    padding: 0 !important;
                  }
                }
                .add {
                  top: auto !important;
                  position: relative;
                  margin: 0;
                  border-radius: 0;
                  font-size: 20px;
                  @media (max-width: 767px) {
                    padding: 0 !important;
                  }
                }
              }
            }
          }
        }
      }
    }
    .order-cstm {
      table {
        tr {
          th {
            text-align: left;
          }
        }
      }
    }
    .btn-sec-cstm {
      .refund_button_class {
        float: right;
      }
    }
  }
  .refund {
    .row.order-cstm {
      margin-top: 40px;
    }
  }

  @media (max-width: 991px) {
    .row.order-cstm {
      padding: 0 10px;
    }
    .Cart_product.order-detail-sec.reorder_detail_section {
      margin: 0 13px !important;
      width: 96.5% !important;
    }
    .row.btn-sec-cstm {
      margin: 010px 0;
    }
    .order-cstm [class*="col-md"]:first-child {
      margin-bottom: 20px;
    }
  }
  @media (max-width: 480px) {
    .main_reorder_section_class {
      margin: 0 10px !important;
      width: 95% !important;
    }
  }

  .main-button-group {
    margin-top: 15px;
  }
`;

const RefundHistoryStyle = styled.div`
  && .main_reorder_section_class {
  }
`;

const ReOrderStyle = styled.div`
  && .main_reorder_section_class {
    width: 100%;
    max-width: 1338px;
    margin: 0 auto;

    .reorder_detail_section {
      width: 100%;
      max-width: 100%;
      margin: 40px auto 0;
      table {
        width: 100%;
        thead {
          tr {
            th {
              border-bottom: 1px solid #ddd;
            }
          }
        }
        tbody {
          tr {
            td {
              padding: 14px;
              border-bottom: 1px solid #ddd;
              position: relative;
              .cart-product-details {
                display: unset;
              }
              .price_amount {
                /* position: absolute;
                            right: 103px;
                            top: 13px; */
                text-align: center;
              }
            }
          }
        }
      }
    }
    .order-cstm {
      table {
        tr {
          th {
            text-align: left;
            padding: 10px 20px;
          }
          td {
            padding: 10px 20px;
          }
        }
      }
    }
    .btn-sec-cstm {
      .refund_button_class {
        float: right;
      }
    }
  }
`;
export default RefundOrderStyle;
export { RefundHistoryStyle, ReOrderStyle };
