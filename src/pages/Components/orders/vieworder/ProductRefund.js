import { defaultTo } from "lodash"
import NumberFormat from 'react-number-format';

const ProductRefund = ({ datas, product_qty, customerCheck }) => {
    return (
        <tr>
            <td>
                {datas?.product_name} {datas?.variant_name != '' && ('(' + datas?.variant_name + ')')}

            </td>
            {customerCheck > 2 &&
                <td>
                    {datas?.bonus_value}
                </td>
            }
            <td>
                <input
                    name={`${datas?.product},${datas?.variant}`}
                    type="text"
                    className="product-qty"
                    value={datas?.product_quantity}
                    readOnly
                />
            </td>
            <td>
                <NumberFormat value={parseFloat(datas?.price_per_unit * datas?.product_quantity).toFixed(2)}
                    displayType={'text'}
                    thousandSeparator={true}
                    prefix={'$'}
                    renderText={value => <div>{value} USD</div>} />
            </td>
        </tr>

    )
}
export default ProductRefund;