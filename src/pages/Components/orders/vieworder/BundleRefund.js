import NumberFormat from "react-number-format"

const BundleRefund = ({ expend, data_bundle, product_qty, is_autoship, bonus_value }) => {

    return (
        <>
            {data_bundle?.product_variant === null ?
                expend &&
                <tr>
                    <td data-value="NMAEEEEEE">
                        {data_bundle?.product?.name}
                    </td>
                    <td>{bonus_value}</td>
                    <td>
                        <input
                            name={`${data_bundle?.product},${data_bundle?.variant}`}
                            type="text"
                            className="product-qty"
                            value={product_qty}
                            readOnly
                        />
                    </td>
                    <td>
                        <NumberFormat value={parseFloat(data_bundle?.quantity * is_autoship === false ? data_bundle?.product?.cost_price : data_bundle?.product?.is_autoship).toFixed(2)}
                            displayType={'text'}
                            thousandSeparator={true}
                            prefix={'$'}
                            renderText={value => <div>{value}</div>}
                        />
                    </td>
                </tr>
                :
                expend &&
                <tr>
                    <td data-value="name">
                        {data_bundle?.product?.name} {data_bundle?.product_variant?.name != '' && ('(' + data_bundle?.product_variant?.name + ')')} X {data_bundle?.quantity}
                    </td>
                    <td>
                        {data_bundle?.bonus_value}
                    </td>
                    <td>
                        <input
                            name={`${data_bundle?.product},${data_bundle?.variant}`}
                            type="text"
                            className="product-qty"
                            value={product_qty}
                            readOnly
                        />
                    </td>
                    <td>
                        <NumberFormat value={parseFloat(data_bundle?.quantity * is_autoship === false ? data_bundle?.product_variant?.cost_price : data_bundle?.product_variant?.is_autoship).toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <div>{value}</div>} />
                    </td>
                </tr>
            }
        </>
    )
}
export default BundleRefund;