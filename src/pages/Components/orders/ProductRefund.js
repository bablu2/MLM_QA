import { defaultTo } from "lodash"
import NumberFormat from 'react-number-format';

const ProductRefund = ({ datas, Add, Sub, register, product_qty }) => {
    return (
        <tr>
            <td>
                <input ref={register && register({ required: false })} defaultValue={`${datas?.product},${datas?.variant},${product_qty}`} type="checkbox" name="refundprod" />
            </td>
            <td data-value="Product" className="">
                {datas?.product_name} {datas?.variant_name != '' && ('(' + datas?.variant_name + ')')} X {datas?.product_quantity}
            </td>
            <td data-value="Qty" className="qty-sec" >
                <div className="main-qty-sec">
                    <div className="box">
                        <div id="qty">
                            <button type="button"
                                id={product_qty}
                                className="sub"
                                onClick={(e) => { Sub(e, e.target.id, datas?.product) }}>-
                            </button>
                            <input
                                name={`${datas?.product},${datas?.variant}`}
                                type="text"
                                className="product-qty"
                                value={product_qty}
                                ref={register && register({ required: false })}
                                readOnly
                            />
                            <button type="button"
                                id={product_qty}
                                onClick={(e) => { Add(e, e.target.id, datas?.product, datas?.product_quantity) }}>+</button>
                        </div>
                    </div>
                </div>
            </td>
            <td data-value="Total" className=" ">
                <div className="">
                    <NumberFormat value={parseFloat(datas?.price_per_unit * datas?.product_quantity).toFixed(2)}
                        displayType={'text'}
                        thousandSeparator={true}
                        prefix={'$'}
                        renderText={value => <div>{value} USD</div>} />
                </div>
            </td>
        </tr>

    )
}
export default ProductRefund;