import _ from 'lodash';
import { RefundOrderStyle, RefundHistoryStyle } from './Style'

const RefundHtml = ({ number }) => {
    const textTitle = ["Refund Status", "Result", "Refunded Amount", "Refund Date"];
    let classValue = "";
    return (
        <>
            <h4 className="tittle">Order Refund History</h4>
            <div className="row">
                {textTitle?.slice(0, number)?.map((value, index) => {
                    if (number <= 2 && index === 0) {
                        classValue = "col-md-8";
                    } else if (number <= 2 && index === 1) {
                        classValue = "col-md-4";
                    } else {
                        classValue = "col-md-3";
                    }
                    return (
                        <div className={classValue} key={index + 1}> <div className="pro-name">{value}</div></div>
                    )
                })}
            </div>
        </>
    )
}

const RefundOrderComp = ({ number, refundOrderData, section }) => {
    return (
        <div className="row">
            {(number <= 2) ?
                <>
                    <div className="col-md-8">
                        Refund Status for Product ID {refundOrderData?.id}
                    </div>
                    <div className="col-md-4">{refundOrderData?.status}</div>
                </>
                :
                <>
                    <div className="col-md-3">
                        Refund Status for Product ID {refundOrderData?.id}
                    </div>
                    <div className="col-md-3">{refundOrderData?.status}</div>
                    <div className="col-md-3">{refundOrderData?.decOrRefund}</div>
                    <div className="col-md-3">{refundOrderData?.createAt}<br /></div>
                </>
            }
        </div>
    )
}

const RefundHistory = ({ refundHistoryData, data }) => {
    return (
        <>
            <RefundHistoryStyle>
                <div className="container refund_history_class">
                    <div className="Cart_product order-detail-sec">
                        {(() => {
                            let _number = 0;
                            if (data?.status) {
                                switch (data?.status) {
                                    case "refund raised":
                                        _number = 2;
                                        break;
                                    case "refund approved":
                                        _number = 4;
                                        break;
                                    case "refund declined":
                                        _number = 4;
                                        break;
                                }
                            }
                            return _number > 0 ? <RefundHtml number={_number} /> : "";
                        })()}

                        {refundHistoryData?.map((refundOrderData, index) => {
                            let sectionObject = {};
                            let _number = 0;
                            if (refundOrderData?.refund_status) {
                                let declineArray = {
                                    id: refundOrderData?.id,
                                    status: refundOrderData?.refund_status,
                                    decOrRefund: refundOrderData?.decline_reason,
                                    createAt: refundOrderData?.created_at.slice(0, 10)
                                };
                                let data = _.assign(data, { decOrRefund: refundOrderData?.refunded_amount })
                                sectionObject = refundOrderData?.refund_status === "decline" ? declineArray : data;
                                switch (refundOrderData?.refund_status) {
                                    case "pending":
                                        sectionObject = { id: refundOrderData?.id, status: refundOrderData?.refund_status };
                                        _number = 2;
                                        break;
                                    case "decline":
                                    case "full order":
                                    case "full product":
                                    case "partial":
                                    case "full":
                                        _number = 3;
                                        break;

                                }
                                return _number !== 0 ? <RefundOrderComp refundOrderData={sectionObject} number={_number} key={index + 1} /> : ""
                            } else {
                                return "";
                            }
                        })}
                    </div>
                </div>
            </RefundHistoryStyle>
        </>
    )
}

export default RefundHistory;