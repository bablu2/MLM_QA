

import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react';
import api from '../../../api/Apis';
import NumberFormat from 'react-number-format';
import BundleRefund from './vieworder/BundleRefund';
import ProductRefund from './vieworder/ProductRefund';
import RefundHistory from './Refundhistory';
import AddressesSection from '../../../Components/Common/AddressesSection';
import ReviewComponent from '@Components/Common/RatingSection/ReviewComponent';
import RefundOrder from './RefundOrder';
import PaymentTitleSectionHtml from '@Components/Common/PaymentTitleSectionHtml';
import { KeyboardArrowRight, KeyboardArrowDown } from '@material-ui/icons';

export default function ViewOrderDetails({ orderid, setorderid, customerCheck, setshowdetailsorder }) {
    const router = useRouter();
    const [data, setdata] = useState()
    const [refundHistoryData, setRefundHistoryData] = useState()
    const [Logintoken, setLogintoken] = useState()
    const [BundleData, setBundleData] = useState()
    const [order_id, setOrderId] = useState();
    const [expend, setExpend] = useState(false);
    const [productType, setProductType] = useState("");
    const [reorderData, setReorderData] = useState()
    useEffect(() => {
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLogintoken(token)
        const update_data = {
            token: token,
            order_id: orderid
        }
        api.getOrderDetailForRefund(update_data).then(res => {
            if (res?.data.code === 1) {
                setdata(res?.data?.orders)
            }
        })

        api.getRefundHistory(orderid, token).then(res => {
            if (res?.data.code === 1) {
                setRefundHistoryData(res?.data?.refund_details)
            }
        })

    }, [orderid, order_id]);

    const Backfromhere = (e) => {
        setorderid()
        setshowdetailsorder(false)
    }

    const Refund = (id) => { setOrderId(orderid); }
    let product_qty;

    const update_data = {
        token: Logintoken,
        order_id: orderid
    }

    function handleReorder() {
        api.GetOrderDetail(update_data).then((res) => {
            if (res?.data?.code === 1) {
                setReorderData(res?.data)
            }
        })
        const reorderdata = {
            token: Logintoken,
            order_id: orderid
        }
        api.reorderProducts(reorderdata).then(res => {
            if (res?.data?.code === 1) {
                router.push(`/${router.query.page}/checkout/addresslist`)
            }
        })
    }

    return (
        <>
            <Head>
                <title>Order Details</title>
            </Head>
            {order_id ?
                <RefundOrder orderid={order_id} setorderid={setOrderId} />
                :
                <>
                    <div className="heading-top-main right-address">
                        <div className="heading-main-text">
                            <h1>Order #{data?.public_order_id}</h1>
                        </div>
                    </div>
                    <div className="main_order_detail_class">
                        <div className="row order-cstm">
                            <div className="col-md-6">
                                <div className="thn-lft thn-rgt raido-addres-sel">
                                    <h2>BILLING ADDRESS</h2>
                                    <AddressesSection addressdata={data} type="billing_address" />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="thn-lft thn-rgt raido-addres-sel">
                                    <h2>SHIPPING ADDRESS</h2>
                                    <AddressesSection addressdata={data} type="shipping_address" />
                                </div>
                            </div>
                        </div>
                        <br /><br />
                        <div className="Cart_product order-detail-sec">
                            <h4 className="tittle">Order details</h4>
                            <div className="orer-details-tables">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>PRODUCT</th>
                                            {customerCheck > 2 &&
                                                <th>BV</th>}
                                            <th>QTY</th>
                                            <th>TOTAL</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {data?.order_details.map((datas, ind) => {
                                            let is_variant;

                                            try {
                                                is_variant = JSON.parse(datas?.metadata)
                                            }
                                            catch {
                                                is_variant = ''
                                            }
                                            const product_type = datas?.product_type
                                            if (product_type === 'Bundle') {
                                                api.getBundleProduct(datas?.product, Logintoken).then(res => {
                                                    const datassss = res?.data?.bundle_data;
                                                    setProductType(product_type);
                                                    if (BundleData === undefined) {
                                                        setBundleData(datassss);
                                                    }
                                                })
                                            }
                                            return (
                                                <>
                                                    {product_type === 'Bundle' ?
                                                        <>
                                                            <tr className="bundale_title">
                                                                <td data-value="NAMWEEEEE">
                                                                    <span>
                                                                        {!expend ?
                                                                            <KeyboardArrowRight sx={{ fontSize: 20, position: "relative", top: "5px" }} onClick={() => setExpend(true)} />
                                                                            :
                                                                            <KeyboardArrowDown sx={{ fontSize: 20, position: "relative", top: "5px" }} onClick={() => setExpend(false)} />
                                                                        }
                                                                    </span>
                                                                    <product_typespan>{datas?.product_name}</product_typespan>
                                                                </td>
                                                                <td >{datas?.bonus_value}</td>
                                                                <td >{datas?.product_quantity}</td>
                                                                <td> {datas?.price_per_unit} </td>

                                                            </tr>
                                                            {BundleData?.map((data_bundle, index) => {
                                                                return <BundleRefund
                                                                    BundleData={BundleData}
                                                                    expend={expend}
                                                                    data_bundle={data_bundle}
                                                                    product_qty={data_bundle.quantity}
                                                                    is_autoship={datas?.is_autoship}
                                                                    key={ind + index + 1}
                                                                />
                                                            })}
                                                        </>
                                                        :
                                                        <ProductRefund datas={datas} product_qty={product_qty} key={ind + 1} customerCheck={customerCheck} />
                                                    }
                                                </>)
                                        })}
                                        <tr>
                                            {customerCheck > 2 ?
                                                <PaymentTitleSectionHtml text="Subtotal" numberValue={data?.amount} colSpan="3" />
                                                :
                                                <PaymentTitleSectionHtml text="Subtotal" numberValue={data?.amount} colSpan="2" />
                                            }
                                        </tr>
                                        {data?.discount_amount > 0 && <>
                                            <tr>
                                                {customerCheck > 2 ?
                                                    <PaymentTitleSectionHtml text={`Discount (${data?.coupon_name !== undefined ? (data.coupon_name) : ''})`}
                                                        numberValue={data?.discount_amount} colSpan="3"
                                                    />
                                                    :
                                                    <PaymentTitleSectionHtml text={`Discount (${data?.coupon_name !== undefined ? (data.coupon_name) : ''})`}
                                                        numberValue={data?.discount_amount} colSpan="2"
                                                    />
                                                }


                                            </tr>
                                        </>}
                                        {data?.shipping_amount > 0 && <>
                                            <tr>
                                                {customerCheck > 2 ?
                                                    <PaymentTitleSectionHtml text="Shipping Amount" numberValue={data?.shipping_amount} colSpan="3" />
                                                    :
                                                    <PaymentTitleSectionHtml text="Shipping Amount" numberValue={data?.shipping_amount} colSpan="2" />
                                                }

                                            </tr>
                                        </>}
                                        <tr>
                                            {customerCheck > 2 ?
                                                <PaymentTitleSectionHtml text="Total" numberValue={data?.gross_total} colSpan="3" />
                                                :
                                                <PaymentTitleSectionHtml text="Total" numberValue={data?.gross_total} colSpan="2" />
                                            }
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        {data?.order_details.map(({ product_name, product, ...data }, index) => {
                            if (product_name !== "MEMBERSHIP") {
                                return <ReviewComponent productId={product} productName={product_name} key={index + 1} />
                            }
                        })}
                        {refundHistoryData && <RefundHistory data={data} refundHistoryData={refundHistoryData} />}

                        <div className="main-button-group">
                            <div className="d-flex">
                                <button className="re-order" onClick={() => handleReorder()} >Reorder</button>

                                <button className="refund_button_class" onClick={() => { Refund(data?.id) }}>Refund Request</button>
                                <button className="re-order" onClick={(e) => {
                                    Backfromhere()
                                }} >Back</button>
                                {/*  {
                                    data?.status === 'completed' ?
                                      
                                        :
                                        <button className=" refund_button_class disabled">Refund </button>
                                }*/}
                            </div>
                        </div>
                    </div>
                </>}
        </>)
}