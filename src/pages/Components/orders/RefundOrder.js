import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react';
import api from '../../../api/Apis';
import { useForm } from 'react-hook-form';
import NProgress from 'nprogress'
import { toast } from 'react-toastify';
import BundleRefund from './BundleRefund';
import ProductRefund from './ProductRefund';
import PaymentTitleSectionHtml from '@Components/Common/PaymentTitleSectionHtml';
import RefundOrderStyle from './Style'

export default function RefundOrder({ orderid, setorderid }) {
    const router = useRouter();
    const [data, setdata] = useState()
    const [Logintoken, setLogintoken] = useState()
    const [order_refund, setorder_refund] = useState([])
    const [order_refund_variant, setorder_refund_variant] = useState([])
    const [BundleData, setBundleData] = useState()

    useEffect(() => {
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLogintoken(token)
        const update_data = {
            token: token,
            order_id: orderid
        }
        api.getOrderDetailForRefund(update_data).then(res => {
            if (res?.data.code === 1) {
                setdata(res?.data?.orders)
            }
        })
    }, [orderid]);

    // add product qty on refund time
    const Add = (e, qty, product_id, max_qty, type, bundle_id) => {
        if (type === 'variant_refund') {
            if (qty < max_qty) {
                const objIndex = order_refund_variant.findIndex((obj => obj.product_id == product_id && obj.bundle_id == bundle_id));
                if (objIndex !== -1) {
                    const data = order_refund_variant[objIndex].qty = +order_refund_variant[objIndex].qty + 1
                    order_refund_variant.splice(objIndex, 1);
                    setorder_refund_variant([...order_refund_variant, { product_id: product_id, qty: data, bundle_id: bundle_id }])
                }
                else {
                    setorder_refund_variant([...order_refund_variant, { product_id: product_id, qty: qty + 1, bundle_id: bundle_id }])
                }
            }
            else {
                // toast.error('Refund quantity should be atleast 1', {
                //     duration: 1
                // })
            }
        }
        else {
            if (qty < max_qty) {
                const objIndex = order_refund.findIndex((obj => obj.product_id == product_id));
                if (objIndex !== -1) {
                    const data = order_refund[objIndex].qty + 1
                    order_refund.splice(objIndex, 1);
                    setorder_refund([...order_refund, { product_id: product_id, qty: data }])
                }
                else {
                    setorder_refund([...order_refund, { product_id: product_id, qty: qty + 1 }])
                }
            }
            else {
                // toast.error('Max qty is already selected', { hideProgressBar: true, duration: 1 })
            }
        }
    }

    // substract product qty during refund
    const Sub = (e, qty, product_id, type, bundle_id) => {
        if (type === 'variant_refund') {
            if (qty > 1) {
                const objIndex = order_refund_variant.findIndex((obj => obj.product_id == product_id && obj.bundle_id == bundle_id));
                if (objIndex !== -1) {

                    const data = order_refund_variant[objIndex].qty = +order_refund_variant[objIndex].qty - 1

                    order_refund_variant.splice(objIndex, 1);

                    setorder_refund_variant([...order_refund_variant, { product_id: product_id, qty: data, bundle_id: bundle_id }])
                }
                else {
                    setorder_refund_variant([...order_refund_variant, { product_id: product_id, qty: qty - 1, bundle_id: bundle_id }])
                }
            }
            else {
                // toast.error('Refund quantity should be atleast 1', { hideProgressBar: true, duration: 1 })
            }
        }
        else {
            if (qty > 1) {
                const objIndex = order_refund.findIndex((obj => obj.product_id == product_id));
                if (objIndex !== -1) {
                    const data = order_refund[objIndex].qty = +order_refund[objIndex].qty - 1
                    order_refund.splice(objIndex, 1);
                    setorder_refund([...order_refund, { product_id: product_id, qty: data }])
                }
                else {
                    setorder_refund([...order_refund, { product_id: product_id, qty: qty - 1 }])
                }
            }
            else {
                // toast.error('Refund quantity should be atleast 1', { hideProgressBar: true, duration: 1 })
            }
        }
    }
    const { register, handleSubmit, errors } = useForm({
        mode: "all"
    });

    // onclik refund this will be execute
    const onSubmit = data => {
        let data_refnd = [];
        if (Array.isArray(data?.refundprod) === false && data?.refundprod != '') {
            const data_refunds = data?.refundprod.split(',');
            data_refnd.push({ product_id: +data_refunds[0], variant_id: data_refunds[1] !== "null" ? +data_refunds[1] : null, quantity: +data_refunds[2] })

        }
        else if (Array.isArray(data?.refundprod) === true) {
            data?.refundprod?.map((refunddata) => {
                const data_refunds = refunddata.split(',');
                data_refnd.push({ product_id: +data_refunds[0], variant_id: data_refunds[1] !== "null" ? +data_refunds[1] : null, quantity: +data_refunds[2] })
            })
        }
        const final_data = {
            order_id: +orderid,
            reason: data?.refund,
            products: data_refnd
        }
        // const data1={"order_id":157,"reason":"rdtg","products":[{"product_id":25,"variant_id":null,"quantity":1}]}
        {
            data_refnd.length >= 1 &&
                api.refundOrder(final_data, Logintoken).then(res => {
                    if (res?.data?.code === 1) {
                        NProgress.done()
                        // toast.success(res?.data?.message, { hideProgressBar: true, duration: 1 })
                        setorderid()
                    }
                })
            // :
            // toast.error('Select atleast one product before refund request.', { hideProgressBar: true, duration: 1 })
        }
    }

    let product_qty;
    return (<>
        <Head>
            <title>Order Details</title>
        </Head>
        <div>
            <div className="heading-top-main right-address">
                <div className="heading-main-text">
                    <h1>Order #{data?.public_order_id}</h1>
                </div>
            </div>
            <RefundOrderStyle>
                <div className="main_refund_section_class">
                    <form className="refund" onSubmit={handleSubmit(onSubmit)}>
                        <div className="Cart_product order-detail-sec refund_detail_section cstm-refund">
                            <h4 className="tittle">Order details</h4>
                            <table className="commission-table">
                                <thead className="">
                                    <tr>
                                        <th></th>
                                        <th>Product</th>
                                        <th>Qty</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {data?.order_details.map((datas) => {
                                        let is_variant;
                                        const objIndex = order_refund?.findIndex((obj => obj?.product_id == datas?.product));
                                        product_qty = (order_refund[objIndex]?.product_id === datas?.product) ? (order_refund[objIndex]?.qty) : (datas?.product_quantity)
                                        try {
                                            is_variant = JSON.parse(datas?.metadata)
                                        }
                                        catch {
                                            is_variant = ''
                                        }
                                        const product_type = is_variant[0]?.fields?.product_type
                                        if (product_type === 'Bundle') {
                                            api.getBundleProduct(datas?.product, Logintoken).then(res => {
                                                const datassss = res?.data?.bundle_data;
                                                if (BundleData === undefined) {
                                                    setBundleData(datassss)
                                                }
                                            })
                                        }
                                        return (
                                            <>
                                                {product_type === 'Bundle' ?
                                                    <div className="bundle-product">
                                                        <h1 className="title">{datas?.product_name}</h1>
                                                        {BundleData?.map((data_bundle, index) => {
                                                            return <BundleRefund bundle_id={datas?.product}
                                                                is_autoship={datas?.is_autoship}
                                                                order_refund_variant={order_refund_variant}
                                                                order_refund={order_refund}
                                                                BundleData={BundleData}
                                                                data_bundle={data_bundle}
                                                                register={register}
                                                                Add={Add}
                                                                Sub={Sub}
                                                                product_qty={data_bundle.quantity} />
                                                        })}
                                                    </div>
                                                    :
                                                    <ProductRefund datas={datas} Add={Add} Sub={Sub} register={register} product_qty={product_qty} />
                                                }
                                            </>)
                                    })}
                                    <tr className="Sub-total">
                                        <td></td>
                                        <PaymentTitleSectionHtml text={"Subtotal"} numberValue={data?.amount} />
                                    </tr>
                                    <tr className="Sub-total">
                                        {data?.discount_amount > 0 &&
                                            <>
                                                <td></td>
                                                <PaymentTitleSectionHtml text={`Discount ${data?.coupon_name !== undefined ? (data.coupon_name) : ''}`} numberValue={data?.discount_amount} />
                                            </>
                                        }
                                    </tr>
                                    <tr className="Sub-total">
                                        <td></td>
                                        <PaymentTitleSectionHtml text={"Total"} numberValue={data?.amount_paid} />
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="row order-cstm">
                            <div className="col-md-12">
                                <div className="thn-lft">
                                    <h4>Reason For Refund</h4>
                                    <textarea name="refund"
                                        ref={register({ required: "Please enter reason" })}
                                        placeholder="Please add your message here...."
                                        className="refund-message-box">
                                    </textarea>
                                    {errors?.refund && <span className="error">{errors.refund?.message}</span>}
                                </div>
                            </div>
                        </div>
                        <div className="main-button-group">
                            <div className="d-flex">
                                <button className="re-order" onClick={() => { setorderid() }} >Back</button>
                                <button type="submit" className="re-order refund_button_class">Refund</button>
                            </div>
                        </div>
                    </form>
                </div>

            </RefundOrderStyle>
        </div>
    </>)
}