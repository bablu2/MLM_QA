
import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react';
import api from '../../../api/Apis';
import NumberFormat from 'react-number-format';
import { ReOrderStyle } from './Style';
import PaymentTitleSectionHtml from '@Components/Common/PaymentTitleSectionHtml';
import AddressesSection from '@Components/Common/AddressesSection';
import { KeyboardArrowDown, KeyboardArrowRight } from "@material-ui/icons";

export default function Reorder({ orderid, setShowreorder, setorderid }) {
    const router = useRouter();
    const [data, setdata] = useState()
    const [expend, setExpend] = useState(false);
    const [bundleProduct, setBundleProduct] = useState({});

    const [Logintoken, setLogintoken] = useState()
    useEffect(() => {
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLogintoken(token)
        const update_data = {
            token: token,
            order_id: orderid
        }
        api.GetOrderDetail(update_data).then(res => {
            if (res?.data?.code === 1) {
                setdata(res?.data?.orders);
                const onlyBundleProduct = _.reject(res?.data?.orders?.order_details, (row) => row?.product_type !== "Bundle");
                _.map(onlyBundleProduct, (row) => {
                    _getBundleData(row?.product_type, +row?.product_id);
                })
            }
        })
    }, [orderid]);
    // reorder
    const Reorder = () => {
        const reorderdata = {
            token: Logintoken,
            order_id: orderid
        }
        api.reorderProducts(reorderdata).then(res => {
            if (res?.data?.code === 1) {
                router.push(`/${router.query.page}/checkout/addresslist`)
            }
        })
    }
    function _getBundleData(productType = "Product", productId) {
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        if (productType === 'Bundle') {
            api.getBundleProduct(productId, token).then(res => {
                if (res?.data?.code === 1) {
                    setBundleProduct({ ...bundleProduct, [productId]: res?.data?.bundle_data })
                }
            }).catch((err) => {
                console.log("bbbb", err);
            })
        }
    }
    return <>
        <Head>
            <title>Order Details</title>
        </Head>
        <ReOrderStyle>
            <>
                <div className="heading-top-main right-address">
                    <div className="heading-main-text">
                        <h1>Order #{data?.public_order_id}</h1>
                    </div>
                </div>
                <div className="main_reorder_section_class">
                    <div className="Cart_product order-detail-sec reorder_detail_section">
                        <h4 className="tittle">Order details</h4>
                        <table>
                            <thead className=" ">
                                <tr>
                                    <th className=" ">PRODUCT IMAGE</th>
                                    <th className=" ">PRODUCT</th>
                                    <th className=" ">BV</th>
                                    <th className=" ">TOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                {data?.order_details.map((datas) => {
                                    return (
                                        <>
                                            <tr className={`${datas.product?.is_active === 'True' ? 'active' : 'not-active'}`} >
                                                <td className='' data-value="PRODUCT IMAGE">
                                                    <div className=''>
                                                        {(datas?.product_type === "Bundle") &&
                                                            <span>
                                                                {!expend ?
                                                                    <KeyboardArrowRight sx={{ fontSize: 20, position: "relative", top: "5px" }} onClick={() => setExpend(true)} />
                                                                    :
                                                                    <KeyboardArrowDown sx={{ fontSize: 20, position: "relative", top: "5px" }} onClick={() => setExpend(false)} />
                                                                }
                                                            </span>
                                                        }
                                                        <span className="cart-product-details">

                                                        </span>
                                                        <img src={process.env.API_URL + datas?.product?.product_images?.[0]?.image}
                                                            style={{ 'width': '100%', 'maxWidth': '54px', 'maxheight': '100px' }}
                                                        />
                                                    </div>
                                                </td>
                                                <td className=" " data-value="PRODUCT">
                                                    <div className="cart-product-details">
                                                        {datas.product?.name} X {datas.product_quantity}
                                                    </div>

                                                </td>
                                                <td className=" " data-value="BV">
                                                    <div>
                                                        {datas?.bonus_value}
                                                    </div>

                                                </td>
                                                <td className=" " data-value="TOTAL">
                                                    <div className="cart-product-details">
                                                        <NumberFormat value={parseFloat(datas.price_per_unit * datas.product_quantity).toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <div>{value}</div>} />
                                                    </div>
                                                </td>
                                            </tr>
                                            {
                                                (expend && datas?.product_type === "Bundle") &&
                                                <tr>
                                                    <td colSpan={5} className="bundle_detail_table">

                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <th>Product Name</th>
                                                                    <th>BV</th>
                                                                    <th>Quantity</th>
                                                                    <th>price</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {(() => {
                                                                    if ([bundleProduct]?.length > 0) {
                                                                        return _.map(bundleProduct[datas?.product_id], (val, key) => (
                                                                            <tr key={key}>
                                                                                <td>{val?.product?.name}</td>
                                                                                <td>{val?.product?.bonus_value}</td>
                                                                                <td>{val?.quantity}</td>
                                                                                <td>{val?.product?.cost_price}</td>
                                                                            </tr>
                                                                        ));
                                                                    }
                                                                })(datas?.product_id)}
                                                                <tr>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                    </td>
                                                </tr>
                                            }
                                        </>
                                    )
                                })}
                                <tr>
                                    <td className='subtotal' style={{ 'textAlign': 'center' }} ></td>
                                    <PaymentTitleSectionHtml text="Subtotal" numberValue={data?.amount} />
                                </tr>
                                <tr>
                                    {data?.discount_amount > 0 &&
                                        <PaymentTitleSectionHtml text={`Discount ${data?.coupon_name !== undefined ? (data.coupon_name) : ''}`} numberValue={data?.discount_amount} />
                                    }
                                </tr>
                                <tr>
                                    <td className='total' style={{ 'textAlign': 'center' }}></td>
                                    <PaymentTitleSectionHtml text="Total" numberValue={data?.gross_total} />
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="row order-cstm">
                        <div className="col-md-6">
                            <div className="thn-lft">
                                <h4>Billing Address</h4>
                                <AddressesSection addressdata={data} type="billing_address" />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="thn-lft thn-rgt">
                                <h4>Shippping Address</h4>
                                <AddressesSection addressdata={data} type="shipping_address" />
                            </div>
                        </div>
                    </div>
                    <div className="btn-sec-cstm main-button-group">
                        <div className="d-flex">
                            <button className="re-order" onClick={() => {
                                setShowreorder(false)
                                setorderid()
                            }}>Back</button>

                            <button className="refund_button_class" onClick={Reorder}>Reorder</button>
                        </div>
                    </div>
                </div>
            </>
        </ReOrderStyle>
    </>
}