import styled from 'styled-components';

const CreateFormStyle = styled.div`
&&
form {
    width: 100%;
    margin: 27px auto;
    max-width: 1320px;
}
form.Create-order-form {
    padding: 20px;
}
form.Create-order-form .form-group label {
    font-size: 15px;
}
.main-qty-sec .box:last-child #qty {
    max-width: 90%;
    flex: 0 0 90%;
}
div#qty {
    display: flex;
}
.cart-product-details{
    margin-right: 10px;
}
form.Create-order-form table {
    margin-top: 20px;
}
form.Create-order-form input{
    margin-left: 10px !important;
}
form.Create-order-form button[type="submit"] {
    margin-top: 15px;
}
form.Create-order-form table {
    margin-top: 15px;
    margin-bottom: 15px;
}
.ui.fluid.multiple.search.selection.dropdown {
    margin-top: 20px;
    margin-left: 0;
}
.search.selection.dropdown input[type="text"]{
    margin-left: 0 !important;
}
button.add-product-autoshiporder {
    display: block;
}
.MuiAutocomplete-endAdornment.css-1q60rmi-MuiAutocomplete-endAdornment {
    top: 7px;
}
table.product-sec tr:nth-child(even) {
    height: 50px;
}
span.error_create_product {
    margin-top: 10px;
    display: block;
}
.MuiAutocomplete-root.MuiAutocomplete-hasPopupIcon.css-1kpdewa-MuiAutocomplete-root {
    width: 100% !important;
    max-width: 610px;
}
table.product-sec tr.total_value {
    height: auto !important;
}
@media (max-width: 991px){
    .col-md-12 {
        width: 100%;
    }
}
@media (max-width: 767px){
    table.product-sec td, th {
        padding: 0;
        display: inline-block;
    }
    .box{
        max-width: 100% !important;
        flex: 0 0 100% !important;
        padding: 0;
        margin: 5px 0px;
    }
    td.is_autoship {
        margin: 10px 0px;
    }

}
`;



export default CreateFormStyle