import NumberFormat from 'react-number-format';
import { FaTrash } from "react-icons/fa";
import NumberFormatComp from '@Components/Common/NumberFormatComp';

const VariantDetails = ({ productdata, Add, Sub, ProductAutoship, DeleteProduct }) => {

    return (
        <>
            <td className=" ">
                <div className="cart-product-details">
                    {productdata?.Product_name} X {productdata?.quantity} {productdata?.is_autoship}
                </div>
            </td>
            <td className=" qty-sec">
                <div className="main-qty-sec">
                    <div className="box">
                        <div id="qty">
                            <button type="button"
                                data-qty={productdata?.quantity}
                                data-product_id={productdata?.product_id}
                                data-variant_id={+productdata?.variant_id}
                                className="sub"
                                onClick={(e) => { Sub(e) }}>-</button>
                            <input
                                name={`${productdata?.variant_id},${null}`}
                                type="text"
                                value={productdata?.quantity}
                                readOnly
                            />
                            <button type="button"
                                data-qty={productdata?.quantity}
                                data-product_id={productdata?.product_id}
                                data-variant_id={+productdata?.variant_id}
                                data-max_qty={+productdata?.total_quantity}
                                className="add"
                                value={productdata?.quantity}
                                onClick={(e) => { Add(e) }}>+</button>
                        </div>
                    </div>
                    {/* <div className="delete">
                    <FaTrash onClick={()=>{deleteautoship(datas?.product?.id, +datas?.variant?.id)}} />
                </div> */}
                </div>
            </td>

            <td className="">
                <div className="cart-product-details">
                    <NumberFormatComp value={productdata?.is_autoship === false ? +productdata?.cost * +productdata?.quantity : +productdata?.autoShipCost * +productdata?.quantity} />
                </div>
            </td>
            <td className="is_autoship ">
                <div className="cart-product-details">
                    SmartShip <input type="checkbox" name="chk" onChange={(e) => {
                        ProductAutoship(e, productdata?.product_id, +productdata?.variant_id)
                    }}></input>
                </div>
            </td>
            <td className="is_autoship">
                <div className="cart-product-details">
                    <FaTrash onClick={(e) => {
                        DeleteProduct(e, productdata?.product_id, +productdata?.variant_id)
                    }} />
                </div>
            </td>
        </>
    )
}
export default VariantDetails;