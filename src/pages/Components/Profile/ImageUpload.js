import React, { useEffect, useState } from 'react';
import ImageUploading from 'react-images-uploading';
import api from '../../../api/Apis';
import { toast } from 'react-toastify';
import { FaTrash } from "react-icons/fa";
var randomstring = require("randomstring");

export default function ImageUpload({ setshowloader, setInnerLoader,
    profilepageData, changePasswardStatus, setchangePasswardStatus, editdetails }) {
    const [images, setImages] = React.useState([]);
    const [logintoken, setLogintoken] = useState()
    const [updateProfile, setUpdateProfile] = useState()
    const maxNumber = 69;

    const onChange = (imageList, addUpdateIndex) => {
        // data for submit
        setImages(imageList);

    };
    useEffect(() => {
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLogintoken(token)
        // api.getCommissionReport(token, 'week').then(res => {
        // api.GetCommissionsApproved(token, 'week').then(res => {

    }, [])

    const UpdateImageToprofile = () => {
        setInnerLoader(true)
        const data = images[0].file
        api.profileImageUpload(data, logintoken).then(res => {
            if (res?.data?.code === 1) {
                localStorage.removeItem("profileData")
                setInnerLoader(false)
                setImages([])
            }
            else {
                setInnerLoader(false)
            }
        })
        api.getMyProfileDetails(logintoken).then(res => {
            if (res?.data?.code === 1) {
                setUpdateProfile(res?.data)
            }
            else {
                setInnerLoader(false)
            }
        })
    }
    return (
        <div className="App">
            <ImageUploading
                value={images}
                onChange={onChange}
                maxNumber={maxNumber}
                dataURLKey="data_url"
            >
                {({
                    imageList,
                    onImageUpload,
                    onImageRemoveAll,
                    onImageUpdate,
                    onImageRemove,
                    isDragging,
                    dragProps,
                }) => (

                    <div className="upload__image-wrapper profile-section-now main-profile-update">
                        {images.length <= 0 &&
                            <>
                                {!(profilepageData?.user_profile?.image) &&
                                    <button style={isDragging ? { color: 'red' } : undefined} onClick={onImageUpload} {...dragProps}>Click or Drop here</button>
                                }

                                <div className="image-section">
                                    <img src={updateProfile?.user_data?.user_profile?.image ? `${process.env.DOC_URL}/media/${updateProfile?.user_data?.user_profile?.image}?id=${randomstring.generate(16)}` : profilepageData?.user_profile?.image ?
                                        `${process.env.DOC_URL}/media/${profilepageData?.user_profile?.image}?id=${randomstring.generate(16)}`
                                        :
                                        "/images/user1-profile.jpg"} alt="" width="100" />
                                </div>

                                <div className="profile-btn">
                                    <a className="change-password" onClick={() => setchangePasswardStatus('new')}>
                                        <i className="fa fa-lock" aria-hidden="true"></i>Change Password
                                    </a>
                                    <a onClick={onImageUpload} {...dragProps}
                                        className={profilepageData?.user_profile?.image ? "edit-now" : "edit-now disabled"}>
                                        <i className="fa fa-pencil-square-o" aria-hidden="true" ></i>Update Picture
                                    </a>

                                </div>

                            </>
                        }

                        {/* <button onClick={onImageRemoveAll}>Remove all images</button> */}
                        {images?.map((image, index) => (
                            <div key={index} className="image-item">
                                <div className="image-section">
                                    <img src={image['data_url']} alt="" width="100" />

                                </div>
                                <div className="image-item__btn-wrapper">
                                    {/* <button onClick={() => onImageUpdate(index)}>Update</button> */}
                                    <div className="image_update_div">
                                        <button onClick={() => UpdateImageToprofile()}>Update</button>
                                    </div>

                                    {/* <button onClick={() => onImageRemove(index)}>Remove</button> */}
                                    <div className="image_delete_div">
                                        <FaTrash onClick={() => onImageRemove(index)} />
                                    </div>
                                </div>
                                <div className="profile-btn">
                                    <a className="change-password" onClick={() => setchangePasswardStatus('new')} >
                                        <i className="fa fa-lock" aria-hidden="true"></i> Change Password
                                    </a>
                                    <a onClick={onImageUpload} {...dragProps}
                                        className={image['data_url'] ? "edit-now" : "edit-now disabled"}>
                                        <i className="fa fa-pencil-square-o" aria-hidden="true" ></i>Edit
                                    </a>
                                </div>
                            </div>
                        ))}

                    </div>
                )
                }
            </ImageUploading >
        </div >
    );
}