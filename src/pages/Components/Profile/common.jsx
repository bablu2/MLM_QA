const PersonalDetails = ({ profilepageData }) => {
    return (
        <>
            <tr>
                <th>First Name:</th>
                <td>{profilepageData?.userdetails[0]?.public_first_name}
                </td>
            </tr>
            <tr>
                <th>Last Name:</th>
                <td>{profilepageData?.userdetails[0]?.public_last_name}
                </td>
            </tr>
            <tr>
                <th>Email:</th>
                <td>{profilepageData?.userdetails[0]?.public_email}
                </td>
            </tr>
        </>
    )
}
export default PersonalDetails;