import { useForm } from "react-hook-form";
import api from '../../../api/Apis';
import { useState } from "react";
import DialogComponent from "@Components/Common/DialogComponent";
import { DialogSectionCss } from "@PagesComponent/ActiveSmartShip/ActiveStyleComp";
import { FaCommentsDollar } from "react-icons/fa";

const ProfileEdit = ({ profilepageData, LoginToken, customerCheck, setEditDetails, setreloadData, validReferralCheck }) => {

    const { register, handleSubmit, watch, errors, getValues } = useForm();
    const [sameforpublic, setSameForPublic] = useState(true)
    const [open, setOpen] = useState()

    const onSubmit = async (data) => {
        // data['first_name'] = data?.first_name;
        // data['last_name'] = data?.last_name;
        // data['user_email'] = data?.user_email;
        // data['phone_number'] = data?.phone_number;
        data['update_referral'] = false;
        data['public_data'] = false;
        // data['show_email'] = profilepageData?.userdetails[0] ? profilepageData?.userdetails[0].show_email : null;
        // data['show_phone_number'] = profilepageData?.userdetails[0] ? profilepageData?.userdetails[0].show_phone_number : null;

        await api.profileUpdate(data, LoginToken).then(res => {
            setEditDetails(false)
            setreloadData(true)
            if (res?.data?.code === 1) {
                // toast(res?.data?.message, { duration: 5, type: "success", })
            }
            if (res?.data?.code === 0) {
                // toast(res?.data?.message, { duration: 5, type: "error", })

            }
        })

    }

    return (
        <div className="update-details main-profile">
            <h4 className="update-profile">Update </h4>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="row">
                    <div className="col-md-12">
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1" className="form-label"> First Name</label>
                            <input type="text"
                                defaultValue={profilepageData?.first_name}
                                className="form-control" name="first_name" id="exampleInputPassword2" ref={register({ required: true })} />
                            {errors.first_name && <span>This field is required</span>}
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1" className="form-label">Last Name</label>
                            <input type="text"
                                defaultValue={profilepageData?.last_name}
                                className="form-control" name="last_name" id="exampleInputPassword1"
                                ref={register({
                                    required: "This field is required",
                                })}
                            />
                            {errors.last_name && <span>{errors.last_name.message}</span>}
                        </div>
                    </div>

                    <div className="col-md-12">
                        <div className="form-group">
                            <label htmlFor="exampleInputAge" className="form-label">Email</label>
                            <input type="text" className="form-control" name="user_email" id="user_email"
                                defaultValue={profilepageData?.email}
                                aria-describedby="emailnoHelp" ref={register({
                                    required: "This field is required",
                                    pattern: {
                                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                        message: "Invalid email address"
                                    }
                                })} />
                            {errors.public_email && <span>{errors.public_email.message}</span>}
                        </div>
                        <p><strong>Note: </strong>Changing your email address will also change the email you use to login to Kaire.</p>
                    </div>
                    <div className="col-md-12">
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1" className="form-label">Phone Number</label>
                            <input type="text"
                                defaultValue={profilepageData?.userdetails[0]?.phone_number}
                                className="form-control" name="phone_number" id="exampleInputPassword1"
                                ref={
                                    register({
                                        required: "This field is required",
                                        pattern: {
                                            value: /^[0-9]/,
                                            message: "Enter only number"
                                        },
                                        minLength: {
                                            value: 5,
                                            message: "Enter minimum 5 digit"
                                        },
                                        maxLength: {
                                            value: 10,
                                            message: "Phone number not longer then 10 digit"
                                        },
                                    })}
                            />
                            {errors.phone_number && <span>{errors.phone_number.message}</span>}
                        </div>

                    </div>
                    {sameforpublic === false && <>

                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1" className="form-label"> Public First Name</label>
                                <input type="text"
                                    defaultValue={
                                        profilepageData?.userdetails[0]?.public_first_name ? profilepageData?.userdetails[0]?.public_first_name : profilepageData?.first_name

                                    }
                                    className="form-control" name="public_first_name" id="exampleInputPassword2" ref={register({ required: true })} />
                                {errors.public_first_name && <span>This field is required</span>}
                            </div>

                        </div>
                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1" className="form-label">Public Last Name</label>
                                <input type="text"
                                    defaultValue={profilepageData?.userdetails[0]?.public_last_name
                                        ?
                                        profilepageData?.userdetails[0]?.public_last_name
                                        :
                                        profilepageData?.last_name
                                    }

                                    className="form-control" name="public_last_name" id="exampleInputPassword1"
                                    ref={register({
                                        required: "This field is required",


                                    })}
                                />
                                {errors.public_last_name && <span>{errors.public_last_name.message}</span>}
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor="exampleInputAge" className="form-label">Public  Email</label>
                                <input type="text" className="form-control" name="public_email" id="public_email"
                                    defaultValue={profilepageData?.userdetails[0]?.public_email}
                                    aria-describedby="emailnoHelp" ref={register({
                                        required: "This field is required",
                                        pattern: {
                                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                            message: "Invalid email address"
                                        }
                                    })} />
                                {errors.public_email && <span>{errors.public_email.message}</span>}
                                <div className="show-email-check">
                                    <div className="lables">Visible for public</div>
                                    <div className="content"><input
                                        defaultChecked={profilepageData?.userdetails[0]?.show_email}
                                        type="checkbox" className="check_box_align form-check-input" id="Terms-Condition" name="show_email" ref={register({ required: false })} /></div>

                                </div>
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1" className="form-label">Public Phone Number</label>
                                <input type="text"
                                    defaultValue={profilepageData?.userdetails[0]?.public_phone_number
                                        ?
                                        profilepageData?.userdetails[0]?.public_phone_number
                                        :
                                        profilepageData?.userdetails[0]?.phone_number
                                    }
                                    className="form-control" name="public_phone_number" id="exampleInputPassword1"
                                    ref={
                                        register({
                                            required: "This field is required",
                                            pattern: {
                                                value: /^[0-9]/,
                                                message: "Enter only number"
                                            },
                                            minLength: {
                                                value: 5,
                                                message: "Enter minimum 5 digit"
                                            },
                                            maxLength: {
                                                value: 10,
                                                message: "Phone number not longer then 10 digit"
                                            },
                                        })}
                                />
                                <div className="show-phone-check">
                                    <div className="lables">Visible for public</div>
                                    <div className="content"><input
                                        defaultChecked={profilepageData?.userdetails[0]?.show_phone_number}
                                        type="checkbox" className="check_box_align form-check-input" name="show_phone_number" ref={register({ required: false })} /></div>

                                </div>
                                {errors.public_phone_number && <span>{errors.public_phone_number.message}</span>}
                            </div>
                        </div>
                    </>
                    }
                    {!validReferralCheck &&
                        <>
                            {customerCheck < 3 ?
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <label htmlFor="exampleInputPassword1" className="form-label">Referral Code</label>
                                        <input type="text" name="referral_code" defaultValue={profilepageData?.userdetails[0]?.referral_code}
                                            disabled className="form-control" id="referral_code" ref={register({ required: true })} />
                                        {errors?.referral_code && <span>This field is required</span>}
                                    </div>

                                </div>
                                :
                                <div className="col-md-12">
                                    <div className="form-group">
                                        <label htmlFor="exampleInputPassword1" className="form-label">Referral Code</label>
                                        <input type="text" name="referral_code" defaultValue={profilepageData?.userdetails[0]?.referral_code}
                                            className="form-control" id="referral_code" ref={register({ required: true })} />
                                        {errors?.referral_code && <span>This field is required</span>}
                                    </div>

                                </div>
                            }
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label htmlFor="exampleInputPassword1" className="form-label">Opt in to email</label>
                                    <input type="checkbox" name="email_communication" id="email_communication"
                                        defaultChecked={profilepageData?.userdetails[0]?.email_communication !== false ? true : false}
                                        ref={register({ required: false })}
                                    />

                                </div>

                            </div>
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label htmlFor="exampleInputPassword1" className="form-label">Opt in to message</label>
                                    <input type="checkbox" name="text_communication" id="text_communication"
                                        defaultChecked={profilepageData?.userdetails[0]?.text_communication !== false ? true : false}
                                        ref={register({ required: false })}
                                    />
                                </div>

                            </div>
                        </>
                    }
                    {/*    <div className="col-md-12">
                        <div className="form-group">
                            <input type="checkbox" defaultChecked={sameforpublic} onChange={(e) => {
                                setSameForPublic(e.target.checked)
                            }}
                                className="profile_checkbox" />
                            <label htmlFor="exampleInputPassword1" className="form-label">Same for public</label>

                        </div>

                        </div>*/}
                </div>
                <div className="row">
                    <button type="submit" className="btn btn-primary">Update Details</button>
                </div>
            </form >
            <DialogComponent opend={open} handleClose={() => setOpen(false)} title="Description" classFor="order_details">
                <DialogSectionCss>
                    <h3> Google Analytics</h3>

                    <p>you can use Google Analytics. Google Analytics lets you track visitors, sessions, and other customer behavior on your store.
                    </p> <br></br>
                    <h3> In this section</h3>

                    <li> <a href="#">Setting up Google Analytics</a></li>

                    <li>Setting up Google Analytics goals and funnels</li>

                    <li>Excluding referrer domains from Google Analytics tracking</li>

                    <li> Google Tag Manager</li>

                </DialogSectionCss>
            </DialogComponent>
        </div >

    )
}
export default ProfileEdit;