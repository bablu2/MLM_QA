import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Button from '@material-ui/core/Button';
import { useForm } from 'react-hook-form';

const SaveAnalyticsCode = () => {
    const { register, handleSubmit, errors } = useForm();
    // login user
    const onSubmit = data => {
        // setdata(data)
    }
    return (<><h3 className="title">Save Code</h3>
        <form className="signupform main-sign-frm" onSubmit={handleSubmit(onSubmit)}>

            <div className="google analytics">
                <label className="google">Google analytics</label>
                <TextareaAutosize aria-label="minimum height" name="google_code" rowsMin={3} placeholder="Enter google analytics code"
                    ref={register({ required: true })}
                />
            </div>
            <div className="google analytics">
                <label className="google">Facebooks</label>
                <TextareaAutosize aria-label="minimum height" name="facebok_code" rowsMin={3} placeholder="Enter facebook pixel  code"
                    ref={register({ required: true })}
                />
            </div>
            <Button type="submit" variant="contained" color="primary">
                Save
      </Button>
        </form>
    </>)
}

export default SaveAnalyticsCode;