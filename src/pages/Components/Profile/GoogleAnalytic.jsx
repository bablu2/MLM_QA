import React from 'react'

const GoogleAnalytic = () => {
    return (
        <>
            <h3>Setting up Google Analytics</h3> <br></br>
            <p>Before you can start gathering data about your store, you need to have a Google account, sign up for analytics tracking,
                and choose what data you want to track.</p>
            <div className='first_step'>
                Step 1: Make sure you don't have Google Analytics enabled already <br></br>
                Enabling Google Analytics more than once results in inaccurate data. If you're sure you've never enabled Google Analytics for your store before, then skip to
                Step 2: Get a Google Account. If you aren't sure whether you've enabled Google Analytics for your store before, then follow these steps: <br></br>

            </div>

        </>
    )
}

export default GoogleAnalytic