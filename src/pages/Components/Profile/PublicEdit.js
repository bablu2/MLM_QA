import { useForm } from "react-hook-form";
import api from '../../../api/Apis';
import { useState } from "react";
import DialogComponent from "@Components/Common/DialogComponent";
import { DialogSectionCss } from "@PagesComponent/ActiveSmartShip/ActiveStyleComp";

const PublicEdit = ({ profilepageData, LoginToken, setreloadData, reloaddata, setPublicEditData }) => {
    const { register, handleSubmit, watch, errors, getValues } = useForm();

    const onSubmit = async (datas) => {
        datas['public_data'] = true;
        datas['update_referral'] = false;
        datas['show_email'] = profilepageData?.userdetails[0] ? profilepageData?.userdetails[0].show_email : null;
        datas['show_phone_number'] = profilepageData?.userdetails[0] ? profilepageData?.userdetails[0].show_phone_number : null;

        await api.profileUpdate(datas, LoginToken).then(res => {
            if (res?.data?.code === 1) {
                setPublicEditData(false)
                setreloadData(true)
                // toast(res?.data?.message, { duration: 5, type: "success", })
            }
            if (res?.data?.code === 0) {
                // toast(res?.data?.message, { duration: 5, type: "error", })
            }
        })
        api.getMyProfileDetails(LoginToken).then(res => {
            if (res?.data?.code === 1) {
                setreloadData(false)
            }
        })

    }

    return (
        <div className="update-details main-profile">
            <h4 className="update-profile">Update </h4>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="row">
                    <>
                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1" className="form-label"> Public First Name</label>
                                <input type="text"
                                    defaultValue={profilepageData?.userdetails[0]?.public_first_name}
                                    className="form-control" name="public_first_name" id="exampleInputPassword2" ref={register({ required: true })} />
                                {errors.public_first_name && <span>This field is required</span>}
                            </div>

                        </div>
                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1" className="form-label">Public Last Name</label>
                                <input type="text"
                                    defaultValue={profilepageData?.userdetails[0]?.public_last_name}
                                    className="form-control" name="public_last_name" id="exampleInputPassword1"
                                    ref={register({
                                        required: "This field is required",

                                    })}
                                />
                                {errors.public_last_name && <span>{errors.public_last_name.message}</span>}
                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor="exampleInputAge" className="form-label">Public  Email</label>
                                <input type="text" className="form-control" name="public_email" id="public_email"
                                    defaultValue={profilepageData?.userdetails[0]?.public_email}
                                    aria-describedby="emailnoHelp" ref={register({
                                        required: "This field is required",
                                        pattern: {
                                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                            message: "Invalid email address"
                                        }
                                    })} />
                                {errors.public_email && <span>{errors.public_email.message}</span>}

                            </div>
                        </div>

                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1" className="form-label">Public Phone Number</label>
                                <input type="text"
                                    defaultValue={profilepageData?.userdetails[0]?.public_phone_number}
                                    className="form-control" name="public_phone_number" id="exampleInputPassword1"
                                    ref={
                                        register({
                                            required: "This field is required",
                                            pattern: {
                                                value: /^[0-9]/,
                                                message: "Enter only number"
                                            },
                                            minLength: {
                                                value: 5,
                                                message: "Enter minimum 5 digit"
                                            },
                                            maxLength: {
                                                value: 10,
                                                message: "Phone number not longer then 10 digit"
                                            },
                                        })}
                                />
                            </div>
                        </div>
                    </>


                </div>
                <div className="row">
                    <button type="submit" className="btn btn-primary">Update Details</button>
                </div>
            </form >

        </div >

    )
}
export default PublicEdit;