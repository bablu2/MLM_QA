import { useForm } from "react-hook-form";
import { ToastContainer, toast } from 'react-toastify';
import NumberFormat from 'react-number-format';
import PersonalDetails from './common'
import { useState } from "react";
import PublicEdit from "./PublicEdit";
const Walletsection = ({ profilepageData, LoginToken, reloaddata, setreloadData, router, commission_wallet }) => {

    const [publicEditData, setPublicEditData] = useState(false)

    var USNumber = profilepageData?.userdetails[0]?.public_phone_number?.match(/(\d{3})(\d{3})(\d{4})/);
    USNumber = "(" + USNumber?.[1] + ") " + USNumber?.[2] + "-" + USNumber?.[3];

    return (<>
        {/* <div className="wallet_data">

            <ul className="kaire_cash_ul">

                <li><strong>Kaire Cash: </strong></li>

                <li><NumberFormat value={parseFloat(profilepageData?.userdetails[0]?.kaire_cash).toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <div>{value}</div>} /></li>

                <li><button type="button" onClick={() => {
                    router.push({
                        pathname: `/${router.query.page}/user/wallet/`,
                        query: { logtype: "kairecash" }
                    });
                }}>View logs</button></li>

            </ul>
            <ul className="commission_cash_ul">

                <li><strong>Commission Wallet: </strong></li>

                <li><NumberFormat value={parseFloat(commission_wallet).toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <div>{value}</div>} /></li>

                <li><button type="button" onClick={() => {
                    router.push({
                        pathname: `/${router.query.page}/user/wallet/`,
                        query: { logtype: "Commissions" }
                    });
                }}>View logs</button></li>

            </ul>


            <div className="buttton-reem">
                <button type="button" className="btn btn-primary"   >Shop to Redeem</button>

                <button type="button" className="btn btn-primary"   >Redeem to Bank</button>

            </div>
        </div> */}


        <div className="profile-img-left">
            <>
                {publicEditData === false ?
                    <>
                        <h4>PUBLIC DETAILS</h4>
                        <table>
                            <tbody>
                                <PersonalDetails profilepageData={profilepageData} />
                                <tr>
                                    <th>Phone:</th>
                                    <td>{profilepageData?.userdetails[0]?.public_phone_number && USNumber}</td>
                                </tr>

                            </tbody>
                        </table>
                    </>
                    :
                    <PublicEdit
                        publicEditData={publicEditData}
                        setreloadData={setreloadData}
                        reloaddata={reloaddata}
                        LoginToken={LoginToken}
                        profilepageData={profilepageData}
                        setPublicEditData={setPublicEditData} />
                }
            </>
            <button type="button" className="btn btn-primary" onClick={() => { setPublicEditData(!publicEditData) }}  >{publicEditData ? "Back" : "Edit"}</button>

        </div>

        <div className="public_details_section ">
            <>
                {/* <input type="checkbox" name="email" onChange={(e) => {
                  SetpublicDetails(e,'email')
              }} /> */}



                {/* <input type="checkbox" name="phone_number" onChange={(e) => {
                  SetpublicDetails(e,'phone_number')
              }} /> */}
                {/* <button type="button" className="btn btn-primary" onClick={() => { setEditPublicDetails(true) }}  >Edit Public Details</button> */}
            </>

        </div>
    </>

    )
}
export default Walletsection;