import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import api from '../../../api/Apis'


export const totalPaidFunction = (reviewshow, discountAmount, paidamount,
    setpaidamount, selectedCheckBox, shippingChargesAmount, kaireCashtoPaid,
    freeShipping, walletToPaid) => {

    let currentAmount = 0;
    const discountPrice = isNaN(discountAmount) ? +discountAmount : discountAmount;

    if ((reviewshow['amount'] - discountPrice) > 0) {
        currentAmount = (freeShipping !== true) ? reviewshow['amount'] - discountPrice : (reviewshow['amount'] + shippingChargesAmount) - discountPrice;
    } else {
        currentAmount = (freeShipping !== true) ? reviewshow['amount'] - 0 : reviewshow['amount'] + shippingChargesAmount - discountPrice;
    }
    let finalamount = reviewshow['tax_amount'] + reviewshow['shipping_amount'];
    if (selectedCheckBox === "kaire") {

        let theAmount = finalamount;
        theAmount += kaireCashtoPaid(currentAmount, freeShipping);
        setpaidamount(theAmount);

    } else if (selectedCheckBox === "wallet") {

        currentAmount = (freeShipping !== true) ? (finalamount + currentAmount + shippingChargesAmount) : (finalamount + currentAmount)
        const theAmountPaid = walletToPaid(currentAmount);
        setpaidamount(theAmountPaid);
    } else {
        if (shippingChargesAmount && freeShipping !== true) {
            currentAmount += shippingChargesAmount;
        }
        setpaidamount(finalamount + currentAmount);
    }
}

const Discount = ({ paidamount, selectedCheckBox,
    kaireCashtoPaid, shippingChargesAmount, shippingaddress, coupon,
    setdiscount_amount, setpaidamount, discount_amount1, reviewshow,
    setToken, logintoken, setcopanerror, setshipamount, setcoupon, walletToPaid,
    setFreeShipping, freeShipping }) => {

    const { register, handleSubmit } = useForm();
    const [couponCheck, setCouponCheck] = useState('');

    useEffect(() => {
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setToken(token)
        if (localStorage.getItem('coupon_name')?.length > 0) {
            const alreadyExistCoupon = localStorage.getItem('coupon_name');
            setCouponCheck(alreadyExistCoupon);
        }
    }, [])
    //on click applay discount coupon method call
    const onSubmit = data => {

        if (couponCheck !== "") {
            let orderdata = {
                "coupon": couponCheck,
                "shipping_id": shippingaddress ? shippingaddress : null
            }
            // }
            //varify coupon is valid or not
            // if (shippingaddress) {
            api.verifyCopan(orderdata, logintoken).then(res => {
                let freeShippingStatus = false;
                if (res?.data?.code === 1) {
                    setcopanerror('')
                    localStorage.setItem('coupon_name', res.data.coupon_details?.code)
                    if (res.data?.coupon_details?.Fodiscount_type) {
                        if (res?.data?.coupon_details.discount_type === 1) {
                            reviewshow['discount_amount'] = +res?.data?.coupon_details.discount_percent;
                            setdiscount_amount(reviewshow['discount_amount']);
                            setFreeShipping(false);
                        }
                        if (res?.data?.coupon_details.discount_type === 2) {
                            reviewshow['discount_amount'] = +(reviewshow['amount'] * res?.data?.coupon_details.discount_percent) / 100;
                            setdiscount_amount(reviewshow['discount_amount']);
                            setFreeShipping(false);
                        }

                        if (res?.data?.coupon_details.free_shipping === true) {
                            reviewshow['shipping_amount'] = 0;
                            reviewshow['discount_amount'] = shippingChargesAmount ?? 0;
                            setFreeShipping(true);
                            freeShippingStatus = true;
                            setdiscount_amount(reviewshow['discount_amount']);
                        }
                    }
                    if (res?.data?.coupon_details.discount_type === 1) {
                        reviewshow['discount_amount'] = +res?.data?.coupon_details.discount_amount;
                        setdiscount_amount(reviewshow['discount_amount']);
                        setFreeShipping(false);
                    }
                    if (res?.data?.coupon_details.discount_type === 2) {
                        reviewshow['discount_amount'] = +(reviewshow['amount'] * res?.data?.coupon_details.discount_percent) / 100;
                        setdiscount_amount(reviewshow['discount_amount']);
                        setFreeShipping(false);
                    }

                    if (res?.data?.coupon_details.free_shipping === true) {
                        reviewshow['shipping_amount'] = 0;
                        reviewshow['discount_amount'] = shippingChargesAmount ?? 0;
                        setFreeShipping(true);
                        freeShippingStatus = true;
                        setdiscount_amount(reviewshow['discount_amount']);
                    }
                    setshipamount(reviewshow['shipping_amount'])
                    //paid amount function
                    totalPaidFunction(reviewshow, reviewshow['discount_amount'],
                        paidamount, setpaidamount,
                        selectedCheckBox,
                        shippingChargesAmount,
                        kaireCashtoPaid,
                        freeShippingStatus,
                        walletToPaid
                    );

                    setcoupon(res?.data?.coupon_details)
                }
                else {
                    setcopanerror(res?.data?.message)
                }
            });
            // } else {
            //     // toast.warn('Please select shipping address')
            // }
        } else {

        }
    }
    // <form className="signupform" onSubmit={handleSubmit(onSubmit)}>

    return (<>
        {((discount_amount1 !== undefined || discount_amount1 !== null) && coupon?.name) ? '' :
            <>

                <div className="apply-coupn">
                    <label><b> COUPON CODE </b></label>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="wrap-form">
                                <input
                                    type="text" className="form-control"
                                    name="coupon_id" placeholder="coupon"
                                    id="coupon_id"
                                    value={couponCheck}
                                    onChange={(e) => setCouponCheck(e.target.value)}
                                // ref={register({ required: false })} 
                                />
                                <button type="button" className="btn btn-primary" onClick={() => onSubmit()}>Apply</button>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        }
    </>)
}
export default Discount;