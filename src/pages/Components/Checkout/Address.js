import Head from 'next/head'
import Link from 'next/link'
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import api from '../../../api/Apis'
import PayPal from '../../[page]/Paypal/paypal';
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';
import { toast } from 'react-toastify';
import _ from 'lodash';

export default function Address({
    withoutLogin, defaultValues, logintoken, setaddressData, setAddresstype, addressDetails, addressType,
    addresstype, saveBillingAddress, saveShippingAddress
}) {


    const { register, handleSubmit, watch, errors } = useForm({
        mode: "all",
        defaultValues: addressDetails ? addressDetails : {}
    });
    const [data_address, setdata] = useState();
    const [addresstypes, setAddresstypes] = useState();
    const [sameAddressError, setSameAddressError] = useState();

    const [countryError, setCountryError] = useState()
    const [stateError, setStateError] = useState()
    const [country, setCountry] = useState();
    const [state, setState] = useState()

    if (addressDetails?.country && addressDetails?.state && country === undefined && state === undefined) {

        setCountry(addressDetails.country);
        setState(addressDetails?.state)
    }
    const handleCountryStateError = () => {
        setAddresstypes('new')

        if (country === undefined && countryError === undefined) {

            setCountryError("Please Select The Country")
        }
        if (state === undefined && stateError === undefined) {

            setStateError("Please Select The State")
        }
    }

    //save address
    const onSubmit = data => {
        if (country !== undefined && state !== undefined) {
            data.country = country
            data.state = state
            // data["street_address_2"] = "";
            if (logintoken) {
                const formData = { data: addressDetails === null ? data : { ...data, address_id: addressDetails?.id }, token: logintoken, address_type: addresstypes };
                const apiObj = addressDetails !== null ? api.updateAddress(formData) : api.saveAddress(formData);
                apiObj.then(res => {
                    if (res?.data?.code === 1) {
                        if (data?.address_type === 'shipping' && data?.shipping_address_is_same === 'false') {
                            localStorage.setItem('shippingAddress', +res.data.new_address_id);
                            saveShippingAddress(+res.data.new_address_id)
                        }
                        else if (data?.address_type === 'billing' && data?.shipping_address_is_same === false) {
                            localStorage.setItem('billingAddress', +res.data.new_address_id);
                            saveBillingAddress(+res.data.new_address_id)
                        }
                        else if (data?.address_type === 'billing' && data?.shipping_address_is_same === true) {
                            localStorage.setItem('billingAddress', +res.data.new_address_id);
                            saveBillingAddress(+res.data.new_address_id)
                            localStorage.setItem('shippingAddress', +res.data.new_address_id);
                            saveShippingAddress(+res.data.new_address_id)
                        }
                    }
                    if (res?.data?.code === 0 && res?.status === 200) {
                        setSameAddressError(res?.data?.message)
                    }
                    if (res?.data?.code === 1) {
                        api.manageAddress(logintoken).then(res => {
                            setAddresstype('existing')
                            if (res?.data?.code === 1) {
                                setaddressData(res?.data)
                            }

                        })
                    }
                })
            } else {
                const REMOVE_TYPE = _.omit(data, 'address_type');
                if (withoutLogin?.section === "billing") {
                    let OBJECT = {};
                    localStorage.setItem('billingAddress', JSON.stringify(REMOVE_TYPE));
                    if (data['shipping_address_is_same']) {
                        const REMOVE_SAME_SHIP = _.omit(REMOVE_TYPE, 'shipping_address_is_same');
                        localStorage.setItem('shippingAddress', JSON.stringify(REMOVE_SAME_SHIP));
                        OBJECT = { shippingaddress: REMOVE_SAME_SHIP };
                    } else {
                        OBJECT = { shippingaddress: withoutLoginAddress?.shippingaddress };
                    }
                    withoutLogin?.setWithoutLoginAddress({ ...OBJECT, billingaddress: REMOVE_TYPE });

                } else {
                    localStorage.setItem('shippingAddress', JSON.stringify(data));
                    withoutLogin?.setWithoutLoginAddress({ ...withoutLogin?.withoutLoginAddress, shippingaddress: REMOVE_TYPE });
                }
            }


        }
    }
    return <>
        <Head>
            <title>Address setup</title>
        </Head>
        <div className="container">
            <form onSubmit={handleSubmit(onSubmit)}>
                {/* <h3 className="response message">{responsedata && responsedata}</h3> */}
                {/* <h3>{addresstype}</h3> */}
                <input type="hidden" className="form-control" name="address_type" id="addresstype" aria-describedby="nameHelp"
                    ref={register({ required: "This field is required" })}
                    value={addresstype}
                />

                <div className="row">
                    <div className="col-md-6">
                        <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">First Name</label>
                            <input type="text" className="form-control" name="first_name" id="first_name" aria-describedby="nameHelp"
                                ref={register({ required: "This field is required" })}
                                defaultValue={addressDetails?.first_name}
                            />
                            {errors.last_name && <span className="error">{errors.last_name?.message}</span>}
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">Last Name</label>
                            <input type="text" className="form-control" name="last_name" id="l_name" aria-describedby="nameHelp"
                                ref={register({ required: "This field is required" })}
                                defaultValue={addressDetails?.last_name}
                            />
                            {errors.last_name && <span className="error">{errors.last_name?.message}</span>}
                        </div>
                    </div>
                </div>


                <div className="row">
                    <div className="col-md-12 company_space">
                        <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">Company Name</label>
                            <input type="text" className="form-control" name="company_name"
                                defaultValue={addressDetails?.company_name}
                                ref={register({ required: false })} />
                            {errors.company_name && <span className="error">{errors.company_name?.message}</span>}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="mb-3">
                                <label htmlFor="exampleInputEmail1" className="form-label">Address 1</label>
                                <input type="text" className="form-control" name="street_address_1" id="billing_address"
                                    defaultValue={addressDetails?.street_address_1}
                                    aria-describedby="billing_addressHelp"
                                    ref={register({ required: "This field is required" })}
                                />
                                {errors.street_address_1 && <span className="error">{errors.street_address_1?.message}</span>}
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="mb-3" >
                                <label htmlFor="exampleInputEmail1" className="form-label">Address 2</label>
                                <input type="text" className="form-control" name="street_address_2" id="shiping_address"
                                    defaultValue={addressDetails?.street_address_2}
                                    aria-describedby="shiping_addressHelp"
                                    ref={register({ required: false })}
                                />

                            </div>
                        </div>
                    </div>

                </div>
                <div className="col-md-6">
                    <div className="mb-3">
                        <label htmlFor="exampleInputEmail1" className="form-label">Postal Code</label>
                        <input type="text" className="form-control" name="postal_code"
                            defaultValue={addressDetails?.postal_code}
                            ref={
                                register({
                                    required: "This field is required",
                                    pattern: {
                                        // value: /^[0-9]/,
                                        // message: "Enter only number"
                                    },
                                    minLength: {
                                        value: 4,
                                        message: "Enter minimum 4 digits"
                                    },
                                })
                            } />
                        {errors.postal_code && <span className="error">{errors.postal_code?.message}</span>}
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-6">

                        <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">Country</label>
                            <div className="select">
                                <CountryDropdown
                                    ref={register}
                                    value={country}
                                    priorityOptions={['US', 'CA']}
                                    name="country"
                                    valueType="short"
                                    onChange={(val) => setCountry(val)}
                                />
                            </div>
                            {country === undefined && <span className="error">{countryError}</span>}
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="mb-3">
                            {country &&
                                <>
                                    <label htmlFor="exampleInputEmail1" className="form-label">State</label>
                                    <div className="select">
                                        <RegionDropdown
                                            country={country}
                                            value={state}
                                            name="state"
                                            valueType="full"
                                            defaultOptionLabel="Select State"
                                            countryValueType="short"
                                            onChange={(val) => setState(val)} />
                                    </div>
                                    {state === undefined && <span className="error">{stateError}</span>}
                                </>
                            }
                        </div>
                    </div>
                </div>


                <div className="row">
                    <div className="col-md-6">
                        <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">City</label>
                            <div >
                                <input
                                    type="text"
                                    name="city"
                                    defaultValue={addressDetails?.city}
                                    placeholder="please enter your city"
                                    ref={register({ required: "This field is required" })}
                                />
                            </div>
                            {errors.city && <span className="error">{errors.city?.message}</span>}
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">Phone</label>
                            <input type="text" className="form-control" name="phone_number"
                                defaultValue={addressDetails?.phone_number?.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3")}
                                ref={
                                    register({
                                        required: "This field is required",
                                        // pattern: {
                                        //     value: /^[0-9]/,
                                        //     message: "Enter only number"
                                        // },
                                        // minLength: {
                                        //     value: 10,
                                        //     message: "Phone number must be 10 digit"
                                        // },
                                        // maxLength: {
                                        //     value: 10,
                                        //     message: "Phone number not longer than 10 digit"
                                        // },
                                    })}
                            />
                            {errors.phone_number && <span className="error">{errors.phone_number?.message}</span>}
                        </div>
                    </div>

                </div>


                <div className="row">
                    <div className="col-md-12">
                        {addresstype === 'billing' ?
                            <div className="mb-3 form-check">
                                <input type="checkbox" className="form-check-input" id="shipping_address_is_same" name="shipping_address_is_same"
                                    ref={register({ required: false })}
                                />
                                <p className="form-check-label">same for shipping Address?</p>
                            </div>
                            :
                            <input type="hidden" className="form-control" name="shipping_address_is_same" id="shipping_address_is_same" aria-describedby="nameHelp"
                                ref={register({ required: "This field is required" })}
                                value={false}
                            />
                        }
                    </div>
                </div>
                {(addressDetails !== null) ?
                    <button type="submit" className="btn btn-primary" onClick={() => handleCountryStateError()}  >Update</button>
                    :
                    <button type="submit" className="btn btn-primary" onClick={() => handleCountryStateError()}  >Next</button>
                }
                {sameAddressError && <span className="error deletemsg" style={{ color: 'green', fontSize: "16px", display: "flex", justifyContent: "center" }}>{sameAddressError}</span>}
            </form>
        </div >
        {/* {data_address1 && <PayPal checkoutData={data_address1}/>}
        <PayPal checkoutData={data_address1}/> */}
    </>
}