import api from "@api/Apis";
import {
    CheckCircle,
    Clear,
    HighlightOff,
    Visibility,
    VisibilityOff,
} from "@material-ui/icons";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { useFormContext } from "react-hook-form";
function CheckoutLogin(props) {
    const { refferalCode, setRefferalCode, logintoken, subDomain, hide, setHide } = props;

    // const { register, handleSubmit, getValues, errors } = methods;

    const router = useRouter();
    const methods = useFormContext();

    const [passwordShow, setPasswordShow] = useState({
        password: false,
        confirm_password: false,
    });

    async function onBlurChange(e) {
        const email = e.target.value;
        await api
            .CheckUserEmail({ email })
            .then((res) => {
                console.log(res);
                if (res?.status === 200) {
                    setHide({ loginLink: true, passwordField: false });
                }
                if (res?.status === 401) {
                    setHide({ loginLink: false, passwordField: true });
                }
            })
            .catch((err) => {
            });
    }
    // <LoginHtml loginProps={{
    //     register, errors, showPassword, diplayeSignup: false, passwordShow, onBlurChange
    // }} />

    function VisibilityIconsCheck(condition, section) {
        return condition ? (
            <Visibility
                onClick={() => setPasswordShow({ ...passwordShow, [section]: false })}
            />
        ) : (
            <VisibilityOff
                onClick={() => setPasswordShow({ ...passwordShow, [section]: true })}
            />
        );
    }


    async function VarificationFunction() {
        if (refferalCode?.value) {
            await api
                .subDomainCheckFunction({ referral_code: refferalCode?.value })
                .then((res) => {
                    if (res?.status === 200) {
                        if (res?.data?.status === true) {
                            setRefferalCode({ ...refferalCode, verify: true });
                        } else {
                            setRefferalCode({ ...refferalCode, verify: false });
                        }
                    }
                })
                .catch((err) => console.log(err));
        }
    }

    return (
        <div className="container">
            {!logintoken ? (
                <>
                    <div className="mainSignupTop">
                        <div className="formLeftdata">
                            <h4 className="title">Signup</h4>
                            <div className="main-left">
                                <div className="field-class">
                                    <label htmlFor="exampleInputEmail1" className="form-label">
                                        Email Address
                                    </label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        onBlur={(e) => onBlurChange(e)}
                                        ref={methods?.register({
                                            required: "Please enter your email",
                                            pattern: {
                                                value:
                                                    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
                                                message: "Invalid Email Address",
                                            },
                                        })}
                                        name="email"
                                        id="exampleInputEmail1"
                                        aria-describedby="emailHelp"
                                    />
                                    {methods?.errors.email?.message && (
                                        <span className="error">
                                            {methods?.errors.email.message}
                                        </span>
                                    )}
                                </div>
                                {hide?.loginLink && (
                                    <div className="already_exist_customer">
                                        <span>
                                            You are already registered with this email please
                                        </span>
                                        &nbsp;
                                        <a href="/us/login">Login</a>
                                    </div>
                                )}
                                {hide?.passwordField && (
                                    <>
                                        <div className="field-class">
                                            <label
                                                htmlFor="exampleInputPassword1"
                                                className="form-label"
                                            >
                                                Password
                                            </label>
                                            <div className="form-wrap">
                                                <input
                                                    type={passwordShow?.password ? "text" : "password"}
                                                    className="form-control"
                                                    name="password"
                                                    id="exampleInputPassword1"
                                                    ref={methods?.register({
                                                        required: "This field is required",
                                                        minLength: {
                                                            value: 6,
                                                            message: "Passwords must be at least 6 characters "
                                                        }
                                                    })}

                                                />
                                                {VisibilityIconsCheck(
                                                    passwordShow?.password,
                                                    "password"
                                                )}
                                            </div>
                                            {methods?.errors.password && (
                                                <span className="error">
                                                    {methods?.errors?.password?.message}
                                                </span>
                                            )}
                                        </div>
                                        <div className="field-class">
                                            <label
                                                htmlFor="exampleInputPassword1"
                                                className="form-label"
                                            >
                                                Confirm Password
                                            </label>
                                            <div className="form-wrap">
                                                <input
                                                    type={
                                                        passwordShow?.confirm_password
                                                            ? "text"
                                                            : "password"
                                                    }
                                                    className="form-control"
                                                    name="confirm_password"
                                                    id="exampleInputPassword1"
                                                    ref={methods?.register({
                                                        required: "This field is required",
                                                        validate: (value) =>
                                                            methods?.getValues("password") === value ||
                                                            "Please enter same password",
                                                    })}
                                                />
                                                {VisibilityIconsCheck(
                                                    passwordShow?.confirm_password,
                                                    "confirm_password"
                                                )}
                                            </div>
                                            {methods?.errors.confirm_password && (
                                                <span className="error">
                                                    {methods?.errors?.confirm_password?.message}
                                                </span>
                                            )}
                                        </div>
                                    </>
                                )}
                            </div>
                        </div>
                        {hide?.passwordField && !subDomain && (
                            <div className="main-ref">
                                <div className="refferal_section">
                                    <h4 className="title referrer">Who referred you?</h4>
                                    <div className="main-refferal_section">
                                        <div>
                                            <p>Help us say thank you. </p>
                                            <p>
                                                Please enter the user name of the person who referred
                                                you to Kaire to receive best pricing on future
                                                purchases.
                                            </p>
                                            <div className="d-flex">
                                                <input
                                                    className="form-control"
                                                    type="text"
                                                    name="refferal"
                                                    value={refferalCode?.value}
                                                    onChange={(e) => {
                                                        setRefferalCode({
                                                            value: e.target.value,
                                                            verify: null,
                                                        });
                                                    }}
                                                />

                                                <button
                                                    type="button"
                                                    disabled={!refferalCode?.value}
                                                    onClick={() => VarificationFunction()}
                                                >
                                                    {refferalCode?.verify === true && (
                                                        <CheckCircle style={{ color: "green" }} />
                                                    )}
                                                    {refferalCode?.verify === false && (
                                                        <HighlightOff style={{ color: "red" }} />
                                                    )}
                                                    <span>Verified</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </>
            ) : (
                <div className="empty_div_section"></div>
            )}
        </div>
    );
}

export default CheckoutLogin;
