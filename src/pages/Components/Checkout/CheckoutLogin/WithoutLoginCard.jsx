import React from 'react';
import { useFormContext } from 'react-hook-form';


const MonthYear = ({ start, end, name, type, values, label, validate, methods }) => {
    return (
        <>
            <label htmlFor={name}>{label}</label>
            <select name={name} defaultValue="" ref={methods?.register(validate)} className="form-control">
                <option value="" disabled={true}>{type}</option>
                {(() => {
                    const options = [];
                    for (let i = start; i <= end; i++) {
                        let value = (i < 10) ? ("0" + i) : i;
                        options.push(value)
                    }
                    return options?.map((value, i) => <option value={value} key={value}>{value}</option>)
                })()}
            </select>
            {methods?.errors?.[name] && <span className="error">{methods?.errors?.[name]?.message}</span>}
        </>
    );
}

function WithoutLoginCard() {

    const methods = useFormContext();
    var date = new Date();
    var currentYear = date.getFullYear();

    return (
        <div className="cart_form">
            <h4>Payment Method</h4>
            <div className="main-wrapper-check">
                <div className="section_first">
                    <div className="logo">
                        <img src="/images/creditcard.png/" />
                    </div>
                    <div className="form-wrappper">
                        <input
                            type="text"
                            placeholder="Card Holder Name"
                            name="cardHolder" className="form-control"
                            ref={methods?.register({
                                required: "Please enter your card name."
                            })}
                        />
                        {methods?.errors?.cardHolder && (
                            <span className="error_card">
                                {methods?.errors?.cardHolder?.message}
                            </span>
                        )}
                    </div>
                    <div className="form-wrappper">
                        <input
                            type="text" className="form-control"
                            placeholder="Card Number"
                            name="cardNumber"
                            ref={methods?.register({
                                required: "Please enter your card number.",
                                pattern: {
                                    value: /^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/,
                                    message: "Please Enter Valid card Number"
                                }
                            })}
                        />
                        {methods?.errors?.cardNumber && (
                            <span className="error_card">
                                {methods?.errors?.cardNumber?.message}
                            </span>
                        )}
                    </div>
                </div>
                <div className="section_second">
                    <div className="col-md-6 extra">
                        <MonthYear start={1} end={12} name="card_expiry_month" type="MM" label="Month" validate={{
                            required: "Enter your expiry date."
                        }} methods={methods} />
                    </div>
                    <div className="col-md-6 extra">
                        <MonthYear start={currentYear} end={2050} name="card_expiry_year" type="YY" label="Year" validate={{
                            required: "Enter your expiry Year."
                        }} methods={methods} />
                    </div>
                    <div className="form-wrappper">
                        <label>CVV</label>
                        <input
                            type="text"
                            placeholder="Card CVV "
                            name="cardCVC" className="form-control"
                            ref={methods?.register({
                                required: "Enter CVV.",
                                pattern: {
                                    value: /^[0-9]{3,4}$/,
                                    message: "Enter valid CVV."
                                }
                            })}
                        />
                        {methods?.errors?.cardCVC && (
                            <span className="error_card">
                                {methods?.errors?.cardCVC?.message}
                            </span>
                        )}
                    </div>
                </div>
                <div className='save-credit-check'>
                    <input type="checkbox"
                        ref={methods?.register({ required: false })}
                        className="form-check-input"
                        id="save_credit_card"
                        name="save_credit_card"
                    />  Save Credit Card
                </div>
            </div>
        </div>
    )
}

export default WithoutLoginCard;