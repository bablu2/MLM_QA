import React from "react";
import { CountryDropdown, RegionDropdown } from "react-country-region-selector";
import { useFormContext } from "react-hook-form";

function WithoutForm({ children, ...props }) {
    const methods = useFormContext(); // retrieve all hook methods

    const { section, country, setCountry, state, setState } = props;
    return (
        <div className="signupform main-sign-frm">
            <h4 className="title">{`${section} Address`}</h4>
            <div className="same_as_shipping">{children}</div>
            <div className="main-wrapper d-flex">
                <div className="field-class">
                    <input
                        type="text"
                        className="form-control"
                        name={`${section}_first_name`}
                        id="f_name"
                        aria-describedby="nameHelp"
                        placeholder="First Name"
                        ref={methods?.register({
                            required: "This field is required",
                            minLength: {
                                value: 3,
                                message: "Enter minimum 3 character",
                            },
                        })}
                    />
                    {methods?.errors?.[section + "_first_name"] && (
                        <span className="error">
                            {methods?.errors?.[section + "_first_name"]?.message}
                        </span>
                    )}
                </div>
                <div className="field-class">
                    <input
                        type="text"
                        className="form-control"
                        name={`${section}_last_name`}
                        id="l_name"
                        aria-describedby="nameHelp"
                        placeholder="Last Name"
                        ref={methods?.register({
                            required: "This field is required",
                            minLength: {
                                value: 3,
                                message: "Enter minimum 3 character",
                            },
                        })}
                    />
                    {methods?.errors?.[`${section}_last_name`] && (
                        <span className="error">
                            {methods?.errors?.[`${section}_last_name`]?.message}
                        </span>
                    )}
                </div>
                <div className="field-class company-line">
                    <input
                        type="text"
                        name={`${section}_company_name`}
                        className="form-control"
                        id="city"
                        placeholder="Company Name"
                        ref={methods?.register({
                            required: false
                        })}
                    />
                </div>

                <div className="field-class">
                    <input
                        type="text"
                        name={`${section}_street_address_1`}
                        className="form-control"
                        id="address1"
                        placeholder="Address 1"
                        ref={methods?.register({
                            required: "This field is required",
                        })}
                    />
                    {methods?.errors?.[`${section}_street_address_1`] && (
                        <span className="error">
                            {methods?.errors?.[`${section}_street_address_1`]?.message}
                        </span>
                    )}
                </div>


                <div className="field-class">
                    <input
                        type="text"
                        name={`${section}_street_address_2`}
                        className="form-control"
                        id="address1"
                        placeholder="Address 2"
                        ref={methods?.register({
                            required: false,
                        })}
                    />
                </div>
                <div className="field-class">
                    <input
                        type="text"
                        name={`${section}_postal_code`}
                        className="form-control"
                        id="zipcode"
                        placeholder="Zip Code"
                        ref={methods?.register({
                            required: "This field is required",
                            minLength: {
                                value: 4,
                                message: "Enter minimum 4 digits",
                            },
                        })}
                    />
                    {methods?.errors?.[`${section}_postal_code`] && (
                        <span className="error">
                            {methods?.errors?.[`${section}_postal_code`]?.message}
                        </span>
                    )}
                </div>

                <div className="field-class">
                    <div className="select">
                        <CountryDropdown
                            value={country}
                            priorityOptions={['US', 'CA']}
                            defaultOptionLabel="Country"
                            name={`${section}_country'`}
                            valueType="short"
                            onChange={(val) => setCountry(val)}
                        />
                    </div>

                </div>
                <div className="field-class">
                    <div className="select">
                        <RegionDropdown
                            country={country}
                            disabled={!country}
                            value={state}
                            name={`${section}_state'`}
                            valueType="full"
                            defaultOptionLabel="State"
                            countryValueType="short"
                            onChange={(val) => setState(val)}
                        />
                    </div>

                </div>
                <div className="field-class">
                    <input
                        type="text"
                        name={`${section}_city`}
                        className="form-control"
                        id="city"
                        placeholder="City"
                        ref={methods?.register({
                            required: "This field is required",
                        })}
                    />
                    {methods?.errors?.[`${section}_city`] && (
                        <span className="error">
                            {methods?.errors?.[`${section}_city`]?.message}
                        </span>
                    )}
                </div>



                <div className="field-class company-line">
                    <input
                        type="text"
                        className="form-control"
                        name={`${section}_phone_number`}
                        id="phone_no"
                        aria-describedby="emailnoHelp"
                        placeholder="Phone"
                        ref={methods?.register({
                            required: "This field is required",
                            pattern: {
                                value: /^[0-9]*$/,
                                message: "Enter only number",
                            },
                            validate: (value) =>
                                value.match(/^(\+\d{1,3}[- ]?)?\d{10}$/) ||
                                "Please enter 10 digit.",
                        })}
                    />
                    {methods?.errors?.[`${section}_phone_number`] && (
                        <span className="error">
                            {methods?.errors?.[`${section}_phone_number`].message}
                        </span>
                    )}
                </div>
            </div>
        </div>
    );
}

export default WithoutForm;
