import styled from "styled-components";

const CheckoutLoginStyle = styled.div`
.lowerSection {
    padding: 0 15px;
    @media (max-width: 767px){
      padding: 0;
    }
}
.checkbox {
  position: relative;
    padding: 20px 10px 20px 40px;
    background: #fff;
    margin-top: 40px;
    @media (max-width: 767px){
      margin-top: 15px;
    }
  input {
    height: auto;
}
 
label.form-check-label:empty {
    display: none;
}
}
  .form_sections {
    .container.main_sign_up_form_class {
      display: flex;
      gap: 30px;
      @media (max-width: 991px){
        flex-wrap: wrap;
        gap: 0;
      }
    } 
  }
  input {
    border-radius: 30px;
    height: 40px;
    box-shadow: none;
    padding-right: 40px;
  }

  .mainSignupTop {
    display: flex;
    justify-content: space-between; 
    gap: 30px;
    @media (max-width: 991px){
      flex-wrap: wrap;
    }
    & > div {
      width: 100%;
      max-width: calc(50% - 10px);
      background: #fff;
      box-shadow: 0 0 20px 7px rgba(0, 0, 0, 0.05);
      @media (max-width: 991px){
        max-width: 100%;
      }
      h4.title {
        margin: 0 0 0px;
      }
    }
    .formLeftdata {
      .main-left {
        padding: 25px;
        float: left;
        width: 100%;
        .field-class:first-child {
          .form-label {
            margin-top: 0;
          }
        } 
        .form-wrap {
          float: left;
          width: 100%;
          svg {
            position: absolute;
            right: 15px;
            top: 50%;
            transform: translateY(-50%);
          }
        }
      }
    }
    .main-ref {
      .main-refferal_section {
        padding: 20px;
        p {
          font-size: 15px;
          line-height: 1.5;
          margin-bottom: 25px;
          &:first-child {
            font-family: var(--common-font-bd);
            font-size: 16px;
            margin-bottom: 15px;
          }
        }

        .d-flex {
          align-items: center;
          max-width: 70%;
          border: 1px solid #ddd;
          border-radius: 30px;
          overflow: hidden;
          margin-left: 0;
          margin-right: auto;
          position: relative;
          @media (max-width: 767px){
            max-width: 100%;
          }
          .form-control {
            border: none;
            box-shadow: none;
            @media (max-width: 767px){
              padding-right: 150px;
            }
          }
          button {
            margin: 0 !important;
            max-width: 145px;
            width: 100%;
            font-size: 14px;
            background: #999;
            font-weight: 600;
            display:inline-flex;
            justify-content:center;
            align-items:center;
            @media (max-width: 767px){
              button {
                position: absolute;
                right: 3px;
            }
            }
            &:hover {
              background: #fff;
              color: #999;
            }
            svg{
              margin-right:5px; 
            }
          }
          button:not([disabled]) {
            border-color: var(--blue);
            background: var(--blue);
            &:hover {
              color: var(--blue);
              background: #fff;
            }
          }
        }
      }
    }
  }
  .container.main_sign_up_form_class { 
    .signupform.main-sign-frm {
      margin-top: 50px;
      padding: 0;
      display: block;
      margin-bottom: 0;
      border-radius: 0;
      @media (max-width: 991px){
        flex-wrap: wrap;    width: 100%;
      }
      @media (max-width: 991px){
        margin-top: 30px;
      }
      h4.title {
        width: 100%; 
        @media (max-width: 767px){
          margin-top: 0;
        }
      }

    }
  }
  .same_as_shipping {
    width: 100%;  
    &>div{
      justify-content: center;
    }
    input[type="checkbox"]{
      height: auto;
    }
    div {
      display: flex;
      align-items: center;
      margin-bottom: 25px;
      font-size: 14px;
      color: var(--blue);
      font-weight: 600;
      line-height: 1;
      input[type="checkbox"] {
        margin-right: 10px;
        margin-top: 1px;
        height: auto;
    }
    span.error {
      position: absolute;
      top: 100%;
      left:0;
      font-size: 12px;
  }
  }
}
  .main-wrapper {
    width: 100%;
    flex-wrap: wrap;
    padding:0 20px 20px;
    justify-content: space-between;
    gap: 25px 20px;
    @media (max-width: 767px){
      gap: 15px 20px;
    }
    .field-class {
      margin: 0 !important;
      &:nth-child(5){
        /*border:1px solid red;*/
      }
    } 
    .signupform.main-sign-frm {
      margin-bottom: 0;
      border-radius: 0;
    }
  }
  .cart_form {
    background: #fff;
    max-width: 600px;
    margin: 0 auto 0 0;
    @media (max-width: 991px){
      max-width: 100%
    }
    h3 {
      margin: 0;
      text-align: center;
      background: var(--blue);
      color: #fff;
      font-size: 18px;
      padding: 10px;
  } 
 
}
.main-wrapper-check{
  padding: 20px;
  .section_second {
    display: flex;
    gap: 15px;
    justify-content: space-between;
    @media (max-width: 767px){
      gap: 0;
      flex-wrap: wrap;
    }
      .form-wrappper {
        max-width: 100%;
        flex: 100%;
    }
  } 
}
.form-wrappper {
  margin-bottom: 15px;
  span.error_card {
    color: #f00;
    font-size: 12px;
}
}
.section_first{
  input.form-control {
    padding: 10px;
  }
}
.row.view-review {
  @media (max-width: 767px){
    flex-wrap:  wrap;
  }
  &:after {
    display: none;
  }
  display: flex;
  gap: 0 0;
  align-items: center;
  .col-md-6 {
    max-width: calc(100%/2 - 10px);
    padding: 0;
    @media (max-width: 767px){
      max-width: 100%;
      flex: 100%;
    }
  }
}
.wrap-checkout{
  a {
    color: var(--blue);
    font-weight: 600;
}
}

/* form-email */
.title+.mb-3.field-class>.form-label input {
  border-radius: 30px;
}

.container.main_sign_up_form_class .signupform.main-sign-frm {
  border-radius: 0;
}

.container.main_sign_up_form_class::before,
.container.main_sign_up_form_class::after {
  display: none;
}

.container.main_sign_up_form_class .signupform.main-sign-frm .title {
  margin: 0 0 25px !important;
  /* height: 46px;  */
}

.MuiBackdrop-root[aria-hidden="true"] {
  background: rgba(0, 0, 0, 0.6);
}

.main_sign_up_form_class .signupform.main-sign-frm .main-wrapper.d-flex .field-class:where(:nth-child(9), :nth-child(8), :nth-child(12), :nth-child(13)) {
  margin: 0 !important;
}

.already_exist_customer a {
  color: var(--blue);
}

.container.checkout-main.checkout_main_section_class {
  margin-bottom: 45px;
}

.container.checkout-main.checkout_main_section_class .cart_form+button {
  margin: 25px 0 0 auto;
  display: flex;
  align-items: center;
  line-height: normal;
  padding: 0 25px;
  font-size: 14px;
}

`;

export default CheckoutLoginStyle;
