import React from 'react'
import { useFormContext } from 'react-hook-form'

function WithoutLoginConditon({ smartShipCheck }) {
    const methods = useFormContext();

    return (
        <div className="checkbox">
            <div className="form-group">
                <div className="policy">
                    <div className='wrap-checkout'>
                        <input type="checkbox" className="form-check-input"
                            id="termsconditions" name="termscondition1"
                            ref={methods?.register({
                                required: "please accept the condition"
                            })} />
                        <label className="form-check-label" for="termsconditions"></label> I have read and agree to the
                        <a className="privacy_style" target='_blank' href='https://admin.shopkaire.com/media/pdf/Kaire_ReturnPolicy.pdf'> Return Policy</a>,
                        <a className="privacy_style" target='_blank' href='https://admin.shopkaire.com/media/pdf/Kaire_PrivacyPolicy.pdf'> Privacy Policy</a>, and
                        <a className="privacy_style" target='_blank' href='https://admin.shopkaire.com/media/pdf/Kaire_TermsConditions.pdf'> Terms & Conditions </a>
                    </div>
                </div>

                {methods?.errors.termscondition1 && (<div style={{ color: "red" }}>{methods?.errors.termscondition1?.message} </div>)}
            </div>

            {smartShipCheck && (
                <div className="form-group">
                    <div className='wrap-checkout'>
                        <input
                            type="checkbox"
                            className="form-check-input"
                            id="acknowledging"
                            name="termscondition2"
                            ref={methods?.register({
                                required: "please accept the condition"
                            })}
                        />

                        <label className="form-check-label" for="acknowledging" ></label> By acknowledging, I am agreeing to an annual
                        automatic renewal purchase.This purchase will be
                        automatically processed each month on the Smartship
                        Order renewal date.
                    </div>

                    {methods?.errors.termscondition2 && (
                        <div style={{ color: "red" }}>
                            {methods?.errors.termscondition2?.message}
                        </div>
                    )}
                </div>
            )}
        </div>
    )
}

export default WithoutLoginConditon;