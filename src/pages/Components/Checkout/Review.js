import Head from "next/head"
import { forwardRef, useEffect, useImperativeHandle, useRef, useState } from "react";
import api from '@api/Apis'
import Discount, { totalPaidFunction } from "./Discount";
import NumberFormat from 'react-number-format';
import { FaTrash } from "react-icons/fa";
import Hover from "../../Components/Cart/hovercomponent";
import cookieCutter from 'cookie-cutter'
import { toast } from 'react-toastify';
import PayCardSection from "../../../Components/Common/PayCardSection";
import ShippingSystem from "@Components/Common/ShippingSystem";
import NumberFormatComp from "@Components/Common/NumberFormatComp";
import CustomPackTable from '../../[page]/cart/CustomPackTable';
import { FormControlLabel, Radio, RadioGroup } from '@material-ui/core'
import { KeyboardArrowDown, KeyboardArrowRight } from "@material-ui/icons";
import { useRouter } from "next/router";
import SmartshipPopup from "src/pages/[page]/checkout/SmartshipPopup";
import WithoutLoginCard from "./CheckoutLogin/WithoutLoginCard";
import { useFormContext } from "react-hook-form";

const Review = forwardRef(({
    section, setShowError, refferalCode, costtype, setCartData, cartdata, setcosttype, _onLoadSetCostType, minCartAutoshipCheck, smartShipCheck, setSmartShipCheck, reviewshow, ShowReviewPage, setcounts, counts,
    autoShipAmount, shippingaddress, billingaddress, setInnerLoader, setshowloader, validateauth, withoutLogin,
}, ref) => {

    const methods = useFormContext(); // retrieve all hook methods
    // const { register, handleSubmit, errors } = useForm();

    const router = useRouter();
    const [logintoken, setToken] = useState();
    const [discount_amount1, setdiscount_amount] = useState();
    const [paidamount, setpaidamount] = useState();
    const [shipamount, setshipamount] = useState();
    const [copanerror, setcopanerror] = useState();
    // const [cartdata, setCartData] = useState();
    const [coupon, setcoupon] = useState();
    // const [costtype, setcosttype] = useState({ data: [{ id: '', value: '' }] })
    const [kairecash, setkairecash] = useState(false);
    // const [kairecashamout, setkairecashamout] = useState();
    const [kairecashamoutmain, setkairecashamoutmain] = useState(0);
    const [autoshipis, setautoshipis] = useState(false);
    const [saveCardId, setSavedCardId] = useState(null);
    const [showContinue, setShowContinue] = useState(null);
    const [shippingChargesAmount, setShippingChargesAmount] = useState(null);
    const [checkCustomPack, setCheckCustomPack] = useState({ products: [], totalAmount: 0 });
    const [freeShipping, setFreeShipping] = useState(false)
    const [loader, setLoader] = useState(false)
    const [buttonDis, setButtonDis] = useState(false)
    const [selectedCheckBox, setSelectedCheckBox] = useState('');
    const [kaireVisible, setKaireVisible] = useState({
        checked: false, available: false, kaireAmount: null, reduceAmount: null
    });
    const [walletVisible, setWalletVisible] = useState({
        checked: false, available: false,
        kaireAmount: null, reduceAmount: null
    });
    const [expend, setExpend] = useState(false);
    const [bundleProduct, setBundleProduct] = useState({});
    const [smartshipOpen, setSmartShipOpen] = useState(false);
    const [newCardData, setNewCardData] = useState({})
    const [showErrorPayment, setShowErrorpayment] = useState('')
    //without Login
    const [withoutLoginShippingError, setWithoutLoginShippingError] = useState(null);
    const [kaireCashAdd, setKaireCashAdd] = useState()
    const [cashError, setCashError] = useState('')
    const [kaireCashError, setKaireCashError] = useState('')
    let QTYCOUNTS = cartdata?.products?.reduce(
        (prevValue, currentValue) => prevValue + (+currentValue?.quantity), 0);

    function GetCounts(count) {
        const dataSss = localStorage.getItem('packProduct') ? JSON.parse(localStorage.getItem('packProduct')) : null
        let couuntValue = (dataSss !== null) ? count + 1 : count
        setcounts(couuntValue)
    }

    useEffect(() => {
        // setInnerLoader(true)
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        if (token) {
            validateauth(token);
        }
        api.getAllCartProduct(token).then(res => {
            if (res?.data?.code === 1) {
                const onlyBundleProduct = _.reject(res?.data?.products, (row) => row?.product?.product_type !== "Bundle");
                _.map(onlyBundleProduct, (row) => {
                    _getBundleData(row?.product?.product_type, row?.product?.id);
                })
                GetCounts(QTYCOUNTS)
                setCartData(res?.data);
                setcounts(+QTYCOUNTS)
                setkairecashamoutmain(res?.data?.available_kaire)
                const objIndex = res?.data?.products?.findIndex((obj => obj.is_autoship == 'True'));
                if (objIndex !== -1) {
                    setautoshipis(true)
                }
                setInnerLoader(false)
            }
            else {
                setInnerLoader(false)
            }
        })

        const data = localStorage.getItem('packProduct');
        if (data) {
            let values = JSON.parse(data);
            setCheckCustomPack({ products: values?.customPack?.products, totalAmount: values?.customPack?.totalAmount });
        }
        setpaidamount(reviewshow?.amount);
        redeemCash(token);
    }, [reviewshow, shippingChargesAmount]);

    const redeemCash = (token) => {
        api.redeemKaireCash(reviewshow?.subamount, token).then(res => {
            if (res?.status === 200) {
                const { wallet_commission_availability, kaire_cash_availability, commission_wallet, kaire_cash } = res?.data;
                setWalletVisible((prv) => ({
                    checked: prv.checked === true ? true : false,
                    available: wallet_commission_availability,
                    WallatAmount: commission_wallet,
                    reduceAmount: null
                }))
                setKaireVisible((prv) => ({
                    checked: prv.checked === true ? true : false,
                    available: kaire_cash_availability,
                    kaireAmount: kaire_cash,
                    reduceAmount: null
                }))
                setKaireCashAdd((prv) => prv ? prv : '');

            }
        }).catch((err) => console.log(err));
    }

    function _setAutoshipFunction(event, id, orderId) {

        // const found = _.find(costtype, { id: id });
        // if (found) {
        //   const removeData = _.reject(costtype, found);
        //   setcosttype([...removeData, { id: id, autoship: event?.target?.value }]);
        // } else {
        //   setcosttype([...costtype, { "id": id, "autoship": event?.target?.value }]);
        // }
        swithtoautoship(event, orderId);
    }
    const swithtoautoship = async (e, cartId) => {
        let qty = e.currentTarget.getAttribute('data-product-qty')
        let variant_id = e.currentTarget.getAttribute('data-variant-id')
        let product_id = e.currentTarget.getAttribute('data-product-id')
        let types = e.currentTarget.value === 'AutoShip' ? true : false

        const datas = {
            product_id: +(product_id),
            id: cartId,
            variant_id: variant_id,
            quantity: +(qty),
            cookie_id: cookieCutter.get('sessionkey'),
            is_autoship: types
        }
        const update_data = {
            token: logintoken,
            datas: datas
        }
        await api.updateCart(update_data).then(res => {
            if (res?.status === 200 && res?.data) {
                PRODUCT_cart(logintoken);
            }
        })
    }
    async function PRODUCT_cart(token) {
        await api.getAllCartProduct(token).then(res => {
            if (res?.status === 200) {
                setCartData(res?.data);
                GetCounts(res?.data?.products?.length)
                const objIndex = res?.data?.products?.findIndex((obj => obj.is_autoship == 'True'));
                const status = (objIndex > -1) ? true : false;
                setautoshipis(status);
                _onLoadSetCostType(res?.data?.products)
                setSmartShipCheck(status);
            }
        })
    }

    //get bundleProduct data usign product id
    function _getBundleData(productType = "Product", productId) {
        if (productType === 'Bundle') {
            api.getBundleProduct(productId, logintoken).then(res => {
                if (res?.data?.code === 1) {
                    setBundleProduct({ ...bundleProduct, [productId]: res?.data?.bundle_data })
                }
            }).catch((err) => {
                console.log("bbbb", err);
            })
        }
    }
    useImperativeHandle(ref, () => ({
        placerorder: (res) => placerorder(res)
    }))

    //Place Create order
    const placerorder = async (data = null) => {
        setButtonDis(true)
        // let payamount = reviewshow.amount - reviewshow.discount_amount < 0 ? 0 : reviewshow.amount - reviewshow.discount_amount;
        // payamount = payamount + (reviewshow.shipping_amount || shippingChargesAmount) + reviewshow.tax_amount + checkCustomPack?.totalAmount;
        let payamount = paidamount + reviewshow?.tax_amount;
        // if (kairecash === true) {
        // payamount = payamount - (+cartdata?.available_kaire)
        // }
        let shipping_address = parseFloat(shippingaddress, 2) || null;
        let billing_address = parseFloat(billingaddress, 2) || null

        if (data !== null) {
            Object.assign(data, { line2: data["line2"] !== "" ? data["line2"] : null })
        }
        const orderdata = {
            "billing_details": data?.billing_details ? data?.billing_details : null,
            "payment_details": data === null ? {} : data?.payment_details,
            "save_credit_card": data?.save_credit_card ? data?.save_credit_card : false,
            "shipping_address_id": shipping_address,
            "billing_address_id": data?.billing_details !== undefined && Object.keys(data?.billing_details)?.length > 0 ? "" : billing_address,
            "amount": parseFloat((reviewshow?.amount), 2),
            "tax_amount": reviewshow.tax_amount,
            "shipping_amount": parseFloat(shippingChargesAmount, 2),
            "gross_total": parseFloat(payamount, 2),
            "coupon_id": parseInt(reviewshow.coupon_id),
            "discount_amount": reviewshow.discount_amount,
            "amount_paid": parseFloat(payamount, 2),
            "sku": "umb-20",
            "coupon": localStorage.getItem('coupon_name')?.length > 0 ? localStorage.getItem('coupon_name') : null,
            "shipping_module_id": +showContinue,
            "saved_card_id": saveCardId ? saveCardId : null,
            "products": reviewshow.products,
            "is_autoship": autoshipis,
            "kaire_cash_used": kaireVisible?.checked === true ? kaireCashAdd : '',
            "is_kaire_cash_used": kaireVisible?.checked,
            "wallet_cash_used": walletVisible?.checked === true ? walletVisible?.reduceAmount : '',
            "is_wallet_used": walletVisible?.checked,
            "order_total_bv": reviewshow?.bonus_value,
            "order_for": 'self',
        }
        if (checkCustomPack?.products?.length > 0) {
            orderdata['packData'] = checkCustomPack?.products;
            orderdata['custompack_total_amount'] = checkCustomPack?.totalAmount
            orderdata['customPack'] = true;
        } else {
            orderdata['customPack'] = false;
        }

        if (billing_address !== null && shipping_address !== null && logintoken) {
            _orderSubmissionApi(orderdata, data, logintoken)
        }
        else {
            orderdata['new_user'] = data['new_user'];
            orderdata['new_billing_address'] = data['new_billing_address'];
            if (shippingChargesAmount) {
                _orderSubmissionApi(orderdata, data, logintoken)
            } else {
                setWithoutLoginShippingError("Please select shipping method.")
            }
        }
    }


    async function _orderSubmissionApi(orderdata, data, logintoken) {
        setInnerLoader(true)
        if (kaireVisible?.checked === true && !kaireCashAdd) {
            setKaireCashError('Please Add Kaire Cash')
            return;
        }
        const result = await api.createOrder(orderdata, logintoken, 'self').then(res => {
            if (res?.data?.code === 1) {
                setLoader(false)
                setButtonDis(false)
                setcounts()
                setInnerLoader(false)
                // toast.success(res?.data?.message)
                localStorage.removeItem('coupon_name')
                localStorage.removeItem('PromoterPack')
                localStorage.removeItem('packProduct')
                router.push({
                    pathname: `/${router.query.page}/order/thankYou`,
                    query: { orderid: res?.data.order_id }
                });

            } else {
                if (data === null) {
                    // setLoader(false)
                    setButtonDis(false)
                    // toast.error(result?.data?.message)
                    setInnerLoader(false)
                }
                if (res?.data?.code === 0) {
                    setInnerLoader(false)
                    setShowErrorpayment(res?.data?.message)
                    setTimeout(() => {
                        setShowErrorpayment('')
                    }, 3000);
                    setShowError(res?.data?.message)
                    setTimeout(() => {
                        setShowError('')
                    }, 3000);
                    setInnerLoader(false)
                }
                setLoader(false)
                // setShowError(res?.data?.message ? res?.data?.message : "Server Error");
            }
            return res;
        });
        return result;
    }

    function smartshipCheckPopup(value) {
        setNewCardData(value);
        if (smartShipCheck) {
            setSmartShipOpen(true);
            // placerorder(value);
        } else {
            placerorder(value);

        }
    }
    // remove coupon
    const removecoupon = () => {
        localStorage.removeItem('coupon_name');
        setdiscount_amount(0);
        setFreeShipping(false);
        if (kaireVisible?.checked === true) {
            let amount = checkShippingForKaire(reviewshow['amount'] + discount_amount1, "kaire", false);
            setpaidamount(amount);
        } else if (walletVisible?.checked === true) {
            let amount = checkShippingForKaire(reviewshow['amount'] + discount_amount1, "wallet", false);
            setpaidamount(amount);
        } else {
            let currentAmount = (reviewshow['amount'] + shippingChargesAmount);
            setpaidamount(currentAmount);
        }
        setcoupon('')
        ShowReviewPage(logintoken)
    }

    // kaire which paid 
    function _kaireCashtoPaid(totalPaid, freeShipCheck, onChangeValue) {
        let checkFreeShip = 0;
        if (freeShipCheck === false) {
            checkFreeShip = 0;

        } else if (freeShipCheck === true) {
            checkFreeShip = shippingChargesAmount;
        }

        let kiareValues = onChangeValue ? +onChangeValue : kaireCashAdd;
        let kairePaid = totalPaid
        if (kiareValues - totalPaid > 0) {
            kairePaid = totalPaid - totalPaid;
            if (freeShipCheck !== true) {
                setKaireVisible(prv => ({ ...prv, reduceAmount: totalPaid + checkFreeShip }));
            } else {
                setKaireVisible(prv => ({ ...prv, reduceAmount: totalPaid }));
            }
        } else {
            kairePaid = totalPaid - kiareValues
            setKaireVisible(prv => ({ ...prv, reduceAmount: kiareValues }));
        }
        if (shippingChargesAmount) {
            kairePaid += shippingChargesAmount - checkFreeShip;
        }
        return kairePaid;
    }

    //wallet money spended 
    function _walletToPaid(totalPaid) {
        let walletPaid = totalPaid;
        if (walletVisible?.WallatAmount - totalPaid > 0) {
            walletPaid = totalPaid - totalPaid;
            setWalletVisible(prv => ({ ...prv, reduceAmount: totalPaid }));
        } else {
            walletPaid = totalPaid - kaireCashAdd
            setWalletVisible(prv => ({ ...prv, reduceAmount: walletVisible?.WallatAmount }));
        }
        return walletPaid;
    }

    const checkShippingForKaire = (total_amount, section, freeShipCheck, onChangeValue) => {
        let totalPaid = total_amount;
        //kaire discount check
        if (section === 'kaire') {
            if (discount_amount1 && !freeShipCheck) {
                if ((totalPaid - discount_amount1) > 0) {
                    totalPaid = totalPaid - discount_amount1;
                    totalPaid = _kaireCashtoPaid(totalPaid, freeShipCheck,);
                } else {
                    totalPaid = totalPaid - 0;
                    totalPaid = _kaireCashtoPaid(totalPaid, freeShipCheck);
                }
            } else {
                totalPaid = _kaireCashtoPaid(+reviewshow?.subamount, freeShipCheck);
            }
        }
        else {
            let shippingAmount = shippingChargesAmount ? shippingChargesAmount : 0;
            if (discount_amount1 && !freeShipCheck) {
                if ((totalPaid - discount_amount1) > 0) {
                    totalPaid = totalPaid - discount_amount1;
                } else {
                    totalPaid = totalPaid - 0;
                }
            }
            totalPaid = _walletToPaid(totalPaid + shippingAmount);
        }
        return totalPaid;
    }

    const applyKaireCash = (e, section, onChangeValue) => {
        let shippingAmount = shippingChargesAmount ? shippingChargesAmount : 0;
        if (e.target.checked) {
            if (section === "kaire") {
                const toalValuePaid = checkShippingForKaire(reviewshow?.amount, "kaire", freeShipping);
                setpaidamount(toalValuePaid);
                setKaireVisible((prev) => ({ ...prev, checked: true }))
                setWalletVisible((prev) => ({ ...prev, checked: false }))
            } else {
                const toalValuePaid = checkShippingForKaire(reviewshow?.amount, "wallet", freeShipping);
                setpaidamount(toalValuePaid);
                setKaireVisible((prev) => ({ ...prev, checked: false }))
                setWalletVisible((prev) => ({ ...prev, checked: true }))
            }
            setSelectedCheckBox(section);
        } else {
            const checkFreeShip = (freeShipping === true) && 0
            // : shippingChargesAmount;
            if (section === "kaire") {
                if (kaireCashAdd > 0) {
                    const currentAmount = paidamount + (+kaireCashAdd);
                    setpaidamount(currentAmount + checkFreeShip);
                }
                setKaireVisible((prev) => ({ ...prev, checked: false }))
            } else {
                if (walletVisible?.reduceAmount > 0) {
                    setpaidamount(walletVisible?.reduceAmount);
                }
                setWalletVisible((prev) => ({ ...prev, checked: false }))
            }
            setSelectedCheckBox('');
        }

    }

    useEffect(() => {
        if (shippingChargesAmount) {
            if (walletVisible?.checked === true) {
                setpaidamount(0);
                setWalletVisible((prv) => ({ ...prv, reduceAmount: prv.reduceAmount + parseFloat(shippingChargesAmount) }))

            } else {
                let values = 0;
                if (discount_amount1) {
                    values = (+reviewshow?.subamount - +discount_amount1) + parseFloat(shippingChargesAmount)
                    if (kaireCashAdd && kaireVisible?.checked) {
                        values = values - kaireCashAdd;
                    }
                } else {
                    values = +reviewshow?.subamount + parseFloat(shippingChargesAmount)
                    if (kaireCashAdd && kaireVisible?.checked) {
                        values = values - kaireCashAdd;
                    }
                }
                setpaidamount(values)
            }
            setWithoutLoginShippingError(null);
        }
    }, [shippingChargesAmount, reviewshow,])


    useEffect(() => {
        if (!(cartdata?.products)) {
            const data = localStorage.getItem('packProduct');
            if (!data) {
                router.push('/us/allProduct/')
            }
        }
    }, [cartdata])

    function _changeAmountOnCouponBased(shippingAmount, freeShipDiscount = reviewshow['discount_amount']) {
        totalPaidFunction(reviewshow,
            freeShipDiscount, paidamount,
            setpaidamount, selectedCheckBox,
            shippingAmount, _kaireCashtoPaid,
            freeShipping, _walletToPaid)
    }

    function _kaireCommonCall(onChangeValue = 0) {
        if (kaireVisible?.checked === true) {
            let totalPaid = reviewshow?.amount;
            if (discount_amount1 && !freeShipping) {
                if ((totalPaid - discount_amount1) > 0) {
                    totalPaid = totalPaid - discount_amount1;
                    totalPaid = _kaireCashtoPaid(totalPaid, freeShipping, onChangeValue);
                } else {
                    totalPaid = totalPaid - 0;
                    totalPaid = _kaireCashtoPaid(totalPaid, freeShipping, onChangeValue);
                }
            }
            else {
                totalPaid = _kaireCashtoPaid(+reviewshow?.subamount, freeShipping, onChangeValue);
            }
            setpaidamount(totalPaid);
            setKaireVisible((prev) => ({ ...prev, checked: true }))
            setWalletVisible((prev) => ({ ...prev, checked: false }))
        }
    }

    function handlechangeKaire(e) {
        let onChangeValue = e.target.value;
        if (onChangeValue === "") {
            _kaireCommonCall('0')
            setKaireCashAdd('');
        }

        else {
            if (kaireCashError) {
                setKaireCashError('')
            }
            if (onChangeValue > kaireVisible?.kaireAmount) {
                setCashError('Insufficient Kaire Cash')
                setTimeout(() => {
                    setCashError('')
                }, 2000);
                setKaireCashAdd('');
                _kaireCommonCall('0');
            } else {
                onChangeValue = onChangeValue > reviewshow?.subamount ? reviewshow?.subamount : onChangeValue;
                setKaireCashAdd(onChangeValue)
                _kaireCommonCall(onChangeValue);
            }
        }
    }

    // function _onLoadSetCostType(data) {
    //     const cosTypeData = [];
    //     _.forEach(data, (row) => {
    //         if (row?.is_autoship === "True") {
    //             setSmartShipCheck(true);
    //         }
    //         cosTypeData.push({ id: +row?.product?.id, orderId: row?.id, autoship: row?.is_autoship === "True" ? "AutoShip" : "Normal" });
    //     })
    //     setcosttype(cosTypeData)
    // }

    //for increase qty
    const Add = (e, cartId) => {
        if (+(e.target.value) >= 0 && +(e.target.value) < +(e?.target?.dataset?.value)) {
            const data_update = e?.target?.name.split(',')
            let is_auto_ship = _.find(costtype, { orderId: cartId })?.autoship === "AutoShip" ? true : false;
            const datas = {
                product_id: +(data_update[0]),
                variant_id: +(data_update[1]),
                quantity: +(e.target.value) + 1,
                id: cartId,
                cookie_id: cookieCutter.get('sessionkey'),
                is_autoship: is_auto_ship
            }
            const update_data = {
                token: logintoken,
                datas: datas
            }
            setInnerLoader(true)
            api.updateCart(update_data).then(res => {
                // toast.success(res.data.message, {
                //   duration: 5
                // })
                // api.getAllCartProduct(logintoken).then(res => {
                //     setCartData(res?.data)
                //     _onLoadSetCostType(res?.data?.products);
                //     setInnerLoader(false)
                // })
                ShowReviewPage();
                _onLoadSetCostType(res?.data?.products)
                setcoupon('')
            })
        }
    }
    //descrease product qty
    const Sub = (e, cartId) => {
        // let is_auto_ship = e.currentTarget.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.querySelector('.form-select').value;
        setInnerLoader(true)
        const data_update = e?.target?.name.split(',')
        let is_auto_ship = _.find(costtype, { orderId: cartId })?.autoship === "AutoShip" ? true : false;

        const datas = {
            product_id: +(data_update[0]),
            variant_id: +(data_update[1]),
            quantity: +(e.target.value) > 0 ? (+(e.target.value) - 1) : +(e.target.value),
            cookie_id: cookieCutter.get('sessionkey'),
            id: cartId,
            is_autoship: is_auto_ship

        }
        const update_data = {
            token: logintoken,
            datas: datas
        }
        api.updateCart(update_data).then(res => {
            // toast.success(res.data.message, { duration: 5 })
            // api.getAllCartProduct(logintoken).then(res => {
            //     setCartData(res?.data)
            //     _onLoadSetCostType(res?.data?.products);
            //     setInnerLoader(false)

            // })
            _onLoadSetCostType(res?.data?.products)
            ShowReviewPage();
            setcoupon('')

        })
    }

    return (
        <>
            <Head>
                <title>Review-Page</title>
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
            </Head>
            <br /><br />
            <div className="container order-detail-page" >
                <br /><br />
                <div className="row cupon_and_kiare_section">
                    <div className="col-md-4 coupam-enter">
                        <Discount paidamount={paidamount}
                            kaireCashtoPaid={(value, check) => _kaireCashtoPaid(value, check)}
                            walletToPaid={(value) => _walletToPaid(value)}
                            selectedCheckBox={selectedCheckBox}
                            shippingChargesAmount={shippingChargesAmount}
                            shippingaddress={shippingaddress}
                            setdiscount_amount={setdiscount_amount}
                            setpaidamount={setpaidamount}
                            discount_amount1={discount_amount1}
                            reviewshow={reviewshow}
                            setToken={setToken}
                            logintoken={logintoken}
                            coupon={coupon}
                            setcopanerror={setcopanerror}
                            freeShipping={freeShipping}
                            setFreeShipping={setFreeShipping}
                            setshipamount={setshipamount}
                            setcoupon={setcoupon} />
                    </div>
                    <>
                        {(cartdata?.available_kaire > 0) &&
                            <>
                                <div className="col-md-4 kairecassh">
                                    <label> <b> REDEEM KAIRE </b></label>
                                    <div className="kairch-main-text">
                                        <div className="text0">

                                        </div>
                                        <div className="text0">
                                            Kaire Cash
                                        </div>
                                        <div className="text0">
                                            <NumberFormatComp value={cartdata?.available_kaire} />
                                        </div>
                                    </div>
                                    <div className="add-kaire">
                                        <input type="checkbox"
                                            name="kaire_cash"
                                            checked={kaireVisible?.checked}
                                            onChange={(e) => {
                                                applyKaireCash(e, "kaire")
                                            }} />
                                        <input type="text" name="kaire" placeholder="Use Kaire Cash"
                                            value={kaireCashAdd}
                                            onChange={(e) => handlechangeKaire(e)}
                                        />
                                        {cashError && <span className="error">{cashError}</span>}
                                    </div>
                                    {kaireCashError && <span className="error">{kaireCashError}</span>}

                                </div>

                            </>
                        }

                        {walletVisible?.WallatAmount > 0 &&
                            <div className="col-md-4 kairecassh">
                                <label> <b> REDEEM WALLET </b></label>
                                <div className="kairch-main-text">
                                    <div className="text0">
                                        <input type="checkbox" name="kaire_cash"
                                            checked={walletVisible?.checked}
                                            onChange={(e) => { applyKaireCash(e, "wallet") }}
                                        />
                                    </div>
                                    <div className="text0">
                                        Wallet Cash
                                    </div>
                                    <div className="text0">
                                        <NumberFormatComp value={walletVisible?.WallatAmount}
                                        />
                                    </div>

                                </div>
                            </div>
                        }
                    </>
                </div>

                <h2>Order Details</h2>
                {copanerror &&
                    <h4 className="title">{copanerror}</h4>}
                {((discount_amount1 !== undefined || discount_amount1 !== null) && coupon?.name) &&
                    <h4 className="title">{coupon?.name} Coupon Added <button className="dlt" onClick={(e) => { removecoupon(e) }} >
                        <FaTrash onClick={(e) => {
                            removecoupon(e)
                        }} />
                    </button></h4>}
                <div className="cart-now-main">
                    <table className="Cart_product">
                        <thead className="row">
                            <tr>
                                <th className="pro-name">product Image</th>
                                <th className="pro-name">product name</th>
                                <th className="pro-name">Quantity</th>
                                <th className="pro-name">Variation</th>
                                {/* <th className="pro-name">BV</th>*/}
                                <th className="pro-name">Price</th>
                                <th className="pro-name">SmartShip Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                cartdata?.products?.map((Cart, index) => {
                                    return (
                                        <>
                                            <tr key={index + 1}>
                                                {Cart?.variant === null ?
                                                    <>
                                                        <td className="cart-product-details" data-value="product Image">
                                                            <div className="">
                                                                <img src={`${process.env.API_URL}${Cart?.product?.product_images[0]?.image}`}
                                                                    height='100' width='100' />
                                                            </div>
                                                        </td>

                                                        <td className="name_tableData" data-value="product name">
                                                            {(Cart?.product?.product_type === "Bundle") &&
                                                                <span>
                                                                    {!expend ?
                                                                        <KeyboardArrowRight sx={{ fontSize: 20, position: "relative", top: "3px" }} onClick={() => setExpend(true)} />
                                                                        :
                                                                        <KeyboardArrowDown sx={{ fontSize: 20, position: "relative", top: "3px" }} onClick={() => setExpend(false)} />
                                                                    }
                                                                </span>
                                                            }
                                                            <span className="cart-product-details">
                                                                {Cart?.product?.name}
                                                            </span>
                                                        </td>

                                                        <td>
                                                            <div className="main-qty-secc">
                                                                <div id="qty">
                                                                    <button type="button"
                                                                        name={`${Cart?.product?.id},${null}`}
                                                                        id={Cart?.product?.id}
                                                                        disabled={Cart?.quantity === "1"}
                                                                        className="sub"
                                                                        data-value={Cart?.product?.quantity}
                                                                        value={Cart?.quantity}

                                                                        onClick={(e) => { Sub(e, Cart?.id) }}>-</button>
                                                                    <input
                                                                        name={`${Cart?.product?.id},${null}`}
                                                                        type="text"
                                                                        value={Cart?.quantity}
                                                                    />
                                                                    <button
                                                                        type="button"
                                                                        name={`${Cart?.product?.id},${null}`}
                                                                        id={Cart?.product?.id}
                                                                        className="add"
                                                                        data-value={Cart?.product?.quantity}
                                                                        value={Cart?.quantity}

                                                                        onClick={(e) => { Add(e, Cart?.id) }}>+</button>
                                                                </div>
                                                            </div>
                                                        </td>

                                                        <td className="cart-product-details" data-value="Variation">
                                                            <div className="box">
                                                                {(["Product", "Bundle"]?.includes(Cart?.product?.product_type)) &&
                                                                    <div className="">
                                                                        {["Product", "Bundle"].includes(Cart?.product?.product_type) &&
                                                                            <select className="form-select form-select-lg mb-3" aria-label=".form-select-lg example"
                                                                                data-product-id={Cart?.product?.id}
                                                                                data-product-qty={Cart?.quantity}
                                                                                data-variant-id={null}
                                                                                onChange={(e) => _setAutoshipFunction(e, Cart?.product?.id, Cart?.id)}
                                                                                value={_.find(costtype, { orderId: Cart?.id })?.autoship}
                                                                            >
                                                                                <option value="Normal">One-time Only</option>
                                                                                <option value="AutoShip">SmartShip</option>
                                                                                {/* <option value="AutoShip" selected={is_autoship==='True'?true:false} >AutoShip</option> */}
                                                                            </select>
                                                                        }
                                                                    </div>
                                                                }
                                                            </div>
                                                        </td>
                                                        {/* <td> {Cart?.product?.bonus_value}</td>*/}
                                                        <td className=" " data-value="Price">
                                                            <NumberFormatComp value={Cart?.product?.cost_price * Cart?.quantity} render={(value) => (
                                                                <div> {
                                                                    (minCartAutoshipCheck !== "True" && smartShipCheck !== true)
                                                                        ?
                                                                        value + " " + "USD"
                                                                        :
                                                                        `$${parseFloat(Cart?.product?.[`${(minCartAutoshipCheck === "True" || smartShipCheck) ? "autoship_cost_price" : "cost_price"}`] * Cart?.quantity).toFixed(2)}` + " " + "USD"
                                                                        +
                                                                        (Cart.is_autoship === "True" ? ' / $' + parseFloat(Cart?.product?.autoship_cost_price * Cart?.quantity).toFixed(2) + " " + "USD" : "") + " " + (Cart?.is_autoship === "True" ? 'SmartShip' : "")}
                                                                </div>)} />
                                                        </td>
                                                        <td className="smartData" data-value="SmartShip Status">
                                                            {Cart?.is_autoship === "True" ?
                                                                <div className="title">
                                                                    <Hover data="Activated" />
                                                                </div>
                                                                :
                                                                <div className="title">
                                                                    <Hover data="Deactivated" />
                                                                </div>
                                                            }

                                                        </td>
                                                    </>
                                                    :
                                                    <>
                                                        <td>
                                                            <div className="cart-product-details">
                                                                {(product?.product_type === "Bundle") &&
                                                                    <span>
                                                                        {!expend ?
                                                                            <KeyboardArrowRight sx={{ fontSize: 20, position: "relative", top: "5px" }} onClick={() => setExpend(true)} />
                                                                            :
                                                                            <KeyboardArrowDown sx={{ fontSize: 20, position: "relative", top: "5px" }} onClick={() => setExpend(false)} />
                                                                        }
                                                                    </span>
                                                                }
                                                                <span>
                                                                    {Cart?.product?.name}({Cart?.variant?.name})
                                                                </span>
                                                            </div>
                                                        </td>

                                                        <td className="">
                                                            <div className="cart-product-details">
                                                                {Cart?.quantity}
                                                            </div>
                                                        </td>
                                                        <td className="cart-product-details">
                                                            <div className="box">
                                                                <div className="">
                                                                    {["Product", "Bundle"].includes(Cart?.product?.product_type) &&
                                                                        <select className="form-select form-select-lg mb-3" aria-label=".form-select-lg example"
                                                                            data-product-id={Cart?.product?.id}
                                                                            data-product-qty={Cart?.quantity}
                                                                            data-variant-id={null}

                                                                            onChange={(e) => _setAutoshipFunction(e, Cart?.product?.id, Cart?.id)}
                                                                            value={_.find(costtype, { orderId: Cart?.id })?.autoship}
                                                                        >
                                                                            <option value="Normal">One-time Only</option>
                                                                            <option value="AutoShip">SmartShip</option>

                                                                        </select>
                                                                    }
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td className=" ">
                                                            <NumberFormatComp value={Cart?.variant?.cost_price * Cart?.quantity} render={(value) => (
                                                                <div> {(minCartAutoshipCheck !== "True" && smartShipCheck !== true)
                                                                    ?
                                                                    value
                                                                    :
                                                                    `$${parseFloat(Cart?.variant?.[`${(minCartAutoshipCheck === "True" || smartShipCheck) ? "autoship_cost_price" : "cost_price"}`] * Cart?.quantity).toFixed(2)}`
                                                                    +
                                                                    (Cart?.is_autoship === "True" ? ' / $' + parseFloat(Cart?.variant?.autoship_cost_price * Cart?.quantity).toFixed(2) : "") + ' SmartShip'}
                                                                </div>)} />
                                                        </td>

                                                        <td className=" ">
                                                            <Hover data="Activated" />
                                                        </td>
                                                    </>
                                                }
                                            </tr>
                                            <tr className="not-found">
                                                <td colSpan={5} className="bundle_detail_table">
                                                    {
                                                        (expend && Cart?.product?.product_type === "Bundle") &&
                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <th>Product Name</th>
                                                                    <th>Quantity</th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {(() => {
                                                                    if ([bundleProduct]?.length > 0) {
                                                                        return _.map(bundleProduct[Cart?.product?.id], (val, key) => (
                                                                            <tr key={key}>
                                                                                <td>{val?.product?.name}</td>
                                                                                <td>{val?.quantity}</td>
                                                                            </tr>
                                                                        ));
                                                                    }
                                                                })(Cart?.product?.id)}
                                                                <tr>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    }
                                                </td>
                                            </tr>
                                        </>)
                                })
                            }
                        </tbody>
                    </table>
                    <CustomPackTable setCheckCustomPack={setCheckCustomPack} setshowloader={setshowloader} />
                </div>
                <div className="row view-review">
                    <div className="col-md-6">
                        <div className="shipping_payment">
                            <ShippingSystem
                                Datashow={reviewshow}
                                shippingaddress={logintoken ? shippingaddress : "null"}
                                shippingWithoutLogin={{
                                    "first_name": "",
                                    "last_name": "",
                                    "company_name": "",
                                    "phone_number": "",
                                    "city": "",
                                    "postal_code": "",
                                    "street_address_1": "",
                                    "country": withoutLogin?.shippingCountry,
                                    "state": withoutLogin?.shippingState,
                                    "street_address_2": ""
                                }}
                                product={cartdata?.products}
                                setShowContinue={setShowContinue}
                                section="checkout"
                                discount={{ coupon, setdiscount_amount, discount_amount1 }}
                                changeAmountOnCouponBased={(freeshippingDiscount, shippingAmount) => _changeAmountOnCouponBased(freeshippingDiscount, shippingAmount)}
                                setShippingChargesAmount={setShippingChargesAmount}
                                AutoshipSelected={{}}
                                freeShipping={freeShipping}
                            />
                        </div>
                        {(withoutLoginShippingError && !logintoken) && <span class="error">{withoutLoginShippingError}</span>}
                    </div>
                    <div className="col-md-6">
                        <span className="Total">
                            <strong>Sub-total:</strong>
                            <NumberFormatComp value={+reviewshow?.subamount} />
                        </span>
                        {shippingChargesAmount &&
                            <span className="Total">
                                <strong>Shipping Charges:</strong>
                                <NumberFormatComp value={shippingChargesAmount} />
                            </span>
                        }
                        {coupon?.name && <>
                            <span className="Discount"><strong>Discount Amount ({coupon?.name}):</strong>
                                {(discount_amount1 !== undefined && discount_amount1 !== null) ?
                                    <>- <NumberFormatComp value={discount_amount1} />
                                    </>
                                    :
                                    <NumberFormatComp value={reviewshow?.discount_amount} />
                                }
                            </span>
                        </>
                        }
                        {kaireVisible?.checked === true && kaireCashAdd &&
                            <span className="Total">
                                <strong>Kaire Cash:</strong>
                                <NumberFormatComp value={kaireCashAdd} />
                            </span>
                        }
                        {walletVisible?.checked === true &&
                            <span className="Total">
                                <strong>Wallet Cash:</strong>
                                <NumberFormatComp value={walletVisible?.reduceAmount} />
                            </span>
                        }

                        <span className="amount_paid">
                            <strong>Total:</strong>
                            {paidamount >= 0 ? <NumberFormatComp value={paidamount} />
                                :
                                <>
                                    <NumberFormatComp value={0} />
                                </>
                            }
                        </span>
                    </div>
                </div>
                {
                    (section !== "withoutLogin") &&
                    <div>
                        <PayCardSection
                            compFunction={(value) => smartshipCheckPopup(value)}
                            orderData={(value) => placerorder(value)}
                            section="checkout"
                            product={cartdata?.products}
                            setSavedCardId={setSavedCardId}
                            buttonDis={buttonDis}
                            walletCheck={walletVisible?.checked}
                            saveCardId={saveCardId}
                            showContinue={showContinue}
                            showErrorPayment={showErrorPayment}
                            setShowContinue={setShowContinue}
                            setshowloader={setshowloader}
                        />
                    </div>

                }
                <SmartshipPopup
                    open={smartshipOpen}
                    setOpen={() => setSmartShipOpen()}
                    orderData={(value) => placerorder(value)}
                    paidamount={autoShipAmount}
                    Data={newCardData}
                    loader={loader}
                    setLoader={setLoader}
                    nextDate={cartdata?.next_shipping_on}
                    title=" SmartShip Information"
                    classFor="main_checkout_autoship"
                />
            </div >
        </>)
})
export default Review;
