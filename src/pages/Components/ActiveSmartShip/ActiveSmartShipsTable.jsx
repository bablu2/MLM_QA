import React, { useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  Paper,
  DialogContent,
  DialogContentText,
} from "@material-ui/core";
import { Avatar } from "@material-ui/core";
import DialogComponent from "@Components/Common/DialogComponent";
import api from "@api/Apis";
import { toast } from "react-toastify";
import { OuterTableStyle } from "./ActiveStyleComp";
import { DialogSectionCss } from "./ActiveStyleComp";

function RowOfTable(props) {
  const [showData, setShowData] = useState();
  const [open, setOpen] = useState(false);

  const previewData = async (public_order_id) => {
    let payload = {
      token: localStorage.getItem("Token")
        ? localStorage.getItem("Token")
        : null,
      order_id: public_order_id,
    };

    setInnerLoader(true);
    await api
      .autoshipOrderById(payload)
      .then((res) => {
        if (res?.status === 200) {
          const {
            first_name,
            last_name,
            street_address_1,
            state,
            postal_code,
            country,
            phone_number,
          } = res?.data?.addresses?.[0]?.billing_address;
          const ship = res?.data?.addresses?.[0]?.shipping_address;

          let data = {
            billingAddress: `${first_name} ${last_name},${street_address_1},${state},${country},${phone_number},${postal_code}`,
            shippingAddress: `${ship?.first_name} ${ship?.last_name},${ship?.street_address_1},${ship?.state},${ship?.country},${ship?.phone_number},${ship?.postal_code}`,
            amount_paid: res?.data?.orders?.amount_paid,
            orderId: res?.data?.orders?.public_order_id,
            nextDate: res?.data?.next_shipping_date,
            productData: res?.data?.orders?.autoship_order_details,
          };

          setShowData(data);
          setOpen(true);
          setInnerLoader(false);
        } else {
          // toast.error("Order id isn't valid");
          setInnerLoader(false);
        }
      })
      .catch((err) => {
        setInnerLoader(false);
        console.log("err", err);
      });
  };

  const { smartShip, setshowloader, setInnerLoader } = props;
  return (
    <>
      {smartShip?.map((value, index) => (
        <TableRow
          key={index + value?.user?.first_name}
          onClick={() => previewData(value?.order_id)}
        >
          <TableCell
            align="center"
            component="td"
            scope="row"
            className="year_block"
            data-value="IMAGE"
          >
            <Avatar
              alt={value?.user?.first_name}
              src={process?.env?.MEDIA_URL + value?.avatar}
            />
          </TableCell>
          <TableCell
            align="center"
            component="td"
            scope="row"
            className="year_block"
            data-value="NAME"
          >
            {value?.user?.first_name}
          </TableCell>
          <TableCell
            align="center"
            component="td"
            scope="row"
            className="year_block"
            data-value="REFERRAL"
          >
            {value?.user?.referral_code}
          </TableCell>
          <TableCell
            align="center"
            component="td"
            scope="row"
            className="year_block"
            data-value="PARENT"
          >
            {value?.parent?.first_name}
          </TableCell>
          <TableCell
            align="center"
            component="td"
            scope="row"
            className="year_block"
            data-value="COUNTRY"
          >
            {value?.user?.country}
          </TableCell>
          <TableCell
            align="center"
            component="td"
            scope="row"
            className="year_block"
            data-value="LEVEL"
          >
            {value?.level}
          </TableCell>
          <TableCell
            align="center"
            component="td"
            scope="row"
            className="year_block"
            data-value="START DATE"
          >
            {value?.startDate}
          </TableCell>
          <TableCell
            align="center"
            component="td"
            scope="row"
            className="year_block"
            data-value="NEXT DATE"
          >
            {value?.nexDate}
          </TableCell>
          <TableCell
            align="center"
            component="td"
            scope="row"
            className="year_block"
            data-value="BV"
          >
            {value?.bv}
          </TableCell>
        </TableRow>
      ))}
      <DialogComponent
        opend={open}
        handleClose={() => setOpen(false)}
        title="Downline Active SmartShip"
        classFor="order_details"
      >
        <DialogSectionCss>
          <div
            className="orders_date"
            style={{ display: "flex", justifyContent: "space-between" }}
          >
            <div className="shipping_address">
              <h5>ORDER ID</h5>
              <span>{showData?.orderId}</span>
            </div>
            <div className="shipping_address">
              <h5>NEXT RUN DATE</h5>
              <span>{showData?.nextDate}</span>
            </div>
          </div>

          <label>Details</label>
          <div className="content_container">
            <div className="billing_address">
              <h6>Billing Address</h6>
              <span>{showData?.billingAddress}</span>
            </div>
            <div className="shipping_address">
              <h6>Shipping Address</h6>
              <span>{showData?.shippingAddress}</span>
            </div>

            <div className="shipping_address">
              <h6>Amount Paid</h6>
              <span>{showData?.amount_paid}</span>
            </div>
          </div>
          <div className="table-wrap">
            <table>
              <thead>
                <tr>
                  <th>PRODUCT NAME</th>
                  <th>PRODUCT QUANTITY</th>
                  <th>BV</th>
                  <th>AUTO SHIP COST PRICE </th>
                </tr>
              </thead>
              <tbody>
                {showData?.productData?.map((items, index) => (
                  <tr key={index}>
                    <td data-value="PRODUCT NAME">{items?.product?.name}</td>
                    <td data-value="PRODUCT QUANTITY">
                      {" "}
                      {items?.product_quantity}
                    </td>
                    <td data-value="BV"> {items?.product?.bonus_value}</td>
                    <td data-value="AUTO SHIP COST PRICE">
                      {" "}
                      {items?.product?.autoship_cost_price}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </DialogSectionCss>
      </DialogComponent>
    </>
  );
}
export default function ActiveSmartShipsTable(props) {
  const HeaderArray = [
    "IMAGE",
    "NAME",
    "CODE",
    "SPONSER NAME",
    "COUNTRY",
    "LEVEL",
    "STARTED ON",
    "NEXT RUN",
    "BV",
  ];
  return (
    <div className="active_smartship_table">
      <TableContainer component={Paper}>
        <div className="active_smartship_table_main_div">
          <Table aria-label="collapsible table">
            <TableHead>
              <TableRow>
                {(() => {
                  return HeaderArray?.map((value, index) => (
                    <TableCell
                      align="center"
                      component="th"
                      scope="row"
                      key={index + 1}
                    >
                      {value}
                    </TableCell>
                  ));
                })()}
              </TableRow>
            </TableHead>
            <TableBody className="active_amart_tbody">
              {props?.smartShip?.length > 0 ? (
                <RowOfTable key="actve_smart_tabless" {...props} />
              ) : (
                <TableRow>
                  <TableCell
                    align="center"
                    component="td"
                    scope="row"
                    colSpan={9}
                    className="year_block"
                  >
                    No result found
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
      </TableContainer>
    </div>
  );
}
