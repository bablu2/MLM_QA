import Head from "next/head";
import router, { useRouter } from "next/router";
import { useEffect, useRef, useState } from "react";
import Address from '../../Components/Checkout/Address'
import api from '../../../api/Apis'
import Review from "../../Components/Checkout/Review";
import DialogComponent from "@Components/Common/DialogComponent";
import CheckoutStyle, { AddressPopUpStyle } from "./CheckoutStyle";
import SvgAnimationBackground from '@Components/Common/SvgAnimationBackground';
import { IconButton } from "@material-ui/core";
import { FaEdit } from "react-icons/fa";
import LoginHtml from "../login/LoginHtml";
import CheckoutLogin from "@PagesComponent/Checkout/CheckoutLogin";
import _ from 'lodash';
import { useForm, FormProvider, useFormContext } from "react-hook-form";
import WithoutForm from "@PagesComponent/Checkout/CheckoutLogin/WithoutForm";
import WithoutLoginCard from "@PagesComponent/Checkout/CheckoutLogin/WithoutLoginCard";
import CheckoutLoginStyle from "@PagesComponent/Checkout/CheckoutLogin/checkoutLogin.style";
import SignupPageStyle from "../signup/SignupPageStyle";
import CardType from 'credit-card-type';
import SmartshipPopup from "./SmartshipPopup";
import WithoutLoginConditon from "@PagesComponent/Checkout/CheckoutLogin/WithoutLoginConditon";
import cookieCutter from 'cookie-cutter'

export default function AddressList({ minCartAutoshipCheck, setCartData, counts, cartdata, smartShipCheck, setSmartShipCheck, ...props }) {
    const [billingaddress, saveBillingAddress] = useState();
    const [shippingaddress, saveShippingAddress] = useState();
    const [addresstype, setAddresstype] = useState();
    const [addressData, setaddressData] = useState();
    const [logintoken, setToken] = useState();
    const [costtype, setcosttype] = useState({ data: [{ id: '', value: '' }] })

    const [reviewshow, setreviewshow] = useState();
    // const [addressDetails, setaddressDetails] = useState();
    const [autoShipAmount, setAutoShipAmount] = useState(null)
    const [addressDetails, setDefaultData] = useState(null);

    const [refferalCode, setRefferalCode] = useState({ value: null, verify: null });
    const [withoutLoginAddress, setWithoutLoginAddress] = useState({
        billingaddress: null,
        shippingaddress: null
    })
    const [errorShow, setShowError] = useState(null);
    const router = useRouter()
    //without login
    const [nextShippingDate, setNextShippingDate] = useState('');
    const [OrderParameter, setOrderParameter] = useState({});
    const [smartshipOpen, setSmartShipOpen] = useState(false);
    const myRef = useRef();
    const [hide, setHide] = useState({ loginLink: false, passwordField: false });
    //show list of product and load all required data

    useEffect(() => {
        // props?.setshowloader(true);
        props?.setInnerLoader(true)
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setToken(token)
        api.manageAddress(token).then(res => {
            if (res?.data?.code === 0) {
                setAddresstype('new');
                // props?.setshowloader(false); 
                props?.setInnerLoader(false)

            }
            if (res?.data?.code === 1) {
                saveBillingAddress(+res?.data?.addresses[0]?.id);
                saveShippingAddress(+res?.data?.addresses[0]?.id);
                // props?.setshowloader(false);
                props?.setInnerLoader(false)
            }
            setaddressData(res?.data)
        });
    }, []);

    useEffect(() => {
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        ShowReviewPage(token);
    }, [smartShipCheck, counts])

    useEffect(() => {
        const billing = localStorage.getItem('billingAddress') ? JSON.parse(localStorage.getItem('billingAddress')) : null;
        const shipping = localStorage.getItem('shippingAddress') ? JSON.parse(localStorage.getItem('shippingAddress')) : null;
        setWithoutLoginAddress({ billingaddress: billing === undefined ? null : billing, shippingaddress: shipping === undefined ? null : shipping });
    }, [])

    function AddressSectionHtml({ addresses = [], section = "", check = null }) {

        return <div className="col-md-6">
            <h4 className="title"><strong>{section}</strong> address</h4>
            {addresses?.map((address, index) => {
                return (
                    <div className="radio-bnt" key={index}>
                        <input type="radio" name={`${section}address"`} value={address?.id}
                            checked={+check === +address?.id}
                            onChange={(e) => {
                                if (section === "billing") {
                                    saveBillingAddress(e.target.value)
                                } else if (section === "shipping") {
                                    saveShippingAddress(e.target.value)
                                }
                                localStorage.setItem(`${section}Address`, e.target.value);
                            }} />
                        <div className="checkout-addrees">
                            <h5 className="tittle">
                                {`${address?.first_name}
                                 ${address?.last_name} 
                                 ${address?.company_name || ""}
                                ${address?.street_address_1}
                                ${address?.street_address_2 || ""}
                                ${address?.city}
                                ${address?.state}
                                ${address?.postal_code}
                                ${address?.country}
                                ${address?.phone_number || ""}`}
                            </h5>
                            <FaEdit onClick={() => {
                                setDefaultData(address);
                                setAddresstype(section);
                            }}
                            />
                        </div>
                    </div>
                )
            })}
            <button type="button" className="btn btn-primary" onClick={(e) => {
                setDefaultData(null);
                setAddresstype(section);
            }}> Add New</button>
        </div>
    }


    //review section method
    function _onLoadSetCostType(data) {
        const cosTypeData = [];
        _.forEach(data, (row) => {
            if (row?.is_autoship === "True") {
                setSmartShipCheck(true);
            }
            cosTypeData.push({ id: +row?.product?.id, orderId: row?.id, autoship: row?.is_autoship === "True" ? "AutoShip" : "Normal" });
        })
        setcosttype(cosTypeData)
    }

    const ShowReviewPage = (token) => {
        const data = localStorage.getItem('packProduct');
        const jsonPraseData = data ? JSON.parse(data) : {};
        api.getAllCartProduct(token).then(res => {
            let productId;
            let variant_id;
            let product_qty;
            let is_autoship;
            let total = 0;
            let totalBounus = 0;
            let products = [];
            let weight = 0;
            const cartdata = res?.data;
            setNextShippingDate(res?.data?.next_shipping_on);
            let amount = 0;
            _.forEach(_.filter(res?.data?.products, { is_autoship: "True" }),
                (row) => {
                    amount += row?.product?.autoship_cost_price * +row?.quantity
                })
            setAutoShipAmount(amount)
            cartdata?.products?.map((cart) => {
                weight += parseFloat(+cart?.quantity) * parseFloat(+cart?.product?.weight);
                if (cartdata?.is_autoship_user === 'True' || smartShipCheck) {
                    total = (cart.variant ? +(cart.variant.autoship_cost_price * cart.quantity) : +(cart.product.autoship_cost_price * cart.quantity)) + total;
                    totalBounus = (cart.variant ? +(cart.variant.bonus_value) : +(cart.product.bonus_value)) + totalBounus
                    is_autoship = cart.is_autoship === "True" ? true : false
                }

                else if (cart.is_autoship === "True") {
                    total = (cart.variant ? +(cart.variant.cost_price * cart.quantity) : +(cart.product.cost_price * cart.quantity)) + total;
                    totalBounus = (cart.variant ? +(cart.variant.bonus_value) : +(cart.product.bonus_value)) + totalBounus
                    is_autoship = true
                }
                else {
                    total = (cart.variant ? +(cart.variant.cost_price * cart.quantity) : +(cart.product.cost_price * cart.quantity)) + total;
                    totalBounus = (cart.variant ? +(cart.variant.bonus_value) : +(cart.product.bonus_value)) + totalBounus
                    is_autoship = false
                }

                productId = cart?.product.id;
                product_qty = cart.quantity;
                variant_id = cart?.variant?.id ? cart.variant.id : '';
                products.push({ product_id: parseInt(productId), quantity: parseInt(product_qty), variant_id: parseInt(variant_id), is_autoship: is_autoship })
            });


            const orderdata = {
                "shipping_address_id": parseInt(billingaddress),
                "amount": (jsonPraseData?.customPack) ? parseFloat((jsonPraseData?.customPack?.totalAmount + total), 2) : parseFloat(total, 2),
                "subamount": (jsonPraseData?.customPack) ? parseFloat((jsonPraseData?.customPack?.totalAmount + total), 2) : parseFloat(total, 2),
                "bonus_value": totalBounus,
                "tax_amount": 0,
                "weight": (jsonPraseData?.customPack) ? +(jsonPraseData?.customPack?.totalWeight) + weight : weight,
                "shipping_amount": 0,
                "gross_total": parseFloat(total, 2),
                "coupon_id": null,
                "discount_amount": "00.00",
                "amount_paid": parseFloat(total, 2),
                "sku": "umb-20",
                "productsCount": _.sum(_.map(cartdata?.products, 'quantity')) + (jsonPraseData?.customPack) ? 1 : 0,
                "products": products,
                "is_autoship": false,
            }
            setreviewshow(orderdata);
            _onLoadSetCostType(res?.data?.products);

        })
    }
    // useEffect(() => {
    //     let token = localStorage.getItem('Token');
    //     if (shippingaddress && [reviewshow]?.length > 0) availableShippingModules(token);
    // }, [shippingaddress, reviewshow])

    // const availableShippingModules = async (token) => {
    //     const payload = {
    //         shipping_address_id: parseInt(shippingaddress),
    //         cart_items: reviewshow?.products?.length,
    //         cart_weight: +reviewshow?.weight,
    //         cart_total: reviewshow?.subamount
    //     }
    //     await api.availableShippingModules(token, payload).then((response) => {

    //     }).catch((err) => console.log(err))
    // }


    const methods = useForm({ mode: 'all' });

    function submitFunction(data) {
        let ADDRESS = {
            billingaddress: null,
            shippingaddress: null
        }
        _.map(['billing', 'shipping'], (row) => {
            ADDRESS[`${row}address`] = {
                "first_name": data[row + '_first_name'],
                "last_name": data[row + '_last_name'],
                "company_name": data[row + '_company_name'],
                "city": data[row + '_city'],
                "postal_code": data[row + '_postal_code'],
                "street_address_1": data[row + '_street_address_1'],
                "street_address_2": data[row + '_street_address_2'] ? data[row + '_street_address_2'] : "",
                "country": (row === "billing") ? billingCountry : shippingCountry,
                "state": (row === "billing") ? billingState : shippingState,
                "phone_number": data[row + '_phone_number']
            }
        });
        // const SPLIT_EXPIRY = _.split(data["cardExpiry"], '/', 2);
        let PAYMENT_DETAILS = {
            card_expiry_month: data?.card_expiry_month,
            card_expiry_year: data?.card_expiry_year,
            card_number: data["cardNumber"],
            card_owner_name: data["cardHolder"],
            card_type: CardType(data["cardNumber"]).length > 0 ? CardType(data["cardNumber"])[0]?.type : 'visa',
            security_code: data["cardCVC"]
        }
        let NEW_USER = {
            new_email: data['email'],
            leadid: localStorage.getItem('leadid') ? JSON.parse(localStorage.getItem('leadid')) : null,
            new_password: data['password'],
            parentrefferalPromoter: refferalCode?.verify ? refferalCode?.value : null || props?.subDomain ? props?.subDomain : null,
            cookie_id: cookieCutter.get('sessionkey'),

        }

        const ORDER_DETAIL = {
            billing_details: ADDRESS?.billingaddress,
            payment_details: PAYMENT_DETAILS,
            new_user: NEW_USER,
            new_billing_address: ADDRESS,
            save_credit_card: data?.save_credit_card

        }
        if (autoShipAmount) {
            setOrderParameter(ORDER_DETAIL);
            setSmartShipOpen(true);
        } else {
            myRef?.current?.placerorder(ORDER_DETAIL);
        }
    }

    const [billingCountry, setBillingCountry] = useState('');
    const [billingState, setBillingState] = useState('');
    const [shippingCountry, setShippingCountry] = useState('');
    const [shippingState, setShippingState] = useState('');

    function SameAsShipping() {
        methods?.setValue("billing_first_name", methods?.getValues(`shipping_first_name`), { shouldValidate: true });
        methods?.setValue("billing_last_name", methods?.getValues(`shipping_last_name`), { shouldValidate: true });
        methods?.setValue("billing_company_name", methods?.getValues(`shipping_company_name`), { shouldValidate: true });
        methods?.setValue("billing_city", methods?.getValues(`shipping_city`), { shouldValidate: true });
        methods?.setValue("billing_street_address_1", methods?.getValues(`shipping_street_address_1`), { shouldValidate: true });
        methods?.setValue("billing_street_address_2", methods?.getValues(`shipping_street_address_2`), { shouldValidate: true });
        methods?.setValue("billing_postal_code", methods?.getValues(`shipping_postal_code`), { shouldValidate: true });
        methods?.setValue("billing_phone_number", methods?.getValues(`shipping_phone_number`), { shouldValidate: true });
        setBillingCountry(shippingCountry);
        setBillingState(shippingState);
    }

    return (
        <>
            {
                <>
                    <Head>
                        <title>Add List</title>
                    </Head>
                    <CheckoutStyle>
                        <div className="container checkout-main checkout_main_section_class">
                            <h1 className="title">CHECKOUT</h1>

                            {/* <h4 className="title">{addressData?.message && addressData.message}</h4> */}
                            {logintoken &&
                                <>
                                    <div className="row checkout_row">
                                        <AddressSectionHtml addresses={addressData?.addresses} section="billing" checkData={billingaddress} check={billingaddress} />
                                        <AddressSectionHtml addresses={addressData?.addresses} section="shipping" checkData={shippingaddress} check={shippingaddress} />
                                    </div>
                                    {['shipping', 'billing'].includes(addresstype) &&
                                        <DialogComponent opend={!!addresstype} handleClose={(value) => setAddresstype(value)}
                                            title={`ADD ${addresstype?.toUpperCase()} ADDRESS`} classFor="add_address_from_CheckoutPage">
                                            <AddressPopUpStyle>
                                                <Address
                                                    saveBillingAddress={saveBillingAddress}
                                                    saveShippingAddress={saveShippingAddress}
                                                    addresstype={addresstype}
                                                    logintoken={logintoken}
                                                    withoutLoginAddress={withoutLoginAddress}
                                                    setWithoutLoginAddress={setWithoutLoginAddress}
                                                    // defaultValues={addressDetails}
                                                    setaddressData={setaddressData}
                                                    setAddresstype={setAddresstype}
                                                    addressDetails={addressDetails}
                                                />
                                            </AddressPopUpStyle>
                                        </DialogComponent>

                                    }
                                </>
                            }
                            {(reviewshow === undefined) ? '' : (logintoken) &&
                                <Review
                                    minCartAutoshipCheck={minCartAutoshipCheck}
                                    setCartData={setCartData}
                                    cartdata={cartdata}
                                    counts={props?.counts}
                                    _onLoadSetCostType={(value) => _onLoadSetCostType(value)}
                                    setSmartShipCheck={setSmartShipCheck}
                                    setcosttype={setcosttype}
                                    costtype={costtype}
                                    smartShipCheck={smartShipCheck}
                                    reviewshow={reviewshow}
                                    refferalCode={refferalCode}
                                    autoShipAmount={autoShipAmount}
                                    ShowReviewPage={ShowReviewPage}
                                    setcounts={props?.setcounts}
                                    billingaddress={billingaddress}
                                    shippingaddress={shippingaddress}
                                    setshowloader={props?.setshowloader}
                                    setInnerLoader={props?.setInnerLoader}
                                    validateauth={props?.validateauth}
                                />
                            }

                            {!(logintoken) &&
                                <FormProvider {...methods} >
                                    <CheckoutLoginStyle>
                                        <form onSubmit={methods.handleSubmit(submitFunction)}>
                                            <CheckoutLogin
                                                {...{ refferalCode, setRefferalCode, logintoken, subDomain: props?.subDomain, hide, setHide }}
                                            />
                                            {!(hide?.loginLink) &&
                                                <div className="form_sections">
                                                    <SignupPageStyle>
                                                        <div className="container main_sign_up_form_class">
                                                            {['shipping', 'billing']?.map((row, index) => (
                                                                <WithoutForm {...{
                                                                    section: row,
                                                                    country: (row === "billing") ? billingCountry : shippingCountry,
                                                                    state: (row === "billing") ? billingState : shippingState,
                                                                    setCountry: (row === "billing") ? setBillingCountry : setShippingCountry,
                                                                    setState: (row === "billing") ? setBillingState : setShippingState,
                                                                }} >
                                                                    {index === 1 && <div><input type="checkbox" onChange={() => SameAsShipping()} />Same as Shipping</div>}
                                                                </WithoutForm>

                                                            ))}


                                                        </div>
                                                    </SignupPageStyle>
                                                </div>
                                            }

                                            {(reviewshow === undefined) ? '' :
                                                !(hide?.loginLink) && <Review
                                                    section="withoutLogin"
                                                    minCartAutoshipCheck={minCartAutoshipCheck}
                                                    setCartData={setCartData}
                                                    cartdata={cartdata}
                                                    _onLoadSetCostType={(value) => _onLoadSetCostType(value)}
                                                    setSmartShipCheck={setSmartShipCheck}
                                                    smartShipCheck={smartShipCheck}
                                                    reviewshow={reviewshow}
                                                    refferalCode={refferalCode}
                                                    autoShipAmount={autoShipAmount}
                                                    withoutLogin={{ shippingCountry, shippingState }}
                                                    setShowError={setShowError}
                                                    ref={myRef}
                                                    ShowReviewPage={ShowReviewPage}
                                                    setcounts={props?.setcounts}
                                                    counts={props?.counts}
                                                    billingaddress={billingaddress}
                                                    shippingaddress={shippingaddress}
                                                    setshowloader={props?.setshowloader}
                                                    setInnerLoader={props?.setInnerLoader}
                                                    validateauth={props?.validateauth}
                                                    setcosttype={setcosttype}
                                                    costtype={costtype}

                                                />}
                                            {!(hide?.loginLink) &&
                                                <div className="lowerSection">
                                                    <div>
                                                        <WithoutLoginCard />
                                                    </div>
                                                    <div>
                                                        <WithoutLoginConditon smartShipCheck={smartShipCheck} />
                                                    </div>
                                                    <button type="submit">Order Now</button>
                                                </div>
                                            }
                                            {errorShow && <span className="error">{errorShow}</span>}
                                            <SmartshipPopup
                                                open={smartshipOpen}
                                                orderData={() => myRef.current.placerorder(OrderParameter)}
                                                nextDate={nextShippingDate}
                                                paidamount={autoShipAmount}
                                                setOpen={(value) => setSmartShipOpen(value)}
                                                title="Smartship Alert"
                                            />
                                        </form>
                                    </CheckoutLoginStyle>
                                </FormProvider>

                            }
                        </div>
                    </CheckoutStyle>
                </>
            }
            <SvgAnimationBackground />
        </>)
}