import styled from "styled-components";

const CheckoutStyle = styled.div`
  & .checkout_main_section_class { 
    margin: 120px auto 50px;
    br {
      display: none;
    }
    h4 {
      padding: 15px;
      text-transform: capitalize;
    }
    .checkout_row {
      margin-left: 0;
      margin-right: 0;
      .col-md-6 {
        @media (max-width: 767px) { 
          padding: 0;   
      }
    }
    }
    .checkbox{
      .form-group{
        margin-bottom: 0;
      }
    }
    .checkout-addrees {
      margin-left: 15px;
      display: flex;
      align-items: center;
      width: 100%;
      justify-content: space-between;
      h5 {
        font-size: 14px;
      }
    }
    .radio-bnt {
      input {
        margin-top: 0;
      }
    }
    .apply-coupn {
      max-width: 100%;
      .wrap-form {
      position: relative;
      @media (max-width: 767px){
        height: 95px;
      }
      .form-control {
        height: 47px;
        padding-right: 195px;
        border-radius: 30px;
           /* 11-10-2022 */
           @media (max-width: 767px){
          padding-right: 10px;
          height: 40px;
        }
        /* end */
        }
      button.btn.btn-primary {
          position: absolute;
          right: 0;
          top: 1px;
          max-width: 190px;
          @media (max-width: 767px){
            max-width: 100%;
            bottom: 0;
            top: auto;
            height: 40px;
          }
      }
      }  
    }
    .Cart_product {
      width: 100%;
      tr {
        th {
        }
        td {
         @media (max-width: 767px){
          :not(:first-child) {
            border-top: 1px solid #ddd;
        }
         }
          text-align: center;
          &.cart-product-details {
            margin: 10px 0 0;
            display: table-cell;
            padding: 10px 0;
            @media (max-width: 767px){
              margin: 0 !important;
            }
            img {
             object-fit: contain;
            }
          }
          .tootltip_icon_button {
            margin: 10px 0;  
            border-radius: 40px;
            min-width: 35px;
            height: 35px; 
            width: 35px;
            padding: 0; 
          }
          & button[aria-label="Deactivated"] {
                opacity: 0.3;
            }
          &.name_tableData {
            /* display: flex;
            justify-content: center; */
          }
          &.bundle_detail_table {
            table {
              width: 100%;
              max-width: 80%;
              margin: 10px auto;
              tr {
                th {
                  text-align: center;
                  padding: 10px 12px;
                  border: 1px solid #16356a;
                  color: #fff;
                  background: #16356a;
                }
                td {
                  padding: 10px 12px;
                  border: 1px solid #16356a;
                }
              }
            }
          }
        }
      }
      .row {
        margin: 0;
        .Total {
          padding-right: 16px;
        }
      }
      @media (max-width: 767px){
        margin-bottom: 30px;
      }
    }
    .view-review {
      margin-top: 0;
    }
    .pro-name {
      background: #06356a;
      color: #fff;
      padding: 10px 0;
      text-align: center;
    }
    .cart-product-details {
      text-align: center;
      justify-content: center;
      select {
        width: 155px;
        height: 40px;
        border: 1px solid #ddd;
        border-radius: 50px;
        background: transparent;
        padding: 0 10px;
      }
    }
    .Cart_product {
      /* box-shadow: 0 0 10px #ddd; */
    }
    .container.order-detail-page {
      @media (max-width: 767px){
        min-height: 100%;
        margin-top: 30px;
       padding-bottom: 15px;
      }
      h4{
        position: relative;
        button.dlt {
           padding: 0;
          border: none;
          position: absolute;
          right: 10px;
          top: 50%;
          transform: translateY(-50%);
      } 
    }
      .row {
        border-bottom: none;
        padding-bottom: 0px; 
        -webkit-box-pack: start;
        justify-content: flex-start;
        flex-wrap: wrap;
        column-gap: 15px;
        row-gap: 20px; 
        margin: 40px  0;
            &:before {
            display: none;
            } 
      }
      .row.cupon_and_kiare_section{ 
          & > div{
              max-width: calc(100%/3 - 20px);width: 100%; 
              @media (max-width: 1200px){
                max-width: calc(100%/2 - 20px);
              }
              @media (max-width: 991px){
                max-width: 100%;
              }
        }
        @media (max-width: 991px){
          margin-bottom: 0;
        }
      }
      .row.view-review{
        padding-bottom: 10px;
        margin-top: 0;
        @media (max-width: 991px){
          justify-content: space-between;
        }
      } 
    }

      .apply-coupn .row {
        margin-bottom: 0;
      } 
      &.cupon_and_kiare_section {
        margin-top: 50px;
        margin-bottom: 50px;
        @media (max-width: 767px){
          flex-wrap: wrap;
          margin-top: 10px;
          margin-bottom: 10px;
        }
      }
    }
    .row.view-review {
        background: #fff;
        margin: 0;
        padding: 10px;
        border-top: 1px solid #ddd;
      }
    .cupon_and_kiare_section {
      display: flex;
      flex-wrap: nowrap;
      gap: 15px;
      .container.order-detail-page {
        h1 {
          text-align: center;
          margin-bottom: 20px;
          text-transform: uppercase; 
        }
      }
      .col-md-4.kairecassh {
        border: 1px solid #ddd;
        padding: 21px;
        max-width: 100%;
        border-radius: 0px;
        border-top: 4px solid #06356a;
        /* display: table; */
        /* width: 50%;
            margin: 0 auto 40px; */
        /* height: 109px; */
        label {
          width: 100%;
          text-align: left;
        }
      }

      .kairch-main-text {
        display: flex;
        justify-content: flex-start;
        padding: 13px 0;
      }
      .text0 {
        max-width: 34%;
        flex: 0 0 34%;
        font-size: 18px;
        font-weight: 600;
        color: #06356a;
        &:first-child {
          max-width: 20px;
          flex: 0 0 20px;
        }
        &:empty {
          display: none;
        }
      }
    }
    form.form_sections {
      display: flex;
    }
  }
  .checkout-main button {
    margin: 19px 0;
  }
  .row.saved-nmain {
    margin: 30px 0 0;
    padding-left: 20px;
  }
  .PayCardSection__SaveCardStyle-sc-1dxlv0m-0 {
    margin: 0;
  }

  .main_saved_card_class {
      .row.saved-nmain {
        display: flex;
        gap: 10px;
        justify-content: center;
    }
    .savedClass {
      .card_details {
        max-width: calc(100% - 20px);
        flex: 100%;
      }
      .delete_section {
        max-width: 30px;
       width: 100%;
      }
    }
  }
      .main_saved_comp {
        .row.saved-nmain{
          padding: 0;
        }
        .saved_card_section{
          width: 100%;
          .savedClass {
          width: 100%;
          max-width: calc(100%/3 - 20px);
          margin: 0;
          @media (max-width: 767px){
                max-width: 100%;
            }
          }
        }
      } 
  .payment_section .main_saved_comp {
    display: flex;
    gap: 10px;
    margin: 0;
    border: none;
  }
  [class*="PayCardSection"]{
  .main_saved_comp{
      background: #fff; 
      flex-wrap: wrap;
      padding: 0 5px;
      border: none;
      margin-top: 0;
      button{
        margin: 0;
      }
}

.col-md-12 .error {
    max-width: max-content;
}
.payment_fields .error {
    bottom: 0;
    height: auto;
    margin: 0;
    min-height: inherit;
    max-width: max-content;
}


  @media (max-width: 1024px) {
    .main_saved_card_class {
      .card_details {
        min-width: 184px;
      }
      .cardType {
        min-width: 165px;
      }
    }
  }

  @media (max-width: 480px) {
    .cart-now-main {
      width: 100%;
      overflow-x: scroll;
    }
    table.Cart_product th {
      min-width: 107px;
    } 
    .main_saved_card_class .savedClass {
      width: 97%;
    }
    .main_saved_card_class .savedClass .card_details {
      min-width: 137px;
    }
  }

  `;

export const AddressPopUpStyle = styled.div`
  font-family: var(--common-font);
  .select::after {
    top: 30%;
    margin: 0;
  }
  button.btn.btn-primary {
    font-family: var(--common-font);
    border: 2px solid #06356a;
    border-radius: 40px;
    background: #06356a;
    height: 45px;
    font-size: 14px;
    &:hover {
      background: #fff;
      color: #06356a;
    }
  }
`;
export default CheckoutStyle;
