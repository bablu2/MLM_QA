import DialogComponent from '@Components/Common/DialogComponent'
import { DialogSectionCss } from '@PagesComponent/ActiveSmartShip/ActiveStyleComp'
import React, { useState } from 'react'
import { CircularProgressbar } from 'react-circular-progressbar'


const SmartshipPopup = ({ open, loader, compFunction, setLoader, orderData, nextDate, paidamount, setOpen, title, classFor, Data }) => {
    const GETData = () => {
        setOpen(false)
        orderData(Data)
    }

    return (
        <>
            <DialogComponent opend={open} handleClose={() => setOpen(false)} title={title} classFor={classFor}>
                <DialogSectionCss className='check_out_main'>
                    <p> By selecting SmartShip, you are giving Kaire authorization to enroll you in our automatic shipping program.  You can cancel, modify or skip your SmartShip (without penalty) at any time. On {nextDate}, your card will be automatically charged
                        in the amount of ${parseFloat(paidamount).toFixed(2)} + (Shipping charges)
                        You will be notified of this order 5 days before.</p>
                    <div className='check_out'>
                        <button onClick={() => GETData()}> I would like to Create a SmartShip Order</button>
                        {loader &&

                            <img src='page_loader.gif' />
                            // <CircularProgressbar
                            //     size={68}
                            //     sx={{
                            //         color: "green",
                            //         position: 'absolute',
                            //         zIndex: 99,
                            //         minWidth: "400px",
                            //         position: "absolute",
                            //         justifyContent: "center",
                            //         display: "flex",
                            //         top: "50% !important"
                            //     }}
                            // />
                        }
                    </div>
                    <div className="check_out-btn">
                        <button onClick={() => setOpen(false)}> I do not want to Create a SmartShip Order</button>
                    </div>

                </DialogSectionCss>
            </DialogComponent>

        </>
    )
}

export default SmartshipPopup