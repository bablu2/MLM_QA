import Link from 'next/link'

export default function FourOhFour() {
    return <>
        <br /><br /><br /><br />
        <div className="container">
            <h2 className="title">404 - Page Not Found</h2>

            <Link href="/">
                <h3>  <a>Home</a></h3>
            </Link>
        </div>
    </>
}