import styled from 'styled-components';

const PrivacyPolicyCss = styled.div`
    .proven_banifits_section_banner {
        img {
            width: 100%;
        }
    }
    ul {
        list-style-type: none;
        li {
            font-size: 16px;  
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 18px; 
    &:not(:last-child){
        padding-bottom: 10px;  
    }
        }
    }
    @media (max-width:767px){
        .return h2{
            font-size: 23px;
        }  
        .return h1 {
            font-size: 26px;
            text-align: center;
            
        }
    }
`;

export default PrivacyPolicyCss;