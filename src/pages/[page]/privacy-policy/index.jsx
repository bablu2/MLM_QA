import SvgAnimationBackground from '@Components/Common/SvgAnimationBackground'
import React from 'react'
import PrivacyPolicyCss from './PrivacyPolic.style'

function PrivacyPolicy() {
    return (
        <PrivacyPolicyCss>
            <div className="proven_banifits_section_banner">
                <img src="/images/pricacy-policy-topbanner.jpg" />
            </div>
            <div className="container return">
                <h1>PRIVACY POLICY</h1>
                <p>This Privacy Policy governs the manner in which eKaire.com, Inc. collects, uses, maintains and discloses information collected from users (each, a "User") of the shopkaire.com website ("Site"). This privacy policy applies to the Site and all products and services offered by eKaire.com</p>

                <h3>Personal identification information</h3>

                <p>We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, place an order, subscribe to the newsletter, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address, mailing address, phone number, credit card information. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>

                <h3>Non-personal identification information</h3>

                <p>We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>

                <h3>Web browser cookies</h3>

                <p>Our Site may use "cookies" to enhance User experience. User's web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>

                <h3>How we use collected information</h3>

                <p>eKaire.com, Inc. may collect and use Users personal information for the following purposes:</p>
                <ul>
                    <li>
                        - To improve customer service
                        Information you provide helps us respond to your customer service requests and support needs more efficiently.
                    </li>
                    <li>
                        - To improve our Site
                        We may use feedback you provide to improve our products and services.
                    </li>
                    <li>
                        - To run a promotion, contest, survey or other Site feature
                        To send Users information they agreed to receive about topics we think will be of interest to them.
                    </li>
                    <li>
                        - To send periodic emails
                        We may use the email address to send User information and updates pertaining to their order. It may also be used to respond to their inquiries, questions, and/or other requests. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email or User may contact us via our Site.
                    </li>
                </ul>

                <h3>How we protect your information</h3>

                <p>We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.</p>

                <p>Sensitive and private data exchange between the Site and its Users happens over a SSL secured communication channel and is encrypted and protected with digital signatures.</p>

                <h3>Sharing your personal information</h3>

                <p>We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above.</p>

                <h3>Changes to this privacy policy</h3>

                <p>eKaire.com, Inc. has the discretion to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>

                <h3>Your acceptance of these terms</h3>

                <p>By using this Site, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.</p>

                <h3>Contacting us</h3>

                <p>If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at:</p>
                <p>eKaire.com, Inc.</p>
                <p>shopkaire.com</p>
                <p>PO Box 2318 Goldenrod, FL 32708</p>
                <p>877-603-1710</p>
                <p>customersupport <a className="ref__mailto" href="mailto:support@kaire.com">@kaire.com</a></p>
                <p>This document was last updated on March 06, 2014</p>
            </div>
        </PrivacyPolicyCss>
    )
}

export default PrivacyPolicy
