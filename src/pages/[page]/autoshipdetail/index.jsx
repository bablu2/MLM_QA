import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import NumberFormat from "react-number-format";
import api from "../../../api/Apis";
import DBOP from "@Components/Common/DBOP";
import AddressesSection from "@Components/Common/AddressesSection";
import PaymentTitleSectionHtml from "@Components/Common/PaymentTitleSectionHtml";
import AutonShipDetailStyle from "./AutoshipDetailStyle";
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";

const AutonShipDetailPage = ({ showprotectedpage }) => {
  const router = useRouter();
  const [data, setdata] = useState();
  const [addressdata, setaddressdata] = useState();
  const [Logintoken, setLogintoken] = useState();
  const [AddressName, setAddressName] = useState([]);

  useEffect(() => {
    const token = localStorage.getItem("Token")
      ? localStorage.getItem("Token")
      : "";
    setLogintoken(token);
    const update_data = {
      token: token,
      order_id: router?.query?.orderid,
    };
    if (router?.query?.orderid) {
      api.autoshipOrderById(update_data).then((res) => {
        setdata(res?.data?.orders);
        if (res?.data?.addresses) {
          setaddressdata(res?.data?.addresses[0]);
          setAddressName(Object.keys(res?.data?.addresses[0]));
        }
      });
    }
  }, [router?.query?.orderid]);
  let total = 0;
  return (
    <>
      <AutonShipDetailStyle>
        {showprotectedpage === true ? (
          <div className="main_autoshipDetail_section_class">
            <div className="Cart_product order-detail-sec autoship_detail_section">
              <h4 className="tittle">Order #{data?.public_order_id} </h4>
              <div className="row address_section">
                <div className="col-md-6">
                  <div className="thn-lft">
                    <h4>Billing Addresses</h4>
                    <AddressesSection
                      addressdata={addressdata}
                      type="billing_address"
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="thn-lft thn-rgt">
                    <h4>Shipping Addresses</h4>
                    <AddressesSection
                      addressdata={addressdata}
                      type="shipping_address"
                    />
                  </div>
                </div>
              </div>
              <h4 className="tittle">SmartShip Order Details</h4>
              <table>
                <thead>
                  <tr>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                  {data?.autoship_order_details?.map((datas, index) => {
                    if (datas?.variant !== null) {
                      total =
                        total +
                        +datas?.variant?.autoship_cost_price *
                          +datas.product_quantity;
                    } else {
                      total =
                        total +
                        +datas?.product?.autoship_cost_price *
                          +datas.product_quantity;
                    }
                    return (
                      <>
                        <tr className=" " key={index + 1}>
                          <td className=" ">
                            <div className="cart-product-details">
                              {datas?.product?.name}
                              {` ${
                                datas?.variant !== null
                                  ? ` - ${datas?.variant?.name}`
                                  : ""
                              }`}{" "}
                              X {datas?.product_quantity}
                            </div>
                          </td>
                          <td className=" ">{datas?.product_quantity}</td>
                          <td className="col-md- ">
                            <div className="cart-product-details">
                              <NumberFormat
                                value={
                                  datas?.variant !== null
                                    ? parseFloat(
                                        datas?.variant?.autoship_cost_price *
                                          datas?.product_quantity
                                      ).toFixed(2)
                                    : parseFloat(
                                        +datas?.product?.autoship_cost_price *
                                          +datas?.product_quantity
                                      ).toFixed(2)
                                }
                                displayType={"text"}
                                thousandSeparator={true}
                                prefix={"$"}
                                renderText={(value) => <div>{value}</div>}
                              />
                            </div>
                          </td>
                        </tr>
                      </>
                    );
                  })}
                  <tr>
                    <td></td>
                    <PaymentTitleSectionHtml
                      text={"Subtotal"}
                      numberValue={total}
                    />
                  </tr>
                  <tr>
                    {data?.discount_amount > 0 && (
                      <>
                        <td></td>
                        <PaymentTitleSectionHtml
                          text={`Discount ${
                            data?.coupon_name !== undefined
                              ? data.coupon_name
                              : ""
                          }`}
                          numberValue={data?.discount_amount}
                        />
                      </>
                    )}
                  </tr>
                  <tr>
                    <td></td>
                    <PaymentTitleSectionHtml
                      text={"Total"}
                      numberValue={total}
                    />
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        ) : (
          <div className="empty_div_section"></div>
        )}
      </AutonShipDetailStyle>
      <SvgAnimationBackground />
    </>
  );
};

export default AutonShipDetailPage;
