import styled from 'styled-components';

const AutonShipDetailStyle = styled.div`
&&
.main_autoshipDetail_section_class {
    width: 100%;
    max-width: 1338px;
    margin: 0 auto;
    .autoship_detail_section {
        width: 100%;
        max-width: 100%;
        margin: 40px auto 0;    
        table {
            width: 100%;
            thead {
                tr {
                    th {
                        border-bottom: 1px solid #ddd;
                    }
                }
            }
            tbody {
                tr{
                    border-bottom: 1px solid #ddd;
                    td {
                        padding: 14px;
                        &:first-child {
                            float: left;
                        }
                        .cart-product-details {
                            display: unset;
                        }
                        .main-qty-sec {
                            margin: 0;
                            display: flex;
                            justify-content: center;
                        }
                        #qty {
                            border: none !important;
                            input {
                                background: #ddd;
                                width: 30px !important;
                                height: 30px !important;
                            }
                            .sub {
                                left: 131px;
                                top: -4px !important;
                            }
                            .add {
                                top: -4px !important;
                            }
                        }
                    }
                }
            }
        }
    }
    .address_section{
        table {
            tr {
                th {
                    text-align: left;
                }
            }
        }
    }
    .btn-sec-cstm {
        .refund_button_class {
            float: right;
        }
    }



}

`;

export default AutonShipDetailStyle;