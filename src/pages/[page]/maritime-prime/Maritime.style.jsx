import styled from "styled-components";

const MartinPrimeStyle = styled.div`
  & .main_martin_prime_sections {
    .martin_prime_banner {
      width: 100%;
      height: 650px;
      overflow: hidden;
      margin-top: 70px;
      background: ${(props) =>
    props?.bannerImg
      ? `url(${props?.bannerImg})`
      : `url("/images/MartinPrime.png")`};
        background-repeat: no-repeat;
    background-size: cover;
    position: relative;
    iframe {
    width: 100vw;
    height: 210%;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50% , -40%);
} 
    }
    .image_section3 {
      position: relative;
      height: 650px;
      @media (max-width: 1440px){
        height: 520px
      }
      @media (max-width: 1366px){
        height: 450px;
      }
      @media (max-width: 1199px){
        height: 450px;
      }   
      @media (max-width: 767px){
        height: 260px;
      }
      iframe {
    width: 100vw;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -33%);
    height: 160%;
    @media (max-width: 991px){
      transform: translate(-50%,-50%);
    }
    @media (max-width: 480px){
      transform: translate(-50%,-57%);
    }
}
      img {
        width: 100%;                                                                        
      }
    }
    .section1 {
      h2.title {
        color: #fff;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%);
        width: 100%;
        margin: 0;
        max-width: 1672px;
        padding: 0 15px;
      }
      position: relative;
      /* border-bottom: 1px solid #AFAFAF; */
      padding-bottom: 15px;
    }
    .section2 {
      p {
        font-size: 17px;
        color: #000;
      }
      h2 {
        margin: 0;
        text-align: left;
        font-size: 31px;
        font-weight: 600;
        margin-bottom: 10px;
      }
      max-width: 1672px !important;
      margin: auto;
      padding: 50px 15px 70px;
      .second_passage,
      .first_passage {
        p {
          margin-bottom: 25px;
        }
      }
      .third_passage {
        ol {
          list-style: none;
          li {
            line-height: 41px;
            font-size: 17px;
            color: #000;
          }
          em {
            color: #3b648d;
            font-size: 27px;
            display: inline-block;
            padding: 0 5px;
            font-style: normal;
          }
        }
        em {
          font-size: 14px;
          color: #000;
          line-height: 28px;
          font-style: normal;
        }
      }
    }
    .image_section3 {
      overflow: hidden;
      img {
        max-height: 445px;
        object-fit: cover;
        object-position: 0 -255px;
      }
    }
    .section3 {
      .content_section3 {
        max-width: 1138px;
        margin: auto;
        height: 254px;
        background: #fff;
        padding: 38px 50px;
        position: relative;
        top: -60px;
        box-shadow: 0 0 10px 5px rgba(0, 0, 0, 0.04);
      }
      .content_section3 {
        h3 {
          font-size: 53px;
          letter-spacing: -2px;
          color: #000;
          margin: 0 0 5px;
        }
      }
      p {
        font-size: 30px;
        line-height: 50px;
        color: #000;
        margin: 0;
      }
    }
    .section4 {
      .martin_prime_section {
        margin-top: 32px;
        max-height: inherit;
        max-width: 542px;
        h2 {
          margin: 0 0 10px;
          color: rgb(0 53 106) !important;
        }
      }
      h2 {
        font-size: 81px;
        line-height: 75px;
        color: #fff;
        margin-top: 107px;
      }
      .question-text {
        margin-top: 0;
      }
    }
  }
  @media (max-width: 1536px) {
    .main_martin_prime_sections .image_section3 img {
      object-position: 0 -85px !important;
    }
  
  }
 
  @media (max-width: 1439px) {
    .main_martin_prime_sections .martin_prime_banner {
     height: 770px;
} 
    .main_martin_prime_sections .image_section3 img {
      object-position: 0 0px;
    }
    .main_martin_prime_sections .martin_prime_banner iframe { 
    transform: translate(-50% , -50%);
}
  }


  @media (max-width:1280px){
    .main_martin_prime_sections .martin_prime_banner {
     height: 650px;
} 
  }
  @media (max-width: 1024px) {
    .main_martin_prime_sections .section3 .content_section3 {
      top: -15px;
      padding: 20px 50px;
      height: 200px;
    }
    .main_martin_prime_sections .section3 .content_section3 h3 {
      font-size: 40px;
    }
    .question-text h1 {
      font-size: 50px;
      margin: 0 0 90px;
    }
    .main_martin_prime_sections .image_section3 img {
      object-position: 0 0;
    }
    .main_martin_prime_sections .martin_prime_banner {
    width: 100%;
    height: 570px;
}
    /* .main_martin_prime_sections .martin_prime_banner iframe {
      height: 100%;
    } */
  }
  @media (max-width: 991px) {
    .main_martin_prime_sections .martin_prime_banner { 
    height: 415px;  
    }
    .main_martin_prime_sections .section2 {
      max-width: 1200px;
      margin: auto;
      padding: 50px 15px 50px;
    }
    .main_martin_prime_sections .image_section3 img {
      max-height: 100% !important;
      object-fit: cover;
      object-position: 0 5px;
    }
    .main_martin_prime_sections .section3 .content_section3 {
      height: auto;
    }
  }

  @media (max-width: 767px) { 
    .main_martin_prime_sections .section1 h2.title {
      font-size: 25px !important;
    }
    .main_martin_prime_sections .section3 .content_section3 {
      padding: 20px 20px;
    }
    .main_martin_prime_sections .section3 .content_section3 h3 {
      font-size: 25px;
    }
    .main_martin_prime_sections .section3 p {
      font-size: 18px;
      line-height: 25px;
    }
    .martin_prime_section h2 {
      font-size: 30px !important;
      line-height: 45px !important;
    }
    .main_martin_prime_sections .section4 .martin_prime_section {
      max-width: 340px;
    }
    .main_martin_prime_sections .section2 .third_passage ol li {
      line-height: 30px;
    }
    .main_martin_prime_sections .section2 .third_passage ol span {
      font-size: 17px;
    }
    .main_martin_prime_sections .section2 .martin_prime_title {
      font-size: 25px;
      margin-bottom: 10px;
    }
    .main_martin_prime_sections .image_section3 img {
      object-position: 0 -5px !important;
    }
    .main_martin_prime_sections .martin_prime_banner {
      height: 250px;
    }
    .main_martin_prime_sections .section2 .third_passage ol em { 
    font-size: 18px; 
}
.fotter-list ul li a { 
    display: block;
}
.main_martin_prime_sections .section2 h2 {
    font-size: 24px;
}
.martin_prime_section img {
    width: 100%;
}
  }

  @media (max-width: 480px) {
    .main_martin_prime_sections .martin_prime_banner {
    height: 200px;
}
  }
`;
export default MartinPrimeStyle;
