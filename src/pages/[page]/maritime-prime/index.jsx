import MaritimePrimeSection from '@Components/Common/MaritimePrimeSection';
import SubscribSection from '@Components/Common/SubscribSection';
import SvgAnimationBackground from '@Components/Common/SvgAnimationBackground';
import VedioPlayer from '@Components/Common/VedioPlayer';
import React, { useState, useEffect } from 'react';
import MartinPrimeStyle from './Maritime.style';
import { Parallax } from 'react-scroll-parallax';
import api from '@api/Apis';
import { useRouter } from 'next/router';
import parse from 'html-react-parser';

function MaritimePrime({ setInnerLoader }) {
    const { page } = useRouter();
    const [state, setState] = useState({});

    useEffect(() => {
        getMaritimePrime();
    }, [])

    const getMaritimePrime = async () => {
        setInnerLoader(true)
        await api.getMaritimePrimeFunction(page).then(res => {
            const { code, content, message } = res?.data;
            if (code === 1) {
                setState(content[0]);
                setInnerLoader(false)
            } else if (code === 0) {
                setState(message);
                setInnerLoader(false)
            }

        }).catch(err => {
            // toast.error("Somthing went wrong")
        })
    }
    return (
        <MartinPrimeStyle bannerImg={`${process.env.API_URL + state?.banner_image}`}>
            <div className="main_martin_prime_sections">
                <div className="section1">
                    <div className="martin_prime_banner">
                        <VedioPlayer url={state?.banner_background_url} />
                    </div>
                    {/* <h2 className="title">{state?.banner_heading || "The Ultimate Antioxidant Formulation!"}</h2>*/}
                </div>
                <div className="section2">
                    {state?.section2_heading ? parse(state?.section2_heading) : <h2>The Maritime Prime Story</h2>}
                    <div className="first_passage">
                        {state?.section2_content_first_passage ? parse(state?.section2_content_first_passage) : <h2>content .......</h2>}
                    </div>
                    <div className="second_passage">
                        {state?.section2_content_second_passage ? parse(state?.section2_content_second_passage) : <h2>content .......</h2>}
                    </div>
                    <div className="third_passage">
                        {state?.section2_content_third_passage ? parse(state?.section2_content_third_passage) : <h2>content .......</h2>}
                    </div>
                </div>
                <div className="section3">
                    <div className="image_section3">
                        <VedioPlayer url={state?.section3_background_url} />
                    </div>
                    <Parallax y={[-50, 5]} tagOuter="figure" className="custom-class">
                        <div className="content_section3">
                            {state?.section3_heading ? parse(state?.section3_heading) : <h3>content .......</h3>}
                            {state?.section3_content ? parse(state?.section3_content) : <p>content .......</p>}
                        </div>
                    </Parallax>
                </div>
                <div className="section4">
                    <MaritimePrimeSection
                        content={{
                            product_image: process.env.API_URL + state?.section4_product_image,
                            productName: parse(String(state?.section4_Product_name)),
                            btnText: state?.section4_btn_text,
                            btnLink: state?.section4_btn_link
                        }}
                    />
                    <SubscribSection innerText="3,000,000 Bottles Sold World Wide" />
                </div>
                <SvgAnimationBackground />
            </div>
        </MartinPrimeStyle>
    )
}

export default MaritimePrime;
