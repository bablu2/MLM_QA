import React from 'react';
import Link from 'next/link';
import { Box } from '@material-ui/core';
import SvgAnimationBackground from '@Components/Common/SvgAnimationBackground';
import ProvenBenefitsStyle from './Proven.style';
import api from '@api/Apis';
import { useRouter } from 'next/router';
import parse from 'html-react-parser';
import VedioPlayer from '@Components/Common/VedioPlayer';

export default function ProvenBanifits({ setInnerLoader }) {
    const router = useRouter();
    const store = (router?.query?.page) ? router?.query?.page : 'us';

    const [headingData, setHeadingData] = React.useState([]);
    const [provenContent, setProvenContent] = React.useState([]);
    const CommonContent = ({ heading, paragraph, link, linkText }) => (
        <>
            {heading ? parse(heading) : ""}
            {paragraph ? parse(paragraph) : ""}
            <p>
                <Link href={link}>
                    <a target="_blank">{linkText || "Learn more"}</a>
                </Link>
            </p>
        </>
    );

    React.useEffect(() => {
        getProvenBanifitContent();
    }, []);

    // call api for proven content
    function getProvenBanifitContent() {
        setInnerLoader(true)
        api.getProvenBanifitContent(store).then((res) => {
            if (res?.status === 200 && res?.data?.code === 1) {
                if (res?.data?.main_page_content?.length > 0) {
                    setHeadingData(res?.data?.main_page_content[0])
                    setInnerLoader(false)
                }
                else {
                    setInnerLoader(false)
                }
                if (res?.data?.content?.length > 0) {
                    setProvenContent(res?.data?.content);
                }
            }
        }).catch((err) => console.log(err))
    }
    return (
        <ProvenBenefitsStyle>
            <Box>
                <div className="proven_banifits_section_banner">
                    {   /*  <img src="/images/proven_benefits.png" />*/}
                    <VedioPlayer url={headingData?.section1_vedio_link} />

                </div>
                <div className="content_container">
                    <p>  {headingData.page_heading ? parse(headingData.page_heading) : ""}
                        {headingData.page_subheading ? parse(headingData.page_subheading) : ""}</p>
                    {provenContent?.map((val, index) => (
                        <CommonContent heading={val?.heading} paragraph={val?.subheading} link={val?.link} linkText={val?.linkText} key={index + 1} />
                    ))}
                    <p>* Individual results may vary and are not guaranteed</p>
                </div>
                <SvgAnimationBackground />
            </Box>
        </ProvenBenefitsStyle >
    )
}
