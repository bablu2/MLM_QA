import styled from "styled-components";

const ProvenBenefitsStyle = styled.div`
  & .proven_banifits_section_banner {
    position: relative;
    float: left;
    width: 100%;
    overflow: hidden;
    margin-bottom: 70px;
    height: 650px;
    margin-top: 90px;
    @media (max-width: 1199px){
      height: 500px;
      margin-top: 70px;
    } 
    @media (max-width: 768px){ 
      margin-bottom: 50px;
    }
    @media (max-width: 767px){
      margin-top: 65px;
    }
    img {
      width: 100%;
    }
  iframe {
    width: 100vw;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    height: 210%;
    @media (max-width: 991px) {
      width: 120vw;
    }
  }
  }
  .content_container {
    max-width: 1390px;
    margin: 90px auto;
    padding: 0 15px;
    clear: both;
    p {
      text-align: center;
      br {
        display: none;
      }
    }
  @media (max-width: 991px){
    margin: 50px 0; 
  }
  }
  .content_container a {
    background: #00356a;
    padding: 3px 15px;
    border-radius: 50px;
    color: #fff;
    margin-top: 10px;
    display: inline-block;
    margin-bottom: 20px;
    font-size: 16px;
    transition: 0.3s ease all;
    border: 2px solid #00356a;
    text-decoration: none;
    padding: 6px 30px;
    &:hover {
      color: #00356a;
      background: #fff;
    }
  }
  .content_container h4 {
    color: #00356a;
    font-weight: 500;
    font-size: 25px;
    margin-top: 25px;
  }

  @media (max-width: 1400px) {
    .content_container {
      padding: 0px 15px;
    }
  }
  @media (max-width: 767px) {
    .proven_banifits_section_banner{
      height: 300px;
    }
    .content_container {
      margin: 40px auto;flex-wrap: wrap;
    }
    .content_container > *{
    max-width: 100% !important;
    width: 100%!important;
    flex: 100%;
}
    .content_container p {
      font-size: 15px;
      line-height: 25px;
    }
    .content_container h4 {
      font-size: 20px;
    }
    .content_container h1 {
      font-size: 30px;
    }
    .proven_banifits_section_banner img {
      width: 100%;
      height: 250px;
      object-fit: cover;
    }
    @media (max-width: 578px){
      .proven_banifits_section_banner {
    height: 250px;
    }
  }
  }
`;

export default ProvenBenefitsStyle;
