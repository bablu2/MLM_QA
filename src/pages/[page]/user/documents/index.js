import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Leftnav from "@Components/LeftNav";
import api from '../../../../api/Apis'
import { FaDownload } from "react-icons/fa";
import DBOP from "@Components/Common/DBOP";
import { FaFilePdf } from 'react-icons/fa';
import MobileLeftNav from "@Components/LeftNav/MobileLeftNav";
import { IconButton } from "@material-ui/core";
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";

const Documents = ({ validateauth, setshowloader, showprotectedpage, customerCheck, setInnerLoader }) => {
    const router = useRouter();
    const [documentdata, setDocumentData] = useState()
    // getDocuments
    useEffect(() => {
        document.body.classList.add('dashboard');
        setInnerLoader(true)
        validateauth()
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        api.getDocuments(token).then(res => {
            setDocumentData(res?.data)
            setInnerLoader(false)
        })
    }, [])


    const downloaddata = (url1, docpath) => {
        const files = docpath.split('/')
        fetch(url1, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/pdf',
            },
        })
            .then((response) => response.blob())
            .then((blob) => {
                // Create blob link to download
                const url = window.URL.createObjectURL(
                    new Blob([blob]),
                );
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute(
                    'download',
                    `${files[files.length - 1]}`,
                );

                // Append to html link element page
                document.body.appendChild(link);

                // Start download
                link.click();

                // Clean up and remove the link
                link.parentNode.removeChild(link);
            });

    }
    return (<>
        <Head>
            <title>Documents</title>
        </Head>
        {showprotectedpage === true ?
            <div className="mainorder-detail-sec document_page_section transactions-detail-sec ">
                <DBOP>
                    <div className="container ">
                        <div className="row">
                            <div className="dashboard_main_div main_class_responsive">
                                <Leftnav customerCheck={customerCheck} />
                                <div className="col-md-10 dashboard-main">
                                    <MobileLeftNav customerCheck={customerCheck} />
                                    <div className="container  order-detail-page">
                                        <div className="heading-top-main right-address">
                                            <div className="heading-main-text">
                                                <h1>Documents</h1>
                                            </div>
                                        </div>
                                        <div className="main_document_section">
                                            <div className="document-table">
                                                <table >
                                                    {/* <thead className="even-tr">
                                                    <th>Name</th>
                                                    <th>Download</th>
                                                </thead> */}
                                                    <tbody>
                                                        {documentdata?.data?.map((doc, index) => {
                                                            return (
                                                                <tr key={index + 1}>
                                                                    <td>
                                                                        <div className="icon-pdf">
                                                                            <FaFilePdf />
                                                                            <div className="psd-text">
                                                                                <a href={`${process.env.DOC_URL}${doc.document}`} download={`${doc.document_name}`} target="_blank" >{doc.document_name}</a>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div className="download-now">
                                                                            <IconButton onClick={() => { downloaddata(`${process.env.DOC_URL}${doc.document}`, doc.document) }} >
                                                                                <FaDownload />DOWNLOAD
                                                                            </IconButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>)
                                                        })}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </DBOP>
            </div>
            :
            <div className="empty_div_section"></div>
        }
        <SvgAnimationBackground />
    </>)

}

export default Documents;