import Head from "next/head";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Leftnav from "@Components/LeftNav";
import api from '../../../../api/Apis'
import * as React from 'react';
import Moment from 'moment';
import Form from "../../../Components/Commissions/Form";
import FormData from "../../../Components/Commissions/FormData";
import GraphicalCommission from "../../../Components/Commissions/GraphicalCommission";
import ViewOrderDetails from "../../../Components/Commissions/ViewOrderDetails";
import ExportCSV from "../../../Components/Commissions/ExportCSV";
import { useRouter } from "next/router";
import 'react-calendar/dist/Calendar.css';
import DBOP from "@Components/Common/DBOP";
import MobileLeftNav from "@Components/LeftNav/MobileLeftNav";
import { FormDataStyle, GraphicalCommissionStyle } from "src/pages/Components/Commissions/CommissionStyle";
import classNames from 'classnames';
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";
const Commission = ({ validateauth, showprotectedpage, customerCheck, setshowloader, setInnerLoader }) => {
    const [LoginToken, setLoginToken] = useState()
    const { register, handleSubmit, errors } = useForm();
    const [value, setValue] = useState([null, null]);
    const [iscustomdate, IsCustomdate] = useState('month')
    const [viewtype, setViewtype] = useState('list')
    const [offset, setOffset] = useState(0);
    const [data, setData] = useState();
    const [perPage] = useState(10);
    const [pageCount, setPageCount] = useState(0)
    const [commissionData, setcommissionData] = useState()
    const [commissionDataError, setcommissionDataError] = useState()
    const [filterdata, setfilterdata] = useState()
    const [showorderdetails, setshowdetailsorder] = useState(false)
    const [orderid, setorderid] = useState()
    const [allcommissiondata, setAllCommisiondata] = useState()
    const [firstdate, setFirstDate] = useState()
    const [lastdate, setLastDate] = useState()
    const router = useRouter()

    const [value1, onChange] = useState(new Date());
    const [value2, onChange1] = useState(new Date());
    const [showcalender, setShowCalender] = useState();

    useEffect(() => {
        validateauth()
        if (!router?.query?.Commission_order_id) {
            if (Object.keys(router?.query).length > 1) {
                router.push('/us/404/')
            }
        }
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        let payload = []
        payload.order_id = router?.query?.Commission_order_id;
        payload.filter_by_date = iscustomdate;


        setLoginToken(token)
        {
            filterdata === undefined &&
                router?.query?.Commission_order_id ?
                api.getCommissionsFilter(payload, token).then(res => {
                    setAllCommisiondata(res?.data?.commissions_data)

                    const slice = res?.data?.commissions_data?.slice(offset, offset + perPage)
                    setPageCount(Math.ceil(res?.data?.commissions_data?.length / perPage))
                    setcommissionData(slice)
                    if (res?.data?.code === 1) {
                        setcommissionDataError(res?.data?.message)
                    }
                    if (res?.data?.code === 0) {
                        setcommissionDataError()
                    }
                })

                :

                api.getCommissionReport(token, 'month').then(res => {
                    setAllCommisiondata(res?.data?.commissions_data)

                    const slice = res?.data?.commissions_data?.slice(offset, offset + perPage)
                    setPageCount(Math.ceil(res?.data?.commissions_data?.length / perPage))
                    setcommissionData(slice)
                    if (res?.data?.code === 1) {
                        setcommissionDataError(res?.data?.message)
                    }
                    // setcommissionData(res?.data)

                })
        }
    }, [offset, router?.query?.Commission_order_id]);

    const onSubmit = data => {
        setfilterdata(data)
        if (iscustomdate === 'custom') {
            let daterange = convert(value);
            daterange = daterange.split(" - ");
            data.from_date = Moment(value[0]).format('YYYY-MM-DD')
            data.til_date = Moment(value[1]).format('YYYY-MM-DD')
        }
        // setInnerLoader(true)
        api.getCommissionsFilter(data, LoginToken).then(res => {
            setAllCommisiondata(res?.data?.commissions_data)
            const slice = res?.data?.commissions_data?.slice(offset, offset + perPage)
            setPageCount(Math.ceil(res?.data?.commissions_data?.length / perPage))
            setcommissionData(slice)
            setInnerLoader(false)
            if (res?.data?.code === 1) {
                setcommissionDataError(res?.data?.message)
            }
            if (res?.data?.code === 0) {
                setcommissionDataError()
            }
        })

    }

    const convert = dateRange =>
        dateRange.map(date => new Intl.DateTimeFormat('en-US').format(new Date(date)))
            .join(" - ")

    const FilterData = (data, offset) => {
        setfilterdata(data)
        if (iscustomdate === 'custom') {
            let daterange = convert(value);
            daterange = daterange.split(" - ");
            data.from_date = Moment(value[0]).format('YYYY-MM-DD')
            data.til_date = Moment(value[1]).format('YYYY-MM-DD')
        }
        api.getCommissionsFilter(data, LoginToken).then(res => {
            const slice = res?.data?.commissions_data?.slice(offset, offset + perPage)
            setPageCount(Math.ceil(res?.data?.commissions_data?.length / perPage))
            setcommissionData(slice)
            if (res?.data?.code === 1) {
                setcommissionDataError(res?.data?.message)
            }
            if (res?.data?.code === 0) {
                setcommissionDataError()
            }
        })

    }
    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * perPage;
        if (filterdata !== undefined) {
            FilterData(filterdata, offset)
        }
        setOffset(offset)

    };
    const ShoworderDetails = (id) => {
        setshowdetailsorder(true)
        setorderid(id)
    }

    let CsvData = [];
    const setShowCalenderData = (val) => {
        setShowCalender(val)
    }
    return (<>

        <Head>
            <title>Commissions</title>
        </Head>
        {showprotectedpage === true &&
            <DBOP>
                <div className={classNames("mainorder-detail-sec commision-now", { "main_order_section_class": showorderdetails })}>
                    <div className="container dashboard-main commission-cstm">
                        <div className="row">
                            <div className="dashboard_main_div main_class_responsive">

                                <Leftnav customerCheck={customerCheck} />
                                <div className="col-md-10 " >
                                    <MobileLeftNav customerCheck={customerCheck} />
                                    <div className="container order-detail-page">
                                        {showorderdetails === true ?
                                            <ViewOrderDetails setshowdetailsorder={setshowdetailsorder} orderid={orderid} setorderid={setorderid}
                                                setInnerLoader={setInnerLoader} />
                                            :
                                            <>
                                                <Form
                                                    allcommissiondata={allcommissiondata}
                                                    setAllCommisiondata={setAllCommisiondata}
                                                    value1={value1}
                                                    value2={value2}
                                                    setShowCalender={setShowCalenderData}
                                                    showcalender={showcalender}
                                                    onChange1={onChange1}
                                                    onChange={onChange}
                                                    handlePageClick={handlePageClick}
                                                    pageCount={pageCount}
                                                    value={value} setValue={setValue} iscustomdate={iscustomdate} register={register} IsCustomdate={IsCustomdate} handleSubmit={handleSubmit} onSubmit={onSubmit} />
                                                {commissionDataError === undefined &&
                                                    <div className="export-to-xls">
                                                        <ExportCSV csvData={CsvData} fileName={`CommissionReport ${lastdate}  -  ${firstdate}`} />
                                                    </div>
                                                }
                                                <div className={`groups tabs ${showcalender === true ? 'commission-tab' : ''}`}>
                                                    <ul className="tabs">
                                                        {/* <li className={`col-md-6 ${viewtype === 'list' ? 'active' : ''}`} onClick={() => { setViewtype('list') }}>
                                                            <button>List View</button>
                                            </li>*/}
                                                        {/*<li className={`col-md-6 ${viewtype === 'graphical' ? 'active' : ''}`} onClick={() => { setViewtype('graphical') }}>
                                                            <button>Graphical View</button>
                                            </li>*/}
                                                    </ul>
                                                </div>
                                                {viewtype === "graphical" ?
                                                    <GraphicalCommissionStyle>
                                                        <div className="graphical-rep-commission">
                                                            <GraphicalCommission iscustomdate={iscustomdate} />
                                                        </div>
                                                    </GraphicalCommissionStyle>
                                                    :
                                                    <FormDataStyle>
                                                        <div className="comission-data-table comission-data-t2">
                                                            <FormData
                                                                setLastDate={setLastDate}
                                                                setFirstDate={setFirstDate}
                                                                allcommissiondata={allcommissiondata}
                                                                CsvData={CsvData}
                                                                commissionDataError={commissionDataError}
                                                                handlePageClick={handlePageClick}
                                                                pageCount={pageCount}
                                                                commissionData={commissionData}
                                                                ShoworderDetails={ShoworderDetails} />
                                                        </div>
                                                    </FormDataStyle>
                                                }
                                            </>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </DBOP>
        }
        <SvgAnimationBackground />
    </>)
}

export default Commission;