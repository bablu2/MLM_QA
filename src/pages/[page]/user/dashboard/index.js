import Head from "next/head";
import { useEffect, useState } from "react";
import Leftnav from "@Components/LeftNav";
import api from '../../../../api/Apis'
import React from 'react';
import PieCharts from "../../../Components/Dashboard/ReactD3PieChart";
import LineGraph from "../../../Components/Dashboard/LineGraph";
import Commission from "../../../Components/Dashboard/Commission";
import { useForm } from "react-hook-form";
import FilterCommissionForm from "../../../Components/Dashboard/FilterCommissionForm";
import { useRouter } from "next/router";
import Leftsection from "../../../Components/Dashboard/Leftsection";
import DBOP from "../../../../Components/Common/DBOP";
import MobileLeftNav from "@Components/LeftNav/MobileLeftNav";
import NumberFormatComp from "@Components/Common/NumberFormatComp";
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";
import _ from "lodash";
import Smartshipforcustomer from "../smartshiporder/Smartshipforcustomer";
import LatestOrders from "../smartshiporder/LatestOrders";
import DialogComponent from "@Components/Common/DialogComponent";
import { DialogSectionCss } from "@PagesComponent/ActiveSmartShip/ActiveStyleComp";
import Cookie from 'js-cookie';
var randomstring = require("randomstring");


const BONUS_ARRAY = ['FOC', 'FO_match', 'retail_customer_bonus', 'unilevel_commission', 'unilevel_commission_match'];

const Dashboard = ({ validateauth, setshowloader, ProfileFunction, customerCheck, showprotectedpage, profileData, isLogin, getDetails, setInnerLoader }) => {
  const [value, setValue] = useState([null, null]);
  const [iscustomdate, IsCustomdate] = useState('month');
  const [bonusFilter, setBonusFilter] = useState('all');
  const [linedata, setLineData] = useState()
  const [newsdata, setNewsdata] = useState()
  const [documentdata, setDocumentData] = useState()
  const [upComingData, setUpcomingData] = useState([]);
  const [userProfile, setUserProfile] = useState()
  const [alertProfile, setAlertProfile] = useState(false)
  const [popupData, setPopupData] = useState()
  const [commissionValue, setCommissionValue] = useState({
    prevCommission: [], newCommission: []
  });
  const [dateOnly, setDateOnly] = useState([]);
  const [orderData, setOrderData] = useState([]);
  const [pqvgvData, setPqvgvData] = useState({
    pqvData: [],
    gvData: []
  });
  const [duplicateBonusData, setDuplecateBonusData] = React.useState({
    'FOC': [], 'FO_match': [],
    'retail_customer_bonus': [], 'unilevel_commission': [],
    'unilevel_commission_match': []
  });
  const [bonusValueDataState, setBonusValueDataState] = useState({
    'FOC': [], 'FO_match': [],
    'retail_customer_bonus': [], 'unilevel_commission': [],
    'unilevel_commission_match': []
  });
  //legsSection
  const [legsGraphStruture, setLegsGraphStructure] = useState([{}]);
  const [legsCommission, setLegCommission] = useState({});
  const [legsData, setLegsData] = useState({});
  const [legsFilter, setLegsFilter] = useState("all");

  const [commissionLevel, setCommissionLevel] = useState([]);

  const [rankOption, setRankOption] = useState([]);
  const [pieChartData, setPieChartData] = useState({ data: {}, totalValue: null })
  const [userData, setUserData] = useState()
  const router = useRouter()

  useEffect(() => {
    // setInnerLoader(true)
    document.body.classList.add('dashboard');
    // showprotectedpage === true ? setshowloader(true) : setshowloader(false)
    validateauth()
    if (Object.keys(router?.query).length > 1) {
      router.push('/us/404/')
    }
    if (router.push("/us/user/dashboard/")) {
      document.body.classList.remove('bg-salmon');
    }

    const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
    ProfileFunction(token);
    // setInnerLoader(true)

    if (customerCheck > 2) {
      api.getDashboardCommissions(iscustomdate, token).then(res => {
        if (res?.data?.code === 1) {
          let WEEKDATA = {};
          setLineData(res?.data?.commissions_data);
          _.forEach(BONUS_ARRAY, (row) => {
            const VALUES = res?.data?.weekwise_month_data?.[row];
            const Data = _.map(_.entries(VALUES), ([key, value]) => ({
              date: key,
              total_commission: value
            }));
            WEEKDATA = { ...WEEKDATA, [row]: Data };
          })
          if (iscustomdate === "month" || iscustomdate === "last_month") {
            setCommissionLevel(WEEKDATA);
          } else {
            setCommissionLevel(res?.data?.context);
          }
          setLegsData(res?.data?.teamorleg);
          setPieChartData({ data: res?.data?.bv_commType_sum, totalValue: res?.data?.all_bonus_sum });
          // setInnerLoader(false)

        }

      })

      api.getDashboardNews(token).then(res => {
        setNewsdata(res?.data)
      })
      api.getDocuments(token).then(res => {
        setDocumentData(res?.data)
        // setInnerLoader(false)

      })
    }
  }, [iscustomdate, customerCheck]);

  useEffect(() => {
    if (localStorage.getItem('profileData') && JSON.parse(localStorage.getItem('profileData'))?.user_data?.user_profile?.image === "profile_pics/default.jpg") {
      setTimeout(() => {
        if (Cookie.get('popupCookie')?.length == undefined) {
          setAlertProfile(true)
          Cookie.set('popupCookie', "true")
        }
      }, 3000);
    }
    else {
      setAlertProfile(false)
    }
  }, [])
  function gotoProfile() {
    router.push("/us/user/profile/")
    setAlertProfile(false)
  }

  useEffect(() => {
    api.getdashboardpopup().then((res) => {
      if (res?.data?.code === 1) {
        setPopupData(res?.data?.pop_data)
      }
    })
    const DATA = localStorage?.getItem('profileData') ? JSON.parse(localStorage?.getItem('profileData')) : ""
    setUserData(DATA)
  }, [router])




  //get PQV and PGV customer data
  function getOrderPQVandGVFunction(token) {
    let order_pqv = [];
    let order_gv = [];
    api.getOrderPQVandGV(token).then(({ data, status }) => {
      if (status === 200 && data?.code === 1) {
        data?.data?.forEach((value, index) => {
          order_pqv = [...order_pqv, _.pick(value, ['public_order_id', 'order_pqv', 'customer_name'])];
          order_gv = [...order_gv, _.pick(value, ['public_order_id', 'order_gv', 'customer_name'])];
        });
        setPqvgvData({ pqvData: order_pqv, gvData: order_gv });
        setRankOption(data?.promoter_option);
      }
    }).catch((err) => console.log(err))
  }

  useEffect(async () => {
    let token = localStorage.getItem("Token");
    if (token) {
      await api.getUpcommingAutoship(token).then((res) => {
        let data = [];
        if (res?.data?.code === 1) {
          if (res?.data?.orders?.length > 0) {
            res?.data?.orders?.map((value) => {
              if (res?.dara?.autoship_order_details?.length > 0) {
              }
              data?.push({
                orderId: value?.order?.public_order_id,
                createdDate: value?.created_at,
                upcomingDate: value?.next_shipping_date
              });
            })
            setUpcomingData(data);
          }
        }
      }).catch((err) => console.log(err));

      // setInnerLoader(true)
      if (customerCheck > 2) {
        await api.getUserOrder(token).then(res => {
          if (res?.data?.code === 1) {
            setOrderData(res?.data?.orders)
            setInnerLoader(false)

          }
          else {
            setInnerLoader(false)

          }
        })
      }
      getOrderPQVandGVFunction(token);

    }
  }, [])


  const { register, handleSubmit, errors } = useForm();

  const onSubmit = data => {
    // let daterange = convert(value);
    // daterange = daterange.split(" - ");
    // const from_date = Moment(daterange[0]).format('YYYY-MM-DD');
    // const til_date = Moment(daterange[1]).format('YYYY-MM-DD');
    // data.from_date = from_date
    // data.til_date = til_date

    // api.getCommissionsFilter(data, LoginToken).then(res => {
    //     const slice = res?.data?.commissions_data?.slice(offset, offset + perPage)
    //     setPageCount(Math.ceil(res?.data?.commissions_data?.length / perPage))
    //     setcommissionData(slice)
    // })


  }
  const convert = dateRange => new Intl.DateTimeFormat('en-US').format(new Date(dateRange))
  return (
    <>
      <Head>
        <title>Dashboard</title>
      </Head>
      <DBOP>
        {showprotectedpage === true ?
          <div className="mainorder-detail-sec dashboad-deails Dashboard_bg main_dashboard_section_class">
            <div className="profile-detail-section">
              <div className="container">
                <div className="row">
                  <div className="dashboard_main_div main_class_responsive">
                    <Leftnav customerCheck={customerCheck} getDetails={getDetails} />
                    <div className="col-md-10 dashboard-main">
                      <MobileLeftNav customerCheck={customerCheck} getDetails={getDetails} />
                      <div className="container order-detail-page">
                        <div className="heading-top-main right-address">
                          <img src="Dashboard.png" width="100%" />
                          <div className="heading-main-text">
                            <h1>Dashboard</h1>
                          </div>
                        </div>
                        <div className="row">
                          <Leftsection
                            customerCheck={customerCheck}
                            profileData={profileData}
                            newsdata={newsdata}
                            pqvgvData={pqvgvData}
                            documentdata={documentdata}
                            rankOption={rankOption}
                          />
                          <div className="col-md-9 statis_datas ">
                            {
                              customerCheck > 2 &&
                              <div className="top-head-commission">
                                <div className="left-commison-sec">
                                  <span>Downline Sales:</span><NumberFormatComp value={profileData?.total_sales} />
                                </div>
                                <div className="right-commison-sec">
                                  <span>Paid as Rank:</span>
                                  {
                                    profileData?.ranks?.map((ranks) => {
                                      return (
                                        +profileData?.user_data?.userdetails[0]?.paid_as_rank === ranks.id &&
                                        <div className="title1 " key={ranks?.rank_name}>{ranks?.rank_name}</div>
                                      )
                                    })
                                  }
                                  {
                                    +profileData?.rank_as_per_sales === 11 || +profileData?.rank_as_per_sales === 12 || +profileData?.rank_as_per_sales === 13 ? 'th' :
                                      +profileData?.rank_as_per_sales % 10 === 1 ? '' :
                                        +profileData?.rank_as_per_sales % 10 === 2 ? '' :
                                          +profileData?.rank_as_per_sales % 10 === 3 ? '' : profileData?.rank_as_per_sales ?
                                            'th' : ''
                                  }
                                </div>
                              </div>
                            }

                            {(getDetails > 2 || customerCheck > 2) ?
                              <>
                                <div className="main-top">
                                  <FilterCommissionForm
                                    value={value}
                                    setValue={setValue}
                                    iscustomdate={iscustomdate}
                                    register={register}
                                    commissionLevel={commissionLevel}
                                    bonusFilter={bonusFilter}
                                    setBonusFilter={setBonusFilter}
                                    IsCustomdate={IsCustomdate}
                                    handleSubmit={handleSubmit}
                                    onSubmit={onSubmit}
                                    duplicateBonusData={duplicateBonusData}
                                    setBonusValueDataState={setBonusValueDataState}
                                    legsCommission={legsCommission}
                                    setLegCommission={setLegCommission}
                                    setLegsGraphStructure={setLegsGraphStructure}
                                    legsFilter={legsFilter}
                                    setLegsFilter={setLegsFilter}
                                    commissionValue={commissionValue}
                                    setCommissionValue={setCommissionValue}
                                  />
                                  <div className="chart-main">
                                    <div className="chart-LineGraph  ">

                                      {iscustomdate &&
                                        <LineGraph
                                          linedata={linedata}
                                          filter={iscustomdate}
                                          bonusData={commissionLevel}
                                          bonusFilter={bonusFilter}
                                          setBonusFilter={setBonusFilter}
                                          bonusValueDataState={bonusValueDataState}
                                          setBonusValueDataState={setBonusValueDataState}
                                          setDuplecateBonusData={setDuplecateBonusData}
                                          legsData={legsData}
                                          legsCommission={legsCommission}
                                          setLegCommission={setLegCommission}
                                          legsGraphStruture={legsGraphStruture}
                                          setLegsGraphStructure={setLegsGraphStructure}
                                          setLegsFilter={setLegsFilter}
                                          commissionValue={commissionValue}
                                          setCommissionValue={setCommissionValue}
                                          dateOnly={dateOnly}
                                          setDateOnly={setDateOnly}

                                        />
                                      }
                                    </div>
                                    <div className="chart-PieCharts">
                                      {/*  <PieCharts data={pieChartData?.data} totalValue={pieChartData?.totalValue} bonusFilter={bonusFilter} />*/}
                                    </div>
                                  </div>
                                </div>
                                <div className="commsion-weekly-dashboard"><Commission /></div>
                              </>
                              :

                              <>
                                <Smartshipforcustomer
                                  validateauth={validateauth}
                                  getDetails={getDetails}
                                  customerCheck={customerCheck}
                                  setInnerLoader={setInnerLoader} />
                                <LatestOrders
                                  validateauth={validateauth}
                                  customerCheck={customerCheck}
                                  setInnerLoader={setInnerLoader} />
                              </>

                            }

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          :
          <div className="empty_div_section"></div>
        }
      </DBOP>
      <SvgAnimationBackground />
      <DialogComponent opend={alertProfile} handleClose={() => setAlertProfile(false)} title="Profile" classFor="order_details">
        <DialogSectionCss>
          <div className='profile-popup'>
            <div className="image-popup">
              <img src="images/default.jpg" />
            </div>
            <div className="user-data">
              <div className="form-group first-name"><span>{userData?.user_data?.first_name} {userData?.user_data?.last_name}</span></div>
              <div className="form-group"><span>{userData?.user_data?.userdetails?.[0]?.public_phone_number}</span></div>
              <div className="form-group"><span>{userData?.user_data?.email}</span></div>
            </div>
          </div>

          <span className="popupData">
            {popupData}
          </span>
          <div className='goto-profile' style={{ cursor: 'pointer' }}>
            <a onClick={() => gotoProfile()}>Go to profile page</a>
          </div>
        </DialogSectionCss>
      </DialogComponent>
    </>)
}
export default Dashboard;