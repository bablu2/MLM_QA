import React, { useEffect, useState } from "react";
import { IconButton, Link, Table, TableCell } from "@material-ui/core";
import CommonLayout from "@Components/Common/CommonLayout";
import InviteLinkPopup from "@PagesComponent/InviteComponent/InviteLinkPopup";
import { MdContentCopy } from "react-icons/md";

import {
  EmailIcon,
  EmailShareButton,
  FacebookIcon,
  FacebookShareButton,
  TwitterIcon,
  TwitterShareButton,
  WhatsappIcon,
  WhatsappShareButton,
} from "react-share";
import { Message } from "@material-ui/icons";
import EditIcon from '@material-ui/icons/Edit';

import { DialogSectionCss } from "@PagesComponent/ActiveSmartShip/ActiveStyleComp";
import DialogComponent from "@Components/Common/DialogComponent";
import CopyToClipboard from "react-copy-to-clipboard";
import { useForm } from "react-hook-form";
import api from "@api/Apis";
const index = (props) => {
  const [openLink, setOpenLink] = useState(false);
  const [getInviteLink, setInviteLink] = useState();
  const [open, setOpen] = useState(false);
  const [userData, setUserData] = useState();
  const [isCopied, setIsCopied] = useState(false)
  const [refCopied, setRefCopied] = useState(false)
  const [googleInfo, setGoogleInfo] = useState(false)
  const [fbInfo, setFbInfo] = useState(false)
  const [dataReferer, setDataRefrer] = useState()
  const [isReadonly, setIsReadonly] = useState(true);
  const [value, setValue] = React.useState(dataReferer);
  const [referralUpdate, setreferralUpdate] = useState()
  const [editRefervalue, setEditReferValue] = useState()
  const [getToken, setGetToken] = useState()
  const [errorref, setErrorRef] = useState('')
  const [successmsg, setSuccessmsg] = useState('')
  const [file, setFile] = useState();
  const { showprotectedpage, customerCheck, validateauth, setInnerLoader } =
    props;
  const { register, handleSubmit, watch, errors, setError, getValues, reset } =
    useForm({
      mode: "all",
    });
  function getdataopen(values,) {
    setUserData(values);
    setOpen(true);
  }

  const onCopyText = (id) => {
    setIsCopied(id);
    setTimeout(() => {
      setIsCopied(false);
    }, 1000);
  };
  //for referral code copied 
  const onCopyRef = (dataReferer) => {
    setRefCopied(dataReferer)
    setTimeout(() => {
      setRefCopied(false);
    }, 1000)
  }
  function onSubmit(data) {
    // alert('hi')
  }

  useEffect(() => {
    const dataref = localStorage.getItem('profileData') ? JSON.parse(localStorage.getItem('profileData'))?.user_data?.userdetails[0]?.referral_code : null
    setDataRefrer(dataref)
    const LoginToken = localStorage?.getItem('Token') ? localStorage?.getItem('Token') : ''
    setGetToken(LoginToken)
  }, [])

  //for upadte referrer
  const UpadteRefer = () => {
    const data = {
      "referral_code": editRefervalue,
      "update_referral": true,
      'public_data': false

    }
    api.profileUpdate(data, getToken).then(res => {
      if (res?.data?.code === 1) {
        setreferralUpdate(res?.data)
        setSuccessmsg(res?.data?.message)
        setTimeout(() => {
          setSuccessmsg('')
        }, 3000);
        localStorage.removeItem('profileData')
        setErrorRef('')
      }
      else if (res?.data?.code == 0) {
        setErrorRef(res?.data?.message)
        setTimeout(() => {
          setErrorRef('')
        }, 3000);
      }
    })

    api.getMyProfileDetails(getToken).then(res => {
      if (res?.data?.code === 1) {
      }
    })
  }
  function handlechange(e) {
    setEditReferValue(e.target.value)
  }

  // function handleChange(e) {
  //   console.log(e.target.files);
  //   setFile(URL.createObjectURL(e.target.files[0]));
  // }
  return (
    <>
      <CommonLayout
        attributes={{
          title: "Marketing",
          section: "activeSmartship",
          showprotectedpage,
          customerCheck,
          validateauth,
        }}
      >
        <div className="main-cstm">
          <div className="order_table order-list" >
            <h2>Referral Code </h2>
            <div className="note-mainbx">
              <p><strong>Note: </strong>If you update your Referral code. The old URL will no longer work.</p>
              {customerCheck > 2 ?
                <div className="newrefshow d-flex">
                  <>
                    <div className="referral-copy">
                      <input type="text"
                        value={editRefervalue ? editRefervalue : dataReferer}
                        onChange={(e) => handlechange(e)}
                        onInput={e => setValue(e)}
                      // defaultValue={dataReferer} 
                      />.shopkaire.com
                      <button
                        onClick={() => UpadteRefer()}>Update</button>
                    </div>
                    <div className="copied_ref">
                      <CopyToClipboard
                        text={`${dataReferer}.shopkaire.com`}
                        onCopy={() => onCopyRef(`${dataReferer}.shopkaire.com`)}
                      >
                        <button aria-label="copy">
                          {refCopied === `${dataReferer}.shopkaire.com` ? "Copied!" : < MdContentCopy />}
                        </button>
                      </CopyToClipboard>
                    </div>
                  </>
                </div>
                :
                <div className="newrefshow d-flex">
                  <>
                    <div className="referral-copy">
                      {dataReferer}.shopkaire.com
                    </div>
                    <div className="copied_ref">
                      <CopyToClipboard
                        text={`${dataReferer}.shopkaire.com`}
                        onCopy={() => onCopyRef(`${dataReferer}.shopkaire.com`)}
                      >
                        <button aria-label="copy">
                          {refCopied === `${dataReferer}.shopkaire.com` ? "Copied!" : < MdContentCopy />}
                        </button>
                      </CopyToClipboard>
                    </div>
                  </>
                </div>
              }
              {errorref && <span className="error">{errorref}</span>}
              {successmsg && <span className="error" style={{ color: 'green' }}>{successmsg}</span>}


            </div>

            {customerCheck > 2 &&
              <div className="n-mainbx cstm-generat">
                <h2>Invite Link Generator</h2>
                <div className="coupon-wrap px-3">
                  <div className="coupon-para">
                    <h5>Easily generate links to specific pages or products on your referral site.  You will be able to share,
                      track activity and calculate revenue generated using your personal links.</h5>
                  </div>
                  <div className="main-order d-flex">
                    <div className="link_buttton">
                      <button onClick={() => setOpenLink(true)}>New Link</button>
                    </div>
                  </div>
                </div>
              </div>
            }
            <div className="main-invitaion">
              <table className="comistion-table-data dashboard-table-commsion address-list commission-table">
                <thead>
                  <th
                    align="center"
                    component="td"
                    scope="row"
                    className="year_block"
                  >
                    Name
                  </th>

                  { /*<th
                    align="center"
                    component="td"
                    scope="row"
                    className="year_block"
                  >
                    Created
      </th>*/}
                  <th
                    align="center"
                    component="td"
                    scope="row"
                    className="year_block"
                  >
                    Click Count
                  </th>
                  <th
                    align="center"
                    component="td"
                    scope="row"
                    className="year_block"
                  >
                    Last Clicked
                  </th>
                  <th
                    align="center"
                    component="td"
                    scope="row"
                    className="year_block"
                  >
                    Total Revenue
                  </th>
                  <th
                    align="center"
                    component="td"
                    scope="row"
                    className="year_block"
                  >
                    Conversions
                  </th>
                  <th
                    align="center"
                    component="td"
                    scope="row"
                    className="year_block"
                  >
                    Share
                  </th>
                </thead>
                <tbody>
                  {getInviteLink?.lead_obj?.map((items) => {
                    return (
                      <>
                        <tr>
                          <td data-value="Name">{items?.name}</td>
                          {/*td data-value="Created">
                            {items?.created_at ? items?.created_at : ""}
                    </td>*/}
                          <td data-value="Click Count">
                            {items?.no_of_clicks ? items?.no_of_clicks : 0}
                          </td>

                          <td data-value="Clicked">
                            {items?.clicked_on ? items?.clicked_on : ""}
                          </td>

                          <td data-value="Total Revenue">
                            {items?.net_downline_revenue ? `$${parseFloat(items?.net_downline_revenue).toFixed(2)}` : '$0.00'}
                          </td>

                          <td data-value="Converted">
                            <a
                              onClick={() => getdataopen(items?.reg_users)}
                              className="btn btn-primary"
                            >
                              View
                            </a>
                          </td>

                          <td data-value="Share">
                            <FacebookShareButton
                              url={items?.link}
                              className="Demo__some-network__share-button"
                            >
                              <FacebookIcon size={32} round />
                            </FacebookShareButton>

                            <TwitterShareButton url={items?.link}>
                              <TwitterIcon size={32} round />
                            </TwitterShareButton>
                            <WhatsappShareButton url={items?.link}>
                              <WhatsappIcon size={32} round />
                            </WhatsappShareButton>
                            <EmailShareButton url={items?.link}>
                              <EmailIcon size={32} round />
                            </EmailShareButton>
                            <CopyToClipboard
                              text={items?.link}
                              onCopy={() => onCopyText(items?.id)}
                            >
                              <button aria-label="copy">
                                {isCopied === items?.id ? (
                                  "Copied!"
                                ) : (
                                  <MdContentCopy />
                                )}
                              </button>
                            </CopyToClipboard>
                          </td>
                        </tr>
                        <DialogComponent
                          opend={open}
                          handleClose={() => setOpen(false)}
                          title=" Link Conversions"
                          classFor="order_details"
                        >
                          <DialogSectionCss>
                            <table>
                              <thead>
                                <tr>
                                  <th>User ID</th>
                                  <th>Name</th>
                                  <th>Revenue</th>
                                  <th>Products</th>
                                </tr>
                              </thead>
                              <tbody>
                                {userData?.map((data, index) => {
                                  return (
                                    <>
                                      <tr>
                                        <td>
                                          {data?.public_user_id
                                            ? data?.public_user_id
                                            : ""}
                                        </td>
                                        <td>
                                          {data?.user_name
                                            ? data?.user_name
                                            : ""}
                                        </td>
                                        <td>
                                          {data?.revenue ? `$${data?.revenue}` : 0}
                                        </td>
                                        <td>
                                          {data?.order_products?.length > 0 ? (
                                            <ul>
                                              {_.map(
                                                data?.order_products,
                                                (row) => (
                                                  <li>{row?.product_name}</li>
                                                )
                                              )}
                                            </ul>
                                          ) : (
                                            <span>N/A</span>
                                          )}
                                        </td>

                                      </tr>
                                    </>
                                  );
                                })}
                              </tbody>
                            </table>
                          </DialogSectionCss>
                        </DialogComponent>
                      </>
                    );
                  })}
                </tbody>
              </table>
            </div>
            {/*  <div className="images-upload">
          <input type="file" onChange={handleChange} />
          <img src={file} />
                </div>*/}
            {/*
           <div className="invitation-form">
              <div class="heading-top-main right-address"><div class="heading-main-text"><h1>Advance Analytics & Tracking.</h1></div></div>
              <form className="invitation-link-main" onSubmit={handleSubmit(onSubmit)}>

                <div className="coupon-para">
                  <b>Please note that these settings are for advanced online marketers only.
                    We can only provide limited support on this feature and cannot assist with any account setup
                    on any external websites.</b> <br></br><br></br>
                </div>
                <div className="mb-3 field-class">
                  <label>Google Analytics </label>
                  <input
                    type="text"
                    className="form-control"
                    name="name"
                    id="name"
                    aria-describedby="nameHelp"
                    placeholder="Google analytic key"
                    ref={register({ required: false })}
                  />
                  <a onClick={() => setGoogleInfo(true)}>More info</a>
                </div>
                <div className="mb-3 field-class">
                  <label>Facebook Pixel (Meta Pixel)</label>
                  <input
                    type="text"
                    className="form-control"
                    name="email"
                    id="email"
                    aria-describedby="emailnoHelp"
                    placeholder="Fb analytic key "
                    ref={register({ required: false })}
                  />
                  <a onClick={() => setFbInfo(true)}>More info</a>
                </div>
                <div className="submit_button field-class">
                  <button
                    type="submit"
                    className="btn btn-primary signup_up_button"
                  >
                    Next
                  </button>
                </div>
              </form>
            </div>
              */}
          </div>
        </div>
      </CommonLayout>

      <InviteLinkPopup
        openLink={openLink}
        setOpenLink={setOpenLink}
        getInviteLink={getInviteLink}
        setInviteLink={setInviteLink}
      />

      <DialogComponent opend={googleInfo} handleClose={() => setGoogleInfo(false)} title="Description" classFor="order_details">
        <DialogSectionCss>
          <h5>Google Analytics</h5>
          <p>Google Analytics enables you to track the visitors to your store and generates reports that will help you with your marketing.</p>
          <a href="https://support.google.com/analytics/answer/10269537?hl=en" target="_blank">Learn how to setup google analytics</a>

        </DialogSectionCss>
      </DialogComponent>
      <DialogComponent opend={fbInfo} handleClose={() => setFbInfo(false)} title="Description" classFor="order_details">
        <DialogSectionCss>
          <h5>Facebook Pixel</h5>
          <p>Facebook Pixel tracks customer behavior on your online store. This data improves your marketing abilities, including retargeting ads.</p>
          <a href="https://www.facebook.com/business/help/952192354843755?id=1205376682832142" target="_blank">Learn how to setup facebook pixel</a>
        </DialogSectionCss>
      </DialogComponent>
    </>
  );
};

export default index;
