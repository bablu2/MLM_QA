import Head from "next/head";
import { useEffect, useState } from "react";
import { toast } from 'react-toastify';
import Leftnav from "@Components/LeftNav";
import ReactPaginate from 'react-paginate';
import { FaTrash, FaPlus, FaEdit } from "react-icons/fa";

import api from '../../../../api/Apis'
import AddressData from "../../../Components/Address/Address";
import { useRouter } from "next/router";
import DBOP from "../../../../Components/Common/DBOP";
import ReactpaginateComp from "@Components/Common/ReactPaginateComp";
import MobileLeftNav from "@Components/LeftNav/MobileLeftNav";
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";

const UserAddress = ({ validateauth, showprotectedpage, customerCheck, setshowloader, setInnerLoader }) => {
    const [addressData, setaddressData] = useState();
    const [showaddress, Setshowaddress] = useState(false);
    const [addressDetails, setaddressDetails] = useState();
    const [offset, setOffset] = useState(0);
    const [data, setData] = useState();
    const [perPage] = useState(10);
    const [pageCount, setPageCount] = useState(0)
    const [logintoken, setToken] = useState();
    const [deleteNotify, setDeleteNotify] = useState('')
    const [loading, setLoading] = useState('')
    const [noData, setNoData] = useState('')
    const router = useRouter()
    useEffect(() => {
        document.body.classList.add('dashboard');
        setLoading("Loading")
        setInnerLoader(true)

        // if (Object.keys(router?.query).length > 1 || Object.keys(router?.query).length === 0) {
        //     router.push('/us/404/')
        // }
        // else {

        validateauth()

        // }
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setToken(token)
        api.manageAddress(token).then(res => {
            if (res?.data?.code === 1) {
                const slice = res?.data?.addresses?.slice(offset, offset + perPage)
                setPageCount(Math.ceil(res?.data?.addresses?.length / perPage))
                setaddressData(slice)

                setInnerLoader(false)
                setLoading("")
                setNoData("")
            }
            else {

                setInnerLoader(false)
                setLoading("")
            }


        })
    }, [offset]);
    // delete address
    const deleteAddress = (e) => {
        const id = e.currentTarget.parentNode.value;
        const formDataD = { address_id: id };
        api.deleteAddress(formDataD, logintoken).then(res => {
            api.manageAddress(logintoken).then(res => {
                if (res?.data?.code === 1) {
                    setDeleteNotify("Address deleted successfully")
                    setTimeout(() => {
                        setDeleteNotify("")
                    }, 3000);
                    // toast.success('address deleted successfully', {
                    //     duration: 1
                    // })
                    setaddressData(res?.data?.addresses)
                    if (+localStorage.getItem('billingAddress') || +localStorage.getItem('shipAddress') == +id) {
                        localStorage.removeItem('billingAddress')
                        localStorage.removeItem('shipAddress')
                    }
                    setNoData("")
                }
                if (res?.data?.code === 0) {
                    setDeleteNotify("Address deleted successfully")
                    setTimeout(() => {
                        setDeleteNotify("")
                    }, 3000);
                    // toast.success('address deleted successfully', {
                    //     duration: 1
                    // })
                    setaddressData(res?.data?.addresses)
                    if (+localStorage.getItem('billingAddress') || +localStorage.getItem('shipAddress') == +id) {
                        localStorage.removeItem('billingAddress')
                        localStorage.removeItem('shipAddress')
                    }
                    setNoData("No Data Found")
                }
            })
        })
    }
    // onclick edit address this will be call
    const EditAddress = (id) => {
        const formData = { address_id: +id, token: logintoken };
        api.getAddressDetails(formData).then(res => {
            setaddressDetails(res?.data?.orders)
            Setshowaddress(true)
        })
    }
    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * perPage;
        setOffset(offset)
    };

    return (<>
        {showprotectedpage === true &&
            <DBOP>
                <div className="inner-div">
                    <div className="mainorder-detail-sec">
                        <div className="container address_class_responsive">
                            <Head>
                                <title>Address list</title>
                            </Head>
                            <div className="row addres-page dashboard_main_div">
                                <Leftnav customerCheck={customerCheck} />
                                <div className="col-md-10">
                                    <MobileLeftNav customerCheck={customerCheck} />
                                    <div className="container order-detail-page main_address_page_listing">
                                        {showaddress === true ?
                                            <AddressData addressDetails={addressDetails}
                                                setaddressDetails={setaddressDetails}
                                                Setshowaddress={Setshowaddress}
                                                setaddressData={setaddressData}
                                                logintoken={logintoken}
                                                setInnerLoader={setInnerLoader} />
                                            :
                                            <>
                                                <div className="heading-top-main right-address">
                                                    <div className="heading-main-text">
                                                        <h1>Addresses List</h1>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="button-top">
                                                        <button className="add-address" onClick={() => { Setshowaddress(true) }}>Add <FaPlus /></button>
                                                    </div>
                                                </div>
                                                {/* <div className="container"> */}
                                                <div className="add-address-sec ">
                                                    <div className="order_table address-list order-list ">
                                                        <table className="addressTable-table">
                                                            <thead >
                                                                <th>Addresses</th>
                                                                <th>Actions</th>
                                                            </thead>
                                                            <tbody>
                                                                {addressData?.length > 0 && addressData?.map((address, index) => {

                                                                    return (
                                                                        <tr key={index + 1} className={index % 2 !== 0 ? "odd-tr" : "even-tr"}>

                                                                            <td data-value="Addresses">
                                                                                {address?.first_name + " " + address?.last_name + ", " + `${address?.company_name ? address?.company_name + ", " : ""}` + "" + address?.street_address_1 + ", " + `${address?.street_address_2 ? address?.street_address_2 + "," : ""}` + " " + address?.city + ", " + address?.state + ", " + address?.postal_code + ", " + address?.country + ", " + address?.phone_number}
                                                                            </td>

                                                                            <td data-value="Actions">
                                                                                <div className="edit_address">
                                                                                    <FaEdit onClick={(e) => { EditAddress(address?.id) }} />
                                                                                    <button type="button" value={address?.id} className="btn btn-primary">
                                                                                        <FaTrash onClick={(e) => { deleteAddress(e) }} />
                                                                                    </button>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    )
                                                                })}
                                                                <tr className='not-found'>
                                                                    <td colSpan="2" className="not_found_class"><span >{loading}</span></td>
                                                                </tr>
                                                                <tr className='not-found'>
                                                                    <td colSpan="2" className="not_found_class"><span >{noData}</span></td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                {/* </div> */}
                                                {addressData?.lenght > 0 &&
                                                    <ReactpaginateComp pageCount={pageCount} handlePageClick={handlePageClick} />
                                                }
                                                {deleteNotify && <span className="error deletemsg" style={{ color: 'green', fontSize: "16px", display: "flex", justifyContent: "center" }}>{deleteNotify}</span>}
                                            </>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </DBOP>
        }
        <SvgAnimationBackground />
    </>
    )
}
export default UserAddress