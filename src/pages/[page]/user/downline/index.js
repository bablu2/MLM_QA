import Head from "next/head";
import { useEffect, useState } from "react";
import Leftnav from "@Components/LeftNav";
import api from '../../../../api/Apis'
import React from 'react';
import ListView from "../../../Components/Downline/ListView";
import TreeViewCustom from "../../../Components/Downline/TreeViewCustom";
import TreeViewCustomHori from "../../../Components/Downline/TreeViewCustomHori";
import { useRouter } from "next/router";
import DBOP from "@Components/Common/DBOP";
import MobileLeftNav from "@Components/LeftNav/MobileLeftNav";
import DownlinePageStyle from "@PagesComponent/Downline/Downline.style";
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";

const Downline = ({ validateauth, showprotectedpage, customerCheck, setshowloader, setInnerLoader
}) => {
  const [state, setState] = useState([])
  const [viewtype, setViewtype] = useState('list')
  const router = useRouter()
  useEffect(() => {
    document.body.classList.add('dashboard');
    setInnerLoader(true)

    if (Object.keys(router?.query).length > 1) {
      router.push('/us/404/')
    }
    validateauth()
    const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
    api.getDownlineUsers(token).then(res => {
      setState(res?.data);
      setInnerLoader(false)

    })
  }, []);

  let count_dats = []

  const convert = (names) => {
    const res = {};
    names?.forEach((obj) => {
      const key = `${obj.parent}`;
      if (!res[key]) {
        res[key] = { ...obj, count: 0 };
      };
      res[key].count += 1;
    });
    return Object.values(res);
  };

  for (let i = 2; i <= 4; i++) {
    const names = state[`level${i}`]
    let result = convert(names)
    result.map((filterdata) => {
      count_dats.push({ "parant_id": filterdata.parent, "count": filterdata.count })

    })
  }

  let Downline_data = {
    name: state && `${state[`level0`]?.user?.first_name} ${state[`level0`]?.user?.last_name}`,
    id: state[`level0`]?.id,
    count: state && state[`level1`]?.length,
    sectionClass: "main_parent",
    attributes: {
      email: state[`level0`]?.user.email,
      user_profile: state[`level0`]?.user.user_profile,

      date_joined: state[`level0`]?.user.date_joined,
      orders_count: state[`level0`]?.user.orders_count,
      total_sales: state[`level0`]?.user.total_sales,
      show_email: state[`level0`]?.user.show_email,

      show_phone_no: state[`level0`]?.user.show_phone_no,

      user_rank: state[`level0`]?.user.user_rank,
    },
    children: [],
  }

  function pushdownlinedata(data_downline, child, classNames = "children") {
    const objIndex = count_dats?.findIndex((obj => Number(obj.parant_id) == Number(child?.id)));
    data_downline?.push({
      name: `${child.user.first_name} ${child.user.last_name}`,
      id: child?.id,
      count: count_dats[objIndex]?.count,
      attributes: {
        email: child.user.email,
        user_profile: child.user.user_profile,

        date_joined: child.user.date_joined,
        orders_count: child.user.orders_count,
        total_sales: child.user.total_sales,
        user_rank: child.user.user_rank,
        show_email: child.user.show_email,

        show_phone_no: child.user.show_phone_no,
        public_phone_no: child.user.public_phone_no

      },
      children: [],
    })
  }
  for (let i = 1; i < 16; i++) {
    state[`level${i}`]?.map((child, index) => {
      if (i === 1) {
        if (+Downline_data.id === +child?.parent) {
          pushdownlinedata(Downline_data?.children, child)
        }
      }
      else {
        Downline_data?.children?.map((child_in_child, index1) => {
          if (+child?.parent === +child_in_child?.id) {
            pushdownlinedata(Downline_data?.children[index1]?.children, child)
          }
          Downline_data?.children[index1]?.children.map((child_in_child1, index2) => {
            if (+child_in_child1?.id === +child?.parent) {
              pushdownlinedata(Downline_data?.children[index1]?.children[index2]?.children, child)

            }
            Downline_data?.children[index1]?.children[index2]?.children.map((child_in_child2, index3) => {
              if (+child_in_child2?.id === +child?.parent) {
                pushdownlinedata(Downline_data?.children[index1]?.children[index2]?.children[index3]?.children, child)

              }
            })
          })
        })
      }
    })
  }


  return (<>
    <Head>
      <title>Downline</title>
    </Head>
    {showprotectedpage === true ?
      <DBOP>
        <div className="mainorder-detail-sec">
          <div className="container ">
            <div className="row">
              <div className="dashboard_main_div main_class_responsive">
                <Leftnav customerCheck={customerCheck} />
                <div className="col-md-10 dashboard-main">
                  <MobileLeftNav customerCheck={customerCheck} />
                  <div className="container order-detail-page">
                    <div className="heading-top-main right-address">
                      <div className="heading-main-text">
                        <h1>Downline</h1>
                      </div>
                    </div>
                    <DownlinePageStyle>
                      <div className="groups tabs downline_custom_style">
                        <ul className="tabs">
                          <li
                            className={`col-md-6 ${viewtype === 'list' ? 'active' : ''}`}
                            onClick={() => {
                              setViewtype('list')
                            }}

                          >
                            <button>List View</button></li>
                          <li
                            className={`col-md-6 ${viewtype === 'tree' ? 'active' : ''}`}

                            onClick={() => {
                              setViewtype('tree')
                            }}
                          ><button>Tree View</button></li>
                        </ul>
                        {viewtype === 'tree' ?

                          <TreeViewCustom Downline_data={Downline_data} />
                          // <TreeViewCustomHori

                          //   Downline_data={Downline_data}

                          // />
                          :
                          <div className="lavel-box">
                            {state &&
                              <ListView state={state} setState={() => { setState }} />
                            }
                          </div>
                        }
                      </div>
                    </DownlinePageStyle>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </DBOP>
      :
      <div className="empty_div_section"></div>
    }
    <SvgAnimationBackground />
  </>)
}
export default Downline;