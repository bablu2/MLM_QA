import Head from "next/head"
import { useEffect, useState } from "react";
import api from '../../../../../api/Apis'
import ReactPaginate from 'react-paginate';
import { useRouter } from "next/router";
import Link from "next/link";
import Leftnav from "@Components/LeftNav";
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/core';
import React from 'react';
import { useForm } from "react-hook-form";
import { Dropdown } from 'semantic-ui-react'
import ProductDetails from "../../../../Components/orders/Create/ProductDetails";
import VariantDetails from "../../../../Components/orders/Create/VariantDetails";
import { toast } from 'react-toastify';
import Router from 'next/router';
import MobileLeftNav from "@Components/LeftNav/MobileLeftNav";
import DBOP from "@Components/Common/DBOP";
import CreateFormStyle from "src/pages/Components/orders/Create/CreateStyleComp";

const createOrder = ({ setshowloader, validateauth, showprotectedpage, customerCheck, setInnerLoader }) => {
    const [downlineuser, SetDownlineUser] = useState()
    const [LoginToken, setLoginToken] = useState()

    const { register, handleSubmit, watch, errors, getValues } = useForm();
    const [downlineselecteduserData, SetDownlineSelectedUserData] = useState()
    const [Address, SetAddress] = useState()
    const [billingAddressID, setBillingAddressID] = useState()
    const [shippingAddressID, setshippingAddressID] = useState()
    const [showproducts, addtolistshow] = useState(false)
    const [dataall, setdataall] = useState([])

    const [productdata, setProductData] = useState([])

    const [isauroship, setisautoship] = useState(false)
    const [orderCreated, setOrderCreated] = useState(false)

    const [billingerror, setBillingError] = useState()
    const [shipingadderror, setShipingError] = useState()
    const [userError, Setusererror] = useState()
    const [Setproducterror, setProductError] = useState()



    const router = useRouter()


    useEffect(() => {
        validateauth()

        if (Object.keys(router?.query).length > 1) {
            router.push('/us/404/')
        }
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLoginToken(token)
        api.getNewDownlineUsers(token).then(res => {
            SetDownlineUser(res?.data);
        })

        api.getAllProduct('us').then(res => {
            setdataall(res?.data?.products)
        })
    }, []);

    const setuserdata = (e, data) => {
        SetDownlineSelectedUserData(data)
        api.getDownlineUserAddress(LoginToken, data?.user_id).then(res => {
            SetAddress(res?.data?.addresses);
        })
    }



    let Product_data = []
    {
        productdata ?
            Product_data = productdata
            :
            Product_data = []

    }
    const addtolist = (e, data) => {
        const data_add = data?.value[0]?.split(',')
        let objIndex = ""
        if (+data_add[1] !== NaN) {
            objIndex = Product_data.findIndex((obj => obj.product_id == +data_add[0]));

        }
        else {
            objIndex = Product_data.findIndex((obj => obj.variant_id == +data_add[1]));

        }
        if (objIndex >= 0) {
            Product_data[objIndex].quantity = Product_data[objIndex].quantity + 1
        }
        else {
            const update_data = {
                product_id: +data_add[0],
                variant_id: +data_add[1],
                quantity: 1,
                Product_name: data_add[2],
                total_quantity: data_add[3],
                cost: data_add[4],
                autoShipCost: data_add[5],
                is_autoship: false,
                bonus_value: data_add[6]



            }
            Product_data.push(update_data)
        }
        addtolistshow(false)


        setProductData(Product_data)

    }


    const ProductAutoship = (e, product_id, variant_id) => {
        isauroship === true ? setisautoship(false) : setisautoship(true)

        let objIndex = ""
        if (e.target?.checked === true) {

            if (variant_id) {
                objIndex = Product_data.findIndex((obj => obj.variant_id == +variant_id));
                Product_data[objIndex].is_autoship = true


            }
            else {
                objIndex = Product_data.findIndex((obj => obj.product_id == +product_id));
                Product_data[objIndex].is_autoship = true
            }
            setProductData(Product_data)

        }
        else {


            if (variant_id) {

                objIndex = Product_data.findIndex((obj => obj.variant_id == +variant_id));
                Product_data[objIndex].is_autoship = false

            }
            else {
                objIndex = Product_data.findIndex((obj => obj.product_id == +product_id));
                Product_data[objIndex].is_autoship = false

            }
            setProductData(Product_data)


        }

    }


    const Sub = (e) => {

        isauroship === true ? setisautoship(false) : setisautoship(true)
        let product_quantity = e.currentTarget.getAttribute('data-qty')
        let product_id = e.currentTarget.getAttribute('data-product_id')
        let varient_id = e.currentTarget.getAttribute('data-variant_id')
        let objIndex = ''
        if (product_quantity > 1) {
            if (varient_id == NaN) {
                objIndex = Product_data?.findIndex((obj => obj.variant_id == +varient_id));
                Product_data[objIndex].quantity = Product_data[objIndex]?.quantity - 1
            }
            else {
                objIndex = Product_data.findIndex((obj => obj.product_id == +product_id));
                Product_data[objIndex].quantity = Product_data[objIndex].quantity - 1
            }
        }

        setProductData(Product_data)

    }

    const Add = (e) => {
        isauroship === true ? setisautoship(false) : setisautoship(true)
        let product_quantity = e.currentTarget.getAttribute('data-qty')
        let product_id = e.currentTarget.getAttribute('data-product_id')
        let varient_id = e.currentTarget.getAttribute('data-variant_id')
        let max_qty = e.currentTarget.getAttribute('data-max_qty')
        let objIndex = ''
        if (product_quantity > 0 && +product_quantity < +max_qty) {
            if (varient_id == NaN) {
                objIndex = Product_data?.findIndex((obj => obj.variant_id == +varient_id));
                Product_data[objIndex].quantity = Product_data[objIndex]?.quantity + 1
            }
            else {
                objIndex = Product_data.findIndex((obj => obj.product_id == +product_id));
                Product_data[objIndex].quantity = Product_data[objIndex].quantity + 1
            }
        }
        setProductData(Product_data);
    }




    // userlist dropdown data
    let userList = [];
    for (let i = 1; i <= 4; i++) {
        downlineuser != undefined && downlineuser[`level${i}`]?.length > 0 && downlineuser[`level${i}`].map((users) => {
            userList.push({ "title": users?.user?.first_name, "last_name": users?.user?.last_name, "email": users?.user?.email, "user_id": users?.id, "level": users?.level })
        })
    }

    // AddressLists dropdown data
    let AddressLists1 = [];
    Address && Address.map((addresslist) => {
        AddressLists1.push({
            "title": `${addresslist?.street_address_1},${addresslist?.company_name || ""},${addresslist?.city}`, "address_id": addresslist?.id
        })
    })



    // display product in dropdown
    let stateOptions = []
    dataall?.map((state) => {
        if (state.variants?.length == 0) {
            stateOptions.push(
                {
                    key: state.id,
                    text: state.name,
                    value: `${state.id},${null},${state?.name},${state?.quantity},${state?.cost_price},${state?.autoship_cost_price},${state?.bonus_value}`,
                    image: { avatar: true, src: `${process.env.API_URL}/${state?.product_images[0]?.image}` },
                })
        }
        else {
            state.variants.map((variantss) => {
                stateOptions.push(
                    {
                        key: variantss.id,
                        text: `${state.name} - ${variantss?.name} `,
                        value: `${state.id},${+variantss.id},${state.name}(${variantss?.name}),${variantss?.quantity},${variantss?.cost_price},${variantss?.autoship_cost_price},${state?.bonus_value}`,
                        image: { avatar: true, src: `${process.env.API_URL}/${variantss?.product_variant_images[0]?.image}` },

                    })
            })
        }
    })


    const DeleteProduct = (e, product_id, variant_id) => {
        isauroship === true ? setisautoship(false) : setisautoship(true)

        let objIndex = ""

        if (variant_id) {
            objIndex = Product_data.findIndex((obj => obj.variant_id == +variant_id));
            Product_data.splice(objIndex, 1);


        }
        else {
            objIndex = Product_data.findIndex((obj => obj.product_id == +product_id));
            Product_data.splice(objIndex, 1);

        }
        setProductData(Product_data)

    }


    const onSubmit = data => {

        let productId;
        let variant_id;
        let product_qty;
        let is_autoship;
        let total = 0;
        let totalBounus = 0;
        let products_data = [];
        let autoship = false
        productdata?.map((products) => {
            if (products?.is_autoship === true) {
                total = (products?.autoShipCost * products.quantity) + total;
                is_autoship = true
                autoship = true
                totalBounus = parseFloat(products?.bonus_value) + totalBounus

            }
            if (products?.is_autoship === false) {
                total = (products?.cost * products.quantity) + total;
                is_autoship = false
                autoship = false
                totalBounus = parseFloat(products?.bonus_value) + totalBounus

            }

            productId = products?.product_id;
            product_qty = products?.quantity;
            variant_id = products?.variant_id ? products.variant_id : '';
            products_data.push({ product_id: parseInt(productId), quantity: parseInt(product_qty), variant_id: parseInt(variant_id), is_autoship: is_autoship })
        })

        const orderdata = {
            "shipping_address_id": shippingAddressID,
            "billing_address_id": billingAddressID,
            "amount": parseFloat(total, 2),
            "tax_amount": 0,
            "shipping_amount": 0,
            "gross_total": parseFloat(total, 2),
            "coupon_id": null,
            "discount_amount": 0,
            "amount_paid": parseFloat(total, 2),
            "sku": "umb-20",
            "products": products_data,
            "is_autoship": autoship,
            "kaire_cash_used": '',
            "is_kaire_cash_used": false,
            "order_total_bv": totalBounus,
            "order_for": 'downline',
            "user_id": downlineselecteduserData?.user_id
        }

        let Submiterror1 = false
        let Submiterror2 = false
        let Submiterror3 = false
        let Submiterror4 = false

        if (billingAddressID === undefined) {
            setBillingError('Select billing address')
            Submiterror1 = true
        }
        else {
            setBillingError()
            Submiterror1 = false
        }

        if (shippingAddressID === undefined) {
            setShipingError('Select shipping address')
            Submiterror2 = true
        }
        else {
            setShipingError()
            Submiterror2 = false
        }

        if (downlineselecteduserData === undefined) {
            Setusererror('Select user')
            Submiterror3 = true
        }
        else {
            Setusererror()
            Submiterror3 = false
        }

        if (productdata.length === 0) {
            setProductError("Select atleast 1 product")
            Submiterror4 = true
        }
        else {
            setProductError()
            Submiterror4 = false
        }

        if (Submiterror1 === false && Submiterror2 === false && Submiterror3 === false && Submiterror4 === false) {
            setInnerLoader(true)
            api.createOrder(orderdata, LoginToken, 'downline').then(res => {
                if (res?.data?.code === 1) {
                    Product_data = ''
                    setProductData();
                    SetDownlineSelectedUserData();
                    setOrderCreated(false);
                    setInnerLoader(false)
                    localStorage.removeItem('PromoterPack')
                    localStorage.removeItem('packProduct')

                    // toast.success(res.data.message, { duration: 5 });
                }
                else if (res?.data?.code === 0) {
                    setInnerLoader(false)
                }
            })
        }

    }
    let total = 0;

    productdata?.map((products) => {
        if (products?.is_autoship === true) {
            total = (products?.autoShipCost * products.quantity) + total;

        }
        if (products?.is_autoship === false) {
            total = (products?.cost * products.quantity) + total;


        }


    })
    return (<>
        <Head>
            <title>Create-order</title>
        </Head>
        {showprotectedpage === true ?
            <DBOP>
                <div className="mainorder-detail-sec">
                    <div className="container">
                        <div className="row">
                            <div className="dashboard_main_div main_class_responsive">
                                <Leftnav customerCheck={customerCheck} />
                                <div className="col-md-10 dashboard-main">
                                    <MobileLeftNav customerCheck={customerCheck} />
                                    <div className="container order-detail-page">
                                        <div className="heading-top-main right-address">
                                            <div className="heading-main-text">
                                                <h1>Create Order</h1>
                                            </div>
                                        </div>
                                        {/* <CreateFormStyle></CreateFormStyle> */}
                                        <CreateFormStyle>
                                            <form className="Create-order-form" onSubmit={handleSubmit(onSubmit)}>
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <label htmlFor="exampleInputPassword1" className="form-label">User </label>
                                                            <Autocomplete
                                                                id="combo-box-demo1"
                                                                options={orderCreated === false ? userList : ''}
                                                                getOptionLabel={(option) => option.title}
                                                                style={{ width: 500 }}
                                                                onChange={setuserdata}

                                                                renderInput={(params) => <TextField {...params} label="Select user" variant="outlined" />}
                                                            />

                                                            {downlineselecteduserData &&
                                                                <div className="user-details-section">
                                                                    <p><strong>Name: </strong>{downlineselecteduserData?.title} {downlineselecteduserData?.last_name}
                                                                        <strong> Email: </strong>{downlineselecteduserData?.email}
                                                                        {/* <p>Contact Number : { }</p> */}
                                                                        <strong> Level: </strong>{downlineselecteduserData?.level}</p>
                                                                </div>
                                                            }
                                                        </div>
                                                        {userError && <span className="error_create">{userError}</span>}
                                                    </div>

                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <label htmlFor="exampleInputPassword1" className="form-label">Billing address </label>
                                                            <Autocomplete
                                                                id="combo-box-demo"
                                                                options={orderCreated === false ? AddressLists1 : ''}
                                                                getOptionLabel={(option) => option.title}
                                                                style={{ width: 500 }}

                                                                onChange={(event, value) => setBillingAddressID(value?.address_id)}

                                                                renderInput={(params) => <TextField {...params} label="Select address" variant="outlined" />}
                                                            />

                                                        </div>
                                                        {billingerror && <span className="error_create">{billingerror}</span>}
                                                    </div>

                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <label htmlFor="exampleInputPassword1" className="form-label">Shipping Address </label>
                                                            <Autocomplete
                                                                id="combo-box-demo"
                                                                options={orderCreated === false ? AddressLists1 : ''}
                                                                getOptionLabel={(option) => option.title}
                                                                style={{ width: 500 }}
                                                                onChange={(event, value) => setshippingAddressID(value?.address_id)}
                                                                renderInput={(params) => <TextField {...params} label="Select address" variant="outlined" />}
                                                            />
                                                        </div>
                                                        {shipingadderror && <span className="error_create">{shipingadderror}</span>}
                                                    </div>
                                                </div>

                                                <button type="button" className="add-product-autoshiporder" onClick={() => { addtolistshow(true) }}>Add product</button>
                                                {showproducts === true &&
                                                    stateOptions.length >= 1 &&
                                                    <Dropdown
                                                        fluid
                                                        multiple
                                                        placeholder='State'
                                                        search
                                                        selection
                                                        onChange={addtolist}
                                                        options={stateOptions}

                                                    />
                                                }
                                                {Setproducterror && <span className="error_create_product">{Setproducterror}</span>}
                                                <table className="product-sec">
                                                    <thead></thead>
                                                    <tbody>
                                                        {productdata?.map((productdatas) => {
                                                            return (
                                                                <tr className="">
                                                                    {productdatas?.variant_id !== null ?
                                                                        <VariantDetails DeleteProduct={DeleteProduct} isauroship={isauroship} ProductAutoship={ProductAutoship} Sub={Sub} Add={Add} productdata={productdatas} />
                                                                        :
                                                                        <ProductDetails DeleteProduct={DeleteProduct} isauroship={isauroship} ProductAutoship={ProductAutoship} Sub={Sub} Add={Add} productdata={productdatas} />

                                                                    }
                                                                </tr>
                                                            )
                                                        })}
                                                        <tr className="total_value"><td>Sub Total :</td> <td>{total}</td></tr>
                                                        <tr><td>Total:</td><td>{total}</td></tr>
                                                    </tbody>
                                                </table>
                                                <button type="submit">Create Downline Order</button>
                                            </form>
                                        </CreateFormStyle>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </DBOP>
            :
            <div className="empty_div_section"></div>
        }
    </>)
}
export default createOrder;