import Head from "next/head"
import { useEffect, useState } from "react";
import api from '../../../../api/Apis'
import { useRouter } from "next/router";
import Link from "next/link";
import Leftnav from "@Components/LeftNav/index.jsx";
import { FaEye } from "react-icons/fa";
import { toast } from 'react-toastify';
import NProgress from 'nprogress'
import RefundOrder from "../../../Components/orders/RefundOrder";
import Reorder from "../../../Components/orders/Reorder";
import ViewOrderDetails from "../../../Components/orders/ViewOrderDetails";
import moment from "moment"
import HoverEye from "../../../Components/orders/hovercomponent";
import OrderStyle from "./OrderStyle";
import DBOP from "../../../../Components/Common/DBOP";
import ReactpaginateComp from "@Components/Common/ReactPaginateComp";
import DateFormat from "@Components/Common/DateFormet";
import MobileLeftNav from "@Components/LeftNav/MobileLeftNav";
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";

const Order = ({ validateauth, showprotectedpage, customerCheck, setshowloader, setInnerLoader }) => {

    const [orderData, setOrderData] = useState()
    const [logintoken, setlogintoken] = useState()
    // const [reload, setreload] = useState(false)
    const [orderid, setorderid] = useState()
    const [show, setShowreorder] = useState(false)
    const [showdetailsorder, setshowdetailsorder] = useState(false)
    const [loading, setLoading] = useState('')
    const [noData, setNoData] = useState('')
    const [orderTotal, setOrderTotal] = useState()
    //for pagenation
    const [offset, setOffset] = useState(1);
    const [pageCount, setPageCount] = useState()
    const router = useRouter()
    useEffect(() => {
        document.body.classList.add('dashboard');
        setLoading("Loading...")
        setInnerLoader(true)
        validateauth()
        if (Object.keys(router?.query).length > 1) {
            router.push('/us/404/')
        }
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setlogintoken(token)
        paginateOrder(token)
    }, [orderid]);

    function paginateOrder(token, value = null) {
        api.getUserOrder(token, (value || offset)).then(res => {
            if (res?.status === 200) {
                setOrderData(res?.data)
                setOrderTotal(res?.data?.count)
                setPageCount(res?.data?.total_pages)
                setOffset(res?.data?.current_page)
                setInnerLoader(false)
                setLoading("")
                setNoData("")
            }

            else {
                setInnerLoader(false)
                setLoading("")
            }
            if (res?.data?.code === 0) {
                setNoData("No Data Found")
            }
        })
    }
    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offsetpage = selectedPage + 1;
        setOffset(offsetpage)
        paginateOrder(logintoken, offsetpage)
    };

    const Showreorder = (id) => {
        setShowreorder(true)
        setorderid(id)

    }

    return (<>
        <Head>
            <title>Order-list</title>
        </Head>
        {showprotectedpage === true ?
            <DBOP>

                <div className="mainorder-detail-sec main_order_section_class new_cart_order">
                    <div className="container">
                        <div className="row">
                            <div className="dashboard_main_div main_class_responsive">
                                <Leftnav customerCheck={customerCheck} />
                                <div className="col-md-10">
                                    <MobileLeftNav customerCheck={customerCheck} />
                                    <div className="container order-detail-page order-history">
                                        {(() => {
                                            if (showdetailsorder === true) {
                                                return <ViewOrderDetails orderid={orderid}
                                                    setorderid={setorderid}
                                                    customerCheck={customerCheck}
                                                    setshowdetailsorder={setshowdetailsorder}
                                                />
                                            } else if (show === true) {
                                                return <Reorder orderid={orderid} setShowreorder={setShowreorder} setorderid={setorderid} />
                                            } else {
                                                return <> <div className="heading-top-main right-address">
                                                    <div className="heading-main-text">
                                                        <h1>Order History</h1>
                                                    </div>
                                                </div>
                                                    <div className="order_table address-list order-list ">
                                                        <table className="commission-table">
                                                            <thead>
                                                                <tr>
                                                                    <th>ORDER ID</th>
                                                                    <th>ORDER DATE</th>
                                                                    <th>ORDER TOTAL</th>
                                                                    <th>ORDER TYPE</th>
                                                                    <th>STATUS</th>
                                                                    <th>ACTIONS</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {orderData?.data?.length > 0 && orderData?.data?.map((order_user, index) => {
                                                                    return (
                                                                        <tr key={index + 1} className={index % 2 !== 0 ? "odd-tr" : "even-tr"}>
                                                                            <td data-value="ORDER ID">{order_user?.public_order_id}</td>
                                                                            <td data-value="ORDER DATE"><DateFormat date={order_user?.created_at} /></td>
                                                                            <td data-value="ORDER TOTAL">${order_user?.amount_paid} USD</td>
                                                                            <td data-value="ORDER TYPE">{order_user?.is_autoship === true ? "Smartship" : "Normal"}</td>
                                                                            <td data-value="STATUS">{order_user?.status}</td>
                                                                            <td className="action-btn" data-value="ACTIONS">
                                                                                <div className="title12" onClick={(e) => {
                                                                                    setorderid(order_user?.id)
                                                                                    setshowdetailsorder(true);
                                                                                }}
                                                                                >
                                                                                    <button
                                                                                        setorderid={() => { setorderid(order_user?.id) }}
                                                                                        setshowdetailsorder={() => { setshowdetailsorder(true) }}>
                                                                                        View Order Details
                                                                                    </button>

                                                                                </div>
                                                                                {order_user?.is_autoship === "False" ?
                                                                                    <button onClick={() => { Showreorder(order_user?.id) }}>Reorder</button>
                                                                                    :
                                                                                    <button className="disable">Reorder</button>
                                                                                }
                                                                            </td>
                                                                        </tr>
                                                                    )
                                                                })}

                                                                <tr className="not-found">
                                                                    <td colSpan="4" className="not_found_class"><span >{loading}</span></td>
                                                                </tr>

                                                                <tr className="not-found">
                                                                    <td colSpan="4" className="not_found_class"><span >{noData}</span></td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    {
                                                        orderData?.data?.length > 0 &&
                                                        <ReactpaginateComp pageCount={pageCount} handlePageClick={handlePageClick} />
                                                    }
                                                </>
                                            }
                                        })()}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </DBOP>
            :
            <div className="empty_div_section"></div>
        }
        <SvgAnimationBackground />
    </>)
}
export default Order;