import styled from 'styled-components';

const OrderStyle = styled.div`
&
.main_order_section_class{
    .refund_history_class {
        margin-top: 25px;
        padding-left: 0;
    }
    .refund_button_class {
        background: #06356a; 
        border: 1px solid #06356a;
        color: #fff;
        padding: 15px 40px;
        font-size: 18px;
        line-height: 1;
        display: table;
        margin: 0 auto;
        text-transform: uppercase;
        font-weight: 600;
        letter-spacing: 0.5px;
        border-radius: 6px;
        margin-top: 17px;
        &.disabled {
            background: #c2bbbb !important;
            border: 2px solid #c2bbbb !important;
            &:hover{
                background: none;color: #c2bbbb;
            }
        }
    }  
}
`;
export default OrderStyle;