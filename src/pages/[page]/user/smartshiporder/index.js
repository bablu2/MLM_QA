import Head from "next/head"
import { useEffect, useRef, useState } from "react";
import api from '../../../../api/Apis'
import ReactPaginate from 'react-paginate';
import { useRouter } from "next/router";
import Link from "next/link";
import Leftnav from "@Components/LeftNav";
import AutoShipOrderDetails from "../../../Components/Autoship/AutoShipOrderDetails";
import Moment from 'react-moment';
import { moment } from 'moment';
import { toast } from 'react-toastify';
import { FaTrash, FaEdit } from "react-icons/fa";
import DBOP from "@Components/Common/DBOP";
import Pagination from '@material-ui/lab/Pagination';
import ReactpaginateComp from "@Components/Common/ReactPaginateComp";
import DateFormat from "@Components/Common/DateFormet";
import MobileLeftNav from "@Components/LeftNav/MobileLeftNav";
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";
import { Tooltip } from "@material-ui/core";
import DialogDeleteComponent from "@Components/Common/DialogDeleteComponent";

// import moment from "moment"

const AutoShipOrder = ({ validateauth, showprotectedpage, customerCheck, setInnerLoader }) => {
    const [orderData, setOrderData] = useState()
    //for pagenation
    const [offset, setOffset] = useState(1);
    const [pageCount, setPageCount] = useState()
    const [showdetails, setshowdetails] = useState()
    const [Logintoken, setLogintoken] = useState()
    const [openForDelete, setOpenForDelete] = useState({ open: false, id: null });
    const [deleteNotify, setDeleteNotify] = useState('')
    const [loading, setLoading] = useState('')
    const [noData, setNoData] = useState('')
    const [skipData, setSkipData] = useState('')
    const [getId, setId] = useState('')
    const router = useRouter()
    // load autohip page data
    useEffect(() => {
        document.body.classList.add('dashboard');
        setLoading("Loading...")
        setInnerLoader(true)
        validateauth()
        if (Object.keys(router?.query).length > 1) {
            // router.push('/us/404/')
            setshowdetails(router?.query?.id)
        }
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLogintoken(token)
        paginateData(token)
    }, [router?.query?.orderid]);

    function paginateData(token, value = null) {
        api.autoshipOrderHistory(token, (value || offset)).then(res => {
            if (res?.status === 200) {
                setOrderData(res?.data)
                setPageCount(res?.data?.total_pages)
                setOffset(res?.data?.current_page)
                setInnerLoader(false)
                setLoading("")
                setNoData("")
            }
            if (res?.data?.code === 0) {
                setInnerLoader(false)
                setNoData("No Data Found")
            }
        })
    }
    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offsetpage = selectedPage + 1;
        setOffset(offsetpage)
        paginateData(Logintoken, offsetpage)
    };
    // Skip order

    const skiporder = (id) => {
        const formDataD = { order_id: id };
        api.autoshipSkip(formDataD, Logintoken).then(res => {
            if (res?.data?.code === 1) {
                setSkipData('Order Skiped Successfully')
                setTimeout(() => {
                    setSkipData('')
                }, 2000);
                paginateData(Logintoken, offset)
            }
        })
    }
    // Delete order
    function DELETEORDER(id) {
        const formDataD = { order_id: getId };
        api.autoshipDelete(formDataD, Logintoken).then(res => {
            setId(formDataD?.order_id)
            if (res?.data?.code === 1) {
                setId('')
                if (orderData?.code === 0) {
                    setOrderData('')
                }
                setDeleteNotify('Deleted Successfully')
                setTimeout(() => {
                    setDeleteNotify('')
                }, 3000);
                setOpenForDelete({ open: false, id: null });
            }
            api.autoshipOrderHistory(Logintoken).then(res => {
                if (res?.status === 200) {
                    // const slice = res?.data?.orders?.slice(offset, offset + perPage)
                    // setPageCount(Math.ceil(res?.data?.orders?.length / perPage))
                    // setOrderData(slice)
                    setOrderData(res?.data)
                    setPageCount(res?.data?.total_pages)
                    setOffset(res?.data?.current_page)
                    setInnerLoader(false)
                    // setLoading("")
                    // setNoData("")
                }
                else {
                    setInnerLoader(false)
                }
            })
        })
    }

    return (
        <>
            <Head>
                <title>SmartShip Orders</title>
            </Head>
            {showprotectedpage === true &&
                <DBOP>
                    <div className="mainorder-detail-sec main_autoship_section_class SmrtShip_Orders">
                        <div className="container">
                            <div className="row">
                                <div className="dashboard_main_div main_class_responsive">

                                    <Leftnav customerCheck={customerCheck} />
                                    <MobileLeftNav customerCheck={customerCheck} />
                                    {showdetails !== undefined ?
                                        <AutoShipOrderDetails showdetails={showdetails}
                                            setshowdetails={setshowdetails}
                                            orderData={orderData}
                                            offset={offset}
                                            paginateData={(value) => paginateData(value)}
                                            setOrderData={setOrderData}
                                        />
                                        :
                                        <div className="col-md-10">
                                            <div className="container order-detail-page">
                                                <div className="heading-top-main right-address">
                                                    <div className="heading-main-text">
                                                        <h1>SmartShip Orders</h1>
                                                    </div>
                                                </div>
                                                <div className="order_table address-list order-list  auto-ship-order-table smartship">
                                                    <table className="commission-table">
                                                        <thead>
                                                            <tr>
                                                                <th>PRODUCT IMAGE</th>
                                                                <th>NAME</th>
                                                                <th>ORDER DATE</th>
                                                                <th>UPCOMING DATE</th>
                                                                <th>ACTIONS</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {orderData?.data?.length > 0 && orderData?.data?.map((order_user, index) => {
                                                                const moment = require('moment')
                                                                let startDate = moment(moment().toDate());
                                                                let endDate = moment(order_user?.next_shipping_date);
                                                                // Function call 
                                                                let datePending = Math.abs(endDate.diff(startDate, 'days'))
                                                                return (
                                                                    <tr key={index + 1} className={index % 2 !== 0 ? "odd-tr" : "even-tr"}>
                                                                        <td data-value="PRODUCT IMAGE">  <img src={`${process.env.API_URL}${order_user?.order?.autoship_order_details[0]?.product?.product_images[0]?.image}`} height="60" width="40" /></td>
                                                                        <td data-value="NAME">{order_user?.order?.autoship_order_details[0]?.product?.name}</td>
                                                                        <td data-value="ORDER DATE"><DateFormat date={order_user?.created_at} /></td>
                                                                        <td data-value="UPCOMING DATE"><DateFormat date={order_user?.next_shipping_date} /></td>

                                                                        <td className="autoship-action" data-value="ACTIONS">
                                                                            <Tooltip title="Edit">
                                                                                <div className="Edit">
                                                                                    <FaEdit onClick={() => { setshowdetails(order_user?.order?.id) }} style={{ color: '#1c4576' }} />
                                                                                </div>
                                                                            </Tooltip>
                                                                            {datePending < 5 ?
                                                                                <button><svg height="48" viewBox="0 0 48 48" width="48" xmlns="http://www.w3.org/2000/svg"><path d="M12 36l17-12-17-12v24zm20-24v24h4V12h-4z" /><path d="M0 0h48v48H0z" fill="none" /></svg></button>
                                                                                :
                                                                                <Tooltip title="Skip">
                                                                                    <button onClick={() => { skiporder(order_user?.order?.id) }}> <svg height="48" viewBox="0 0 48 48" width="48" xmlns="http://www.w3.org/2000/svg"><path d="M12 36l17-12-17-12v24zm20-24v24h4V12h-4z" /><path d="M0 0h48v48H0z" fill="none" /></svg>
                                                                                    </button>
                                                                                </Tooltip>
                                                                            }
                                                                            <Tooltip title="Delete">
                                                                                <div className="Delete">
                                                                                    <FaTrash onClick={() => {
                                                                                        setOpenForDelete({ open: true, id: order_user?.order?.id })
                                                                                        setId(order_user?.order?.id)
                                                                                    }} />
                                                                                </div>
                                                                            </Tooltip>

                                                                        </td>
                                                                    </tr>
                                                                )
                                                            })}
                                                            <tr className="not-found">
                                                                <td colSpan="5" className="not_found_class"><span>{noData}</span></td>
                                                            </tr>

                                                            <tr className="not-found">
                                                                <td colSpan="5" className="not_found_class"><span>{loading}</span></td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                                {orderData?.data?.length > 0 &&
                                                    <ReactpaginateComp pageCount={pageCount} handlePageClick={handlePageClick} />
                                                }

                                                {deleteNotify && <span className="error deletemsg" style={{ color: 'green', fontSize: "16px", display: "block", width: '100%', textAlign: "center" }}>{deleteNotify}</span>}

                                                {skipData && <span className="error deletemsg" style={{ color: 'green', fontSize: "16px", display: "block", width: '100%', textAlign: "center" }}>{skipData}</span>}
                                            </div>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </DBOP>
            }
            <SvgAnimationBackground />
            <DialogDeleteComponent
                openForDelete={openForDelete?.open}
                setOpenForDelete={setOpenForDelete}
                deleteConfirm={(value) => DELETEORDER(value)}
                title="Delete" />
        </>
    )
}
export default AutoShipOrder;