import api from '@api/Apis'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import DateFormat from "@Components/Common/DateFormet";
import DialogComponent from '@Components/Common/DialogComponent';
import { DialogSectionCss } from '@PagesComponent/ActiveSmartShip/ActiveStyleComp';

const Smartshipforcustomer = ({ validateauth, customerCheck, setInnerLoader }) => {
    const [orderData, setOrderData] = useState()
    const [Logintoken, setLogintoken] = useState()
    const [showdetails, setshowdetails] = useState()
    const [nomMsg, setNoMsg] = useState('')
    const router = useRouter()

    useEffect(() => {
        setInnerLoader(true)
        validateauth()
        if (Object.keys(router?.query).length > 1) {
            router.push('/us/404/')
        }
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLogintoken(token)
        if (router?.query?.orderid) {
            setshowdetails(router?.query?.orderid)
        }
        if (customerCheck < 3) {
            api.autoshipOrderHistory(token).then(res => {
                if (res?.data?.code === 1) {
                    setOrderData(res?.data)
                    setInnerLoader(false)
                    setNoMsg('')
                }
                else {
                    setNoMsg("No Data Found")
                    setInnerLoader(false)

                }
            })
        }

    }, [router?.query?.orderid]);

    function Data(id) {
        if (id) {
            router.push(`/us/user/smartshiporder/?id=${id}`)
        }
    }
    return (
        <div>
            {
                customerCheck < 3 &&
                <div className="customer_smartship" style={{ width: "100%" }}>
                    <div className="container order-detail-page">
                        <div className="heading-top-main right-address">
                            <div className="heading-main-text">
                                <h1>SmartShip Orders</h1>
                            </div>
                        </div>
                        <div className="order_table address-list order-list  auto-ship-order-table">
                            <table className="commission-table">
                                <thead>
                                    <tr>
                                        <th>ORDER ID</th>
                                        <th>ORDER DATE</th>
                                        <th>UPCOMING DATE</th>
                                        <th>ACTIONS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        orderData?.orders?.map((items) => {
                                            return (
                                                <tr>
                                                    <td data-value="ORDER ID"> {items?.order?.public_order_id} </td>
                                                    <td data-value="ORDER DATE">{items?.created_at}</td>
                                                    <td data-value="UPCOMING DATE">{items?.next_shipping_date}</td>
                                                    <td data-value="ACTIONS"> <button onClick={() => Data(items?.order?.id)}>view</button></td>
                                                    { /* <td> <a href="/us/user/smartshiporder/?items?.order?.id"> View Details</a></td>*/}
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>


                            </table>
                            {nomMsg && <p className="error no-data" style={{ color: 'black', textAlign: 'center', fontSize: '16px', position: 'unset', margin: '0 auto' }}>{nomMsg}</p>}
                        </div>
                    </div>

                </div>
            }
        </div>
    )
}

export default Smartshipforcustomer