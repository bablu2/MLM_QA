import api from '@api/Apis'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import DateFormat from "@Components/Common/DateFormet";
import DialogComponent from '@Components/Common/DialogComponent';
import { DialogSectionCss } from '@PagesComponent/ActiveSmartShip/ActiveStyleComp';

const LatestOrders = ({ validateauth, customerCheck, setInnerLoader }) => {
    const [orderData, setOrderData] = useState()
    const [Logintoken, setLogintoken] = useState()
    const [showdetails, setshowdetails] = useState()
    const [open, setOpen] = useState()
    const [Item, setItem] = useState()
    const [nomMsg, setNoMsg] = useState('')
    const router = useRouter()

    useEffect(() => {
        setInnerLoader(true)
        validateauth()
        if (Object.keys(router?.query).length > 1) {
            router.push('/us/404/')
        }
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLogintoken(token)
        if (router?.query?.orderid) {
            setshowdetails(router?.query?.orderid)
        }

        api.upComingOrders(token).then(res => {
            if (res?.data?.code === 1) {
                setOrderData(res?.data)
                setInnerLoader(false);
                setNoMsg('')
            }
            if (res?.data?.code === 0) {
                setNoMsg('No Data Found')
                setInnerLoader(false)

            }
        })


    }, [router?.query?.orderid]);


    function findData(items) {
        setItem(items);
        setOpen(true)
    }

    return (
        <div>
            {customerCheck < 3 &&
                <div className="customer_smartship" style={{ width: "100%" }}>
                    <div className="container order-detail-page">
                        <div className="heading-top-main right-address">
                            <div className="heading-main-text">
                                <h1>Latest Orders</h1>
                            </div>
                        </div>
                        <div className="order_table address-list order-list  auto-ship-order-table ">
                            <table className="commission-table">
                                <thead>
                                    <tr>
                                        <th>ORDER ID</th>
                                        <th>ORDER DATE</th>
                                        <th>ACTIONS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        orderData?.orders?.map((items) => {
                                            return (
                                                <tr>
                                                    <td data-value="ORDER ID"> {items?.public_order_id} </td>
                                                    <td data-value="ORDER DATE">{items?.created_at}</td>
                                                    <td data-value="ACTIONS"><button onClick={() => findData(items)}>View Details</button></td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>

                            </table>
                            {nomMsg && <p className="error no-data" style={{ color: 'black', textAlign: 'center', fontSize: '16px', position: 'unset', margin: '0 auto' }}>{nomMsg}</p>}

                            <DialogComponent opend={open} handleClose={() => setOpen(false)} title="Orders Detail" classFor="order_details"
                            >
                                <DialogSectionCss>
                                    <table>
                                        <thead>
                                            <tr>
                                                <div className="address_ship" style={{ display: 'flex' }}>
                                                    <div className="billing_details">
                                                        <h3>Billing Details </h3>
                                                        <div>
                                                            {Item?.billing_address?.first_name} {Item?.billing_address?.last_name}, {`${Item?.billing_address?.company_name ? Item?.billing_address?.company_name : ""}`} {Item?.billing_address?.street_address_1}, {`${Item?.billing_address?.street_address_2 ? Item?.billing_address?.street_address_2 : ""}`} {Item?.billing_address?.city}, {Item?.billing_address?.state}, {Item?.billing_address?.postal_code}, {Item?.billing_address?.country}, {Item?.billing_address?.phone_number}
                                                        </div>
                                                    </div>
                                                    <div className="shipping_details">
                                                        <h3>Shipping Details </h3>
                                                        <div>
                                                            {Item?.shipping_address?.first_name} {Item?.shipping_address?.last_name}, {`${Item?.shipping_address?.company_name ? Item?.shipping_address?.company_name : ""}`} {Item?.shipping_address?.street_address_1}, {`${Item?.shipping_address?.street_address_2 ? Item?.shipping_address?.street_address_2 : ""}`} {Item?.shipping_address?.city}, {Item?.shipping_address?.state}, {Item?.shipping_address?.postal_code}, {Item?.shipping_address?.country}, {Item?.shipping_address?.phone_number}
                                                        </div>
                                                    </div>
                                                </div>
                                            </tr>
                                        </thead>
                                    </table>

                                    <table>
                                        <thead>
                                            <tr>
                                                <th>PRODUCT NAME</th>
                                                <th>PRODUCT QUANTITY</th>
                                                <th>BV</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                Item?.order_details?.map((item, index) => {
                                                    return (
                                                        <tr key={index}>
                                                            <td>{item?.product_name}</td>
                                                            <td>{item?.product_quantity}</td>
                                                            <td>{item?.bonus_value}</td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>

                                    </table>
                                </DialogSectionCss>
                            </DialogComponent>

                        </div>
                    </div>

                </div>
            }
        </div>
    )
}

export default LatestOrders