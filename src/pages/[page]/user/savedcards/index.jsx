import React, { useEffect, useState } from "react";
import { ImagesForCard } from "@Components/Common/SaveCardSection";
import DBOP from "@Components/Common/DBOP";
import LeftNav from "@Components/LeftNav";
import Head from "next/head";
import api from "@api/Apis";
import { DeleteForever } from "@material-ui/icons";
import styled from "styled-components";
import { toast } from "react-toastify";
import MobileLeftNav from "@Components/LeftNav/MobileLeftNav";
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";
import { SaveCardComp } from "@Components/Common/PayCardSection";

export const SavedCardPageStyle = styled.div`
  && h2 {
    font-size: 25px;
  }
  .view_all {
    text-align: right;
    margin-right: 36px;
    font-size: 14px;
    font-weight: 600;
  }
  .main_class_responsive {
    .saved_card {
      max-width: 70%;
      flex: 0 0 70%;
    }
  }
  .address-list {
    margin: 0px 24px 15px;
    .saved_card_table {
      width: 100%;
      tr {
        th {
        }
        td {
          text-align: center;
          .edit_delete_option {
            font-size: 23px;
            color: red;
          }
        }
      }
    }
    .main_saved_comp {
      text-align: right;
      width: 100%;
      margin-left: auto;
      padding: 0px 10px;
      @media (max-width: 767px) {
        margin-top: 0;
      }
      button {
        border-radius: 30px;
        font-size: 14px;
        color: #fff;
        border: 2px solid #00356a;
        -webkit-transition: 0.3s ease all;
        transition: 0.3s ease all;
        padding: 10px 30px;
        font-weight: 700;
        margin-top: 0px;
        margin-right: 00px;
      }
    }
  }
`;

export default function SavedCardPage({
  customerCheck,
  showprotectedpage,
  getDetails,
  validateauth,
  setshowloader,
  setInnerLoader,
  ...props
}) {
  const [savedCardData, setSavedCardData] = useState([]);
  const [cardValue, setCardValue] = useState(null);
  const [deleteNotify, setDeleteNotify] = useState("");
  const [loading, setLoading] = useState("");
  const [noData, setNoData] = useState("");

  useEffect(() => {
    document.body.classList.add("dashboard");
    validateauth();
    const token = localStorage.getItem("Token")
      ? localStorage.getItem("Token")
      : "";
    if (!token) {
      route.push(`/${route.query.page}/login`);
    } else {
      savedCardFunction(token);
    }
  }, []);

  const savedCardFunction = async function (token) {
    setLoading("Processing");
    setInnerLoader(true);
    await api
      .getSavedCards(token)
      .then((res) => {
        if (res?.data?.code === 1) {
          setSavedCardData(res?.data?.saved_cards);
          setLoading("");
          setNoData("");
          setInnerLoader(false);

          const selectedData = _.find(res?.data?.saved_cards, {
            default_card: true,
          });
          if (selectedData) {
            setCardValue(selectedData?.id);
          }
        } else {
          setInnerLoader(false);
          setLoading("");
        }
        if (res?.data?.code === 0) {
          setNoData("No Data Found");
        }
      })
      .catch((error) => {
        console.log(error);
        setInnerLoader(false);
      });
  };
  const handleDeleteCard = async (deleteId = null) => {
    // setInnerLoader(true);
    let token = localStorage.getItem("Token");
    if (savedCardData?.length > 1) {
      await api.deleteCard(token, { card_id: deleteId })
        .then((res) => {
          if (res?.data?.code === 1) {
            setDeleteNotify("Card deleted successfully");
            setTimeout(() => {
              setDeleteNotify("");
            }, 3000);
            // setSavedCard(res?.data?.saved_cards);
            // toast.success(res?.data?.message);
            savedCardFunction(token);
            // setInnerLoader(false);
          } else {
            // setInnerLoader(false);
          }
        })
        .catch((error) => {
          console.log(error);
          // setInnerLoader(false);
        });
    }
  };

  //************New card save click ********************** */
  const Save = async (data = null) => {
    setInnerLoader(true);
    let token = localStorage.getItem("Token");
    let billingAddress = localStorage.getItem("billingAddress");
    let payload = {
      payment_details: data !== null ? data?.payment_details : null,
      billing_address_id: billingAddress?.length > 0 ? billingAddress : null,
      billing_details: data?.billing_details ? data?.billing_details : null,
    };
    const result = await api
      .saveNewCard(token, payload)
      .then((response) => {
        savedCardFunction(token);
        setInnerLoader(false);
        return response;
      })
      .catch((error) => {
        setInnerLoader(false);
        console.log(error);
      });
    return result;
  };
  //************Default card**************/
  const handleDefaultCard = async (id) => {
    // setInnerLoader(true);
    let token = localStorage.getItem("Token");
    let payload = {
      Token: token,
      default_card_id: id,
      old_default_id: cardValue ? cardValue : null,
    };
    const cardData = await api
      .defaultCard(token, payload)
      .then((res) => {
        if (res?.status === 200) {
          setCardValue(id);
          // setInnerLoader(false);
        } else {
          // setInnerLoader(false);
        }
      })
      .catch((err) => {
        console.log(err);
        // setInnerLoader(false);
      });
    return cardData;
  };
  return (
    <>
      <Head>
        <title>Saved Card</title>
      </Head>
      <DBOP>
        {showprotectedpage === true ? (
          <div className="cart-sec saved_card_page">
            <div className="container">
              <div className="row">
                <div className="dashboard_main_div main_class_responsive">
                  <LeftNav customerCheck={customerCheck} />
                  <div className="col-md-10 saved_card">
                    <MobileLeftNav customerCheck={customerCheck} />
                    <div className="container order-detail-page saved-card">
                      <div className="heading-top-main right-address">
                        <div className="heading-main-text">
                          <h1>Saved Card</h1>
                        </div>
                      </div>
                      <SavedCardPageStyle>
                        <div className="table_container address-list">
                          <SaveCardComp
                            compFunction={Save}
                            section="savedCard"
                            showContinue={null}
                          />
                          <div className="Table-container table-responsive">
                            <table className="Cart_product saved_card_table">
                              <thead className="">
                                <tr>
                                  <th syle={{ width: "10%" }}>Default card</th>
                                  <th>Card image</th>
                                  <th>Card number</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                {savedCardData?.length > 0 &&
                                  savedCardData?.map((value, index) => {
                                    return (
                                      <>
                                        <tr key={index + 1}>
                                          <td data-value="DEFAULT CARD">
                                            <div className="button">
                                              <input
                                                type="radio"
                                                style={{
                                                  margin: 0,
                                                }}
                                                value={value?.id}
                                                checked={
                                                  value?.id === cardValue
                                                }
                                                name="savedNewCard"
                                                onChange={() =>
                                                  handleDefaultCard(value?.id)
                                                }
                                              />
                                            </div>
                                          </td>
                                          <td data-value="CARD IMAGE">
                                            <div className="saved-new-card">
                                              <ImagesForCard
                                                type={value?.card_type}
                                              />
                                            </div>
                                          </td>
                                          <td data-value="CARD NUMBER">
                                            <div className="card number">
                                              <h5>{value?.card_number}</h5>
                                            </div>
                                          </td>
                                          <td data-value="ACTION">
                                            <div className="edit_delete_option">
                                              <DeleteForever
                                                fontSize="8rem"
                                                onClick={() =>
                                                  handleDeleteCard(value?.id)
                                                }
                                              />
                                            </div>
                                          </td>
                                        </tr>
                                      </>
                                    );
                                  })}
                                <tr className="not-found">
                                  <td className="not_found" colSpan="3">
                                    {loading}
                                  </td>
                                </tr>
                                <tr className="not-found">
                                  <td className="not_found" colSpan="3">
                                    {noData}
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </SavedCardPageStyle>
                      {deleteNotify && (
                        <span
                          className="error deletemsg"
                          style={{
                            color: "green",
                            fontSize: "16px",
                            display: "block",
                            width: "100%",
                            textAlign: "center",
                          }}
                        >
                          {deleteNotify}
                        </span>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="empty_div_section"></div>
        )}
      </DBOP>
      <SvgAnimationBackground />
    </>
  );
}
