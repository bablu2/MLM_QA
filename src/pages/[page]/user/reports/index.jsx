import React from "react";
import ReportsMain from "@PagesComponent/Reports/ReportsMain";
import CommonLayout from "@Components/Common/CommonLayout";

export default function Reports({ customerCheck, showprotectedpage, validateauth, ...props }) {

    return (
        <CommonLayout attributes={{
            title: "Business Info",
            section: "reports_page",
            showprotectedpage,
            customerCheck,
            validateauth,
        }}>
            <ReportsMain {...props} />
        </CommonLayout>
    );
}

