import { useEffect, useState } from "react";
import api from '../../../../api/Apis'
import React from 'react';
import { useRouter } from "next/router";
import _ from "lodash";
import ActiveSmartShipsTable from "../../../Components/ActiveSmartShip/ActiveSmartShipsTable";
import ReactpaginateComp from "@Components/Common/ReactPaginateComp";
import OuterTableStyle from "@PagesComponent/ActiveSmartShip/ActiveStyleComp";
import CommonLayout from "@Components/Common/CommonLayout";

const ActiveSmartShips = ({ showprotectedpage, validateauth, customerCheck, getDetails, setshowloader, setInnerLoader }) => {

    const [smartShip, setSmartShip] = useState([]);
    const router = useRouter()
    const [perPage] = useState(10);
    const [pageCount, setPageCount] = useState(0)
    const [data, setData] = useState([]);

    useEffect(() => {
        // setshowloader(true)
        // setInnerLoader(true)

        document.body.classList.add('dashboard');
        if (Object.keys(router?.query).length > 1) {
            router.push('/us/404/')
        }
        validateauth();
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        api.getActiveSmartships(token).then(res => {
            if (res?.status === 200) {
                let data = [];
                for (const [key, value] of Object.entries(res?.data)) {
                    let levelValue = key?.replace(/[^0-9.]/g, "");
                    _.map(value, (values) => {
                        if (values?.user?.autoship_user_order?.length > 0) {
                            values?.user?.autoship_user_order?.map((orders_value) => {
                                data = [...data, {
                                    ...values,
                                    level: +levelValue,
                                    order_id: orders_value?.order?.public_order_id,
                                    avatar: values?.user?.user_profile?.image,
                                    startDate: orders_value?.created_at,
                                    nexDate: orders_value?.next_shipping_date,
                                    bv: orders_value?.order?.order_bv === "None" ? "-" : orders_value?.order?.order_bv
                                }];
                            })
                        }
                    })
                }
                setSmartShip(data);
                setData(data?.slice(0, 10));
                setPageCount(Math.ceil(data?.length / perPage))
                // setshowloader(false)
                setInnerLoader(false)

            }
            else {
                // setshowloader(false)
                setInnerLoader(false)


            }
        })
    }, []);
    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * perPage;
        const slice = smartShip?.slice(offset, offset + perPage)
        setPageCount(Math.ceil(smartShip?.length / perPage))
        setData(slice)
    };
    return (
        <CommonLayout attributes={{
            title: "Downline Active SmartShip",
            section: "activeSmartship",
            showprotectedpage,
            customerCheck,
            validateauth,
        }}>
            <div className="row">
                <OuterTableStyle>
                    <ActiveSmartShipsTable smartShip={data} setshowloader={setshowloader} setInnerLoader={setInnerLoader} />
                    <ReactpaginateComp pageCount={pageCount} handlePageClick={handlePageClick} />
                </OuterTableStyle>
            </div>
        </CommonLayout>
    )
}
export default ActiveSmartShips;