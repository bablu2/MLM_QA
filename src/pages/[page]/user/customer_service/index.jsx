import CommonLayout from "@Components/Common/CommonLayout";
import CustomerService from "@PagesComponent/CustomerServices";
import React from "react";
import { useEffect } from "react";

const CustomerServices = ({ showprotectedpage, customerCheck, validateauth, ...props }) => {
    React.useEffect(() => {
        document.body.classList.add('dashboard');
    }, [])
    return (
        <CommonLayout attributes={{
            title: "Customer Service",
            section: "customer_service",
            showprotectedpage,
            customerCheck,

            validateauth,
        }}>
            <CustomerService />
        </CommonLayout>
    )
}
export default CustomerServices;