import Head from "next/head";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Leftnav from "@Components/LeftNav";
import api from '../../../../api/Apis'
import * as React from 'react';
import TextField from '@material-ui/core/TextField';
import DateRangePicker from '@material-ui/lab/DateRangePicker';
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import Moment from 'moment';
import Form from "../../../Components/Wallet/Form";
import FormData from "../../../Components/Wallet/FormData";
import FormDataKaieCash from "../../../Components/Wallet/FormDataKaieCash";
import { useRouter } from "next/router";
import moment from "moment"
import DateFormat from "@Components/Common/DateFormet";
import MobileLeftNav from "@Components/LeftNav/MobileLeftNav";
import FormStyleWallet, { FormDataStyleWallet, FormDataKaieCashStyle } from "src/pages/Components/Wallet/WallatStyleComp";
import DBOP from "@Components/Common/DBOP";
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";


const Commission = ({ validateauth, setshowloader, showprotectedpage, customerCheck, setInnerLoader }) => {
    const router = useRouter();

    const [LoginToken, setLoginToken] = useState()
    const { register, handleSubmit, errors } = useForm();
    const [value, setValue] = useState([null, null]);
    const [iscustomdate, IsCustomdate] = useState('month')

    const [offset, setOffset] = useState(0);
    const [offset1, setOffset1] = useState(0);

    const [data, setData] = useState();
    const [perPage] = useState(5);
    const [pageCount, setPageCount] = useState(0)
    const [pageCount1, setPageCount1] = useState(0)

    const [transactiondata, setTransactionData] = useState()
    const [transactiondataError, setTransactiondataError] = useState()
    const [filterdata, setfilterdata] = useState()
    const [viewtype, setViewtype] = useState('Commissions')

    const [transactiondatakaire, setTransactionDatakaire] = useState()
    const [transactiondataErrorkaire, setTransactiondataErrorkaire] = useState()
    const [currentPageCommision, setcurrentPageCommision] = useState(0)
    const [currentPagekaire, setcurrentPageKaire] = useState()
    useEffect(() => {
        validateauth()
        // if (router?.query?.logtype !== "kairecash") {
        //     if (router?.query?.logtype !== "Commissions") {
        //         if (Object.keys(router?.query).length > 1) {
        //             router.push('/us/404/')

        //         }
        //     }
        // }
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLoginToken(token)
        {
            // setInnerLoader(true)

            filterdata === undefined && viewtype === 'Commissions' ?
                api.getAllTranaction(token, 'month').then(res => {
                    getformatCommisiondate()
                    getformatCommisiondate(res)
                    setInnerLoader(false)

                })

                :
                api.getKaireCashTranaction(token, 'month').then(res => {

                    setInnerLoader(false)

                    getformatKaire()

                    getformatKaire(res)


                })

        }
    }, [offset, viewtype, router?.query?.logtype]);
    useEffect(() => {
        if (router?.query?.for === 'kaire') {
            setViewtype("kairecash");
        }
    }, [router?.query])

    const onSubmit = data => {
        setfilterdata(data)
        // setInnerLoader(true)

        if (iscustomdate === 'custom') {
            let daterange = convert(value);
            daterange = daterange.split(" - ");
            const from_date = Moment(daterange[0]).format('YYYY-MM-DD');
            const til_date = Moment(daterange[1]).format('YYYY-MM-DD');
            data.from_date = from_date
            data.til_date = til_date
        }
        {
            viewtype === 'kairecash' ?
                api.getKaireCashTranaction(LoginToken, data?.filter_by_date, data).then(res => {
                    getformatKaire()
                    setInnerLoader(false)

                    getformatKaire(res)

                })
                :

                api.getAllTranaction(LoginToken, data?.filter_by_date, data).then(res => {
                    setInnerLoader(false)

                    getformatCommisiondate(res)


                })

        }

    }

    const convert = dateRange =>
        dateRange.map(date => new Intl.DateTimeFormat('en-US').format(new Date(date)))
            .join(" - ")
    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        setcurrentPageCommision(selectedPage)
        const offset = selectedPage * perPage;
        if (filterdata !== undefined) {
            if (viewtype === 'kairecash')
                FilterData(filterdata, offset1)
            else
                FilterData(filterdata, offset)

        }
        if (viewtype === 'kairecash')
            setOffset1(offset)
        else
            setOffset(offset)
    };

    const handlePageClick1 = (e) => {
        const selectedPage = e.selected;
        setcurrentPageKaire(selectedPage)
        const offset = selectedPage * perPage;
        if (filterdata !== undefined) {
            if (viewtype === 'kairecash')
                FilterData(filterdata, offset1)
            else
                FilterData(filterdata, offset)

        }
        if (viewtype === 'kairecash')
            setOffset1(offset)
        else
            setOffset(offset)


    };
    const FilterData = (data, offset) => {
        setfilterdata(data)

        if (iscustomdate === 'custom') {
            let daterange = convert(value);
            daterange = daterange.split(" - ");
            const from_date = Moment(daterange[0]).format('YYYY-MM-DD');
            const til_date = Moment(daterange[1]).format('YYYY-MM-DD');
            data.from_date = from_date
            data.til_date = til_date
        }

        {
            viewtype === 'kairecash' ?
                api.getKaireCashTranaction(LoginToken, data?.filter_by_date, data).then(res => {
                    // getformatKaire()
                    getformatKaire(res)

                })
                :

                api.getAllTranaction(LoginToken, data?.filter_by_date, data).then(res => {
                    getformatCommisiondate()
                    getformatCommisiondate(res)

                })

        }

    }
    const getformatCommisiondate = (res) => {
        let oldDate = ''

        let tabledata = []
        res?.data?.transactions?.map((trans, index) => {
            let date = trans?.created_at;

            if (oldDate !== "" && oldDate === date) {
                const objIndex = tabledata?.findIndex((obj => obj.Date === date));
                if (objIndex >= 0) {
                    if (trans?.status !== "deduction") {
                        tabledata[objIndex].Commission_Amount += +trans?.commission_balance;
                    } else {
                        tabledata[objIndex].deduction_Amount += +trans?.commission_balance;
                    }
                    tabledata[objIndex].history.push({ date: date, OrderId: trans?.order, amount: +trans?.commission_balance, status: trans?.status })
                }
            } else {
                oldDate = date;
                tabledata.push({
                    "Available_Wallet_Balance": `$${trans?.available_wallet_balance}`,
                    "Commission_Amount": trans?.status === "deduction" ? 0 : +trans?.commission_balance,
                    "deduction_Amount": trans?.status === "deduction" ? +trans?.commission_balance : 0,
                    "Date": date,
                    "history": [
                        {
                            date: date, OrderId: trans?.order, amount: trans?.commission_balance, status: trans?.status
                        }
                    ],
                })
            }
        });
        const slice = tabledata?.slice(offset, offset + perPage)
        setPageCount(Math.ceil(tabledata?.length / perPage))
        setTransactionData(slice)
        if (res?.data?.code === 1) {
            setTransactiondataError(res?.data?.message)
        }
        if (res?.data?.code === 0) {
            setTransactiondataError()
        }

    }


    const getformatKaire = (res) => {
        if (res?.data?.transactions?.length > 0) {
            const slice = res?.data?.transactions?.slice(offset1, offset1 + perPage)
            setPageCount1(Math.ceil(res?.data?.transactions?.length / perPage))
            setTransactionDatakaire(slice)
        }
        if (res?.data?.code === 0) {
            setTransactiondataErrorkaire(res?.data?.message)
        }
        if (res?.data?.code === 1) {
            setTransactiondataErrorkaire()
        }
    }
    return (<>
        <Head>
            <title>Wallet-Transactions</title>
        </Head>
        {showprotectedpage === true ?
            <DBOP>
                <div className="transactions-detail-sec mainorder-detail-sec">
                    <div className="container">
                        <div className="row">
                            <div className="dashboard_main_div main_class_responsive">
                                <Leftnav customerCheck={customerCheck} />
                                <div className="col-md-10">
                                    <MobileLeftNav customerCheck={customerCheck} />
                                    <div className="container order-detail-page">
                                        <FormStyleWallet>
                                            <Form value={value}
                                                setValue={setValue}
                                                iscustomdate={iscustomdate}
                                                register={register}
                                                IsCustomdate={IsCustomdate}
                                                handleSubmit={handleSubmit}
                                                onSubmit={onSubmit} />
                                        </FormStyleWallet>
                                        <div className="groups tabs">
                                            <ul className="tabs">
                                                <li className={`col-md-6 ${viewtype === 'Commissions' ? 'active' : ''}`}
                                                    onClick={() => {
                                                        setViewtype('Commissions')
                                                    }}
                                                >
                                                    <button>Commissions</button></li>
                                                <li className={`col-md-6 ${viewtype === 'kairecash' ? 'active' : ''}`}
                                                    onClick={() => {
                                                        setViewtype('kairecash')
                                                    }}>
                                                    <button>Kaire Cash</button>
                                                </li>
                                            </ul>
                                            {viewtype === 'Commissions' ?
                                                <FormDataStyleWallet>
                                                    <div className="transactions-data-table">
                                                        <FormData currentPageCommision={currentPageCommision} transactiondataError={transactiondataError}
                                                            handlePageClick={handlePageClick} pageCount={pageCount} transactiondata={transactiondata} />
                                                    </div>
                                                </FormDataStyleWallet>
                                                :
                                                <FormDataKaieCashStyle>
                                                    <div className="transactions-data-table">
                                                        <FormDataKaieCash currentPagekaire={currentPagekaire}
                                                            transactiondataError={transactiondataErrorkaire}
                                                            handlePageClick1={handlePageClick1}
                                                            pageCount={pageCount1}
                                                            transactiondata={transactiondatakaire} />
                                                    </div>
                                                </FormDataKaieCashStyle>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </DBOP>
            :
            <div className="empty_div_section"></div>
        }
        <SvgAnimationBackground />
    </>)
}

export default Commission;