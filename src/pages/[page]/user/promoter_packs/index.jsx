import React from 'react';
import PropTypes from 'prop-types'
import PackProduct from '@PagesComponent/PromoterPack/PackProduct';
import PromoterStyle from './Promoter.style';
import CommonLayout from '@Components/Common/CommonLayout';

const PromoterPacks = ({ customerCheck, validateauth, showprotectedpage, ...props }) => {
    React.useEffect(() => {
        document.body.classList.add('dashboard');
    }, [])
    return (
        <CommonLayout attributes={{
            title: "Promoter Pack",
            section: "",
            showprotectedpage,
            customerCheck,
            validateauth,
        }}>
            <PromoterStyle>
                <PackProduct {...props} />
            </PromoterStyle>
        </CommonLayout>
    )
}

PromoterPacks.propTypes = {
    customerCheck: PropTypes.number.isRequired,
    showprotectedpage: PropTypes.bool.isRequired,
    validateauth: PropTypes.func.isRequired
}

export default PromoterPacks;