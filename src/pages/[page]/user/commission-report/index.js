import Head from "next/head";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Leftnav from "@Components/LeftNav";
import api from '../../../../api/Apis'
import * as React from 'react';
import TextField from '@material-ui/core/TextField';
import DateRangePicker from '@material-ui/lab/DateRangePicker';
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import Box from '@material-ui/core/Box';
import MobileDateRangePicker from '@material-ui/lab/MobileDateRangePicker';
import Moment from 'moment';
import ReactPaginate from 'react-paginate';
import Form from "../../../Components/CommissionReport/Form";
import FormData from "../../../Components/CommissionReport/FormData";
import ExportCSV from "../../../Components/CommissionReport/ExportCSV";
import { useRouter } from "next/router";
import MobileLeftNav from "@Components/LeftNav/MobileLeftNav";
import DBOP from "@Components/Common/DBOP";
import FormStyle, { FormDataStyle } from "src/pages/Components/Commissions/CommissionStyle";

const Commission = ({ validateauth, showprotectedpage, customerCheck }) => {
    const [LoginToken, setLoginToken] = useState()
    const { register, handleSubmit, errors } = useForm();
    const [value, setValue] = useState([null, null]);
    const [iscustomdate, IsCustomdate] = useState('month')
    const [viewtype, setViewtype] = useState('list')

    const [offset, setOffset] = useState(0);
    const [data, setData] = useState();
    const [perPage] = useState(10);
    const [pageCount, setPageCount] = useState()
    const [commissionData, setcommissionData] = useState()
    const [commissionDataError, setcommissionDataError] = useState()
    const [filterdata, setfilterdata] = useState()
    const router = useRouter()
    useEffect(() => {
        validateauth()

        // if (Object.keys(router?.query).length > 1 || Object.keys(router?.query).length === 0) {
        //     router.push('/us/404/')
        // }
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLoginToken(token)
        {
            filterdata === undefined &&
                api.GetCommissionsApproved(token, 'month').then(res => {
                    const slice = res?.data?.commissions_data?.slice(offset, offset + perPage)
                    setPageCount(Math.ceil(res?.data?.commissions_data?.length / perPage))
                    setcommissionData(slice)
                    if (res?.data?.code === 1) {
                        setcommissionDataError(res?.data?.message)
                    }
                    // setcommissionData(res?.data)

                })
        }
    }, [offset]);

    const onSubmit = data => {
        setfilterdata(data)
        if (iscustomdate === 'custom') {
            let daterange = convert(value);
            daterange = daterange.split(" - ");
            const from_date = Moment(daterange[0]).format('YYYY-MM-DD');
            const til_date = Moment(daterange[1]).format('YYYY-MM-DD');
            data.from_date = from_date
            data.til_date = til_date
        }

        api.GetCommissionsApproved(LoginToken, iscustomdate, data).then(res => {
            const slice = res?.data?.commissions_data?.slice(offset, offset + perPage)
            setPageCount(Math.ceil(res?.data?.commissions_data?.length / perPage))
            setcommissionData(slice)
            if (res?.data?.code === 1) {
                setcommissionDataError(res?.data?.message)
            }
            if (res?.data?.code === 0) {
                setcommissionDataError()
            }
        })

    }
    const convert = dateRange =>
        dateRange.map(date => new Intl.DateTimeFormat('en-US').format(new Date(date)))
            .join(" - ")

    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * perPage;
        if (filterdata !== undefined) {
            Filterdata(filterdata, offset)

        }

        setOffset(offset)

    };

    const Filterdata = (data, offset) => {
        setfilterdata(data)
        if (iscustomdate === 'custom') {
            let daterange = convert(value);
            daterange = daterange.split(" - ");
            const from_date = Moment(daterange[0]).format('YYYY-MM-DD');
            const til_date = Moment(daterange[1]).format('YYYY-MM-DD');
            data.from_date = from_date
            data.til_date = til_date
        }

        api.GetCommissionsApproved(LoginToken, iscustomdate, data).then(res => {
            const slice = res?.data?.commissions_data?.slice(offset, offset + perPage)
            setPageCount(Math.ceil(res?.data?.commissions_data?.length / perPage))
            setcommissionData(slice)
            if (res?.data?.code === 1) {
                setcommissionDataError(res?.data?.message)
            }
            if (res?.data?.code === 0) {
                setcommissionDataError()
            }
        })

    }
    let CsvData = [];
    return (<>
        <Head>
            <title>Commissions Report</title>
        </Head>
        {showprotectedpage === true &&
            <DBOP>
                <div className="mainorder-detail-sec commision-now ">
                    <div className="container">
                        <div className="row">
                            <div className="dashboard_main_div main_class_responsive">
                                <Leftnav customerCheck={customerCheck} />
                                <div className="col-md-10 dashboard-main">
                                    <MobileLeftNav customerCheck={customerCheck} />
                                    <div className="container order-detail-page">
                                        <FormStyle>
                                            <Form value={value}
                                                setValue={setValue}
                                                iscustomdate={iscustomdate}
                                                register={register}
                                                IsCustomdate={IsCustomdate}
                                                handleSubmit={handleSubmit}
                                                onSubmit={onSubmit} />
                                        </FormStyle>
                                        <div className="groups tabs">
                                            <ul className="tabs">
                                                <li className={`col-md-6 ${viewtype === 'list' ? 'active' : ''}`}
                                                    onClick={() => {
                                                        setViewtype('list')
                                                    }}>
                                                    <button>List View</button></li>
                                                <li className={`col-md-6 ${viewtype === 'graphical' ? 'active' : ''}`}

                                                    onClick={() => {
                                                        setViewtype('graphical')
                                                    }}>
                                                    <button>Graphical View</button>
                                                </li>
                                            </ul>
                                        </div>
                                        <FormDataStyle>
                                            <div className="comission-data-table comission-data-t1">
                                                <FormData CsvData={CsvData}
                                                    commissionDataError={commissionDataError}
                                                    handlePageClick={handlePageClick}
                                                    pageCount={pageCount}
                                                    commissionData={commissionData} />
                                            </div>
                                        </FormDataStyle>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </DBOP>
        }
    </>)
}

export default Commission;