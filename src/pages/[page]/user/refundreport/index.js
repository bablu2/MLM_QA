import Head from "next/head";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Leftnav from "@Components/LeftNav";

import * as React from 'react';

import Moment from 'moment';
import ReactPaginate from 'react-paginate';
import NumberFormat from 'react-number-format';
import PendingClawback from "../../../Components/RefundReport/PendingClawback";
import moment from "moment"
import RefundFilterForm from "../../../Components/RefundReport/RefundFilterForm";
import RefundData from "../../../Components/RefundReport/RefundData";
import api from "../../../../api/Apis";
import ExportCSV from "../../../Components/RefundReport/ExportCSV";
import { useRouter } from "next/router";
import MobileLeftNav from "@Components/LeftNav/MobileLeftNav";
import ReactpaginateComp from "@Components/Common/ReactPaginateComp";
import DBOP from "@Components/Common/DBOP";
import RefundFilterFormStyle, { RefundDataStyle } from '../../../Components/RefundReport/ReFundStyleComp';
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";

const RefundReport = ({ validateauth, showprotectedpage, customerCheck, setInnerLoader }) => {
    const [refundreport, setrefundreport] = useState()
    const [refundreportError, setrefundreportError] = useState('')
    const [pendingrefundreport, Setpendingrefundreport] = useState()

    const [LoginToken, setLoginToken] = useState()
    const { register, handleSubmit, errors } = useForm();
    const [value, setValue] = useState([null, null]);
    const [iscustomdate, IsCustomdate] = useState('month')
    const [firstdate, setFirstDate] = useState()
    const [lastdate, setLastDate] = useState()

    const [offset, setOffset] = useState(0);
    const [perPage] = useState(10);
    const [pageCount, setPageCount] = useState(0)
    const router = useRouter();


    useEffect(() => {
        validateauth()

        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        // if (Object.keys(router?.query).length > 1 || Object.keys(router?.query).length === 0) {
        //     router.push('/us/404/')
        // }
        setLoginToken(token)
        api.getRefundReport(token).then(res => {
            if (res?.data?.code === 1) {
                if (res?.data?.refund_data.length > 0) {
                    const slice = res?.data?.refund_data?.slice(offset, offset + perPage)
                    setPageCount(Math.ceil(res?.data?.refund_data?.length / perPage))
                    setrefundreport(slice)
                    setrefundreportError('')
                    Setpendingrefundreport(res?.data?.pending_clawbacks)
                }
                else {
                    setrefundreportError('Data Not Found')
                }
            }
        })

    }, [offset]);

    const onSubmit = data => {
        if (iscustomdate === 'custom') {
            let daterange = convert(value);
            daterange = daterange.split(" - ");
            data.from_date = Moment(value[0]).format('YYYY-MM-DD')
            data.til_date = Moment(value[1]).format('YYYY-MM-DD')
        }

        // setInnerLoader(true)
        api.getRefundReportFilter(data, LoginToken).then(res => {
            const slice = res?.data?.refund_data?.slice(offset, offset + perPage)
            setPageCount(Math.ceil(res?.data?.refund_data?.length / perPage))
            setrefundreport(slice)
            Setpendingrefundreport(res?.data?.pending_clawbacks)
            if (res?.data?.refund_data?.length === 0) {
                setrefundreportError('Data not found')
                setInnerLoader(false)
            }
            else {
                setrefundreportError('')
            }

        })
    }

    const convert = dateRange =>
        dateRange.map(date => new Intl.DateTimeFormat('en-US').format(new Date(date)))
            .join(" - ")

    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * perPage;
        setOffset(offset)
    };
    let CsvData = []
    return (<>
        <Head>
            <title>refund report</title>
        </Head>
        <DBOP>
            {showprotectedpage === true ?
                <div className="mainorder-detail-sec refund-report section">
                    <div className="container dashboard-main">
                        <div className="row">
                            <div className="dashboard_main_div main_class_responsive">
                                <Leftnav customerCheck={customerCheck} />
                                <div className="col-md-10">
                                    <MobileLeftNav customerCheck={customerCheck} />
                                    <div className="container order-detail-page">
                                        <RefundFilterFormStyle>
                                            <RefundFilterForm
                                                register={register}
                                                handleSubmit={handleSubmit}
                                                IsCustomdate={IsCustomdate}
                                                iscustomdate={iscustomdate}
                                                value={value}
                                                setValue={setValue}
                                                onSubmit={onSubmit}
                                            />
                                        </RefundFilterFormStyle>
                                        <div className="export-to-xls">
                                            <ExportCSV csvData={CsvData} fileName={`RefundReport ${lastdate}  -  ${firstdate}`} />

                                        </div>
                                        <RefundDataStyle>
                                            <RefundData
                                                refundreportError={refundreportError}
                                                setLastDate={setLastDate} setFirstDate={setFirstDate}
                                                refundreport={refundreport}
                                                CsvData={CsvData}
                                            />
                                        </RefundDataStyle>
                                        {refundreport?.length > 0 &&
                                            <ReactpaginateComp pageCount={pageCount} handlePageClick={handlePageClick} />
                                        }

                                        {/* <PendingClawback pendingrefundreport={pendingrefundreport} /> */}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                :
                <div className="empty_div_section"></div>
            }
        </DBOP>
        <SvgAnimationBackground />
    </>)
}

export default RefundReport;