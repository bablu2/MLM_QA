import CouponComponent from '../../../Components/CoupanComponent';
import React, { useEffect } from 'react';
import CommonLayout from '@Components/Common/CommonLayout';

const CouponPage = ({ showprotectedpage, customerCheck, validateauth, ...props }) => {
    return (
        <CommonLayout attributes={{
            title: "Coupon List",
            section: "coupon",
            showprotectedpage,
            customerCheck,
            validateauth,
        }} >
            <CouponComponent {...props} />
        </CommonLayout>
    )
}
export default CouponPage;