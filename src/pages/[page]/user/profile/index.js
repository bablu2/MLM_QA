import { set } from "js-cookie";
import useTranslation from "next-translate/useTranslation";
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Leftnav from "@Components/LeftNav";
import api from '../../../../api/Apis';
import ProfileEdit from "../../../Components/Profile/ProfileEdit";
import NumberFormat from 'react-number-format';
import Walletsection from "../../../Components/Profile/Walletsection";
import SaveAnalyticsCode from "../../../Components/Profile/SaveAnalyticsCode";
import ImageUpload from "../../../Components/Profile/ImageUpload";
import DBOP from "../../../../Components/Common/DBOP";
import PersonalDetails from "src/pages/Components/Profile/common";
import DialogComponent from '@Components/Common/DialogComponent';
import { toast } from "react-toastify";
import MobileLeftNav from "@Components/LeftNav/MobileLeftNav";
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";

const Profile = ({ validateauth, setshowloader, showprotectedpage, setInnerLoader, customerCheck, validReferralCheck,
    setValidReferralCheck, subdomainUserCheckFunction
}) => {
    const { t } = useTranslation();
    const [changePasswardStatus, setchangePasswardStatus] = useState();
    const [profilepageData, setprofilepageData] = useState();
    const [passwordData, setpasswordData] = useState();
    const [changepasswordresponse, setresponsepassword] = useState();
    const { register, handleSubmit, watch, errors, getValues } = useForm({
        mode: 'all'
    });
    const [LoginToken, setLoginToken] = useState();
    const [editdetails, setEditDetails] = useState(false);
    const [reloaddata, setreloadData] = useState(false);
    const [editpublicdetails, setEditPublicDetails] = useState(false);
    const [commission_wallet, SetCommissionWallet] = useState()

    const router = useRouter();
    var USNumber = profilepageData?.userdetails[0]?.phone_number?.match(/(\d{3})(\d{3})(\d{4})/);
    USNumber = "(" + USNumber?.[1] + ") " + USNumber?.[2] + "-" + USNumber?.[3];

    // for reset password
    const onSubmit = data => {
        const password = getValues("password");
        const confirmpassword = getValues('confirm_password');
        if (password !== confirmpassword) {
            setchangePasswardStatus('false');
        }
        else {
            setpasswordData(data);
            setchangePasswardStatus('true');
        }

    }
    // load profile page data
    useEffect(() => {
        document.body.classList.add('dashboard');
        validateauth()
        // subdomainUserCheckFunction(localStorage.getItem('Token'))
        if (Object.keys(router?.query).length > 1) {
            router.push('/us/404/')
        }
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLoginToken(token)
        const formDataDelete = { old_password: passwordData?.old_password, token: token, new_password: passwordData?.password };
        {
            changePasswardStatus === 'true' && passwordData?.old_password ?

                api.changeUserPassword(formDataDelete).then(res => {
                    if (res?.data?.code === 1) {
                        setresponsepassword(res.data.message);
                        // toast.success(res.data.message);
                    } else {
                        // toast.error(res.data.message);
                    }

                })
                :

                api.getProfilePageData(token).then(res => {
                    if (res?.data?.code === 1) {
                        setprofilepageData(res.data.user);
                        SetCommissionWallet(res.data.commission_wallet)
                    }
                })
        }
        api.getProfilePageData(token).then(res => {
            if (res?.data?.code === 1) {
                setprofilepageData(res.data.user);
                SetCommissionWallet(res.data.commission_wallet)
                setreloadData(false)
            }
        })
    }, [changePasswardStatus, reloaddata]);
    return (<>

        <Head>
            <title>Profile</title>
        </Head>
        {showprotectedpage == true ?
            <DBOP>
                <div className="mainorder-detail-sec main_profile_section_class profile-cstm-main">
                    <div className="profile-page-main">
                        <div className="container">
                            <div className="row">
                                <div className="dashboard_main_div main_class_responsive">
                                    <Leftnav customerCheck={customerCheck} />
                                    <div className="col-md-10">
                                        <MobileLeftNav customerCheck={customerCheck} />
                                        <div className="container order-detail-page">
                                            <div className="heading-top-main right-address">
                                                <div className="heading-main-text">
                                                    <h1>Personal details</h1>
                                                </div>
                                            </div>
                                            <div className="table-profile-details">
                                                <Walletsection
                                                    profilepageData={profilepageData}
                                                    setreloadData={setreloadData}
                                                    reloaddata={reloaddata}
                                                    LoginToken={LoginToken}
                                                    router={router}
                                                    commission_wallet={commission_wallet}
                                                />
                                                <div className="profile-text-right">
                                                    <ImageUpload setshowloader={setshowloader}
                                                        setInnerLoader={setInnerLoader}
                                                        profilepageData={profilepageData}
                                                        setprofilepageData={setprofilepageData}
                                                        changePasswardStatus={changePasswardStatus}
                                                        setchangePasswardStatus={setchangePasswardStatus}
                                                        editdetails={editdetails}
                                                    />

                                                    <table>
                                                        <tbody>
                                                            {editdetails === false ?
                                                                <>
                                                                    <tr>
                                                                        <th>First Name:</th>
                                                                        <td>{profilepageData?.email && profilepageData?.first_name}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Last Name:</th>
                                                                        <td>{profilepageData?.email && profilepageData?.last_name}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Email:</th>
                                                                        <td>{profilepageData?.email && profilepageData?.email}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Country:</th>
                                                                        <td>{profilepageData?.email && profilepageData?.userdetails[0]?.country}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>State:</th>
                                                                        <td>{profilepageData?.email && profilepageData?.userdetails[0].state}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Phone Number:</th>
                                                                        <td> {USNumber && USNumber}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Referral Code:</th>
                                                                        <td>{profilepageData?.userdetails[0]?.referral_code}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Opt in to email:</th>
                                                                        {
                                                                            profilepageData?.userdetails[0]?.email_communication !== false ?
                                                                                <td>
                                                                                    <input type="checkbox"
                                                                                        checked={profilepageData?.userdetails[0]?.email_communication !== false ? true : false}
                                                                                    />
                                                                                </td>
                                                                                :
                                                                                <td>
                                                                                    <input type="checkbox"
                                                                                        checked={profilepageData?.userdetails[0]?.email_communication !== false ? true : false}
                                                                                    />
                                                                                </td>

                                                                        }
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Opt in to message:</th>
                                                                        {
                                                                            profilepageData?.userdetails[0]?.text_communication !== false ?
                                                                                <td>
                                                                                    <input type="checkbox"
                                                                                        checked={profilepageData?.userdetails[0]?.text_communication !== false ? true : false}
                                                                                    />
                                                                                </td>
                                                                                :
                                                                                <td>
                                                                                    <input type="checkbox"
                                                                                        checked={profilepageData?.userdetails[0]?.text_communication !== false ? true : false}
                                                                                    />
                                                                                </td>
                                                                        }
                                                                    </tr>
                                                                </>
                                                                :
                                                                <ProfileEdit setreloadData={setreloadData}
                                                                    validReferralCheck={validReferralCheck}
                                                                    setEditDetails={setEditDetails}
                                                                    setValidReferralCheck={setValidReferralCheck}
                                                                    subdomainUserCheckFunction={subdomainUserCheckFunction}
                                                                    LoginToken={LoginToken}
                                                                    customerCheck={customerCheck}
                                                                    profilepageData={profilepageData} />
                                                            }
                                                        </tbody>
                                                    </table>

                                                    <button type="button" className="btn btn-primary" onClick={() => { setEditDetails(!editdetails) }}  >{editdetails ? "Back" : "Edit"}</button>
                                                </div>
                                                <div className="row">

                                                    {/* old password form */}
                                                    <DialogComponent opend={(changePasswardStatus === 'new' || changePasswardStatus === 'false') ? true : false}
                                                        handleClose={(value) => setchangePasswardStatus(String(!value))}
                                                        title={changepasswordresponse ? changepasswordresponse : "Change Password"}
                                                        classFor="change_passsword_from_profile"
                                                    >
                                                        {/* {changepasswordresponse && <h3 className="title">{changepasswordresponse}</h3>} */}
                                                        <form onSubmit={handleSubmit(onSubmit)}>
                                                            <div className="row">
                                                                <div className="col-md-12">
                                                                    <div className="form-group">
                                                                        <label htmlFor="exampleInputPassword1" className="form-label"> Old Password</label>
                                                                        <input type="password" className="form-control" name="old_password" id="exampleInputPassword2" ref={register({ required: true })} />
                                                                        {errors.old_password && <span className="error">This field is required</span>}
                                                                    </div>
                                                                </div>
                                                                <div className="col-md-12">
                                                                    <div className="form-group">
                                                                        <label htmlFor="exampleInputPassword1" className="form-label">New Password</label>
                                                                        <input type="password" className="form-control" name="password" id="exampleInputPassword1"
                                                                            ref={register({
                                                                                required: "This field is required",
                                                                                pattern: {
                                                                                    value: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,32}$/,
                                                                                    message: "Enter atleast 8 character password include atleast 1 upercase letter,1 lowercase letter, 1 number"
                                                                                }
                                                                            })}
                                                                        />
                                                                        {errors?.password && <span className="error">{errors?.password?.message}</span>}
                                                                    </div>
                                                                </div>
                                                                <div className="col-md-12">
                                                                    <div className="form-group">
                                                                        <label htmlFor="exampleInputPassword1" className="form-label">Confirm Password</label>
                                                                        <input type="password" className="form-control" name="confirm_password" id="exampleInputPassword3"
                                                                            ref={register({
                                                                                required: "This field is required.",
                                                                                validate: value => getValues('password') === value || "Please enter same password."
                                                                            })} />
                                                                        {errors?.confirm_password && <span className="error">{errors?.confirm_password?.message}</span>}
                                                                    </div>
                                                                    {/* {changePasswardStatus === "false" && <span>Password and confirm password not match</span>} */}
                                                                </div>
                                                            </div>
                                                            <div className="row">
                                                                <button type="submit" className="btn btn-primary">Update Password</button>
                                                            </div>
                                                        </form>


                                                    </DialogComponent>

                                                </div>
                                            </div>
                                        </div>
                                        {/* <SaveAnalyticsCode /> */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </DBOP>
            :
            <div className="empty_div_section"></div>
        }
        <SvgAnimationBackground />
    </>);
}
export default Profile;