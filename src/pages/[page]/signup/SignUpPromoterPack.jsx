import api from "@api/Apis";
import React, { useEffect, useState } from "react";
import { Button, SignUpPromoterCss } from "./SignupPageStyle";
import parse from "html-react-parser";
import _ from "lodash";
import { toast } from "react-toastify";
import DialogComponent from "@Components/Common/DialogComponent";
import { DialogSectionCss } from "@PagesComponent/ActiveSmartShip/ActiveStyleComp";
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";

function SignUpPromoterPack({ handleNext, minBV, setMinBv, setcounts, ...props }) {

  const { setshowloder, userToken, getMiniCartProduct, setUserToken, memberShipProduct, setMemberShipProduct, setshowminicart, activeStep, hideShowState, addToCart,
  } = props;
  const [packProduct, setPackProduct] = useState([]);
  const [activeClass, setActiveClass] = useState();
  const [selectedPack, setSelectedPack] = useState({});
  const [deleteCart, setDeleteCart] = useState();
  const [packDetails, setPackDetails] = useState()
  const [openDetails, setOpenDetails] = useState()

  useEffect(() => {
    document.body.classList.add('signupform-data');
    _promoterPackApi();
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }, []);

  async function _promoterPackApi() {
    // setshowloader(true);

    await api.promoterPackApi().then((res) => {
      const { code, promoter_packs } = res?.data;
      const allvalues = _.map(promoter_packs, (row) => ({
        id: row?.id,
        hidden: true,
        is_autoship: false
      }));
      if (res?.status === 200 && code === 1) {
        const data = _.find(promoter_packs, { name: "MEMBERSHIP" });
        setMemberShipProduct(data);
        setPackProduct(_.reject(promoter_packs, { name: "MEMBERSHIP" }));
        // setshowloader(false);
        setActiveClass(allvalues);
        pack(allvalues);
      }
    });

  }

  function pack(data) {
    const promoterData = JSON.parse(localStorage.getItem("PromoterPack"));
    if (promoterData) {
      const filterId = _.find(data, { id: promoterData?.product_id });
      if (filterId) {
        filterId["hidden"] = !filterId["hidden"];
        const REMAIN_DATA = _.reject(data, filterId);
        if (!filterId["hidden"]) {
          setSelectedPack(filterId);
        }
        setActiveClass([...REMAIN_DATA, filterId]);
      }
    }
  }

  useEffect(() => {
    pack();
  }, [activeStep]);

  useEffect(() => {
    const token = localStorage.getItem("Token")
      ? localStorage.getItem("Token")
      : null;
    setUserToken(token);
  }, []);

  function _handleSelect(id, is_autoship) {
    getMiniCartProduct()
    const filterId = _.find(activeClass, { id: id });
    if (filterId) {
      filterId["hidden"] = !filterId["hidden"];
      const REMAIN_DATA = _.reject(activeClass, filterId);
      if (!filterId["hidden"]) {
        _deletePromoterCustomPack({ product_id: selectedPack?.id, is_autoship: false }, "promoterPack_delete")
        setSelectedPack(filterId);
        const PromoterPack = {
          product_id: id,
          variant_id: null,
          token: userToken,
          quantity: 1,
          is_autoship: false,
          promoterDetail: packProduct?.find((row) => row?.id === id)
        };
        localStorage.setItem("PromoterPack", JSON.stringify(PromoterPack));
        addToCart(PromoterPack); handleNext();
        localStorage.removeItem('packProduct')
        const dataSss = localStorage.getItem("packProduct") ? JSON.parse(localStorage.getItem("packProduct")) : null;

        let couuntValue = dataSss !== null ? PromoterPack + 1 : +PromoterPack;
        setcounts(couuntValue);
        // setcounts(PromoterPack)
        setshowminicart(true);
      } else {
        const formdata = { product_id: id, is_autoship: false };
        _deletePromoterCustomPack(formdata, "promoterPack_remove")
        setSelectedPack({});
      }
      setActiveClass([...REMAIN_DATA, filterId]);
    }
  }

  async function _deletePromoterCustomPack(formdata, section) {
    await api.deleteProductFromAddToCart(formdata).then((res) => {
      if (res?.data?.code === 1) {
        if (section === "promoterPack_remove") {
          localStorage.removeItem('packProduct')
          localStorage.removeItem("PromoterPack");
        } else if (section === "promoterPack_delete") {
          getMiniCartProduct("delete", "");
          localStorage.removeItem('packProduct')
        } else {
          getMiniCartProduct("delete", "");
          localStorage.removeItem("PromoterPack");
        }

        // toast.success(res?.data?.message);
      }
    });
  }


  function _handleAddCustom(id, is_autoship) {
    _deletePromoterCustomPack({ product_id: id, is_autoship: is_autoship }, "customPack")
    hideShowState?.setHideCustomPack(false);
    handleNext();
  }

  function _checkCondition(id) {
    if ("id" in selectedPack) {
      if (id === selectedPack?.id) {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }
  function showpackdetails(id, is_autoship) {
    const detail = {
      is_autoship: is_autoship,
      promoterDetail: packProduct?.find((row) => row?.id === id)
    }
    setOpenDetails(detail)
    setPackDetails(true)
  }

  return (
    <>
      <SignUpPromoterCss>
        <div className="main-bx-pack enroll-cstm">
          <div className="heading css_class">
            <h2> Select a Member Pack</h2>
          </div>

          <div className="signUp_promoter_packSection">
            {_.map(packProduct, (row, index) => (
              <div className="promoter_main" key={index}>
                <div className="image_div">
                  {row?.product_images?.length > 0 && (
                    <img
                      src={process.env.DOC_URL + row?.product_images[0]?.image}
                      alt="helo"
                    />
                  )}
                </div>
                <div className="promoter_content">
                  <div className="headingPrice_section">
                    <h3>{row?.name}</h3>
                    <span>
                      ${parseFloat(row?.autoship_cost_price).toFixed(2)}
                    </span>
                  </div>
                  <div className="discriptionSection">
                    {row?.description ? parse(row?.description) : ""}
                  </div>

                  <div className="buttonSection">
                    <div className="full-pack-details">
                      <a onClick={() => showpackdetails(row?.id, row?.is_autoship)}>Full Pack Details</a>
                    </div>
                    <Button
                      className={_.find(activeClass, { id: row?.id })?.hidden ? "unactive" : "active"}
                      // disabled={_checkCondition(row?.id)}
                      onClick={() => _handleSelect(row?.id, row?.is_autoship)}
                    >
                      {_.find(activeClass, { id: row?.id })?.hidden ? "Select" : "Select"} </Button>
                  </div>
                </div>
              </div>
            ))}
          </div>
          <div className="add_custom_pack css_class">
            <Button
              // disabled={(selectedPack?.id)}
              onClick={() => _handleAddCustom(selectedPack?.id, selectedPack?.is_autoship)}>
              Create Custom Pack
            </Button>
          </div>
          <div className="noThanks">
            <a onClick={() => handleNext()}>No Thanks, Continue</a>
          </div>
        </div>
        <DialogComponent opend={packDetails} handleClose={() => setPackDetails(false)} title={openDetails?.promoterDetail?.name} classFor="promoter_pop_up">
          <DialogSectionCss>
            <div className="promoter_main">
              <div className="image_div">
                <img
                  src={process.env.DOC_URL + openDetails?.promoterDetail?.product_images?.[0]?.image}
                  alt="helo"
                />
              </div>
              <div className="promoter_content">
                <div className="discriptionSection">
                  <div className="more-info">
                    {openDetails?.promoterDetail?.less_info ? parse(openDetails?.promoterDetail?.less_info) : ""}
                  </div>
                  <div className="less-info">
                    {openDetails?.promoterDetail?.more_info ? parse(openDetails?.promoterDetail?.more_info) : ""}
                  </div>

                </div>
              </div>
            </div>
          </DialogSectionCss>
        </DialogComponent>
      </SignUpPromoterCss>
      <SvgAnimationBackground />
    </>


  );
}

export default SignUpPromoterPack;
