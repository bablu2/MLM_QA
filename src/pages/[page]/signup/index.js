import SignUpForm from "./SignUpForm";
import * as React from 'react';
import { Box, Stepper, Step, StepLabel, Button } from '@material-ui/core';
import { MainStapperCss } from "./SignupPageStyle";
import SignUpPromoterPack from "./SignUpPromoterPack";
import SignUpSmartShipProduct from "./SignUpSmartShipProduct";
import SignUpCustomPromoterPack from "./SignUpCustomPromoterPack";
import { toast } from "react-toastify";
import api from "@api/Apis";
import ReviewPage from './ReviewPage';
import router, { useRouter } from "next/router";
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";



const steps = ['SignUp', 'Enrollment Pack', 'Custom Pack', 'SmartShips & More Products', 'Review'];
const stepsd = ['SignUp', 'SmartShips & More Products', 'Review'];

const Signup = (props) => {
    const { setshowloader, setInnerLoader, setMemberPack, memberPack, setActiveStep, activeStep, counts, cartdata, setshowminicart,
        setAddNotify, addNotify, setOpenRefer, openRefer, setPackProduct, packProduct, setDataRefer, dataReferer } = props;

    const [signupDefualtValue, setDefaultValue] = React.useState({});
    const [memberShipProduct, setMemberShipProduct] = React.useState({});
    const [stepsOption, setStepsOption] = React.useState(stepsd);
    const [hideCustomPack, setHideCustomPack] = React.useState(true);
    const [checkCustomPack, setCheckCustomPack] = React.useState({ products: [], totalAmount: 0 });
    const [minBV, setMinBv] = React.useState(150);
    const [referralHide, setRefferalHide] = React.useState(false);
    const [loginToken, setLoginToken] = React.useState('');
    const [userToken, setUserToken] = React.useState('');

    const [data, setData] = React.useState('')
    const [credential, setCredential] = React.useState({
        email: "",
        password: ""
    });
    const Router = useRouter();
    React.useEffect(() => {
        if (Router?.query?.d === "member") {
            setStepsOption(steps);
        }
    }, [Router?.query])


    React.useEffect(() => {
        const data = localStorage.getItem('packProduct');
        if (data) {
            let values = JSON.parse(data);
            setCheckCustomPack(values?.customPack);
        }
        else {
            setCheckCustomPack({})
        }
        const profileData = localStorage.getItem('profileDetail');
        if (profileData) {
            let values = JSON.parse(profileData);
            setDefaultValue(values);
        }

        document.body.classList.add('bg-salmon');
    }, [activeStep]);

    React.useEffect(() => {
        if (Router?.query?.d === "member") {
            if (checkCustomPack?.length > 0) {
                setStepsOption(steps);
            } else if (hideCustomPack) {
                const CHANGE_VALUE = _.reject(steps, (row) => row === 'Custom Pack');
                setStepsOption(CHANGE_VALUE);
            } else if (!hideCustomPack) {
                setStepsOption(steps);
            }
        }

    }, [hideCustomPack, checkCustomPack, Router?.query])

    React.useEffect(() => {
        const data = localStorage.getItem('Token')
        setLoginToken(data)
    }, [])

    const handleNext = () => {
        setActiveStep((prevActiveStep) => {
            return prevActiveStep + 1
        });
    };

    const handleBack = (index) => {
        setActiveStep(index);
    };

    async function _addToCart(formData) {
        // setInnerLoader(true);
        await api.addToCart(formData).then(res => {
            if (res?.data?.code === 0) {
                setInnerLoader(false);
            }
            else {
                setAddNotify([...addNotify, formData?.product_id])
            }
        })
    }
    function _currentStepData(value) {
        let DATA = "";
        let GET_CURRENT_STEP = stepsOption[value]
        switch (GET_CURRENT_STEP) {
            case "SignUp":
                DATA = <SignUpForm {...props}
                    setUserToken={setUserToken}
                    setDataRefer={setDataRefer}
                    dataReferer={dataReferer}
                    signupDefualtValue={signupDefualtValue}
                    setActiveStep={setActiveStep}
                    referralHide={referralHide}
                    setRefferalHide={setRefferalHide}
                    setDefaultValue={setDefaultValue} />
                break;
            case "Enrollment Pack":
                DATA = <SignUpPromoterPack
                    {...props}
                    userToken={userToken}
                    packProduct={packProduct}
                    setPackProduct={setPackProduct}
                    setUserToken={setUserToken}
                    addToCart={(values) => _addToCart(values)}
                    activeStep={activeStep}
                    memberShipProduct={memberShipProduct}
                    hideShowState={{ hideCustomPack, setHideCustomPack }}
                    handleNext={handleNext}
                    minBV={minBV}
                    counts={counts}
                    setMinBv={setMinBv}
                    setMemberShipProduct={setMemberShipProduct} />
                break;

            case "Custom Pack":
                DATA = <SignUpCustomPromoterPack
                    {...props}
                    checkCustomPack={checkCustomPack}
                    handleNext={handleNext}
                    setHideCustomPack={setHideCustomPack}
                    minBV={minBV}
                    setMinBv={setMinBv}
                    handleBack={handleBack}
                    setActiveStep={setActiveStep}
                    memberShipProduct={memberShipProduct} />
                break;

            case "SmartShips & More Products":
                DATA = <SignUpSmartShipProduct
                    {...props}
                    addToCart={(values) => _addToCart(values)}
                    handleNext={handleNext}
                    counts={counts}
                    setActiveStep={setActiveStep}
                    cartdata={cartdata}
                />
                break;
            case "Review":
                DATA = <ReviewPage
                    {...props}
                    checkCustomPack={checkCustomPack}
                    setCheckCustomPack={setCheckCustomPack}
                    setActiveStep={setActiveStep}
                    handleBack={handleBack}
                    stepsOption={stepsOption}
                    signupDefualtValue={signupDefualtValue}
                    setDefaultValue={setDefaultValue}
                />
                break;
            default:
                DATA = "default"
        }
        return DATA;
    }
    const handleHomeBack = () => {
        document.body.classList.remove('bg-salmon');
        if (loginToken) {
            const data_sign = { "email": signupDefualtValue?.email, "password": signupDefualtValue.password }
            setCredential({ email: signupDefualtValue.email, password: signupDefualtValue.password });
            api.loginUser(data_sign).then((res) => {
                if (res?.data?.code === 1) {
                    setData(res?.data)
                    Router.push("/")
                }
            })
        }
        else {
            Router.push("/")
        }
    }
    return (
        <>
            <MainStapperCss>
                <div className="back_button">
                    <Button onClick={() => handleHomeBack()}><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" stroke="#fff" class="bi bi-arrow-bar-left" viewBox="0 0 16 16">
                        <path fillRule="evenodd" d="M12.5 15a.5.5 0 0 1-.5-.5v-13a.5.5 0 0 1 1 0v13a.5.5 0 0 1-.5.5zM10 8a.5.5 0 0 1-.5.5H3.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L3.707 7.5H9.5a.5.5 0 0 1 .5.5z" />
                    </svg> Exit</Button>
                </div>
                <Box sx={{ width: '100%' }} className="main_stapperForm">

                    <div className="header_logo">
                        <img src="images/kaire-sign-logo.png" />
                    </div>

                    <Stepper activeStep={activeStep} alternativeLabel>
                        {stepsOption.map((label, index) => {
                            return (
                                <Step key={label} onClick={() => {
                                    if (index !== 0) {
                                        if ('first_name' in signupDefualtValue) {
                                            if (Router?.query?.d === "member") {
                                                if (index === 3 && counts?.products?.length > 0) {
                                                    handleNext();
                                                }
                                                if (activeStep > index) {
                                                    handleBack(index)
                                                }
                                            }
                                            else {
                                                if (index === 2 && counts?.products?.length > 0) {
                                                    handleNext();
                                                }
                                                if (activeStep > index) {
                                                    handleBack(index)
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        console.log(index)
                                    }
                                    if (index == 1) {
                                        setHideCustomPack(true)
                                    }
                                }}>
                                    <StepLabel>{label}</StepLabel>
                                </Step>
                            );
                        })}
                    </Stepper>

                    <React.Fragment>
                        {_currentStepData(activeStep)}
                        <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
                            {/* <Button
                            color="inherit"
                            disabled={activeStep === 0}
                            onClick={handleBack}
                            sx={{ mr: 1 }}
                        >
                            Back
                        </Button> */}
                            {/* <Button onClick={handleNext}>
                            {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                        </Button> */}
                        </Box>
                    </React.Fragment>
                </Box>
            </MainStapperCss >
            <SvgAnimationBackground />
        </>
    );
}
export default Signup;

