import { parse } from 'date-fns'
import React, { useEffect, useState } from 'react'
import { FaTrash } from "react-icons/fa";
import EditIcon from '@material-ui/icons/Edit';
const CustomPackShow = (props) => {
    const { setCustomerPackProduct, customPackProduct } = props;
    return (
        <>
            <h2>YOUR CURRENT SELECTED MEMBER PACK</h2>
            <div className="member-pack-current">
                <div className="d-flex">
                    <div className="image_div">
                        <img src='/memberpack.png' />
                    </div>

                    <div className='pack-show'>
                        <div className='total-amount'>${parseFloat(customPackProduct?.totalAmount)?.toFixed(2)}</div>
                        {
                            customPackProduct?.products?.map((item) => {
                                return (
                                    <>
                                        <div className="content-right">
                                            <div className="product_name">
                                                <span className="product_name">
                                                    {item?.qty}x
                                                </span>
                                                &nbsp;{item?.product_name}
                                            </div>
                                        </div>
                                    </>

                                )
                            })
                        }
                    </div>


                </div>
            </div>
        </>
    )
}

export default CustomPackShow