import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import api from "@api/Apis";
import { SecondSection } from "../product/common";
import _ from "lodash";
import parse from 'html-react-parser';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { Button, SignUpSmartShipCss } from "./SignupPageStyle";
import { toast } from "react-toastify";
import CustomPackShow from "./CustomPackShow";
import { CustomPack } from "@PagesComponent/PromoterPack/PromoterCompStyle";
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";

function SignUpSmartShipProduct({ handleNext, setActiveStep, ...props }) {
  const {
    setshowloader,
    setInnerLoader,
    minCartAutoshipCheck,
    counts,
    setcounts,
    smartShipCheck,
    addToCart,
    addNotify,
    setAddNotify,
    cartdata
  } = props;
  const route = useRouter();
  const store = route.query.page;
  const [costType, setCostType] = useState([]);
  const [productData, setProductData] = React.useState([]);
  const [cartData, setCartData] = useState();
  const [qtyCount, setQtyCount] = useState([]);
  const [tokenDat, setTokenDat] = useState();
  const [customPackProduct, setCustomerPackProduct] = useState();
  const [memberPack, setMemberPack] = useState()

  useEffect(() => {
    document.body.classList.add('signupform-data');
    const token = localStorage.getItem("Token") ? localStorage.getItem("Token") : null;
    setTokenDat(token);
    const custompackData = localStorage.getItem("packProduct") ? JSON.parse(localStorage.getItem("packProduct")) : null;
    if (custompackData) {
      setCustomerPackProduct(custompackData?.customPack);
    }
    else {
      setCustomerPackProduct()
    }
    const member = localStorage.getItem("PromoterPack") ? JSON.parse(localStorage.getItem("PromoterPack")) : null;
    if (member) {
      setMemberPack(member)
    }
    else {
      setMemberPack()
    }
    _getAllProduct();
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });

  }, [cartdata]);

  async function _getAllProduct() {
    // setshowloader(true);
    // setInnerLoader(true);
    if (store) {
      await api.getAllProduct(store).then((res) => {
        if (res?.status === 200) {
          let CART_DATA = [];
          setProductData(res?.data?.products);
          api.getAllCartProduct().then((response) => {
            if (response?.data?.code === 1) {
              CART_DATA = _.map(response?.data.products, (row) => {
                let GET_ID = _.pick(row.product, "id");
                let VALUES = _.pick(row, ["is_autoship", "quantity"]);
                return { ...GET_ID, ...VALUES };
              });
              let data = _.map(
                response?.data.products,
                ({ product }) => +product?.id
              );
              setAddNotify(data);
            }

            const AllQTY = _.map(res?.data?.products, (row) => ({
              id: row?.id,
              qty:
                CART_DATA?.length > 0 && _.map(CART_DATA, "id").includes(row.id)
                  ? +_.find(CART_DATA, { id: row?.id }).quantity
                  : 1,
            }));

            const allvalues = _.map(res?.data?.products, (row) => ({
              id: row.id,
              value:
                CART_DATA?.length > 0 && _.map(CART_DATA, "id").includes(row.id)
                  ? _.find(CART_DATA, { id: row?.id }).is_autoship === "True"
                    ? "AutoShip"
                    : "Normal"
                  : "Normal",
            }));
            setQtyCount(AllQTY);
            setCostType(allvalues);
            // setshowloader(false);
            // setInnerLoader(false);
          });
        } else {
          // setInnerLoader(false);
        }
      });
    }
  }
  function _onQuantityChange(productId, value) {
    const FINDQTY = _.find(qtyCount, { id: productId });
    if (FINDQTY) {
      FINDQTY["qty"] = value;
      const DATA = _.reject(qtyCount, FINDQTY);
      setQtyCount([...DATA, FINDQTY]);
    }
  }

  function _handleAddtoCart(product) {
    api.getAllCartProduct().then((res) => {
      if (res?.data?.code === 1) {
        setCartData(res?.data);
        let data = _.map(res?.data.products, ({ product }) => +product?.id);
        setAddNotify(data);
        // props?.setcounts(res?.data);
        props?.setshowminicart(true);
      }
    });

    const { id, value } = _.find(costType, (row) => row?.id === product?.id);
    const { qty } = _.find(qtyCount, { id: product.id });
    const formData = {
      product_id: id,
      variant_id: null,
      token: tokenDat,
      quantity: +qty,
      is_autoship: value !== "Normal" ? true : false,
    };
    addToCart(formData);
    // localStorage.setItem("smartship", JSON.stringify(formData))
  }

  function handleHome() {
    if (props?.validateauth) {
      route.push("/");
    }
  }
  function handleBack() {
    setActiveStep((prevActiveStep) => {
      return prevActiveStep - 1
    })
  }
  return (
    <>
      <SignUpSmartShipCss>
        <div className="main-bx-pack">
          {memberPack &&
            <div className="member-pack">
              <h2>YOUR CURRENT SELECTED MEMBER PACK</h2>
              {
                <>
                  <div className="main-flex">
                    <div className="image_div">
                      {<img src={process.env.DOC_URL + memberPack?.promoterDetail?.product_images[0]?.image} alt="helo" />}
                    </div>
                    <div className="content-right">
                      <div className="product_name">{memberPack?.promoterDetail?.name}</div>
                      <div className="product_price">${memberPack?.promoterDetail?.cost_price}</div>
                      <div className="product-des">{memberPack?.promoterDetail?.description ? parse(memberPack?.promoterDetail?.description) : ""}</div>
                    </div>
                  </div>

                </>
              }
            </div>
          }
          {customPackProduct &&
            <CustomPackShow setCustomerPackProduct={setCustomerPackProduct} customPackProduct={customPackProduct} />
          }
          <h2>SELECT BELOW TO ADD MORE PRODUCTS OR SMARTSHIPS</h2>
          <div className="noThanks"></div>
          <div className="signUp_smartShip_Section">
            {_.map(productData, (row, index) => {
              const STOCK_CHECK = +row?.quantity >= 10 ? 10 : +row?.quantity;
              const CURRENTVALUE = _.find(costType, { id: row.id });
              if (row?.retail_open === true) {
                return (
                  <div className="smartShip_main" key={index}>
                    <div className="image_div">
                      {row?.product_images?.length > 0 && (
                        <img src={process.env.DOC_URL + row?.product_images[0]?.image} alt="helo" />
                      )}
                      <div className="heading_section">
                        <h3>{row?.name}</h3>
                        <ArrowDropDownIcon />
                      </div>
                    </div>
                    <div className="smartShip_content">

                      <div className="selectionSection">
                        <SecondSection
                          data={{
                            costtype: costType,
                            productCost: row?.cost_price,
                            productSmartShip: row?.autoship_cost_price,
                            minCartAutoshipCheck,
                            smartShipCheck,
                            setcosttype: setCostType,
                            id: row.id,
                            section: "signUp",
                          }}
                        />
                      </div>
                      <div className="buttonQtySection">
                        {/* <div>
                        <select
                          value={_.find(qtyCount, { id: row?.id })?.qty}
                          onChange={(e) =>
                            _onQuantityChange(row?.id, +e.target.value)
                          }
                        >
                          {_.times(STOCK_CHECK, (row) => (
                            <option value={row + 1} key={row + 1}>
                              {row + 1}
                            </option>
                          ))}
                        </select>
                          </div>*/}
                        <Button
                          disabled={STOCK_CHECK <= 0}
                          onClick={() => _handleAddtoCart(row)}
                        >
                          {STOCK_CHECK <= 0
                            ? "out of stock"
                            : CURRENTVALUE?.value === "Normal"
                              ? "Add To Cart"
                              : "Subscribe"}{" "}
                        </Button>
                      </div>

                      {addNotify?.includes(+row?.id) && (
                        <span className="error" style={{ color: "white" }}>
                          ADDED TO CART
                        </span>
                      )}
                    </div>
                  </div>
                );
              }
            })}
            <div className="noThanks d-flex gap-3">
              <button className="btn btn-primary mr-0" onClick={() => handleBack()}>
                Back
              </button>

              {counts === 0 ?
                <button className="btn btn-primary ml-0" onClick={() => handleHome()}>
                  I do not need additional products at this time
                </button>
                :
                <button className="btn btn-primary ml-0" onClick={() => handleNext()}>
                  Continue
                </button>
              }
            </div>
          </div>
        </div>
      </SignUpSmartShipCss >
      <SvgAnimationBackground />
    </>

  );
}

export default SignUpSmartShipProduct;
