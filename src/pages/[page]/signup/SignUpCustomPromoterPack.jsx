import NumberFormatComp from "@Components/Common/NumberFormatComp";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import api from "@api/Apis";
import { Button, SignUpCustomPackCss } from "./SignupPageStyle";
import { CheckCircle } from "@material-ui/icons";
import { toast } from "react-toastify";
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";

const QunatitySection = (props) => {
  const { onChange, value, disabled, setActiveStep } = props;
  return (
    <div id="qty">
      <button
        type="button"
        id="1"
        className="sub"
        value={value}
        disabled={disabled}
        onClick={(e) => onChange(e, "sub")}
      >
        -
      </button>
      <input
        name="1"
        type="text"
        className="get"
        list="350"
        max="350"
        readOnly
        value={value}
      />
      <button
        type="button"
        id="1"
        className="add"
        value={value}
        disabled={disabled}
        onClick={(e) => onChange(e, "add")}
      >
        +
      </button>
    </div>
  );
};

function SignUpCustomPromoterPack(props) {
  const route = useRouter();
  const store = route.query.page;
  const { setshowloader, handleNext, memberShipProduct, setshowminicart, minBV, setMinBv, checkCustomPack, setHideCustomPack, setInnerLoader, setActiveStep } =
    props;

  const [eligiableMsg, setEligiableMsg] = useState("");
  const [totalAmount, setTotalAmount] = useState(0);
  const [totalWeight, setTotalWeight] = useState(0);
  const [customData, setCustomData] = useState([]);
  const [quantityCheck, setQuantityCheck] = useState([]);
  const [selectedCustomPack, setSelectedCustomPack] = useState([]);

  useEffect(() => {
    document.body.classList.add('signupform-data');

    _getAllProduct();
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }, [checkCustomPack?.products]);

  function _getAllProduct() {
    // setInnerLoader(true);
    if (store) {
      api.getAllProduct(store).then((res) => {
        if (res?.status === 200) {
          setCustomData([...res?.data?.products, memberShipProduct]);
          const MEMBER = {
            id: memberShipProduct?.id,
            product_name: memberShipProduct?.name,
            qty: 1,
            bonus_value: memberShipProduct?.bonus_value,
            weight: 0,
            autoship_cost_price: memberShipProduct?.autoship_cost_price,
          };
          if (checkCustomPack?.products?.length > 0) {
            setSelectedCustomPack(checkCustomPack?.products);
            setQuantityCheck(checkCustomPack?.quantityData);
            setTotalAmount(checkCustomPack?.totalAmount);
            setTotalWeight(checkCustomPack?.totalWeight);
            setMinBv(checkCustomPack?.totalBV);
            setEligiableMsg(checkCustomPack?.eligiable);
          } else {
            setSelectedCustomPack([MEMBER]);
            setQuantityCheck([]);
            setTotalAmount(0);
            setTotalWeight(0);
            setMinBv(150);
            setEligiableMsg('');
          }

          // setCostType(allvalues);
          // setInnerLoader(false);
        }
      });
    }
  }

  // provide total bonus Value of product which is selected
  function _totalBonus(data) {
    let bonusValues = 0;
    _.forEach(data, (row) => {
      let calculate = row["bonus_value"] * +row?.qty;
      bonusValues = bonusValues + +calculate;
    });
    return bonusValues;
  }

  //provided total weight of products
  function _totalWeightFunc(data) {
    let totalWeight = 0;
    _.forEach(data, (row) => {
      let calculate = row["weight"] * +row?.qty;
      totalWeight = totalWeight + +calculate;
    });
    return totalWeight;
  }

  // provide total amount of product which is selected
  function _totalAmount(data) {
    let totalAmount = 0;
    _.forEach(data, (row) => {
      let calculate = row["autoship_cost_price"] * +row?.qty;
      totalAmount = totalAmount + +calculate;
    });
    return totalAmount;
  }

  // message sectin if customer reach min bonus value
  function _checkTotalBonus(data) {
    const totalBonus = _totalBonus(data);
    let currentBV = 150 - totalBonus <= 0 ? 0 : 150 - totalBonus;
    if (currentBV === 0) {
      // toast.success("Your Custom Build Promoter pack is ready ");
    }

    setMinBv(currentBV);
    if (totalBonus >= 150) {
      setEligiableMsg("Your Custom Build Promoter pack is ready");
    } else {
      setEligiableMsg(null);
    }
  }

  function commonDataAndQuantity(value, quantity) {
    const rejectedData = _.reject(selectedCustomPack, value);
    let values = {
      id: value?.id,
      product_name: value?.product_name,
      qty: 1,
      weight: value?.weight,
      bonus_value: value?.bonus_value,
      autoship_cost_price: value?.autoship_cost_price,
    };
    const getQTY = _.find(quantity, { id: value?.id });
    if (getQTY) {
      values = { ...values, ...{ qty: getQTY ? getQTY?.qty : 1 } };
    }
    let data =
      selectedCustomPack?.length > 0 ? [...rejectedData, values] : [values];
    setTotalAmount(_totalAmount(data));
    _checkTotalBonus(data);
    setTotalWeight(_totalWeightFunc(data));
    setSelectedCustomPack(data);
  }

  // selected data set in state
  function _selectedDataFunc(value) {
    const presence = _.find(selectedCustomPack, { id: value?.id });
    if (presence) {
      const rejectedData = _.reject(selectedCustomPack, presence);
      _checkTotalBonus(rejectedData);
      setTotalAmount(_totalAmount(rejectedData));
      setSelectedCustomPack(rejectedData);
    } else {
      value["product_name"] = value?.name;
      commonDataAndQuantity(value, quantityCheck);
    }
  }

  const _QuantitySection = (e, section, producData) => {
    let a = e?.currentTarget?.value;
    let max = producData?.quantity;
    let currentProduct = _.find(selectedCustomPack, { id: producData?.id });
    let values = [{}];

    if (section === "add") {
      if (+max > 0 && +a < +max) {
        let data = _.find(quantityCheck, { id: producData?.id });
        if (data) {
          let rejectData = _.reject(quantityCheck, data);
          values = [...rejectData, { id: producData?.id, qty: data?.qty + 1 }];
          if (currentProduct) {
            commonDataAndQuantity(currentProduct, values);
          }
        } else {
          values = [...quantityCheck, { id: producData?.id, qty: +a + 1 }];
          if (currentProduct) {
            commonDataAndQuantity(currentProduct, values);
          }
        }
      } else {
        values = [{ id: producData?.id, qty: +max }];
      }
      setQuantityCheck(values);
    }

    if (section === "sub") {
      if (a > 1) {
        let data = _.find(quantityCheck, { id: producData?.id });
        if (data) {
          let rejectData = _.reject(quantityCheck, data);
          values = [...rejectData, { id: producData?.id, qty: data?.qty - 1 }];
          if (currentProduct) {
            commonDataAndQuantity(currentProduct, values);
          }
        } else {
          values = [...rejectData, { id: producData?.id, qty: +a - 1 }];
          if (currentProduct) {
            commonDataAndQuantity(currentProduct, values);
          }
        }
        setQuantityCheck(values);
      } else {
        setQuantityCheck(quantityCheck);
      }
    }
  };

  //set all selected custom product on localstorage
  function _storeDataOnLocal() {
    localStorage.setItem(
      "packProduct",
      JSON.stringify({
        customPack: {
          totalAmount: totalAmount,
          products: selectedCustomPack,
          totalBV: minBV,
          eligiable: eligiableMsg,
          quantityData: quantityCheck,
          totalWeight: totalWeight,
        },
      })
    );
    setshowminicart(true)
  };

  const handleBack = () => {
    setHideCustomPack(true)
    setActiveStep((prevActiveStep) => {
      return prevActiveStep - 1
    })
  }

  return (
    <>
      <SignUpCustomPackCss>
        <div className="container">
          <div className="main-bx-pack">
            <h2>Custom Pack List</h2>
            <div className="minTotal_container">
              <div>
                <h2>Bonus Value Required:</h2>
                <NumberFormatComp value={minBV} />
              </div>
              <div>
                <h2>Total:</h2>
                <NumberFormatComp value={totalAmount} />
              </div>
            </div>
            <div className="signUpCustomPack_section">
              {customData?.length > 0 ? (
                _.map(customData, (row, index) => (
                  <div className="promoter_pack_container" key={index + 1}>
                    <div className="image_section">
                      <img
                        src={
                          row.product_images?.length > 0
                            ? process.env.API_URL + row.product_images[0]?.image
                            : "/images/no-image.jpg"
                        }
                      />
                      <h3>{row?.name}</h3>
                    </div>
                    <div className="content_section">
                      <div className="smartShip_section cost_class">
                        <NumberFormatComp value={row?.autoship_cost_price} />
                      </div>
                      <QunatitySection
                        value={
                          _.find(quantityCheck, { id: row?.id })
                            ? _.find(quantityCheck, { id: row?.id })?.qty
                            : 1
                        }

                        // value={1}
                        onChange={(e, section) =>
                          _QuantitySection(e, section, row)
                        }
                        disabled={!!(row?.name === "MEMBERSHIP")}
                      />
                      <div className="checkbox_data">
                        <Button
                          className={`custom_pack ${_.find(selectedCustomPack, { id: row?.id })
                            ? "active"
                            : "unactive"
                            }`}
                          id={`vehicle${row?.id}`}
                          value={row?.id}
                          onClick={() => _selectedDataFunc(row)}
                          disabled={!!(row?.name === "MEMBERSHIP") || (!row?.is_stock_available === "True")}
                        >
                          <span>
                            {_.find(selectedCustomPack, { id: row?.id }) && (
                              <CheckCircle />
                            )}
                          </span>
                          <span>
                            {
                              row?.is_stock_available === "True" ? <span>ADD</span> : <span>Out of Stock</span>
                            }
                          </span>

                        </Button>

                      </div>
                    </div>
                  </div>
                ))
              ) : (
                <div>loading.......................</div>
              )}

              <div className="add_to_custom_pack">
                <div className="add-custom d-flex gap-3">
                  <Button onClick={() => handleBack()}>Back</Button>
                  <Button
                    className={eligiableMsg ? "enable_add" : "disable_add"}
                    disabled={!eligiableMsg}
                    onClick={() => {
                      _storeDataOnLocal();
                      handleNext();
                    }}
                  >
                    Add Custom Pack
                  </Button>
                </div>

              </div>
            </div>
          </div>
        </div>
      </SignUpCustomPackCss >
      <SvgAnimationBackground />
    </>
  );
}

export default SignUpCustomPromoterPack;
