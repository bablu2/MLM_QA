import styled from "styled-components";
export const MainStapperCss = styled.div`
  top: 15px;
  left: calc(-50% + 18px);
  right: calc(50% + 18px);

  .back_button button {
    background: var(--blue);
    margin-left: 10px;
    margin-top: 10px;
    color: #fff;
    gap: 10px;
    font-size: 14px;
    line-height: normal;
    padding: 10px 15px;
  }
  & .main_stapperForm {
    margin-top: 0px;
    .header_logo {
      text-align: center;
      margin-bottom: 50px;
      img {
        max-width: 300px;
        width: 100%;
        @media (max-width: 767px) {
          max-width: 200px;
        }
      }
    }
    .MuiStepLabel-alternativeLabel {
      font-size: 18px;
      text-transform: uppercase;
      color: var(--blue);
      font-weight: 900;
      font-family: var(--common-font);
      @media (max-width: 767px) {
        font-size: 12px;
        letter-spacing: normal;
      }
    }
    .MuiStepLabel-iconContainer {
      text.MuiStepIcon-text {
        position: absolute;
        left: 50%;
        top: 50%;
        font-size: 15px;
        z-index: 9;
        transform: translate(0%, 1%);
        fill: var(--blue);
        @media (max-width: 767px) {
          font-size: 12px;
          letter-spacing: normal;
        }
      }
      > svg {
        position: relative;
        border: 4px solid var(--blue);
        font-size: 40px;
        fill: #fff;
        border-radius: 50%;
        background: #fff;
        z-index: 1;
        @media (max-width: 767px) {
          font-size: 35px;
        }
      }
      > svg.MuiSvgIcon-root.Mui-active {
        border-color: #c9d9ff;
        background: var(--blue);
        fill: var(--blue);

        text.MuiStepIcon-text {
          fill: #fff;
        }
        > svg.Mui-completed {
          fill: var(--blue);
          color: var(--blue);
          border: 4px solid #fff;
        }
      }
      .MuiStepConnector-horizontal {
        left: calc(-50% + 13px);
        right: calc(50% + 20px);
        z-index: 0;
        top: 20px;
        width: 100%;
      }
      .MuiStepConnector-line {
        border-color: var(--blue);
        border-top-style: solid;
        border-top-width: 15px;
      }
    }
    form.signupform.main-sign-frm h1 {
      margin-bottom: 10px;
    }
    .main-cstm {
      width: 100%;
      text-align: center;
      margin-bottom: 30px;
    }
    table.productDetailTable [type="button"] {
      background: none;
      border: none;
      color: #f00;
      padding: 0;
    }
    .MuiStepper-alternativeLabel
      svg.MuiSvgIcon-root.MuiSvgIcon-fontSizeMedium
      path {
      fill: var(--blue);
    }
    @media (max-width: 767px) {
      .MuiStep-root.MuiStep-horizontal {
        padding: 0;
      }
    }
  }
  span.MuiStepConnector-line.MuiStepConnector-lineHorizontal {
    display: block;
    border-color: #16356a;
    border-top-style: solid;
    border-top-width: 11px;
  }
  .MuiStepConnector-horizontal {
    top: 15px;
    left: calc(-50% + 14px);
    right: calc(50% + 14px);
    @media (max-width: 767px) {
      left: calc(-50% + 5px);
    }
  }
`;
export const Button = styled.button`
  text-align: center;
  justify-content: center;
  margin: 0 auto;
  color: #fff;
  max-width: fit-content;
  width: 100%;
  border-radius: 25px;
  border: 2px solid #06356a;
  transition: 0.3s ease all;
  padding: 10px 30px;
  font-size: 16px;
  &:hover {
    background: #fff !important;
    color: #06356a;
  }
  &.disable_add {
    background-color: #dddd;
    color: #fff;
    border: unset;
  }
  &.active {
    background-color: green;
    border-color: green;
    &:hover {
    color: green;
    }
  }
  &:disabled {
    background-color: #aaa7a7;
    border: 2px solid #aaa7a7;
    &:hover {
      color: #aaa7a7;
      background: #fff;
    }
  }
  &.custom_pack {
    display: flex;
    align-items: center;
    gap: 4px;
    span {
      &:first-child {
        margin-top: 4px;
      }
      svg {
        color: #fff;
        fill: #fff;
      }
    }
  }
`;
const SignupPageStyle = styled.div`
  & .main_sign_up_form_class {
    button.btn.btn-primary {
      max-width: 110px;
      margin-top: 0;
    }

    .mainradio p {
      font-size: 14px;
      margin-bottom: 10px;
    }

    span.error {
      font-size: 14px;
    }

    .signupform.main-sign-frm {
      max-width: 1000px;
      margin: 100px auto 70px;
      display: flex;
      flex-wrap: wrap;
      position: relative;
      transform: translate(0);
      left: 0;
      overflow: hidden;
      z-index: 9;
      top: 0;
      border: none;
      border-radius: 70px;
      box-shadow: 0 0 20px 7px rgb(0 0 0 / 5%);
      background: #fff;
      padding: 30px 30px 80px;
      @media (max-width: 1200px) {
        max-width: 100%;
        display: flex !important;
      }
      @media (max-width: 767px) {
        border-radius: 20px;
        padding: 20px;
      }
      .main-cstm a {
        color: #06356a;
        background: var(--blue);
        padding: 10px 90px;
        border-radius: 30px;
        color: #fff;
        font-size: 18px;
        display: inline-block;
        margin: 0 auto;
        cursor: pointer;
        @media (max-width: 991px) {
          padding: 10px 20px;
        }
        @media (max-width: 767px) {
          font-size: 15px;
        }
      }
      .field-class.company-line{
          max-width: 100%;
          flex: 100%;
          .mb-3.field-class{
            max-width: 100%;
            flex: 100%;
          }
        }
      .field-class {
        max-width: calc(100% / 2 - 10px);
        flex: calc(100% / 2 - 10px);
        width: 100%;
        position: relative;
        margin-bottom: 20px;
        :where(:nth-child(9), :nth-child(8), :nth-child(12), :nth-child(13)) {
          margin-bottom: 40px !important;
        } 
        @media (max-width: 767px) {
          max-width: 100%;
          flex: 100%;
          :where(:nth-child(9), :nth-child(8), :nth-child(12), :nth-child(13)) {
            margin-bottom: 20px !important;
          }
        }
        svg {
          position: absolute;
          right: 10px;
          top: 13px;
        }
      }
      label {
        font-size: 14px;
        font-family: var(--common-font);
        font-weight: normal;
        display: flex;
        align-items: center;
      }
      .submit_button.field-class {
        margin-bottom: 0;
      }
      .mb-3.form-check.field-class {
        width: 100%;
        max-width: 100%;
        flex: 100%;
      }
      .captcha_section_div {
        width: 100%;
        @media (max-width: 767px) {
          margin-bottom: 15px;
        }
      }
    }

    h1 {
      font-size: 26px;
      font-family: var(--common-font-bd);
      margin-top: 0;
      color: #06356a;
      width: 100%;
      text-transform: capitalize;
    }

    input:not([type="checkbox"]) {
      height: 40px;
      box-shadow: none;
      outline: none;
      border: 1px solid #ddd;
      border-radius: 30px;
      font-family: var(--common-font);
      &:focus {
        border-color: #00356a;
      }
    }

    select {
      border: 1px solid #ddd;
      border-radius: 30px;
      background: #fff;
    }

    .form-check {
      margin: 20px 0 15px;
      .form-group {
        margin-bottom: 0;
        input {
          padding: 0;
          height: initial;
          width: initial;
          margin-bottom: 0;
          display: none;
          cursor: pointer;
        }
        label {
          position: relative;
          cursor: pointer;
          @media (max-width: 767px) {
            align-items: flex-start;
          }
          &:before {
            content: "";
            -webkit-appearance: none;
            background-color: transparent;
            border: 2px solid var(--blue);
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05),
              inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);
            padding: 0px;
            display: inline-block;
            position: relative;
            vertical-align: middle;
            cursor: pointer;
            margin-right: 5px;
            border-radius: 30px;
            width: 18px;
            height: 18px;
            @media (max-width: 767px) {
              flex: 18px;
              min-width: 18px;
              top: 5px;
            }
          }
        }
        input:checked + label:before {
          background: var(--blue);
        }
        input:checked + label:after {
          content: "";
          display: block;
          position: absolute;
          top: 4px;
          left: 6px;
          width: 5px;
          height: 11px;
          border: solid #fff;
          border-width: 0 2px 2px 0;
          transform: rotate(45deg);
          @media (max-width: 767px) {
            top: 7px;
          }
        }
        input:checked + label {
          color: var(--blue);
        }
      }
    }

    .signup_up_button {
      text-align: center;
      justify-content: center;
      margin: 0 auto;
      max-width: 160px;
      width: 100%;
      height: 40px;
      border-radius: 25px;
      border: 2px solid var(--blue);
      transition: 0.3s ease all;
      &:hover {
        background: #fff !important;
        color: var(--blue);
      }
      text-align: center;
      margin: 27px 0 0;
      width: 100%;
      button {
        &:hover {
          background: #fff !important;
          color: var(--blue);
        }
        &:focus {
          outline: none;
        }
      }
    }

    .mainradio {
      .radiobtn {
        display: flex;
        align-items: center;
        color: var(--blue);
        input {
          margin-right: 10px;
          height: auto;
        }
        &:nth-child(2) {
          margin-bottom: 10px;
        }
      }
    }

    .container.main_sign_up_form_class {
      margin: 100px auto 50px;
    }

    .MuiStepConnector-root {
      span {
        border-color: var(--blue);
        border-top-style: solid;
        border-top-width: 5px;
      }
    }
  }
  .steppercls {
    [class*="MuiStepLabel-iconCon"] {
      padding-right: 0;
    }

    .MuiStep-root.MuiStep-horizontal {
      .MuiStepLabel-root {
        flex-wrap: wrap;
        justify-content: center;
      }
      span.MuiStepLabel-labelContainer {
        span {
          font-size: 16px;
          text-align: center;
          margin-top: 5px;
          color: var(--blue);
          font-weight: 500;
        }
      }
    }
  }
  @media (max-width: 767px) {
    .main_sign_up_form_class .signupform.main-sign-frm {
      max-width: 100%;
      margin: 60px 0;
      width: 100%;
    }
  }
`;
export const SignUpPromoterCss = styled.div`
  button:disabled {
    background: #ccc !important;
    :hover {
      background: #ccc !important;
      color: #fff;
    }
  }

  .heading {
    margin-bottom: 30px;
  }
  .signUp_promoter_packSection {
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    .promoter_main {
      width: 100%;
      display: flex;
      /* max-width: calc(100%/3 - 15px); */
      /* max-width: 600px; */
      @media (max-width: 1199px) {
        flex-wrap: wrap;
      }
      @media (max-width: 767px) {
        max-width: 100%;
      }
      .image_div {
        @media (max-width: 1199px) {
          max-width: 100%;
          text-align: center;
        }
        img {
          width: 100%;
        }
      }
    }
  }
  .add_custom_pack {
    width: 100%;
    text-align: center;
  }

  .main-bx-pack {
    max-width: 78vw;
    margin: 80px auto 70px;
    display: flex;
    flex-wrap: wrap;
    position: relative;
    transform: translate(0);
    left: 0;
    overflow: hidden;
    z-index: 9;
    top: 0;
    border: none;
    border-radius: 70px;
    box-shadow: 0 0 20px 7px rgb(0 0 0 / 5%);
    background: #fff;
    padding: 30px 30px 80px;
    @media (max-width: 1440px) {
      max-width: 98vw;
    }
    @media (max-width: 991px) {
      max-width: 95vw;
      padding: 10px 20px 30px;
      margin-bottom: 0;
    }
    @media (max-width: 767px) {
      margin-top: 40px;
      border-radius: 20px;
    }
  }
  .heading.css_class {
    width: 100%;
    h2 {
    text-align: center;
    font-family: 'Graphik';
    font-weight: 400;
    color: #000;
    text-transform: uppercase;
    font-size: 35px;
    width: 100%;
    @media (max-width: 991px) {
        font-size: 28px;
        margin-bottom: 0;
      }
      @media (max-width: 767px) {
        font-size: 20px;
        margin-bottom: 0;
      }
    }
  }
  .headingPrice_section {
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 10px;
    flex-wrap: wrap;
    gap: 5px;
    h3 {
      margin: 0;
      font-size: 25px;
      font-family: 'Graphik';
      font-weight: 400;
      color: #000;
      text-transform: uppercase;
      width: 100%;
      
      @media (max-width: 1280px) {
        font-size: 26px;
            }
      @media (max-width: 991px) {
        font-size: 24px;
      }
      @media (max-width: 767px) {
        font-size: 20px;
      }
    }
    span {
      font-family: var(--common-font);
      font-weight: 600;
      color: var(--blue);
      font-size: 17px;
      @media (max-width: 767px) {
        font-size: 16px;
      }
    }
  }
  .discriptionSection {
    p {
      font-size: 16px;
      font-family: "Graphik";
      font-weight: 500;
      line-height: 1.5;
    }
  }
  .buttonSection button {
    font-size: 17px;
    height: auto;
    line-height: normal;
    font-family: "Graphik";
    text-transform: capitalize;
    @media (max-width: 991px) {
      font-size: 16px;
    }
    button.active {
      color: green;
    }
  }
  .add_custom_pack.css_class {
    button {
      font-size: 17px;
      line-height: normal;
      height: auto;
      padding: 10px 40px;
      font-family: "Graphik";
      text-transform: capitalize;
      @media (max-width: 767px) {
        font-size: 16px;
      }
    }
  }
  .image_div {
    max-width: 200px;
    flex: 100%;
    width: 100%;
    img {
      max-width: 170px;
      @media (max-width: 767px){
        max-width: 100%;
        margin-bottom: 15px;
        max-height: 120px;
        object-fit: contain;
      }
    }
  }
  .promoter_main {
    align-items: flex-start;
    box-shadow: 0 0 20px 7px rgb(0 0 0 / 5%);
    border-radius: 53px;
    max-width: calc(100% / 2 - 25px);
    padding: 40px 50px;
    min-height: 338px;
    
    :nth-child(5) {
      flex: 0 0 100%;
      width: 100%;
      max-width: 100%;
      min-height: 280px;
      justify-content: center;

      .promoter_content{
        max-width: calc(100% - 950px);
        @media (max-width: 1280px){
          max-width: calc(100% - 650px);
        }
        @media (max-width: 1199px){
              max-width: 100%;
              margin-top: 10px;
        }
      } 
    }
    @media (max-width: 991px) {
      border-radius: 20px;
      padding: 15px;
    }
    .promoter_content {
      max-width: calc(100% - 200px);
      flex: 100%;
      width: 100%;
      @media (max-width: 1199px) {
        max-width: 100%;
      }
    }
  }
  .signUp_promoter_packSection {
    width: 100%;
    justify-content: flex-start;
    gap: 50px;
    @media (max-width: 991px){
      gap: 30px;
    }
  }
  .add_custom_pack.css_class {
    margin-top: 60px;
    @media (max-width: 991px) {
      margin-top: 30px;
    }
  }
  .noThanks {
    width: 100%;
    a {
      font-size: 16px;
      font-style: italic;
      text-decoration: underline;
      margin-top: 14px;
      display: block;
      color: #00356b;
      cursor: pointer;
    }
  }

`;
export const SignUpSmartShipCss = styled.div`

  .main-bx-pack {
    max-width: 78vw;
    margin: 80px auto 70px;
    display: flex;
    flex-wrap: wrap;
    position: relative;
    transform: translate(0);
    left: 0;
    overflow: hidden;
    z-index: 9;
    top: 0;
    border: none;
    border-radius: 70px;
    box-shadow: 0 0 20px 7px rgb(0 0 0 / 5%);
    background: #fff;
    padding: 30px 50px 80px;
    @media (max-width: 1440px) {
      max-width: 98vw;
      padding: 20px;
    }
    @media (max-width: 1199px) {
      max-width: calc(100% - 30px);
    }
    @media (max-width: 767px) {
      max-width: calc(100% - 20px);
      padding: 20px 20px 50px;
      border-radius: 20px;
    }
    h2 {
      width: 100%;
      text-align: center;
      font-family: "Graphik";
      font-weight: 400;
      color: #000;
      margin: 0;
      @media (max-width: 767px) {
        font-size: 16px;
      }
    }
    .normal_radio_container.radio_class.custom_radio {
      min-height: 70px;
      p.green_code,
      span.discount_smartship {
        position: absolute;
        max-width: max-content;
        width: max-content;
        right: 0;
        bottom: 10px;
        font-size: 14px;
      }
      .title_section {
        p.green_code {
          padding: 0;
          font-size: 14px;
        }
      }
    }

    .signUp_smartShip_Section {
      > .smartShip_main {
        max-width: calc(100% / 4 - 20px);
        align-items: flex-start; 
        border-radius: 30px;
        padding: 20px;
        display: flex;
        width: 100%; 
        box-shadow: 0 0 20px 7px rgb(0 0 0 / 5%);
        flex-wrap: wrap;
        justify-content: center;
        @media (max-width: 1750px) {
          gap: 20px;
          max-width: calc(100% / 3 - 20px);
        }
        @media (max-width: 1199px) { 
          max-width: calc(100% / 2 - 20px);
        }
        @media (max-width: 767px) {
          gap: 20px;
          padding: 10px;
          flex-wrap: wrap;
          border-radius: 20px;
          justify-content: center;
          max-width: 100%;
        }
      }

      @media (max-width: 767px) {
        padding: 0;
        border-radius: 20px;
      }
    }
    .member-pack-current {
    box-shadow: 0 0 20px 7px rgb(0 0 0 / 5%);
    padding: 30px;
    border-radius: 53px;
    margin: 30px auto 70px;
    max-width: 583px;
    width: 100%;
    min-height: 250px; 
    @media (max-width: 767px){
      border-radius: 20px;
      margin-bottom: 20px;
    }
    .d-flex{
      @media (max-width: 767px){ 
      flex-wrap: wrap; 
      }
          &>div {
        flex: 100%;
    }
    }
    .total-amount {
    font-size: 18px;
    color: var(--blue);
    font-family: 'Graphik-bold';
    margin-bottom: 10px;
}
.product_name {
    font-size: 18px;
    font-family: 'Graphik';
}
.content-right:not(:last-child) {
    margin-bottom: 4px;
}
} 
  }
  .noThanks {
    width: 100%;

    @media (max-width:767px){
        flex-wrap: wrap; 
        justify-content: center
      }
    > a {
      font-size: 16px;
      font-style: italic;
      -webkit-text-decoration: underline;
      text-decoration: underline;
      margin-top: 14px;
      display: block;
      color: #00356b;
      text-align: center;
      cursor: pointer;

     @media (max-width: 767px){
      max-width: 100%;
      white-space: normal;
      width: 100%;
     }
    }


    .noThanks {
      text-align: center;
   
    }
  }
  .signUp_smartShip_Section {
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    row-gap: 40px;
    column-gap: 20px;
    margin-top: 30px;
    @media (max-width: 767px) {
      gap: 20px;
      padding: 10px 30px 40px;
      flex-wrap: wrap;
      justify-content: center;
      column-gap: 0;
      width: 100%;
    }
    .smartShip_main {
      .image_div{
      max-width: 100%;
      text-align: center;
    }
      .smartShip_content {
        width: 100%;
        .selectionSection {
          .second_section {

            span.discount_smartship {
              line-height: normal;
            }
            .normal_radio_container {
                background: #f0f1f5;
            }
            .radio_class {
              display: flex;
              align-items: center;
              gap: 20px;
              padding: 10px;
              @media (max-width: 767px){
                gap: 10px;
              }
              .title_section {
                max-width: 100%;
                flex: 100%; 
                h3 {
                  font-size: 16px;
                  font-family: "Graphik-bold"; 
                  color: #000;
                  margin: 0; 
                }
                p {
                  font-size: 14px;line-height: normal;
                }
                * {
                  margin: 0;
                }
                p.green_code {
                  font-size: 12px;
                  line-height: normal;
                  padding-left: 10px;
                }
              }
              p.green_code,span.discount_smartship {
                    font-size: 12px;
                    bottom: 5px;
                } 
            }
          }
        }
        .buttonQtySection {
          display: flex;
          gap: 20px;
          justify-content: center;
          button {
            margin: 0 auto;
            font-size: 16px;
            line-height: normal;
            height: auto;
            text-transform: capitalize;
            font-family: "Graphik";
            @media (max-width: 767px) {
              font-size: 15px;
              padding: 10px 20px;
            }
          }
          & > div {
            max-width: 100px;
            width: 100%;
            select {
              width: 100%;
              height: 45px;
              border-radius: 30px;
              border: 1px solid #ccc;
              padding: 10px;
              font-family: "Graphik";
              color: #ccc;
              background: #fff;
            }
          }
        }
      }
    }

    .image_div {
      width: 100%;
      max-width: 150px;  
      img {
        width: 100%;
        max-width: 130px;
        @media (max-width: 767px){
          max-width: 100%;
          height: 200px;
         object-fit: contain;
        }
      }
    }

    .heading_section {  
      & > h3 {
        margin: 0; 
        font-weight: 400;
        color: #000;
        text-transform: uppercase;
        font-family: "Graphik"; 
        @media (max-width: 767px) {
          font-size: 18px;
        }
      }
      svg.MuiSvgIcon-root {
            width: 30px;
            height: 30px;
            fill: var(--blue);
          }
    } 
    .second_section {
      .radio_class {
      align-items: center;
     }
     padding: 0px 0 20px;
      & > .custom_radio {
        border-top: 1px solid #9b9b9b;
        &:last-child {
          border-bottom: 1px solid #9b9b9b;
        }
      }
    }

    .radio_section {
      & > span {
        padding: 0;
      }
    }
    .radio_class > div:last-child div:not(.strickClass) {
      font-size: 16px;
      font-family: 'Graphik-bold';
      font-weight: 600;
      color: #000;
    }
  }
  .strickClass {
    font-size: 14px;
    color: #6a6a6a;
    font-family: var(--common-font);
    text-align: right;
    text-decoration: line-through;
  }
  button.btn.btn-primary.ml-0{
    margin-left: 0;
}

button.btn.btn-primary.mr-0{
    margin-right: 0;
}


.member-pack{ 
  width: 100%; 
    margin: 0 auto 80px; 
    @media (max-width: 991px){
      margin: 0 auto 50px;
    }
    &>h2{
      margin-bottom: 30px;
    }
            .main-flex {
              display: flex;
              box-shadow: 0 0 20px 7px rgb(0 0 0 / 5%);
              border-radius: 53px;
              padding: 40px 50px;
              min-height: 250px;
              max-width: 585px;
              gap: 20px;
              align-items: flex-start;
              margin: 0 auto;
              @media (max-width: 767px){
                flex-wrap: wrap;
                padding: 20px;
                min-height: auto;
              }
              .image_div{
                @media (max-width: 767px){
                  max-width: 100%;
                  flex: 100%;
                  text-align: center;
                  border-radius: 20px;
                }
              }
            }
            .product_name {
            font-size: 25px;
            color: #000;
            text-transform: uppercase; 
            @media (max-width: 767px){
              font-size: 22px;
            }
           }
           .product_price {
            font-size: 17px;
            color: var(--blue);
            font-family: 'Graphik-bold';
            margin-top: 5px;
         }
         .product-des p {
          font-size: 15px;
          line-height: 1.5;
      }
        }

 
 
`;

export const SignUpCustomPackCss = styled.div`
@media (max-sswidth: 767px){
  .add-custom.d-flex.gap-3 {
    flex-wrap: wrap;
    justify-content: center;
    button {margin: 0 auto !important;}
}

}
  button.custom_pack.active {
    background: green !important;border: 2px solid green;
    &:hover{
      background: #fff !important;   color: green;
      svg{
        fill: green;
      }
    }
  }
  .main-bx-pack {
    max-width: 78vw;
    margin: 80px auto 70px;
    display: flex;
    flex-wrap: wrap;
    position: relative;
    transform: translate(0);
    left: 0;
    overflow: hidden;
    z-index: 9;
    top: 0;
    border: none;
    border-radius: 53px;
    box-shadow: 0 0 20px 7px rgb(0 0 0 / 5%);
    background: #fff;
    padding: 30px 30px 80px;
    @media (max-width: 1366px) {
      max-width: 98vw;
    }
    @media (max-width: 1199px) {
      max-width: calc(100% - 30px);
    }
    @media (max-width: 767px) {
      max-width: 100%;
      border-radius: 20px;
    }
    &>h2{
      width: 100%;
    }
    h2 {
      text-align: center;
      font-family: 'Graphik';
      font-weight: 400;
      color: #000;
      text-transform: uppercase;
      font-size: 35px;
      @media (max-width: 767px) {
        font-size: 16px;
      }
    }
  
  .signUpCustomPack_section {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
    gap: 20px;
    width: 100%;
    .promoter_pack_container {
      width: 100%;
      max-width: calc(100% / 4 - 15px);
      text-align: center;
      border-radius: 20px;
      box-shadow: 0 0 20px 7px rgb(0 0 0 / 5%);
      padding: 20px;
      @media (max-width: 1199px){
        max-width: calc(100%/3 - 15px);
      }
      @media (max-width: 991px){
        max-width: calc(100%/2 - 15px);
      }
      @media (max-width: 767px){
        max-width: calc(100%);
      }
      .image_section {
        width: 100%;
        h3 {
          font-size: 20px;
          font-family: "Graphik-bold";
          color: var(--blue);
        }
        img {
          width: 100%;
          max-width: 120px;
          height: 200px;
          object-fit: contain;
        }
      }
      .content_section {
        display: flex;
        flex-direction: column;
        row-gap: 20px;
        .smartShip_section {
          display: flex;
          justify-content: center;
          & > div {
            font-size: 18px;
            font-family: "Graphik";
          }
        }


        
        #qty {
            border: 1px solid #ddd;
            width: auto; 
            display: flex;
            height: 34px;
            justify-content: center;
            max-width: max-content;
            margin: 0 auto;
            button {
              width: 32px;
              position: relative;
              &:hover {
                background: #ddd;
              }
            }
            input {
              background: #fff;
              width: 40px;
              height: 32px;text-align: center;
              border: none;
              border-left: 1px solid #ddd;
              border-right: 1px solid #ddd; 
            }
            button{
              span:empty{
                display: none;
              }
            }
            .sub { 
              position: relative;
              margin: 0;
              font-size: 22px;
              height: 32px;
              bottom: auto;  padding: 0;
              border-radius: 0px;
              display: flex;  
              background: #fff;justify-content: center;align-items: center;
            } 
            .add { 
              position: relative;
              margin: 0;
              padding: 0;
              border-radius: 0;
              font-size: 14px;
              height: 32px;  display: flex;  justify-content: center;align-items: center;
              background: #fff;
            }
          }
        }
      }
      .add-custom { 
          button:first-child{
            margin-right: 0;
          }
          button:last-child {
            margin-left: 0; 
          }
        }
    }
    .custom_pack {
      max-width: 120px;
      height: 45px;
      font-family: "Graphik";
    }
    .add_to_custom_pack {
      width: 100%;
      text-align: center; 
      margin-top: 15px;
    }
  }
 
    .minTotal_container {
      background: #f9f9f9;
      padding: 10px;
      border-radius: 10px;
      display: flex;
      flex-wrap: wrap;
      width: 100%;
      margin: 40px 0;
      align-items: center;
      justify-content: space-between;
      & > div {
        display: flex;
        gap: 10px;
      } 
      h2 {
        font-size: 18px;
        margin: 0;line-height: 1.5;
          & + div {
            font-family: "Graphik";
            font-size: 18px;
            @media (max-width: 767px){
            font-size: 14px;
          }
          }
          @media (max-width: 767px){
            font-size: 15px;
          }
      }
    }
`;
export const SignUpReviewPageCss = styled.div`
  .main-bx-pack {
    max-width: 78vw;
    margin: 80px auto 70px;
    display: flex;
    flex-wrap: wrap;
    position: relative;
    transform: translate(0);
    left: 0;
    overflow: hidden;
    z-index: 9;
    top: 0;
    border: none;
    border-radius: 53px;
    box-shadow: 0 0 8px 7px rgb(0 0 0 / 3%);
    background: #fff;
    padding: 30px 30px 80px;
    @media (max-width: 1199px) {
      max-width: calc(100% - 30px);
    }
    @media (max-width: 767px) {
      max-width: calc(100% - 20px);
      padding: 20px 20px 50px;
      border-radius: 20px;
    }
    h2 {
      text-align: center;
      font-family: 'Graphik';
      font-weight: 400;
      color: #000;
      text-transform: uppercase;
      font-size: 35px;
      width: 100%;
      @media (max-width: 767px) {
        font-size: 24px;
      }
    }
    .product_price {
    font-size: 17px;
    color: var(--blue);
    font-family: 'Graphik-bold';
    margin-bottom: 15px;
}
    .member-pack-current {
    padding: 30px;
    border-radius: 50px;
    box-shadow: 0 0 8px 7px rgb(0 0 0 / 3%);
    max-width: 582px;
    margin: 0 auto;
    @media (max-width: 767px){
      border-radius: 20px;
    } 
    .d-flex>div {
    width: 100%;
      } 
      .detailDisplay_section + h2{
        margin-top: 20px;
      }
      }
     
      .pack-show .total-amount{
        font-size: 18px;
        color: var(--blue);
        font-family: 'Graphik-bold';
        margin-bottom: 10px;
      }
      .content-right{ font-size: 18px;
    font-family: 'Graphik';}
.edit-del.d-flex {
  justify-content: center;
    margin-top: 40px;
    gap: 30px;
    border-bottom: 1px solid #ddd;
        padding-bottom: 60px;
  a.btn-primary-btn {
    display: flex;
    align-items: center;
    color: #000;
    font-weight: 500;
    border-bottom: 1px solid #ddd;
    padding-bottom: 10px;
    text-transform: uppercase;
    cursor: pointer;
     svg {
        width: 20px;
        height: 14px; 
        color: #000;
    }
}
  }
  .amount_and_edit{ 
    .button_for_edit{
    gap: 0;
  }
  .button_for_edit div { 
    display: flex;
    align-items: center;
}
}
.member-pack-current{
  margin-bottom: 30px;
}
  .table_responsive { 
    /* border-top: 1px solid #ddd; */
    border-bottom: 1px solid #ddd;
    margin: 0 0 60px 0;
    padding-top: 50px;
}
  div.signUpReview_page {
    table {
      &.productDetailTable {
        margin: 60px auto;
        width: 100%;
        max-width: 60vw;  
        border: none;

        @media (max-width: 1199px) {
          margin: 60px 0 10px;
          max-width: 100vw; 
        }
        @media (max-width: 767px) {
          margin-bottom: 0;
          width: 100%;
          margin-top: 20px;
          tbody tr {
              display: flex;
              flex-direction: column;
              border:  1px solid #ddd;
                  td {
                    position: relative;
                    padding: 90px 10px 40px !important;
                  
                  &:before {
                        content: attr(data-value);
                        color: #fff;
                        background: var(--blue);
                        position: absolute;
                        top: 0;
                        left: 0;
                        height: 50px;
                        width: 100%;
                        font-size: 18px;
                        display: flex;
                        justify-content: center;
                        align-items: center;
                    }
              }
            }
     
            thead { 
                  display: none; 
            }
        }
        thead {
            border: none
        }
        tr { 
          &:first-child {
            border: unset;
          }
          &:nth-child(2n) {
            td {
              background: #f9f9f9;
            }
          }

          th {
            color: #000;
            padding: 20px 0;
            font-family: "Graphik-bold";
            text-transform: uppercase;
            @media (max-width: 767px) {
              padding: 20px 15px;
            }
            &:first-child {
              width: 580px;
              border-radius: 20px 0 0 20px;
            }
            &:last-child { 
              border-radius:  0 20px 20px  0;
            }
          }
          td {
            color: var(--blue);
            padding: 10px 0;
            .product_nameSection {
              display: flex;
              gap: 10px;
              align-items: center;
              @media (max-width: 767px) {
                flex-wrap: wrap;
                >div{
                  width: 100%;
                  max-width: 100%;
                  text-align: center;
                  h3{
                    text-align: center !important;
                  }
                  img{
                    max-width: 150px;
                  }
                }
              }
              .title {
                h3 {
                  font-family: var(--common-font);
                  font-size: 18px;
                  margin-top: 0;
                  font-weight: 600;
                  text-align: left;
                  @media (max-width: 767px) {
                    font-size: 16px;
                  }
                }
                p {
                  margin-bottom: 0;
                  max-width: 230px;
                  text-align: left;
                  font-size: 16px;
                  line-height: 1.5;
                  @media (max-width: 767px) {
                    font-size: 14px;
                  }
                }
              }
            }
            .productImage {
              max-width: 100px;
              img {
                width: 100%;
              }
            }
            div {
              font-family: var(--common-font);
              text-align: center;
              font-weight: 600;
            }
            &:not(:first-child) {
              vertical-align: middle;
              padding: 65px 0;
            }
          }
          th,
          td {
            background: #fff;
            font-size: 16px;
            text-transform: uppercase;
            text-align: center;
            &:first-child {
              text-align: left;
              padding-left: 20px;
            }
            &:last-child { 
              padding-right: 20px;
              [type="button"]{
                svg{
                  fill: var(--blue);
                  font-size: 18px;
                }
              }
              [type="button"]:first-child{
                margin-right: 10px;
                svg{
                  fill: #ff0000; 
                }
              }
            }
          }
        }
      }
    }

    .table_responsive {
      overflow-x: auto;
    }
    .table_container {
      table {
        width: 100%;
        text-align: left;
        th {
          text-align: left !important;
        }
        td {
          padding: 15px 20px;
        }
      }
    }

    .main {
      display: flex;
      align-items: center;
      margin-top: 35px;
      margin-left: -15px;
      margin-right: -15px;
      @media (max-width: 991px) {
        flex-wrap: wrap;
      }
      .cart_form {
        @media (max-width: 991px) {
          max-width: 100%;
          width: 100%;
        }
        h3 {
          margin: 0 0 10px;
          font-size: 20px;
          font-family: var(--common-font);
          color: var(--blue);
          width: 100%;
          text-transform: uppercase;
          font-weight: 600;
          @media (max-width: 767px) {
            font-size: 16px;
          }
        }
        .row {
          display: flex;
          width: 100%;
          margin: 0 0 10px;
          gap: 20px;
          justify-content: center;
          flex-wrap: wrap;
          @media (max-width: 767px) {
            gap: 10px;
          }
          input {
            padding: 7px 10px;
            border-radius: 25px;
            max-width: 100%;
            width: 100%;
            height: 45px;
            border: 1px solid #ddd;
            font-family: var(--common-font);
            font-size: 14px;
            color: #757c82;
          }
          div {
            width: 100%;
            flex: 100%;
            max-width: calc(100% / 2 - 10px);
            @media (max-width: 767px) {
              max-width: 100%;
            }
          }
          &:before,
          &:after {
            display: none;
          }
        }
      }
      .cart_form{
        box-shadow: 0 0 20px 7px rgb(0 0 0 / 5%);
        border-radius: 30px;
        padding: 15px 30px;

        @media (max-width: 767px){
        padding: 15px 20px;
        max-width: calc(100% - 20px);
        margin: 0 auto;
          
        }
      }
    }

    .totalAmount_section {
      @media (max-width: 991px) {
        max-width: 100%;
        width: 100%;
      }
      .Amountinner_div {
        width: 100%;
        margin: 0 auto;
        box-shadow: 0 0 20px 7px rgb(0 0 0 / 5%);
        border-radius: 30px;
        padding: 15px 30px;
        max-width: 625px; 
        @media (max-width: 991px) {
          padding: 15px 20px;
          margin-top: 30px;
        }
        @media (max-width: 767px) {
          overflow-x: visible;
          margin-top: 20px;
        }
        table {
          width: 100%; 
          tr { 
            display: flex;
            flex-wrap: wrap; 
            border-top: 1px solid #ddd;
            &:first-child {
              border: unset;
            }
            td {
              max-width: calc(100% / 2);
              flex: calc(100% / 2);
              width: 100%;
              padding: 15px 0px;
              font-family: var(--common-font);
              @media (max-width: 767px){
                max-width: 100%;
                flex: 100%;
              }
              & > div {
                max-width: 100%;
                float: right;
                text-align: left;
                width: 100%;
              }
              .total > div {
                font-size: 16px;
                font-family: var(--common-font);
                font-weight: 600;
              }
              &:first-child {
                text-align: left;
                color: var(--blue);
                font-family: var(--common-font);
                font-weight: 600;
                font-size: 16px;
                text-transform: uppercase;
                @media (max-width: 1400px) {
                  width: 20%;
                }
                @media (max-width: 991px) {
                  width: 50%;
                }
                @media (max-width: 767px) {
                  width: 100%;
                  background: var(--blue);
                  text-align: center;
                  color: #fff;
                }
              }
              &:last-child {
                div {
                  text-align: right;
                  width: 100%;
                  @media (max-width: 767px){
                    text-align: center;
                  }
                }
                label.MuiInputLabel-root.MuiInputLabel-animated {
                  display: none;
                }
         
                .radio_section_for_shipping_cost_gsn {
                  label {
                    margin: 0;
                  }
                }
              }
              p {
                font-size: 14px;
                line-height: normal;
                @media (max-width: 1366px) {
                  text-align: left;
                  font-size: 13px;
                }
              }
            }
            
          }
          .main_shipping_method_class {
            .MuiFormControlLabel-root {
              justify-content: flex-end;
              @media (max-width: 767px){
                justify-content: flex-start;
              }
            }
            p {
              margin-bottom: 0;
            }
          }
        }
      }
    }
  }
  .error_card {
    color: #ff0000;
    font-size: 14px;
  }
  .detailDisplay_section+h2{
    margin-top: 60px;
    margin-bottom: 20px;
    border-top: 1px solid #ddd;
    padding-top: 60px;
  }
  .detailDisplay_section {
    width: 100%;
    display: flex;
    margin-top: 40px;
    justify-content: space-around;
    gap: 60px;
    @media (max-width: 1199px) {
      gap: 30px;
    }
    @media (max-width: 991px) {
      gap: 0;
      flex-wrap: wrap;
    }
    @media (max-width: 767px) {
      margin-top: 20px;
    }
    .detailInner_section {
      width: 100%; 
      padding: 15px 30px;
      max-width: calc(100% - 10px);
      border-radius: 53px;
      box-shadow: 0 0 8px 7px rgb(0 0 0 / 3%);
      @media (max-width: 1199px) {
        width: 100%;
        max-width: 100%;
        margin-bottom: 30px;
      }
      @media (max-width: 991px) {
        max-width: 95vw;
      }
      @media (max-width: 767px) {
        border-radius: 20px;
        padding: 5px 10px;
      }
      table {
        margin: 0 auto;
        width: 100%;
        tr {
          border-top: 1px solid #ddd;
          &:first-child {
            border: unset;
          }
          td {
            padding: 9px 0px;
            font-size: 16px;
            font-weight: 500;
            max-width: 50%;
            width: 100%;
            font-family: var(--common-font);
            @media (max-width: 767px) {
              font-size: 13px;
            }
            &:first-child {
              color: var(--blue);
              font-family: var(--common-font);
              font-weight: 600;
              font-size: 16px;
              text-transform: uppercase;
              @media (max-width: 767px) {
                font-size: 13px;
              }
            }
          }
        }
      }
    }

    .biillingForm_Section {
      display: flex;
      flex-direction: column;
      gap: 10px;
      width: 100%;
      max-width: calc(100% / 2);
      padding: 30px;
      border-radius: 50px;
      box-shadow: 0 0 8px 7px rgb(0 0 0 / 3%);
      @media (max-width: 767px){ 
        border-radius: 20px;
        padding: 20px;
      }
        .check_data {
          margin-bottom: 15px;
        }
        @media (max-width: 1199px) {
          gap: 0;
          max-width: 100%;
        }
        h3 {
          margin: 0 0 10px;
          font-size: 20px;
          font-family: var(--common-font);
          font-weight: 600;
          color: var(--blue);
          width: 100%;
          text-transform: uppercase;
        }
      .field {
        margin-bottom: 10px;
        display: flex;
        width: 100%;
        justify-content: space-between;
        @media (max-width: 767px) {
          flex-wrap: wrap;
          /* margin: 0; */
          gap: 10px;
        }
        .text_field {
          width: 100%;
          max-width: calc(100% / 2 - 15px);
          flex: 100%;
          @media (max-width: 767px) {
            max-width: 100%;
          }
        }
        input,
        select {
          padding: 7px 10px;
          border-radius: 25px;
          max-width: 100%;
          width: 100%;
          height: 45px;
          border: 1px solid #ddd;
          font-family: var(--common-font);
          font-size: 14px;
          color: #757c82;
          background-color: #fff;
        }
      }
    }

  }
  .main-bx-pack-member + .payment-method {
    margin-top: 60px;
}
  .main-bx-pack-member {
      margin-top: 60px; 
      border-top: 1px solid #ddd;
      border-bottom: 1px solid #ddd;
      padding: 30px 0;
        .member-pack{
        padding: 30px;
        border-radius: 50px;
        box-shadow: 0 0 8px 7px rgb(0 0 0 / 3%); 
        max-width: 582px;
        margin: 30px auto; 
        @media (max-width: 767px){
          border-radius: 10px;
        }
            .main-flex {
            display: flex;
            gap: 15px;
            @media (max-width: 767px){
               flex-wrap: wrap;
            }
            }
        } 
    .product_name {
      font-size: 25px;
      line-height: normal;
      color: #000000;
      font-weight: 300;
      font-family: "Graphik";
      text-transform: uppercase;
      margin-bottom: 5px;
    }
    .product_price {
        font-size: 17px;
        color: var(--blue);
        font-family: 'Graphik-bold';  
        margin-bottom: 15px;
    }
    .product-des{
      p {
    margin: 0;
    font-size: 15px;
    line-height: normal;
    font-family: 'Graphik';
  }
    }
 

    .edit-del.d-flex {
      justify-content: center;
        margin-top: 40px;
        gap: 30px;

        @media (max-width: 767px){
          margin-top: 30px;
        }
      a.btn-primary-btn {
        display: flex;
        align-items: center;
        color: #000;
        font-weight: 500;
        border-bottom: 1px solid #ddd;
        padding-bottom: 10px;
        text-transform: uppercase;
        text-decoration: none;
        cursor: pointer;
        svg {
            width: 20px;
            height: 14px; 
            color: #000;
        }
    }
    .edit:first-child svg {
      height: 19px; 
    }
     
      }
      


    }
  .checkbox {
    margin: 20px 0 15px;
    .policy {
      display: flex;
      align-items: center;
      & > p {
        margin-bottom: 0;
      }
      a {
        margin: 0 6px;
        font-size: 14px;
        color: var(--blue);
        text-decoration: underline;
        text-underline-offset: 2px; 
        text-decoration-thickness: from-font;
        &:hover {
          text-decoration: none;
        }
      }
    }
    .form-group {
      display: flex;
      align-items: center;
      p {
        margin-bottom: 0;
        font-size: 14px;
        line-height: normal;
      }
      input {
        padding: 0;
        height: initial;
        width: initial;
        margin-bottom: 0;
        display: none;
        cursor: pointer;
      }

      label {
        position: relative;
        cursor: pointer;
        padding-left: 0;
        font-family: var(--common-font);
        &:before {
          content: "";
          -webkit-appearance: none;
          background-color: transparent;
          border: 2px solid var(--blue);
          box-shadow: 0 1px 2px rgba(0, 0, 0, 0.05),
            inset 0px -15px 10px -12px rgba(0, 0, 0, 0.05);
          padding: 0px;
          display: inline-block;
          position: relative;
          vertical-align: middle;
          cursor: pointer;
          margin-right: 5px;
          border-radius: 30px;
          width: 18px;
          height: 18px;
        }
      }

      input:checked + label:before {
        background: var(--blue);
      }

      input:checked + label:after {
        content: "";
        display: block;
        position: absolute;
        top: 3px;
        left: 6px;
        width: 5px;
        height: 11px;
        border: solid #fff;
        border-width: 0 2px 2px 0;
        transform: rotate(45deg);
      }

      input:checked + label {
        color: var(--blue);
      }
    }
  }
  .checkoutButton {
    button {
      font-size: 16px;
      text-transform: uppercase;
      font-family: var(--common-font);
      height: 45px;
      margin-top: 10px;
      @media (max-width: 767px){
      max-width: 100%;
      margin-left: 0;
    }
    }
  
  }
  .signUpReview_page {
    .checkbox .form-group input:checked + label::after {
      top: 5px;
    }
  }

  @media (max-width: 767px) {
    .signUpReview_page {
      padding: 0;
    }
    .checkbox .form-group p {
      font-size: 14px;
      line-height: normal;
    }
    .checkbox .policy a {
      margin: 0 3px;
      font-size: 13px;
    }
    .checkoutButton button {
      font-size: 14px;
    }
  }
  .apply-coupn {
    min-height: 180px;
    display: flex;
    align-items: center;
    flex-wrap: wrap;
    margin-left: 00;
    margin-top: 50px;
    max-width: 650px;
    justify-content: center;
    align-content: center;
  }

  .apply-coupn > .row,
  .apply-coupn label {
    flex: 100%;
    max-width: 100%;
    color: #000;
    padding: 0;
    float: left;
    width: 100%;
  }

  label b {
    font-size: 20px;
    color: var(--blue);
  }

  .apply-coupn .signupform {
    position: unset;
    padding: 0;
    display: flex;
    margin: 0 auto;
    max-width: 100%;
    justify-content: center;
    width: 100%;
    transform: unset;
    height: 50px;
  }

  .apply-coupn .signupform input.form-control {
    height: 100%;
    border-radius: 30px 0 0 30px;
  }

  .apply-coupn .row button.btn.btn-primary:last-child {
    border-radius: 0 20px 20px 0;
  }
  .coupon_dlt {
    margin-top: 30px;
    font-size: 18px;
    font-weight: 700;
    display: flex;
    align-items: center;
    gap: 10px;
    color: var (--blue);
    background: #fff;
    padding: 20px;
    border-radius: 10px;
    max-width: 650px;
    justify-content: space-between;
    margin-bottom: 10px;
    border-top: 10px solid var(--blue);
    box-shadow: 0 0px 15px rgba(0, 0, 0, 0.2);
    text-transform: uppercase;
    color: var(--blue);
  }
  .coupon_dlt svg {
    color: #f00;
  }
}
`;
export default SignupPageStyle;
