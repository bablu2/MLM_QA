import CheckedSection from "@PagesComponent/Reports/CommonComponent";
import React, { useEffect, useState } from "react";
import {
  CountryDropdown,
  RegionDropdown,
  CountryRegionData,
} from "react-country-region-selector";
import { useForm, useFormContext } from "react-hook-form";

function DetailSection({ detail, setBillingAddress, _onhandleChange, isShippingSame, setIsShippingSame, dataName, setDataName, countryValue, setCountryValue, stateValue, setStateValue, countryError, stateError }) {
  const data = useFormContext();

  const { register } = useForm({
    mode: "all",
  });
  // const [unCheckData, setUnCheckData] = useState(false)

  function handleChange(e) {
  }
  return (
    <div className="detailDisplay_section">
      <div className="detailInner_section">
        <table>
          <tbody>
            <tr>
              <td>Email</td>
              <td>{detail?.email}</td>
            </tr>
            <tr>
              <td>First Name</td>
              <td>{detail?.first_name}</td>
            </tr>
            <tr>
              <td>Last Name</td>
              <td>{detail?.last_name}</td>
            </tr>


            <tr>
              <td>Primary Address</td>
              <td>{detail?.address_line1}</td>
            </tr>
            <tr>
              {detail?.address_line2 &&
                <>
                  <td> Address 2</td>
                  <td>{detail?.address_line2}</td>
                </>
              }
            </tr>
            <tr>
              <td>City</td>
              <td>{detail?.city}</td>
            </tr>
            <tr>
              <td>State</td>
              <td>{detail?.state}</td>
            </tr>
            <tr>
              <td>Zip</td>
              <td>{detail?.zipcode}</td>
            </tr>
            <tr>
              <td>Country</td>
              <td>{detail?.country}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div className="biillingForm_Section">
        <h3>Billing Address</h3>
        <div className="check_data">
          <input type="checkbox" onClick={() => _onhandleChange(!isShippingSame)}
          /> &nbsp;
          Same as shipping address
        </div>
        <div className="field">
          <div className="text_field">
            <input
              type="text"
              name="firstName"
              placeholder="First Name"
              // value={dataName}
              // onChange={(e) => handleChange(e)}
              defaultValue={isShippingSame ? detail?.first_name : ""}
              ref={data?.register}

            />

            {!isShippingSame && data?.errors?.firstName && (
              <div className="error_card">
                {data?.errors?.firstName?.message}
              </div>
            )}

          </div>
          <div className="text_field">
            <input
              type="text"
              name="lastName"
              placeholder="Last Name"
              // onChange={(e) => handleChange(e)}
              defaultValue={isShippingSame ? detail?.last_name : ''}
              ref={data?.register}
            />
            {!isShippingSame && data?.errors?.lastName && (
              <div className="error_card">
                {data?.errors?.lastName?.message}
              </div>
            )}
          </div>
        </div>
        <div className="field">
          <div
            className="text_field">
            <input
              type="text"
              name="address"
              ref={data?.register}
              placeholder="Address"
              defaultValue={isShippingSame ? detail?.address_line1 : ''}
            />
            {!isShippingSame && data?.errors?.address && (
              <div className="error_card">{data?.errors?.address?.message}</div>
            )}
          </div>

          <div
            className="text_field">
            <input
              type="text"
              name="address2"
              ref={data?.register}
              placeholder="Address 2"
              defaultValue={isShippingSame ? detail?.address_line2 : ''}
            />
          </div>
        </div>

        <div className="field">
          <div className="text_field">
            <CountryDropdown
              value={countryValue}
              name="country"
              priorityOptions={['US', 'CA']}
              defaultOptionLabel='Select Country'
              valueType="short"
            // onChange={(val) => setCountryValue(val)}
            />
            {countryValue === undefined || countryValue === '' && <span className="error">{countryError}</span>}
          </div>
          <div className="text_field">
            <RegionDropdown
              country={countryValue || "US"}
              value={stateValue}
              name="state"
              valueType="full"
              defaultOptionLabel='Select State'
              disabled={!countryValue}
              countryValueType="short"
              onChange={(val) => setStateValue(val)}
            />
            {stateValue === undefined || stateValue === '' && <span className="error">{stateError}</span>}
          </div>
        </div>
        <div className="field">
          <div className="text_field">
            <input
              type="text"
              name="city"
              ref={data?.register}
              placeholder="Town/City"
              defaultValue={isShippingSame ? detail?.city : ''}
            />
            {!isShippingSame && data?.errors?.city && (
              <div className="error_card">{data?.errors?.city?.message}</div>
            )}
          </div>
          <div className="text_field">
            <input
              type="text"
              name="zipcode"
              placeholder="Zip Code"
              ref={data?.register}
              defaultValue={isShippingSame ? detail?.zipcode : ''}

            />
            {!isShippingSame && data?.errors?.zipcode && (
              <div className="error_card">{data?.errors?.zipcode?.message}</div>
            )}
          </div>
        </div>
        <div className="field">
          <div className="text_field">
            <input
              type="text"
              name="phoneNumber"
              placeholder="Phone "
              defaultValue={isShippingSame ? detail?.phone_no : ''}
              ref={data?.register}
            />
            {!isShippingSame && data?.errors?.phoneNumber && (
              <div className="error_card">
                {data?.errors?.phoneNumber?.message}
              </div>
            )}
          </div>
        </div>
      </div>
    </div>


  );
}
export default DetailSection;
