import api from "@api/Apis";
import { useEffect, useRef, useState } from "react";
import CustomPackTable from "../../cart/CustomPackTable";
import DetailSection from "./DetailSection";
import _ from "lodash";
import parse from 'html-react-parser';
import NumberFormatComp from "@Components/Common/NumberFormatComp";
import { Button, SignUpReviewPageCss } from "../SignupPageStyle";
import { useForm, FormProvider, useFormContext } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { toast } from "react-toastify";
import { FaTrash } from "react-icons/fa";
import EditIcon from '@material-ui/icons/Edit';
import { useRouter } from "next/router";
import {
  FormControl,
  FormControlLabel,
  InputLabel,
  Radio,
} from "@material-ui/core";
import DialogComponent from "@Components/Common/DialogComponent";
import { DialogSectionCss } from "@PagesComponent/ActiveSmartShip/ActiveStyleComp";
import PrivacyPolicy from "../../privacy-policy";
import PrivacyPolicyCss from "../../privacy-policy/PrivacyPolic.style";
import TermsConditions from "../../terms-conditions";
import TermsCondtionCss from "../../terms-conditions/TermsCondition.style";
import { pointer } from "d3";
import Discount from "@PagesComponent/Checkout/Discount";
import SmartshipPopup from "../../checkout/SmartshipPopup";
import CustomPackShow from "../CustomPackShow";
// import { useRouter } from "next/router";

function ReviewPage(props) {
  const {
    signupDefualtValue,
    handleBack,
    stepsOption,
    setshowloader,
    setActiveStep,
    setInnerLoader,
    checkCustomPack,
    setCheckCustomPack,
    minCartAutoshipCheck,
    setSmartShipCheck,
    smartShipCheck,
    setAddNotify,
    addNotify,
    ...other
  } = props;

  const { register, handleSubmit } = useForm();
  const [countryError, setCountryError] = useState();
  const [stateError, setStateError] = useState();


  const [totalPrice, setTotalPrice] = useState({
    subtotal: 0,
    total: 0,
  });

  const router = useRouter();
  const [isShippingSame, setIsShippingSame] = useState(false);
  const [orderProduct, setOrderProduct] = useState([]);
  const [billingAddress, setBillingAddress] = useState(null);
  const [userToken, setUserToken] = useState();
  const [countryValue, setCountryValue] = useState("");
  const [stateValue, setStateValue] = useState("");
  const [deleteCart, setDeleteCart] = useState("");
  const [shippingData, setShippingData] = useState([]);
  const [productsWeight, setProductWeight] = useState(0);
  const [selectedShipping, setSelectedShipping] = useState({
    id: null,
    amount: null,
  });
  const [dataShip, setDataShip] = useState();
  const [paymentDisable, setPaymentDisable] = useState(false);
  const [open, setOpen] = useState(false);
  const [openTerms, setOpenTerms] = useState();
  const [shippingError, setShippingError] = useState("");
  const [getData, setGetData] = useState();
  const [nextDate, setNextDate] = useState();
  const [smartshipOpen, setSmartShipOpen] = useState(false);
  const [couponData, setCouponData] = useState();
  const [shippingId, setShippingId] = useState();
  const [discountCoupon, setDiscountCoupon] = useState();
  const [couponError, setCouponError] = useState("");
  const [hideCoupon, setHideCoupon] = useState(false);
  const [radioError, setRadioError] = useState("");
  const [autoShipAmount, setAutoShipAmount] = useState(null);
  const [discountSection, setDisCountSection] = useState({
    discountAmount: null,
    freeShipping: false,
  });
  const schema = yup.object().shape({
    firstName: yup.string().required("Please enter your first name."),
    lastName: yup.string().required("Please enter your last name."),
    address: yup.string().required("Please enter your address."),
    address2: yup.string().notRequired(),
    city: yup.string().required("Please enter your city."),
    zipcode: yup.string().required("Please enter your zip code.").min(4, "Minimun 4 digits required"),
    phoneNumber: yup
      .string()
      .required("Please enter your phone Number.")
      .min(10, "Minimun 10 digits required"),
    cardHolder: yup.string().required("Please enter your card name."),
    termscondition3: yup.boolean().oneOf([true], "This field is required"),
    termscondition4: yup.boolean().oneOf([true], "This field is required"),
    termscondition5: yup.boolean().oneOf([true], "This field is required"),

    cardExpiry: yup
      .string()
      .required("Enter your expiry date.")
      .matches(
        /^(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{4})$/,
        "please enter date in this format MM/YYYY"
      ),
    cardNumber: yup
      .string()
      .required("Please enter your card number.")
      .matches(
        /^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/,
        "Please Enter Valid card Number"
      ),
    cardCVC: yup
      .string()
      .required("Enter CVV.")
      .min(4, "Enter minimum 4 digits")
    // .matches(/^[0-9]{3,4}$/, "Enter valid CVV."),
  });
  const validInputObj = {
    termscondition3: true,
    termscondition4: true,
    termscondition5: true,
  };

  const invalidInputObj = {
    termscondition3: true,
    termscondition4: true,
    termscondition5: true,
  };

  schema.isValid(validInputObj).then((isValid) => console.log()); // true
  schema.isValid(invalidInputObj).then((isValid) => console.log()); // false

  const [isAuto, setIsAuto] = useState(false);

  useEffect(() => {
    // setshowloader(true);
    // setInnerLoader(true)
    const promoterToken = localStorage.getItem("Token");
    _getAllProductData();
    setUserToken(promoterToken);
  }, [other?.cartdata, checkCustomPack]);

  const methods = useForm({
    mode: "all",
    resolver: yupResolver(schema),
  });

  const availableShippingModules = async (orderData, WEIGHT, totalPrice) => {
    const promoterToken = localStorage.getItem("Token");
    const SHIPPING_ADDRESS = localStorage.getItem("shippingAddress");
    const data = localStorage.getItem('packProduct');
    const jsonPraseData = data ? JSON.parse(data) : {};

    const payload = {
      // shipping_address_id: parseInt(+SHIPPING_ADDRESS),
      shipping_address_id: SHIPPING_ADDRESS,
      cart_items: orderData?.length + (jsonPraseData?.customPack) ? 1 : 0,
      cart_weight: (jsonPraseData?.customPack) ? +(jsonPraseData?.customPack?.totalWeight) + WEIGHT : WEIGHT,
      cart_total: totalPrice,
    };

    await api.availableShippingModules(promoterToken, payload).then((response) => {
      if (response.status === 200 && response?.data) {
        setInnerLoader(false)
        let data = [];
        Object.entries(response?.data).forEach(([key, value]) => {
          if (key !== "code") {
            data = [...data, ...value];
          }
        });
        setShippingData(data);
      }
      else {
        setInnerLoader(false)
      }
    })
      .catch((err) => console.log(err));
  };


  function _getAllProductData() {
    let totalPrice = checkCustomPack?.products?.length > 0 ? +checkCustomPack?.totalAmount : 0;
    let WEIGHT = 0;
    if (_.keys(other?.cartdata).length > 0) {
      setNextDate(other?.cartdata?.next_shipping_on);
      let data = _.map(other?.cartdata.products, ({ product }) => +product?.id);

      _.forEach(other?.cartdata?.products, (row) => {
        WEIGHT += parseFloat(
          +_.get(row, "product.weight", 0) * +row?.quantity
        );
      });
      setProductWeight(WEIGHT);
      // setCartData(other?.cartdata);

      const AUTOSHIP_PRICE = _.map(
        _.filter(other?.cartdata?.products, { is_autoship: "True" }),
        (row) => row?.product?.autoship_cost_price * +row?.quantity
      );
      const PRICE = _.map(
        _.filter(other?.cartdata?.products, { is_autoship: "False" }),
        (row) =>
          (AUTOSHIP_PRICE?.length > 0
            ? row?.product?.autoship_cost_price
            : row?.product?.cost_price) * row?.quantity
      );

      if (AUTOSHIP_PRICE?.length > 0) {
        setIsAuto(true);
        setSmartShipCheck(true);
        let totalAuto = 0;
        _.forEach(AUTOSHIP_PRICE, (row) => {
          totalAuto += row;
        });
        setAutoShipAmount(totalAuto);
      } else {
        setIsAuto(false);
        setSmartShipCheck(false);
      }
      _.forEach([...AUTOSHIP_PRICE, ...PRICE], (row) => {
        totalPrice += +row;
      });

      const ORDER_PASS_PRODUCT = _.map(other?.cartdata?.products, (cart) => ({
        is_autoship: cart?.is_autoship === "True" ? true : false,
        product_id: cart?.product.id,
        quantity: cart.quantity,
        variant_id: cart?.variant?.id ? cart.variant.id : null,
      }));

      setOrderProduct(ORDER_PASS_PRODUCT);
      setTotalPrice({ total: totalPrice, subtotal: totalPrice });
      // setshowloader(false);
      setInnerLoader(false);
      availableShippingModules(ORDER_PASS_PRODUCT, WEIGHT, totalPrice);
      props?.setcounts(other?.cartdata?.products?.length);

      setAddNotify(data);
    } else if (checkCustomPack?.products?.length > 0) {
      setTotalPrice({ total: totalPrice, subtotal: totalPrice });
      availableShippingModules([], 0, totalPrice);

    }
    else {
      setInnerLoader(false);
      // other?.setCartdata([]);
      setTotalPrice({ total: 0, subtotal: 0 });
      availableShippingModules([], 0, totalPrice);
      setInnerLoader(false);
      setShippingData([])

    }

    // api.getAllCartProduct(null).then((res) => {
    //   if (res?.data?.code === 1) {
    //     setNextDate(res?.data?.next_shipping_on);
    //     setInnerLoader(false);
    //     let data = _.map(res?.data.products, ({ product }) => +product?.id);
    //     setAddNotify(data);
    //   } else {
    //     setInnerLoader(false);
    //   }
    //   let totalPrice =
    //     checkCustomPack?.products?.length > 0
    //       ? +checkCustomPack?.totalAmount
    //       : 0;

    //   let WEIGHT = 0;
    //   if (res.data.code === 1) {
    //     _.forEach(res?.data?.products, (row) => {
    //       WEIGHT += parseFloat(
    //         +_.get(row, "product.weight", 0) * +row?.quantity
    //       );
    //     });
    //     setProductWeight(WEIGHT);
    //     setCartData(res?.data);

    //     const AUTOSHIP_PRICE = _.map(
    //       _.filter(res?.data?.products, { is_autoship: "True" }),
    //       (row) => row?.product?.autoship_cost_price * +row?.quantity
    //     );
    //     const PRICE = _.map(
    //       _.filter(res?.data?.products, { is_autoship: "False" }),
    //       (row) =>
    //         (AUTOSHIP_PRICE?.length > 0
    //           ? row?.product?.autoship_cost_price
    //           : row?.product?.cost_price) * row?.quantity
    //     );

    //     if (AUTOSHIP_PRICE?.length > 0) {
    //       setIsAuto(true);
    //       setSmartShipCheck(true);
    //       let totalAuto = 0;
    //       _.forEach(AUTOSHIP_PRICE, (row) => {
    //         totalAuto += row;
    //       });
    //       setAutoShipAmount(totalAuto);
    //     } else {
    //       setIsAuto(false);
    //       setSmartShipCheck(false);
    //     }

    //     _.forEach([...AUTOSHIP_PRICE, ...PRICE], (row) => {
    //       totalPrice += +row;
    //     });

    //     const ORDER_PASS_PRODUCT = _.map(res?.data?.products, (cart) => ({
    //       is_autoship: cart?.is_autoship === "True" ? true : false,
    //       product_id: cart?.product.id,
    //       quantity: cart.quantity,
    //       variant_id: cart?.variant?.id ? cart.variant.id : null,
    //     }));

    //     setOrderProduct(ORDER_PASS_PRODUCT);
    //     setTotalPrice({ total: totalPrice, subtotal: totalPrice });
    //     // setshowloader(false);
    //     setInnerLoader(false);

    //     availableShippingModules(ORDER_PASS_PRODUCT, WEIGHT, totalPrice);
    //     props?.setcounts(res?.data?.products?.length);
    //   } else {
    //     setTotalPrice({ total: totalPrice, subtotal: totalPrice });
    //     // setshowloader(false);
    //     availableShippingModules([], 0, totalPrice);
    //     setInnerLoader(false);

    //     setCartData([]);
    //   }
    // });
  }

  function _handleClick(id, shipping_cost) {
    setSelectedShipping({ id, amount: +shipping_cost });
    setTotalPrice({
      ...totalPrice,
      total: parseFloat(totalPrice.subtotal) + parseFloat(+shipping_cost),
    });
  }

  const onSubmit = (data) => {
    if (!selectedShipping?.id) {
      setRadioError("Please Select Shipping method");
      setTimeout(() => {
        setRadioError("");
      }, 4000);
    }
    if (smartShipCheck) {
      setGetData(data);
      setSmartShipOpen(true);
    } else {
      SmartShipData(data);
    }
  };
  function SmartShipData(datas) {
    let data = _.keys(datas)?.length > 0 ? datas : getData;
    const DATASHIP = JSON.parse(localStorage.getItem("shippingAddress"));
    setDataShip(DATASHIP);
    if (selectedShipping?.id) {
      let payload = {
        amount: totalPrice?.subtotal,
        amount_paid: totalPrice?.total,
        billing_address_id: isShippingSame ? DATASHIP : "",
        billing_details: {
          first_name: data?.firstName,
          last_name: data?.lastName,
          phone_number: data?.phoneNumber,
          city: data?.city,
          country: countryValue,
          state: stateValue,
          company: null,
          street_address_1: data?.address,
          street_address_2: data?.address2 ? data?.address2 : '',
          postal_code: data?.zipcode,
        },
        products: orderProduct,
        coupon: couponData ? couponData?.coupon_details?.code : null,
        coupon_id: null,
        customPack: checkCustomPack?.products?.length > 0 ? true : false,
        discount_amount: discountSection?.discountAmount,
        gross_total: totalPrice?.total,
        is_autoship: isAuto,
        is_kaire_cash_used: false,
        is_wallet_used: false,
        kaire_cash_used: "",
        order_for: "self",
        shipping_amount: selectedShipping?.amount,
        shipping_module_id: selectedShipping?.id,
        sku: "umb-20",
        tax_amount: 0,
        wallet_cash_used: "",
        order_total_bv: 4,
        saved_card_id: null,
        shipping_address_id: isShippingSame ? DATASHIP : DATASHIP,
        payment_details: {
          card_expiry_month: _.split(data?.cardExpiry, "/", 2)?.[0],
          card_expiry_year: _.split(data?.cardExpiry, "/", 2)?.[1],
          card_number: data?.cardNumber,
          card_owner_name: data?.cardHolder,
          card_type: "visa",
          security_code: data?.cardCVC,
        },
      };
      if (checkCustomPack?.products?.length > 0) {
        payload["packData"] = checkCustomPack?.products;
        payload["custompack_total_amount"] = checkCustomPack?.totalAmount;
      }

      // setInnerLoader(true);
      setPaymentDisable(true);
      setInnerLoader(true);
      api.createOrder(payload, userToken, "self").then((res) => {
        setPaymentDisable(true);
        if (res?.data?.code === 1) {
          setPaymentDisable(false);
          setOrderProduct([]);
          localStorage.removeItem("PromoterPack");
          localStorage.removeItem("profileDetail");
          localStorage.removeItem("parentrefferalPromoter");
          localStorage.removeItem("packProduct");
          localStorage.removeItem("shippingAddress");
          localStorage.removeItem("referral_code");
          localStorage.removeItem("correctRefferal");

          props?.setcounts();
          setInnerLoader(false);
          // toast.success(res?.data?.message, { duration: 3 });
          router.push({
            pathname: `/${router.query.page}/order/thankYou`,
            query: { orderid: res?.data.order_id },
          });
        } else {
          setInnerLoader(false);
          setPaymentDisable(false);
          // toast.warn(res?.data?.message, { duration: 3 });
        }
        if (res?.data?.code === 0) {
          setShippingError(res?.data?.message);
          setTimeout(() => {
            setShippingError("");
          }, 3000);
        }
      });
    }
    // toast.warn('please select shipping model')
  }

  const handleDelete = (id, is_autoship) => {
    api.deleteProductFromAddToCart({ product_id: id, is_autoship: is_autoship }).then((res) => {
      if (res?.data?.code === 1) {
        setInnerLoader(false)
        other?.getMiniCartProduct("delete", "");
        _getAllProductData();
        localStorage.removeItem("smartship")
      }
    });
  }

  function _onhandleChange(status) {
    if (status) {
      setCountryValue(signupDefualtValue?.country);
      setStateValue(signupDefualtValue?.state);
    } else {
      setCountryValue("");
      setStateValue("");
    }
    setIsShippingSame(status);
  }

  const handleCountryStateError = () => {
    if (
      countryValue === undefined ||
      (stateValue === "" && countryError === undefined)
    ) {
      setCountryError("Please Select The Country");
    }
    if (
      stateValue === undefined ||
      (stateValue === "" && stateError === undefined)
    ) {
      setStateError("Please Select The State");
    }
  };

  const objIndex = other?.cartdata?.products?.findIndex(
    (obj) => obj.is_autoship == "True"
  );

  useEffect(() => {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }, [])

  useEffect(() => {
    document.body.classList.add('signupform-data');
    if (discountSection?.freeShipping) {
      setDisCountSection({
        currentTotalAmount: totalPrice?.total - selectedShipping?.amount,
        discountAmount: selectedShipping?.amount,
        freeShipping: true,
      });
    } else if (discountSection?.discountAmount) {
      setDisCountSection({
        ...discountSection,
        currentTotalAmount: totalPrice?.total - discountSection?.discountAmount,
      });
    }
  }, [selectedShipping?.amount]);

  // for apply discount coupon-----------
  const handleCoupon = (couponId) => {
    const shippingId = localStorage.getItem("shippingAddress");
    setShippingId(shippingId);
    let orderdata = {
      shipping_id: shippingId ? shippingId : null,
      coupon: couponId,
    };
    if (couponId) {
      api.verifyCopan(orderdata, userToken).then((res) => {
        let freeShippingStatus = false;
        if (res?.data?.code === 1) {
          setCouponData(res?.data);
          setHideCoupon(true);
          if (res?.data?.coupon_details?.discount_type === 1) {
            setDisCountSection({
              currentTotalAmount:
                totalPrice?.total - +res?.data?.coupon_details.discount_amount,
              discountAmount: +res?.data?.coupon_details.discount_amount,
              freeShipping: false,
            });
            // setFreeShippingCoupon(false)
          }
          if (res?.data?.coupon_details?.discount_type === 2) {
            let discountAmount =
              (+totalPrice?.total *
                +res?.data?.coupon_details.discount_percent) /
              100;
            setDisCountSection({
              currentTotalAmount: +totalPrice?.total - +discountAmount,
              discountAmount: discountAmount,
              freeShipping: false,
            });
          }
          if (res?.data?.coupon_details.free_shipping === true) {
            setDisCountSection({
              currentTotalAmount: null,
              discountAmount: null,
              freeShipping: true,
            });
          }
        }
        if (res?.data?.code === 0) {
          setCouponError(res?.data?.message);
          setTimeout(() => {
            setCouponError("");
          }, 4000);
        }
      });
    }
  };

  const removecoupon = () => {
    setDisCountSection("");
    setHideCoupon(false);
  };

  function handleEdit() {
    if (stepsOption?.length === 5) {
      setActiveStep(3)
    }
    else {
      setActiveStep(2)
    }
  }
  function editMemberpack() {
    setActiveStep(1)
  }

  function deleteMemberPack(id, is_autoship) {
    api.deleteProductFromAddToCart({ product_id: id, is_autoship: is_autoship }).then((res) => {
      if (res?.data?.code === 1) {
        other?.getMiniCartProduct("delete", "");
        _getAllProductData();
        localStorage.removeItem("PromoterPack")
      }
    });
  }

  function editCustomPack() {
    setActiveStep(2)
  }
  function deleteCustomPack() {
    _getAllProductData();
    setCheckCustomPack()
    localStorage.removeItem("packProduct")
  }

  function PreviousButton() {
    setActiveStep((prevActiveStep) => {
      return prevActiveStep - 1
    })
  }

  const currentRef = useRef(null);

  return (
    <>
      <SignUpReviewPageCss>
        <div className="main-bx-pack">
          <h2>Review Your MemberShip</h2>
          <div className="container signUpReview_page">
            <FormProvider {...methods}>
              <form onSubmit={methods.handleSubmit(onSubmit)}>
                <DetailSection
                  detail={signupDefualtValue}
                  setBillingAddress={setBillingAddress}
                  countryValue={countryValue}
                  stateValue={stateValue}
                  setCountryValue={setCountryValue}
                  setStateValue={setStateValue}
                  countryError={countryError}
                  _onhandleChange={(value) => _onhandleChange(value)}
                  stateError={stateError}
                  setStateError={setStateError}
                  isShippingSame={isShippingSame}
                  setIsShippingSame={setIsShippingSame}
                />
                {other?.cartdata?.products?.length > 0 || (checkCustomPack?.products?.length > 0 && (
                  <>
                    {!hideCoupon && (
                      <div className="apply-coupn">
                        <label>
                          <b> COUPON CODE </b>
                        </label>
                        <div className="row">
                          <div className="col-md-12">
                            <div className="signupform">
                              <input
                                type="text"
                                ref={currentRef}
                                className="form-control"
                                name="coupon_id"
                                placeholder="coupon"
                              />
                              <button
                                type="button"
                                className="btn btn-primary"
                                onClick={() =>
                                  handleCoupon(currentRef?.current?.value)
                                }
                              >
                                Apply
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                    {couponError && <span className="error">{couponError}</span>}
                    {!hideCoupon ||
                      (couponData && (
                        <div className="coupon_dlt">
                          {couponData?.coupon_details?.name} Coupon is Added
                          <FaTrash onClick={() => removecoupon()}></FaTrash>
                        </div>
                      ))}
                  </>
                ))}
                {checkCustomPack?.products?.length > 0 && (
                  <>
                    <CustomPackShow customPackProduct={checkCustomPack} setCustomerPackProduct={setCheckCustomPack} />
                    <div className="edit-del d-flex">
                      <div className="edit">
                        <a className="btn-primary-btn" onClick={() => editCustomPack()}>Edit <EditIcon /></a>
                      </div>
                      <div className="edit">
                        <a className="btn-primary-btn" onClick={() => deleteCustomPack()}>Delete <FaTrash /></a>
                      </div>
                    </div>
                  </>
                )}
                {(_.filter(other?.cartdata?.products, (row) => row?.category === "Member Pack"))?.length > 0 &&
                  <div className="main-bx-pack-member">
                    <h2>YOUR MEMBER PACK</h2>
                    <div className="member-pack">
                      {
                        _.map(_.filter(other?.cartdata?.products, (row) => row?.category === "Member Pack"), (row, index) => {
                          return (
                            <>
                              <div className="main-flex">
                                <div className="image_div">
                                  {
                                    <img src={process.env.DOC_URL + row?.product?.product_images[0]?.image} alt="helo" />
                                  }
                                </div>
                                <div className="content-right">
                                  <div className="product_name">{row?.product?.name}</div>
                                  <div className="product_price">${row?.product?.cost_price}</div>
                                  <div className="product-des">{row?.product?.description ? parse(row?.product?.description) : ""}</div>
                                </div>
                              </div>
                              <div className="edit-del d-flex">
                                <div className="edit">
                                  <a className="btn-primary-btn" onClick={() => editMemberpack()}>Edit <EditIcon /></a>
                                </div>
                                <div className="edit">
                                  <a className="btn-primary-btn" onClick={() => deleteMemberPack(row?.product?.id, row?.is_autoship)}>Delete <FaTrash /></a>
                                </div>
                              </div>
                            </>
                          )
                        })
                      }
                    </div>

                  </div>
                }
                {(_.filter(other?.cartdata?.products, (row) => row?.category !== "Member Pack"))?.length > 0 &&
                  <div className="table_responsive">
                    <h2> YOUR ADDED PRODUCT/SMARTSHIPS</h2>
                    <table className="productDetailTable table_container">
                      {(_.filter(other?.cartdata?.products, (row) => row?.category !== "Member Pack"))?.length > 0 &&
                        <thead>
                          <tr>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Subtotal</th>
                            <th >Actions</th>

                          </tr>
                        </thead>
                      }
                      <tbody>
                        {_.map(_.filter(other?.cartdata?.products, (row) => row?.category !== "Member Pack"), (row, index) => {
                          let AUTOSHIP_CHECK = objIndex >= 0 ? true : false;
                          let PRICE = AUTOSHIP_CHECK ? row?.product?.autoship_cost_price : row?.product?.cost_price;
                          return (
                            <tr key={row?.product?.name}>
                              <td data-value="Product">
                                <div className="product_nameSection">
                                  <div className="productImage">
                                    {row?.product?.product_images?.length >
                                      0 && (
                                        <img
                                          src={
                                            process.env.DOC_URL +
                                            row?.product?.product_images[0]?.image
                                          }
                                        />
                                      )}
                                  </div>
                                  <div className="title">
                                    <h3>{row?.product?.name}</h3>
                                  </div>
                                </div>
                              </td>
                              <td data-value="Price">
                                <NumberFormatComp value={PRICE} />
                                {row?.is_autoship === "False"
                                  ? ""
                                  : "Smartship"}
                              </td>
                              <td data-value="Quantity">
                                <div>{row?.quantity}</div>
                              </td>
                              <td data-value="Subtotal">
                                <NumberFormatComp
                                  value={PRICE * +row?.quantity}
                                />
                              </td>
                              <td data-value="Actions">
                                <Button type="button" onClick={() => handleDelete(row?.product?.id, row?.is_autoship)}><FaTrash /></Button>
                                <Button type="button" onClick={() => handleEdit()}><EditIcon /></Button>
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </div>
                }
                <>
                  <div className="payment-method">
                    <h2>PAYMENT METHOD AND SHIPPING</h2>
                  </div>
                  <div className="main">
                    <div className="col-md-6 cart_form">
                      <h3>PAYMENT METHOD</h3>
                      <div className="logo">
                        <img src="/images/creditcard.png/" />
                      </div>
                      <div className="row section_first">
                        <div>
                          <input
                            type="text"
                            placeholder="Card Holder Name"
                            name="cardHolder"
                            ref={methods.register}
                          />
                          {methods.errors?.cardHolder && (
                            <span className="error_card">
                              {methods.errors?.cardHolder?.message}
                            </span>
                          )}
                        </div>
                        <div>
                          <input
                            type="text"
                            placeholder="Card Number"
                            name="cardNumber"
                            ref={methods.register}
                          />
                          {methods.errors?.cardNumber && (
                            <span className="error_card">
                              {methods.errors?.cardNumber?.message}
                            </span>
                          )}
                        </div>
                      </div>
                      <div className="row section_second">
                        <div>
                          <input
                            type="text"
                            placeholder="Card Expiry (MM/YYYY)"
                            name="cardExpiry"
                            ref={methods.register}
                          />
                          {methods.errors?.cardExpiry && (
                            <span className="error_card">
                              {methods.errors?.cardExpiry?.message}
                            </span>
                          )}
                        </div>
                        <div>
                          <input
                            type="text"
                            placeholder="Card CVV"
                            name="cardCVC"
                            ref={methods.register}
                          />
                          {methods.errors?.cardCVC && (
                            <span className="error_card">
                              {methods.errors?.cardCVC?.message}
                            </span>
                          )}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-6 totalAmount_section">
                      <div className="Amountinner_div">
                        <table>
                          <tbody>
                            <tr>
                              <td>Order Total</td>
                              <td>
                                <NumberFormatComp
                                  value={totalPrice?.subtotal}
                                />
                              </td>
                            </tr>
                            <tr>
                              {totalPrice?.subtotal > 0 && (
                                <>
                                  <td>Shipping</td>
                                  <td>
                                    <div className="expand-row">
                                      Choose a Shipping method:
                                    </div>
                                    <div className="main_shipping_method_class">
                                      <div>
                                        {shippingData?.length > 0 && (
                                          <InputLabel
                                            style={{ fontSize: "16px" }}
                                            htmlFor="filled-age-native-simple"
                                            className="choose_label"
                                            required
                                          ></InputLabel>
                                        )}
                                      </div>
                                      <div>
                                        <div className="radio_section_for_shipping_cost_gsn">
                                          {shippingData?.length > 0 ? (
                                            shippingData?.map(
                                              (data, index) => {
                                                return (
                                                  <FormControl
                                                    key={index + 1}
                                                    className="radio_option"
                                                  >
                                                    <FormControlLabel
                                                      value="end"
                                                      control={
                                                        <Radio
                                                          name="radioo"
                                                          value={
                                                            data?.shipping_cost
                                                          }
                                                          checked={
                                                            selectedShipping?.id ===
                                                            data?.id
                                                          }
                                                          onChange={() =>
                                                            _handleClick(
                                                              data?.id,
                                                              data?.shipping_cost
                                                            )
                                                          }
                                                          inputProps={{
                                                            "aria-label": "A",
                                                          }}
                                                        />
                                                      }
                                                      label={
                                                        <p>{`${data?.name}: ${data?.shipping_cost}`}</p>
                                                      }
                                                    />
                                                  </FormControl>
                                                );
                                              }
                                            )
                                          ) : (
                                            <span>
                                              No shipping method on this
                                              address.
                                            </span>
                                          )}
                                        </div>
                                        {radioError && (
                                          <span className="error">
                                            {radioError}
                                          </span>
                                        )}
                                      </div>
                                    </div>
                                  </td>
                                </>
                              )}
                            </tr>
                            {discountSection?.discountAmount && (
                              <tr>
                                <td>
                                  Discount Amount<br></br> (
                                  {couponData?.coupon_details?.name}):
                                </td>
                                <td>
                                  <div className="total">
                                    <NumberFormatComp
                                      value={-discountSection?.discountAmount}
                                    />
                                  </div>
                                </td>
                              </tr>
                            )}
                            <tr>
                              <td>Total</td>
                              <td>
                                <div className="total">
                                  <NumberFormatComp
                                    value={
                                      discountSection?.currentTotalAmount ||
                                      totalPrice?.total
                                    }
                                  />
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div className='save-credit-check'>
                      <input type="checkbox"
                        ref={methods?.register({ required: false })}
                        className="form-check-input"
                        id="save_credit_card"
                        name="save_credit_card"
                      />  Save Credit Card
                    </div>
                  </div>
                  <div className="checkbox">
                    <div className="form-group">
                      <input
                        type="checkbox"
                        className="form-check-input"
                        id="termsconditions"
                        name="termscondition3"
                        ref={methods.register}
                      />
                      <label
                        className="form-check-label"
                        for="termsconditions"
                      ></label>
                      <div className="policy">
                        I have read and agree to the
                        <a className="privacy_style" target='_blank' href='https://admin.shopkaire.com/media/pdf/Kaire_ReturnPolicy.pdf'>Return Policy</a>,
                        <a className="privacy_style" target='_blank' href='https://admin.shopkaire.com/media/pdf/Kaire_PrivacyPolicy.pdf'> Privacy Policy</a>, and
                        <a className="privacy_style" target='_blank' href='https://admin.shopkaire.com/media/pdf/Kaire_TermsConditions.pdf'> Terms & Conditions </a>
                      </div>

                      {methods.errors.termscondition3 && (
                        <div style={{ color: "red" }}>
                          {methods.errors.termscondition3?.message}
                        </div>
                      )}
                    </div>

                    <div className="form-group">
                      <input
                        type="checkbox"
                        className="form-check-input"
                        id="certify"
                        name="termscondition4"
                        ref={methods.register}
                      />
                      <label
                        className="form-check-label"
                        for="certify"
                      ></label>
                      <p>
                        I certify that I am atleast 18 years old and eligiable
                        to purchase this product.
                      </p>

                      {methods.errors.termscondition4 && (
                        <div style={{ color: "red" }}>
                          {methods.errors.termscondition4?.message}
                        </div>
                      )}
                    </div>

                    {smartShipCheck && (
                      <div className="form-group">
                        <input
                          type="checkbox"
                          className="form-check-input"
                          id="acknowledging"
                          name="termscondition5"
                          ref={methods.register}
                        />

                        <label
                          className="form-check-label"
                          for="acknowledging"
                        ></label>
                        <p>
                          {" "}
                          By acknowledging, I am agreeing to an annual
                          automatic renewal purchase.This purchase will be
                          automatically processed each month on the Smartship
                          Order renewal date.
                        </p>

                        {methods.errors.termscondition5 && (
                          <div style={{ color: "red" }}>
                            {methods.errors.termscondition5?.message}
                          </div>
                        )}
                      </div>
                    )}
                  </div>
                  <div className="checkoutButton">
                    <Button onClick={() => PreviousButton()}>Back</Button>
                    <Button
                      type="submit"
                      disabled={paymentDisable}
                      onClick={() => handleCountryStateError()}
                    >
                      {paymentDisable ? "Loading..." : " Process Payment"}
                    </Button>

                    {shippingError && (
                      <span className="error">{shippingError}</span>
                    )}
                  </div>
                </>

              </form>
            </FormProvider>
          </div>
        </div>
      </SignUpReviewPageCss>
      <DialogComponent
        opend={open}
        handleClose={() => setOpen(false)}
        title="Privacy Policy"
        classFor="order_details"
      >
        <PrivacyPolicyCss>
          <PrivacyPolicy />
        </PrivacyPolicyCss>
      </DialogComponent>
      <DialogComponent
        opend={openTerms}
        handleClose={() => setOpenTerms(false)}
        title="Privacy Policy"
        classFor="order_details"
      >
        <TermsCondtionCss>
          <TermsConditions />
        </TermsCondtionCss>
      </DialogComponent>

      <SmartshipPopup
        open={smartshipOpen}
        nextDate={nextDate}
        paidamount={autoShipAmount || totalPrice?.total}
        orderData={(value) => SmartShipData(value)}
        setOpen={(value) => setSmartShipOpen(value)}
        title="Smartship Alert"
      />
    </>
  );
}

export default ReviewPage;
