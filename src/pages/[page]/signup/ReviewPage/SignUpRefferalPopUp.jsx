import api from '@api/Apis';
import DialogComponent from '@Components/Common/DialogComponent';
import { CurrentRefferal, RefferalContainer } from '@Components/ViewCartPopup/ViewCartPop.style';
import React from 'react'
import { Button } from '../SignupPageStyle';

function SignUpRefferalPopUp(props) {
    const { states } = props;

    const handleSubmit = async () => {
        if (states?.refferalCode?.value) {
            await api.subDomainCheckFunction({ referral_code: states?.refferalCode?.value }).then((res) => {
                if (res?.status === 200) {
                    if (res?.data?.status === true) {
                        const { user_details } = res?.data?.data;
                        states?.setCorrectRefferal({
                            status: res?.data?.status,
                            data: {
                                image: user_details?.image,
                                name: user_details?.name,
                                email: user_details?.email,
                                phone: user_details?.phone
                            }
                        });
                    } else {
                        states?.setRefferalCode({ ...states?.refferalCode, error: 'This Referrer Code is invalid. Please check with your Referrer and try again.', });
                        states?.setCorrectRefferal({ status: res?.data?.status, data: {} });
                    }
                }
            }).catch((err) => console.log(err))
        } else {
            states?.setRefferalCode({ ...states?.refferalCode, error: 'please enter refferal code', })
        }
    }
    const handlePopup = () => {
        states?.setOpenRefer(false)
        states?.setRefferalHide(true)
        localStorage.removeItem("correctRefferal")
        localStorage.removeItem("referral_code")
    }
    return (
        <DialogComponent opend={states?.openRefer} handleClose={states?.setOpenRefer} title="Kaire" classFor="view_cart_popup">
            {(states?.openRefer && !states?.correctRefferal?.status) &&
                <RefferalContainer>
                    <h3 className="ref_title"> <img src='/images/welcome.png' /></h3>
                    <h4 className="ref_subtitle">Please enter your referring member's Referrer Code:</h4>
                    <div className="text-container">
                        <div className="ref_input-form">
                            <input type="text" className="form-control"
                                name="referring_id"
                                defaultValue={states?.refferalCode?.value}
                                placeholder="Referrer code"
                                onChange={(e) => {
                                    states?.setRefferalCode({ value: e.target.value, error: "" });
                                }} />
                        </div>
                        <div className="sk-ref__input-btn">
                            <Button type="button" className="ref_input-button" onClick={() => handleSubmit()}>Submit</Button>
                        </div>
                    </div>

                    <p className="dont_haveCode" onClick={() => handlePopup()}>I don't have referrer code</p>

                    <p className="ref_msg_new"><span>Not sure what this means?</span> Send an email with subject line 'Referrer Code' to <a className="ref__mailto" href="mailto:support@kaire.com">support@kaire.com</a></p>
                    <p className="error_msg">{states?.refferalCode?.error ? states?.refferalCode?.error : ""}</p>
                </RefferalContainer >
            }
            {(states?.openRefer && states?.correctRefferal?.status) &&
                <CurrentRefferal className="currentRef_body">
                    <h5 className="refby-title">You were referred to Kaire by:</h5>
                    <img src="/images/placeholder.png" className="pv-refby-img img-circle" />
                    <h4 className="refby-name">{states?.correctRefferal?.data?.name}</h4>
                    <div className="refby-contact">
                        <a href={`tel:${states?.correctRefferal?.data?.phone}`} className="refby-contact-info ">{states?.correctRefferal?.data?.phone}</a>
                        <a href={`mailto:${states?.correctRefferal?.data?.email}`} className="refby-contact-info">{states?.correctRefferal?.data?.email}</a>
                    </div>
                    <div className="refby-btn">
                        <Button className="refby_button" onClick={() => {
                            localStorage.setItem("parentrefferalPromoter", states?.refferalCode?.value);
                            states?.setOpenRefer(false);
                        }}>Yes, This is my referrer</Button>

                    </div>
                    <button className="ref-confirm-no" onClick={() => states?.setCorrectRefferal({ ...states?.correctRefferal, status: false })}>
                        No, this is not my referrer
                    </button>
                    <p className="refby_note">Please note, your referrer cannot be changed after your account is created.</p>
                </CurrentRefferal>
            }
        </DialogComponent >
    )
}

export default SignUpRefferalPopUp