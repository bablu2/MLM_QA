import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState, useRef } from "react";
import { useForm } from "react-hook-form";
import api from "../../../api/Apis";
// import { toast } from 'react-nextjs-toast'
import { toast } from "react-toastify";
import { FaInfo } from "react-icons/fa";

import {
  CountryDropdown,
  RegionDropdown,
  CountryRegionData,
} from "react-country-region-selector";
import SignupPageStyle, { Button } from "./SignupPageStyle";
import ReCAPTCHA from "react-google-recaptcha";
import SvgAnimationBackground from "@Components/Common/SvgAnimationBackground";
import SignUpRefferalPopUp from "./ReviewPage/SignUpRefferalPopUp";
import { AiFillEye } from "react-icons/ai";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import { ro } from "date-fns/locale";
import { Tooltip } from "@material-ui/core";
// import { GoogleReCaptcha, GoogleReCaptchaProvider } from "react-google-recaptcha-v3";

function SignUpForm({
  setDefaultValue,
  setActiveStep,
  dataReferer,
  setDataRefer,
  referralHide,
  setRefferalHide,
  ...props
}) {
  const { signUpRefferal,
    validReferralCheck,
    signupDefualtValue,
    setUserToken,
    promoterPackAvailability,
    subDomain,
    LoginSubmit,
  } = props;
  const [credential, setCredential] = useState({
    email: "",
    password: "",
  });
  const { correctRefferal, setCorrectRefferal } = signUpRefferal;
  const router = useRouter();
  const { register, handleSubmit, watch, errors, setError, getValues, reset } =
    useForm({
      mode: "all",
      defaultValues: signupDefualtValue,
    });
  const password = useRef({});
  password.current = watch("password", "");
  const [emailError, setEmailError] = useState("");
  const [reSellererror, setResellerError] = useState("");
  const [countryError, setCountryError] = useState();
  const [openRefer, setOpenRefer] = useState(false);
  const [refferalCode, setRefferalCode] = React.useState({
    value: "",
    error: "",
  });
  const [stateError, setStateError] = useState();
  const [country, setCountry] = useState("");
  const [state, setState] = useState("");
  const [captchaToken, setCaptchaToken] = useState(null);
  const [parentRefferal, setParentReferral] = useState("");
  const [passwordShow, setPasswordShow] = useState(false);
  const [showConfirmpass, setShowConfirmPass] = useState(false);
  // const [termsMobile, setTermsMobile] = useState(false)
  // const [termsEmail, setTermsEmail] = useState(false)


  useEffect(() => {
    const refferal = localStorage.getItem("referral_code")
      ? localStorage.getItem("referral_code")
      : "";
    setRefferalCode({ value: refferal, error: "" });
    // const DataParent = localStorage.getItem('parentrefferalPromoter')
    // setParentData(DataParent)

    // if (subDomain) {
    //   localStorage.setItem("referral_code", subDomain);
    // } else {
    //   localStorage.setItem("referral_code", refferal);
    // }
    if (refferal) {
      const RefferalData = localStorage.getItem("correctRefferal") ? JSON.parse(localStorage.getItem("correctRefferal")) : "";
      setCorrectRefferal(RefferalData);
    }
    if (signupDefualtValue?.country) {
      setCountry(signupDefualtValue?.country);
      if (signupDefualtValue?.state) {
        setState(signupDefualtValue?.state);
      }
    }
  }, []);

  const handleCountryStateError = () => {
    if (
      country === undefined ||
      (country === "" && countryError === undefined)
    ) {
      setCountryError("Please Select The Country");
    }
    if (state === undefined || (state === "" && stateError === undefined)) {
      setStateError("Please Select The State");
    }
  };
  // to signup user
  function onSubmit(data) {
    if (country !== undefined && state !== undefined && country !== "" && state !== "") {
      data.country = country;
      data.state = state;
      data["confirm_password"] = data.password;
      data["parentrefferalPromoter"] = refferalCode.value;

      // data['referral_code'] = subDomain !== ""
      //     ?
      //     subDomain
      //     :
      //     parentrefferalPromoter
      //         ?
      //         parentrefferalPromoter
      //         :
      //         data['referral_code'];

      data["designation"] =
        router?.query?.d === "member" ? router?.query?.d : "customer";
      data["leadid"] = localStorage.getItem("leadid") ? JSON.parse(localStorage.getItem("leadid")) : null;
      // props?.setshowloader(true);
      // if (captchaToken === null) {
      // toast.warn("please fill the captcha")
    }



    // if (captchaToken !== null) {
    props?.setInnerLoader(true);
    api.signUp(data).then((res) => {
      if (res?.data?.code === 1) {
        const data_sign = { email: data.email, password: data.password };
        setCredential({ email: data.email, password: data.password });
        api.loginUser(data_sign).then((res) => {
          if (res?.data?.code === 1) {
            setUserToken(res.data.token);
            localStorage.setItem("Token", res.data.token);
            props?.setInnerLoader(false);
            setActiveStep((prvStep) => {
              return prvStep + 1;
            });
          }
        });
        // props?.setshowloader(false);

        localStorage.setItem("profileDetail", JSON.stringify(data));
        localStorage.setItem("shippingAddress", res?.data?.shipping_address_id);
        setDefaultValue(data);
        // toast.success(res?.data?.message, { duration: 5 })
        // router.push('/')
        setEmailError("");
        setRefferalCode("");
        setParentReferral("");
      } else {
        props?.setInnerLoader(false);
      }
      if (res?.data?.code === 0 && res?.data?.message) {
        setError(res?.data?.field, {
          type: res?.data?.field,
          message: res?.data?.message,
        });
        props?.setInnerLoader(false);
      }
    });
    // }
    // }
    // }
  }
  function showPassword() {
    setPasswordShow(!passwordShow);
  }
  function handleshowConfirmPassword() {
    setShowConfirmPass(!showConfirmpass);
  }
  useEffect(() => {
    document.body.classList.add('signupform-data');
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }, [])

  return (
    <>
      <Head>
        <title>Signup</title>
      </Head>
      <SignupPageStyle>
        <div className="container main_sign_up_form_class">
          {props?.isLogin !== true && (
            <form
              className="signupform main-sign-frm"
              onSubmit={handleSubmit(onSubmit)}
            >
              <SignUpRefferalPopUp
                states={{
                  refferalCode,
                  setRefferalCode,
                  openRefer,
                  setOpenRefer,
                  referralHide,
                  setRefferalHide,
                  ...signUpRefferal,
                }}
              />

              <h1 className="title">Signup</h1>
              <div className="main-cstm">
                {correctRefferal?.data?.name && !referralHide ? (
                  <a onClick={() => setOpenRefer(true)}>
                    Enroller:{correctRefferal?.status &&
                      `${correctRefferal?.data?.name} (Change)`}
                  </a>
                ) :
                  <>
                    {subDomain &&
                      <a>Enroller:{subDomain}</a>
                    }
                  </>
                }
              </div>

              {/* <h3 className="response message">{responsedata && responsedata}</h3> */}
              <div className="mb-3 field-class">
                <input
                  type="text"
                  className="form-control"
                  name="first_name"
                  id="f_name"
                  aria-describedby="nameHelp"
                  placeholder="First Name"
                  ref={register({
                    required: "This field is required",
                    // pattern: {
                    //     value: /^[A-Za-z][A-Za-z0-9_]{2,29}$/,
                    //     message: "Please enter valid name"
                    // },
                    minLength: {
                      value: 3,
                      message: "Enter minimum 3 character",
                    },
                  })}
                />
                {errors.first_name && (
                  <span className="error">{errors.first_name.message}</span>
                )}
              </div>
              <div className="mb-3 field-class">
                <input
                  type="text"
                  className="form-control"
                  name="last_name"
                  id="l_name"
                  aria-describedby="nameHelp"
                  placeholder="Last Name"
                  ref={register({
                    required: "This field is required",
                    // pattern: {
                    //     value: /^[A-Za-z][A-Za-z0-9_]{2,29}$/,
                    //     message: "Please enter valid name"
                    // },
                    minLength: {
                      value: 3,
                      message: "Enter minimum 3 character",
                    },
                  })}
                />
                {errors.last_name && (
                  <span className="error">{errors.last_name.message}</span>
                )}
              </div>

              <div className="mb-3 field-class">
                <input
                  type="text"
                  className="form-control"
                  name="email"
                  id="email"
                  aria-describedby="emailnoHelp"
                  placeholder="Email Address"
                  ref={register({
                    required: "This field is required",
                    pattern: {
                      value:
                        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
                      message: "Invalid email address",
                    },
                    validate: (value) =>
                      !!value.trim() || "This field is required",
                  })}
                />
                {errors.email && (
                  <span className="error">{errors.email.message}</span>
                )}
              </div>
              <div className="mb-3 field-class">
                <input
                  type="text"
                  className="form-control"
                  name="phone_no"
                  id="phone_no"
                  aria-describedby="emailnoHelp"
                  placeholder="Phone Number"
                  ref={register({
                    required: "This field is required",
                    pattern: {
                      value: /^[0-9]*$/,
                      message: "Enter only number",
                    },
                    validate: (value) =>
                      value.match(/^(\+\d{1,3}[- ]?)?\d{10}$/) ||
                      "Please enter 10 digit.",
                  })}
                />
                {errors.phone_no && (
                  <span className="error">{errors.phone_no.message}</span>
                )}
              </div>
              <div className="mb-3 field-class">
                <input
                  type={passwordShow ? "text" : "password"}
                  name="password"
                  className="form-control"
                  id="password"
                  placeholder="Password"
                  ref={register({
                    required: "This field is required",
                    minLength: {
                      value: 6,
                      message: "Passwords must be at least 6 characters",
                    },
                  })}
                />
                {passwordShow ? (
                  <Visibility onClick={() => showPassword()} />
                ) : (
                  <VisibilityOff onClick={() => showPassword()} />
                )}
                {errors?.password && (
                  <span className="error">{errors.password?.message}</span>
                )}
              </div>

              <div className="mb-3 field-class">
                <input
                  type={showConfirmpass ? "text" : "password"}
                  className="form-control"
                  name="confirm_password"
                  id="exampleInputPassword3"
                  placeholder="Confirm Password"
                  ref={register({
                    required: "This field is required",
                    validate: (value) =>
                      getValues("password") === value ||
                      "Please enter same password",
                  })}
                />
                {showConfirmpass ? (
                  <Visibility onClick={() => handleshowConfirmPassword()} />
                ) : (
                  <VisibilityOff onClick={() => handleshowConfirmPassword()} />
                )}

                {errors.confirm_password && (
                  <span className="error">
                    {errors.confirm_password?.message}
                  </span>
                )}
              </div>
              <div className="mb-3 field-class">
                <div className="select">
                  <CountryDropdown
                    value={country}
                    defaultOptionLabel="Country"
                    name="country"
                    priorityOptions={['US', 'CA']}
                    valueType="short"
                    onChange={(val) => setCountry(val)}
                  />
                </div>
                {country === undefined ||
                  (country === "" && (
                    <span className="error">{countryError}</span>
                  ))}
              </div>
              <div className="mb-3 field-class">
                <div className="select">
                  <RegionDropdown
                    country={country || "USA"}
                    disabled={!country}
                    value={state}
                    name="state"
                    valueType="full"
                    defaultOptionLabel="State"
                    countryValueType="short"
                    onChange={(val) => setState(val)}
                  />
                </div>
                {state === undefined ||
                  (state === "" && <span className="error">{stateError}</span>)}
              </div>
              <div className="mb-3 field-class">
                <input
                  type="text"
                  name="address_line1"
                  className="form-control"
                  id="address1"
                  placeholder="Address Line"
                  ref={register({
                    required: "This field is required",
                  })}
                />
                {errors?.address_line1 && (
                  <span className="error">{errors.address_line1?.message}</span>
                )}
              </div>

              <div className="mb-3 field-class">
                <input
                  type="text"
                  name="address_line2"
                  className="form-control"
                  id="address2"
                  placeholder="Address 2 (optional)"
                  ref={register({ required: false })}
                />
              </div>
              <div className="mb-3 field-class">
                <input
                  type="text"
                  name="city"
                  className="form-control"
                  id="city"
                  placeholder="City"
                  ref={register({
                    required: "This field is required",
                  })}
                />
                {errors?.city && (
                  <span className="error">{errors.city?.message}</span>
                )}
              </div>
              <div className="mb-3 field-class">
                <input
                  type="text"
                  name="zipcode"
                  className="form-control"
                  id="zipcode"
                  placeholder="Zip Code"
                  ref={register({
                    required: "This field is required",
                    minLength: {
                      value: 4,
                      message: "Enter minimum 4 digits",
                    },
                  })}
                />
                {errors?.zipcode && (
                  <span className="error">{errors.zipcode?.message}</span>
                )}
              </div>
              <div className="mb-3 field-class">
                <div className="form-wrap">
                  <input
                    type="text"
                    className="form-control"
                    name="referral_code"
                    id="referral_code"
                    aria-describedby="emailnoHelp"
                    ref={register({
                      required: false,
                    })}
                    placeholder=" Set Your Referral Code"
                  />
                  <div className="icon-content">
                    .shopkaire.com
                    <Tooltip
                      classes={{ popper: "distributer_tooltip" }}
                      title="As a Kaire Member, you are provided with your own personal website. You can share your website to
                     introduce customers and new prospects to your business. Your Referral Code doubles as the first part of your site's address."
                    >
                      <div>
                        <FaInfo />
                      </div>
                    </Tooltip>
                  </div>
                </div>
                {errors?.referral_code && (
                  <span className="error">
                    {errors?.referral_code?.message}
                  </span>
                )}
              </div>

              <input
                type="hidden"
                className="form-control"
                name="designation"
                id="designation"
                defaultValue={
                  router?.query?.d === "member" ? router?.query?.d : "customer"
                }
                ref={register({ required: true })}
                disabled={true}
              />
              {/*     <div className="captcha_section_div">
                                <ReCAPTCHA
                                    sitekey="6LfHszkdAAAAAD3E-T2nUd8NOBYVHMql0VwuwKxx"
                                    onChange={(value) => setCaptchaToken(value)}
                                    />

                            </div>*/}
              <div>
                We would like to send you occasional news and information.
                Please select if and how you would like to hear from us.
              </div>
              <div className="mb-3 form-check field-class">
                <div class="form-group">
                  <input
                    type="checkbox"
                    ref={register({ required: false })}
                    className="form-check-input"
                    id="Terms-Condition"
                    name="termscondition"
                  />
                  <label className="form-check-label" htmlFor="Terms-Condition">
                    I would like to receive communications by mobile (text message)
                  </label>
                </div>

                {/* {errors.termscondition && <span className="error">This field  is required</span>}*/}

                <div class="form-group">
                  <input
                    type="checkbox"
                    ref={register({ required: false })}
                    className="form-check-input"
                    id="Terms-Condition2"
                    name="termscondition1"
                  />
                  <label
                    className="form-check-label"
                    htmlFor="Terms-Condition2"
                  >
                    I would like to receive communications by email{" "}
                  </label>
                </div>

                {/*  {errors.termscondition1 && <span className="error">This field  is required</span>}*/}
              </div>
              <div className="submit_button field-class">
                <button
                  type="submit"
                  className="btn btn-primary signup_up_button"
                  onClick={() => handleCountryStateError()}
                >
                  Next
                </button>
              </div>
            </form>
          )}
        </div>
      </SignupPageStyle>
      <SvgAnimationBackground />
    </>
  );
}

export default SignUpForm;
