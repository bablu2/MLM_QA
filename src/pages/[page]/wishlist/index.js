import { event } from "jquery";
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import api from '../../../api/Apis'
import Product from "../../Components/Wishlist/ProductComponext";
import Variant from "../../Components/Wishlist/VariantComponents";
import { toast } from 'react-toastify';
import NumberFormat from 'react-number-format';
import WishlistStyle from "./WishlistStyle";
import SvgAnimationBackground from '@Components/Common/SvgAnimationBackground';

export default function Cart({ cartDetails, setupdatecartdata, showprotectedpage, validateauth }) {
  const [cartdata, setCartData] = useState();
  const [setdelete1, setDelete1] = useState();
  const [setdeletemessage, setDeleteMessage] = useState();
  const [wishlistmessage, setwishlistmessage] = useState()
  const [costtype, setcosttype] = useState({ data: [{ id: '', value: '' }] })
  const [logintoken, setToken] = useState();
  const [is_autoship_user, setis_autoship_user] = useState();


  const route = useRouter();
  // load wishlist page data
  useEffect(() => {
    const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
    setToken(token)
    const formDataDelete = { product_id: setdelete1, token: token };
    {
      setdelete1 ?
        api.deleteProductFromwishlist(formDataDelete).then(res => {
          getAllWishListProduct(token);
        })
        :
        getAllWishListProduct(token);
    }
    validateauth()
  }, [setdelete1]);


  const getAllWishListProduct = async (token) => {
    await api.getAllWishListProduct(token).then(res => {

      setis_autoship_user(res?.data?.is_autoship_user)
      if (res?.data?.code === 0) {
        setwishlistmessage(res.data.message)
      } else {
        setCartData(res.data);
      }
    })
  }

  // delete product from wishlist
  const setDelete = (productId, variantId, section = null) => {
    const formDataDelete = { product_id: productId, variant_id: variantId, token: logintoken };
    api.deleteProductFromwishlist(formDataDelete).then(res => {
      if (res?.data?.code === 1) {
        if (section === null) {
          // toast.success('Product deleted From Wishlist', {
          //   duration: 5
          // })
        }
      }
      getAllWishListProduct(logintoken);
    });
  }

  // Add to cart product from wishlist
  const addToCart = async (e) => {
    let autoship = e.currentTarget.parentNode.parentNode.parentNode.parentNode.querySelector('.form-select').value;
    let variant_id = +(e?.target?.dataset?.value) ? +(e?.target?.dataset?.value) : null;
    const formData = { product_id: +(e.target.id), variant_id: variant_id, token: logintoken, quantity: 1, is_autoship: autoship === 'AutoShip' ? true : false };
    await api.addToCart(formData).then(res => {
      if (res?.data?.code === 1) {
        // toast.success(res.data.message, { duration: 5 });
        setDelete(+(e.target.id), variant_id, "delete");
      } else {
        // toast.success(res.data.message, { duration: 5 });
      }
    })
  }
  return (<>
    <Head>
      <title>WishList</title>
    </Head>
    {showprotectedpage === true ?
      <WishlistStyle>
        <div className="container main_wishlist_section_class">
          <div className="cart-sec">
            <div className="cart-heading">
              <h1 className="title"> WishList Item </h1>
            </div>
            <div className="table_container">
              <table className="Cart_product whistlist-table ">
                {cartdata?.wishlist &&
                  <thead >
                    <tr>
                      <th>Product image</th>
                      <th>product name</th>
                      <th>Variation</th>
                      <th>Price</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                }
                <tbody>
                  {
                    cartdata?.wishlist?.length > 0
                      ?
                      cartdata?.wishlist?.map((Cart, index) => {
                        return (<>
                          {Cart?.variant === null ?
                            <Product
                              key={index + 1}
                              is_autoship_user={is_autoship_user}
                              Cart={Cart}
                              setcosttype={setcosttype}
                              index={index}
                              costtype={costtype}
                              setDelete={setDelete}
                              addToCart={addToCart} />
                            :
                            <Variant
                              key={index + 1}
                              is_autoship_user={is_autoship_user}
                              Cart={Cart}
                              setcosttype={setcosttype}
                              index={index}
                              costtype={costtype}
                              setDelete={setDelete}
                              addToCart={addToCart} />
                          }
                        </>)
                      })
                      :
                      <tr className="title">
                        <td colSpan="3">{wishlistmessage}</td>
                      </tr>
                  }
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </WishlistStyle>
      :
      <div className="empty_div_section"></div>
    }
    <SvgAnimationBackground />
  </>);
}

