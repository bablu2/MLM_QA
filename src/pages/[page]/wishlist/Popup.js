import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { useRouter } from "next/router";
import Link from "next/link";
import styled from 'styled-components';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const WishlistPopStyle = styled.div`
&
h2 {
  &.dialog_title_for_all {
      background: #06356a;
      color: #fff;
      font-size: 19px;
      padding: 10px;
      text-transform: uppercase;
      z-index: 99;
      position: relative;
      text-align: center;
      margin-bottom: 31px;
  }
}
.content_text_section {
  text-align: center;
  font-size: 22px;
  color: #000;
}
button,button:hover {
  background: #06356a;
  color: #fff;
  font-size: 10px;
  font-weight: 600;
  padding: 8px;
}
button {
  a {
    color:#fff;
    &:hover {
      text-decoration:none;
    }
  }
}
`;

export default function Popups(props) {
  const [open, setOpen] = React.useState(true);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const router = useRouter();
  return (
    <div>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        onClose={handleClose}
        maxWidth="lg"
        classes={{ paper: "wishlist_popup_page" }}
      >
        <WishlistPopStyle>
          <DialogTitle id="alert-dialog-slide-title " className="dialog_title_for_all">WishList</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description" className="content_text_section">
              {props?.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button color="secondary" type="button" onClick={() => handleClose()}>Close</Button>
            <Button>
              <Link className="nav-link" exact href={`/${router?.query?.page}/wishlist`}>
                <a>View wishlist</a>
              </Link>
            </Button>
          </DialogActions>
        </WishlistPopStyle>
      </Dialog>
    </div>
  );
}




// import Link from "next/link";
// import { useRouter } from "next/router";
// import React from "react";
// // reactstrap components
// import { Button, Modal, ModalBody, ModalFooter } from "reactstrap";
// function Popups(props) {
//   const [modalOpen, setModalOpen] = React.useState(true);
//   const router = useRouter();
//   return (
//     <>
//       <Modal toggle={() => setModalOpen(!modalOpen)} isOpen={modalOpen}>
//         <div className=" modal-header">
//           <h5 className=" modal-title" id="exampleModalLabel">WishList</h5>
//           <button
//             aria-label="Close"
//             className=" close"
//             type="button"
//             onClick={() => setModalOpen(!modalOpen)}
//           >
//             <span aria-hidden={true}>×</span>
//           </button>
//         </div>
//         <ModalBody>{props?.message}</ModalBody>
//         <ModalFooter>
//           <Button
//             color="secondary"
//             type="button"
//             onClick={() => setModalOpen(!modalOpen)}
//           >
//             Close
//           </Button>
//           <Link className="nav-link" exact href={`/${router?.query?.page}/wishlist/wishlist`}>
//             <a><Button color="primary" type="button">
//               View wishlist
//           </Button></a>
//           </Link>
//         </ModalFooter>
//       </Modal>
//     </>
//   );
// }

// export default Popups;