import styled from "styled-components";

const WishlistStyle = styled.div`
  & .main_wishlist_section_class {
    .cart-sec {
      padding: 42px 0 85px;
      h1 {
        font-weight: 600;
        color: #06356a;
        font-size: 30px;
      }
    }
    .table_container {
      height: 100%;

      .whistlist-table {
        width: 100%;
        border: 1px solid #ddd;
        box-shadow: 0 0 10px #c5bfbf;
        background: #fff;
        border-radius: 6px;
        overflow: hidden;
        th {
          text-align: center;
          background: #06356a;
          color: #fff;
          text-transform: uppercase;
          padding: 15px 0;
          font-family: var(--common-font-bd);
          font-weight: 300;
          &:first-child {
            border-top-left-radius: 0px;
          }
          &:last-child {
            border-top-right-radius: 0px;
          }
          &:nth-child(4) {
            min-width: 90px;
          }
        }

        td {
          text-align: center;
          select {
            width: 155px;
            height: 40px;
            border: 1px solid #ddd;
            border-radius: 50px;
            background: transparent;
            padding: 0 10px;
          }
        }
        .cart-product-details {
          display: flex;
          align-items: center;
          justify-content: center;
        }
        .Add_to_cart button {
          font-size: 13px;
          font-family: var(--common-font);
          border-radius: 40px;
          height: 40px;
          border: 2px solid #06356a;
          transition: 0.3s ease all;
          &:hover {
            background: #fff;
            color: #06356a;
          }
        }
        .pro-cart-table {
          display: flex;
          align-items: center;
          justify-content: center;
        }
      }
    }
  }

  @media (max-width: 480px) {
    table.Cart_product.whistlist-table th {
      min-width: 175px !important;
    }
  }
`;
export default WishlistStyle;
