import styled from 'styled-components';

const TermsCondtionCss = styled.div`

    .proven_banifits_section_banner {
        img {
            width: 100%;
        }
    }
    @media (max-width:767px){
    .return h2{
        font-size: 23px;
    }  
    .return h1 {
        font-size: 26px;
        text-align: center;
    }
    .container.return{
        padding: 20px 0px;
    }  

}
`;

export default TermsCondtionCss;