
import Head from 'next/head'
import styles from '../../../styles/Home.module.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { useRouter } from 'next/router';
import { useEffect, useRef, useState } from 'react';
import api from '../../api/Apis'
import { toast } from 'react-toastify';
import ProductListCategory from '../Components/Product/ProductListCategory';
import VedioPlayer from '@Components/Common/VedioPlayer';
import styled from 'styled-components';
import SvgAnimationBackground from '@Components/Common/SvgAnimationBackground';
import _ from 'lodash';

const ProductPageStyle = styled.div`
&&
.productPage_banner {
    width: 100%;
    height:  calc(100vh - 280px);
    overflow: hidden;
    background: url("/images/AllProducts.png") no-repeat; 
    position: relative;
    margin-top: 95px;
    background-size: cover;
    background-position: center;
    &>{
        h1{
            position: absolute;
            left: 0; 
            bottom: 50px;text-align: center;
            text-transform: uppercase;
            font-size: 80px;
            width: 100%;
            color: #fff;
            font-family: 'Graphik'; 
            @media (max-width: 1200px){
                font-size: 66px;
            }
            @media (max-width: 991px){
                font-size: 58px;    margin: 0;
            }
            @media (max-width: 767px){
                font-size: 38px;    bottom: 10px;
            }
            @media (max-width: 576px){
                font-size: 28px;
            }
        }
    }     
    @media (max-width: 1700px){
        height: auto; 
        background-size: cover;
        padding-bottom: 37%;
    }
    @media (max-width: 1199px){
        margin-top: 83px;
    }
    @media (max-width: 767px){
        margin-top: 112px;
    }
    /* iframe {
    width: 100vw;
    height: 210%;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    @media (min-width: 1921px){
        height: 260%;
    }
    @media (max-width: 767px){
        width: 150vw;
    }
} */
} 
`;

export default function allProduct({ setshowloader, smartShipCheck, setInnerLoader, ...props }) {
    const route = useRouter();
    const store = route.query.page;
    const [searchData, setserchData] = useState('');
    const [productqty, setQty] = useState(1);
    const [Logintoken, setToken] = useState()
    const [allcategory, setallcategory] = useState();
    const [costtype, setcosttype] = useState([]);
    const [quantityCheck, setQuantityCheck] = useState([]);
    const [isauroshipuser, setauroshipuser] = useState()
    //for pagenation
    const [offset, setOffset] = useState(0);
    const [data, setData] = useState();
    const [perPage] = useState(9);
    const [pageCount, setPageCount] = useState(0);
    const [addNotify, setAddNotify] = useState('')
    const [loading, setLoading] = useState('')

    const currentRef = useRef(null);

    useEffect(() => {
        setLoading("Loading...")
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : null;
        setToken(token);
        // setshowloader(true);
        setInnerLoader(true)
        if (store) {
            api.getAllProduct(store).then(res => {
                if (res?.status === 200) {
                    setData(res?.data?.products);
                    setLoading("")
                    const allvalues = _.map(res?.data?.products, (row) => ({ id: row.id, value: "Normal" }))
                    setcosttype(allvalues);
                    // setshowloader(false);
                    setInnerLoader(false)
                }
                setauroshipuser(res?.data?.is_autoship_user)
            })
            api.getAllCategory(store).then(res => {
                setallcategory(res?.data?.categories)
            })
        }
    }, [store, offset, route.query.page]);

    // change
    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * perPage;
        setOffset(offset)
    };
    // for search
    const fetchData = async () => {
        api.filterProduct(route?.query?.page ? route.query.page : 'us', searchData).then(res => {
            if (res?.status === 200) {
                setData(res?.data?.products)
            }
        })

    };

    const handleClick = (event) => {
        event.preventDefault();
        fetchData();
    };
    // search data in list of all product
    const setSearchData = (e) => {
        setserchData(e.target.value);
    }
    // for add to cart
    const addToCart = async (e, quantity) => {
        // setshowloader(true);
        // setInnerLoader(true)
        let autoship = e.target.value;
        const formData = {
            product_id: e.target.id,
            variant_id: null,
            token: Logintoken,
            quantity: quantity,
            is_autoship: (autoship !== "false") ? true : false
        };
        // setInnerLoader(true)
        await api.addToCart(formData).then(res => {
            if (res?.data?.code === 0) {
                // toast.error(res.data.message, { duration: 5 })
                setQty(1);
                // setshowloader(false);
                // setInnerLoader(false)
            }
            if (res?.data?.code === 1) {
                setAddNotify({ id: e.target.id, message: "Added To Cart" })
                setTimeout(() => {
                    setAddNotify('')
                }, 3000);
                props?.setshowminicart(true)
            }
            else {
                // setshowloader(false);
                // setInnerLoader(false)
                props?.setshowminicart(Math.floor(Math.random() * 100));
                // toast.success(res.data.message, { duration: 5 })
                setQty(1)
            }

        })
    }

    //increase quantity value
    const Add = (e, producData) => {
        let a = e?.currentTarget?.value;
        let max = producData?.quantity;
        if (+(max) > 0 && +(a) < +(max)) {
            let data = _.find(quantityCheck, { id: producData?.id });
            if (data) {
                let rejectData = _.reject(quantityCheck, data);
                setQuantityCheck([...rejectData, { id: producData?.id, qty: data?.qty + 1 }]);
            } else {
                setQuantityCheck([...quantityCheck, { id: producData?.id, qty: (+a) + 1 }])
            }
        }
    }
    // decrease product qty
    const Sub = (e, producData) => {
        let a = e.currentTarget.value;
        if (a > 1) {
            let data = _.find(quantityCheck, { id: producData?.id });
            if (data) {
                let rejectData = _.reject(quantityCheck, data);
                setQuantityCheck([...rejectData, { id: producData?.id, qty: data?.qty - 1 }]);
            } else {
                setQuantityCheck([...quantityCheck, { id: producData?.id, qty: (+a) - 1 }])
            }

        }
    }
    return (<>
        <div className={styles.container}>
            <Head>
                <title>Kaire Products</title>
            </Head>
            <main className={styles.main}>
                <ProductPageStyle>
                    <div className="productPage_banner">
                        <h1>All Products</h1>
                    </div>
                    <div className="product_cat">
                        <ProductListCategory
                            minCartAutoshipCheck={props?.minCartAutoshipCheck}
                            isauroshipuser={isauroshipuser}
                            data={data}
                            loading={loading}
                            Add={Add}
                            Sub={Sub}
                            smartShipCheck={smartShipCheck}
                            quantityCheck={quantityCheck}
                            refrence={currentRef}
                            addToCart={addToCart}
                            setAddNotify={setAddNotify}
                            setcosttype={setcosttype}
                            costtype={costtype} allcategory={allcategory}
                            categories="Kaire's Products"
                            setSearchData={setSearchData}
                            searchData={searchData}
                            addNotify={addNotify}
                            handleClick={handleClick}  {...props} />
                        {/* {data?.length > 0 &&
                        <ReactpaginateComp pageCount={pageCount} handlePageClick={handlePageClick} />
                    } */}
                    </div>
                </ProductPageStyle>
            </main>
            <SvgAnimationBackground />
        </div>


    </>)
}