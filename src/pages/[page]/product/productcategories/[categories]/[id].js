
import Head from 'next/head'
import { useRouter } from 'next/router'
import { useEffect, useRef, useState } from 'react';
import api from '../../../../../api/Apis'
import styles from '../../../../../../styles/Home.module.css'
import ProductListCategory from '../../../../Components/Product/ProductListCategory';
import { toast } from 'react-toastify';
import ReactpaginateComp from '@Components/Common/ReactPaginateComp';

export default function AllProduct(props) {
  const router = useRouter()
  const { id, categories, page } = router.query;
  const [searchData, setserchData] = useState('');
  const [Logintoken, setToken] = useState()
  const [productqty, setQty] = useState(1);
  const [allcategory, setallcategory] = useState()
  const [costtype, setcosttype] = useState({ data: [{ id: '', value: '' }] })
  const [isauroshipuser, setauroshipuser] = useState()
  // for pagenation
  const [offset, setOffset] = useState(0);
  const [data, setData] = useState([]);
  const [perPage] = useState(6);
  const [pageCount, setPageCount] = useState(0)
  const [optionValue, setOptionValue] = useState('Normal');

  const currentRef = useRef(null);
  useEffect(() => {
    {
      const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : null;
      setToken(token)
      id &&
        api.getProductByCategories(id).then(res => {
          const slice = res?.data?.products?.slice(offset, offset + perPage)
          setPageCount(Math.ceil(res?.data?.products?.length / perPage))
          setData(slice)
          setauroshipuser(res?.data?.is_autoship_user)
        })
      // get all category
      api.getAllCategory(page).then(res => {
        setallcategory(res?.data?.categories)
      })

    }


  }, [id, offset]);
  const handlePageClick = (e) => {
    const selectedPage = e.selected;
    const offset = selectedPage * perPage;
    setOffset(offset)
  };
  //for filter data
  const fetchData = async () => {
    const req = await fetch(`${process.env.getproductsbycategory}?category_id=${router?.query?.id ? router.query.id : 'us'}&search=${searchData}`);
    const productData = await req.json();
    const slice = productData.products?.slice(offset, offset + perPage)
    setPageCount(Math.ceil(productData.products?.length / perPage))
    setData(slice)
  };
  const handleClick = (event) => {
    event.preventDefault();
    fetchData();
  };
  // seraching data 
  const setSearchData = (e) => {
    setserchData(e.target.value);

  }
  // Add to cart
  const addToCart = (e) => {
    let a = optionValue;
    const formData = { product_id: e.target.id, variant_id: null, token: Logintoken, quantity: productqty, is_autoship: a === 'AutoShip' ? true : false };
    api.addToCart(formData).then(res => {
      if (res?.data?.code === 0) {
      }
      else {
        props?.setshowminicart(Math.floor(Math.random() * 100))
      }
    })
  }



  const Add = (e) => {
    let a = e.currentTarget.parentNode.querySelector('.get').value;
    let max = e.currentTarget.parentNode.querySelector('.get').max;
    if (+(max) > 0 && +(a) < +(max)) {
      e.currentTarget.parentNode.querySelector('.get').value = +(a) + 1;
      setQty(+(a) + 1)

    }
  }
  const Sub = (e) => {
    let a = e.currentTarget.parentNode.querySelector('.get').value;
    if (a > 1) {
      e.currentTarget.parentNode.querySelector('.get').value = +(a) - 1;
      setQty(+(a) + 1)
    }
  }
  return (
    <div className={styles.container}>
      <Head>
        <title>{categories}</title>
      </Head>
      <main className={styles.main}>
        <hr />
        <div className="product_cat">
          <ProductListCategory isauroshipuser={isauroshipuser}
            optionValue={optionValue}
            setOptionValue={setOptionValue}
            Add={Add} Sub={Sub}
            refrence={currentRef}
            data={data} addToCart={addToCart}
            setcosttype={setcosttype} costtype={costtype}
            allcategory={allcategory} categories={categories}
            setSearchData={setSearchData} searchData={searchData}
            handleClick={handleClick} />
          {data?.length > 0 &&
            <ReactpaginateComp pageCount={pageCount} handlePageClick={handlePageClick} />
          }
        </div>
      </main>
    </div>

  )
}






export const getStaticPaths = async () => {
  let paths;
  const defaultValues = {
    paths: [],
    fallback: false,
  };

  try {
    const allSlugs = ["us/product/productcategories/Books212/7/", "us/product/productcategories/Medicine/9/"];
    const res = await fetch(`${process.env.getAllCategory}?slug=us`)
    let data_page = await res?.json();
    data_page = data_page?.categories
    paths = data_page?.map((c, index) => {
      let names = c?.name.replace('25%', '')
      let catname = names.replace('\t', '')
      catname = catname.replace(' ', '_')
      catname = catname.replace(' ', '_')
      catname = catname.replace(' ', '_')
      catname = catname.replace(' ', '_')
      catname = catname.replace(' ', '_')
      return (`/us/product/productcategories/${catname}/${c?.id}`)
    });    // paths = allSlugs.map((slug) => "/" + slug);

  } catch (e) {
    return defaultValues;
  }

  return {
    paths,
    fallback: false,
  };
};


export const getStaticProps = async ({ params: { page, id } }) => {
  const res = await fetch(`${process.env.getAllCategory}?slug=us`)
  let data_page = await res?.json();
  data_page = data_page?.categories
  return {
    props: {
      id, data_page
    },
    revalidate: 1,
  };
};