import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useRef, useState } from 'react';
import api from '@api/Apis';
import { toast } from 'react-toastify';
import _ from 'lodash';
import AccordionSec, { FrequentlyProducts, ReviewSection, SupplimentPopUp, AddCardButtonText } from '../common';
import { Rating, Button } from "@material-ui/core";
import VedioPlayer from '@Components/Common/VedioPlayer';
import { Star } from '@material-ui/icons';
import SvgAnimationBackground from '@Components/Common/SvgAnimationBackground';
import { SecondSection, UniqueDeliveryComp } from '../common';
import dynamic from 'next/dynamic';
import ReviewComponent from '@Components/Common/RatingSection/ReviewComponent';
import { ProductPageFooter } from '../productPage.style';
import { Parallax } from 'react-scroll-parallax';
import DialogComponent from '@Components/Common/DialogComponent';
import { DialogSectionCss } from '@PagesComponent/ActiveSmartShip/ActiveStyleComp';

const ProductPageDesign = dynamic(
  () => import('../productPage.style'),
  { ssr: false }
);

export default function Product(props) {
  const { customerCheck, minCartAutoshipCheck, smartShipCheck, setInnerLoader } = props;

  const router = useRouter()
  const { id, name, page } = router.query;
  const [productqty, setQty] = useState(1);
  const [product, setproduct] = useState();
  const [productvariantid, setproductvariantid] = useState();
  const [costtype, setcosttype] = useState([]);
  const [variantdetals, setvariantdetails] = useState();
  const [Logintoken, setToken] = useState()
  const [relatedData, setRelatedData] = useState();
  const [productDetailContent, setProductDetailContent] = useState();
  const [reviewData, setReviewData] = useState([]);
  const [open, setOpen] = useState(false);
  const [openReview, setOpenReview] = useState(false);
  const [openKosher, setOpenKosher] = useState(false)
  const [cartNotify, setCartNotify] = useState('')

  // For add to cart product/variant
  const myRef = useRef(null);
  const seeRef = useRef(null);
  const addToCart = async (id, quantity) => {
    const formData = {
      product_id: id, variant_id: productvariantid,
      token: Logintoken, quantity: quantity, is_autoship: _.find(costtype, { id: id })?.value === 'AutoShip' ? true : false
    };
    await api.addToCart(formData).then(res => {
      if (res?.data?.code === 1) {
        setCartNotify("Added To Cart")
        setTimeout(() => {
          setCartNotify('');
        }, 3000)
        // toast.success(res.data.message, { duration: 5 })
        props?.setshowminicart(true)
      }
    })
  }
  // for add product to wishlist
  const addToWishList = (e) => {
    const formData = { product_id: id, variant_id: productvariantid, token: Logintoken, quantity: productqty };
    api.addToWishlist(formData).then(res => {
      if (res?.data?.code === 1) {
        props?.setupdatecartdata(true)
        // toast.success('product added in wishlist successfully')
      }
    })
  }

  // slider-loading
  useEffect(() => {
    const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : null;
    setToken(token);
    // setshowloader(true);
    setInnerLoader(true)
    setcosttype([{ id: id, value: "Normal" }])
    { id ? ProductDataFunction(id) : '' }
  }, [id])

  const ProductDataFunction = async (id) => {
    await api.getProductByProductid(id).then(res => {
      if (res?.data?.code === 1) {
        // setshowloader(false);
        setInnerLoader(false)
        setProductDetailContent(res?.data?.product_detail_data);
        setvariantdetails(res?.data?.products?.variants);
        setReviewData(res?.data?.reviews);
        setproduct(res?.data);
        setQty(1)
        setRelatedData(res?.data?.frequently_bought_products)
      }

    }).catch((err) => router.back())
  }

  // const getProductCategories = async (catId = null) => {
  //   await api.getProductByCategories(catId).then((res) => {
  //     if (res?.status && res?.data) {
  //       setRelatedData(res?.data?.products);
  //     }
  //   }).catch((error) => {
  //     toast.error('internal error');
  //   })
  // }

  const ForRatingStar = (count = 0) => {
    const Rating = [];
    for (let i = 1; i <= count; i++) {
      Rating.push(<i className="fa fa-star" aria-hidden="true" key={i + 1}></i>);
    }
    return Rating;
  }

  const handleClickOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  return (
    <>
      <Head><title>{name}</title></Head>
      {productDetailContent &&
        <ProductPageDesign bannerImage={productDetailContent?.banner_dummy_image} section2Image={productDetailContent?.section2_dummyImage}>
          {/* first section bannner */}
          <div className={`product-section-banner extra_${name?.toLowerCase()}_class`}>
            {productDetailContent?.banner_bgVedioUrl && <VedioPlayer url={productDetailContent?.banner_bgVedioUrl} />}
            <div className="Content-banner">
              <h2>{product?.products?.name && product?.products?.name}
                {product?.products?.name !== "BioticKaire+" && product?.products?.name !== "SilverKaire" &&
                  <sup>TM</sup>}
              </h2>

              <p>{productDetailContent?.blurb_description && productDetailContent?.blurb_description}</p>
            </div>
          </div>
          {/* {second section} */}
          <div className="product-section-slider">
            <div className="product-section-details">
              <div className='container'>
                <div className="row product_detail_container">
                  <div className="col-md-5">
                    <div className="product-image">
                      {product?.products?.product_images?.map((productDataimage, index) => (
                        <img src={`${process.env.API_URL}/${productDataimage['image']}`} key={index + 1} className="product_img" />
                      ))}
                    </div>
                  </div>
                  <div className="col-md-7">
                    <div className="custom-cont">
                      <div className="first_section">
                        <h2 className="title">{product?.products?.name &&
                          `${product?.product_name || ""} ${product?.products?.name || ""}`}
                          {product?.products?.name !== "BioticKaire+" && product?.products?.name !== "SilverKaire" &&
                            <sup style={{ fontSize: '18px' }}>TM</sup>}
                        </h2>
                        <p>{productDetailContent?.asked_section && productDetailContent?.asked_section}</p>
                      </div>
                      <SecondSection data={{
                        smartShipCheck,
                        costtype,
                        setcosttype,
                        id: id,
                        minCartAutoshipCheck,
                        productCost: product?.products.cost_price,
                        productSmartShip: product?.products.autoship_cost_price
                      }} />
                      <div className="third_section">
                        <div className="quantity_section box">
                          <span><b>Qty:</b></span>
                          <div id="qty">
                            <button type="button" id="sub" className="sub" onClick={(e) => {
                              setQty(productqty > 1 ? productqty - 1 : productqty)
                            }}>-</button>
                            <input type="text" value={productqty} name="qty" onChange={(e) => {
                              setQty(e.target.value)
                            }} min="1" max={product?.products?.quantity} readOnly />
                            <button type="button" id="add" className="add" onClick={(e) => {
                              setQty(productqty + 1);
                            }}>+</button>
                          </div>
                        </div>
                        <div className="add_button_div">
                          <div className={`btn-sec Add_to_cart ${product?.products?.is_stock_available === 'True' ? '' : 'out-of-stock'}`} >
                            <button disabled={product?.products?.is_stock_available === 'True' ? false : true}
                              type="button" id={product?.products?.id}
                              onClick={(e) => addToCart(id, productqty)}>
                              {AddCardButtonText(product?.products?.is_stock_available, costtype, id)}
                            </button>
                          </div>
                        </div>

                      </div>
                      <div className="fourth_section">
                        <div className="review-section-now">
                          {props.isLogin || Logintoken !== null &&
                            <p>BV: {(_.find(costtype, { id: id })?.value === 'AutoShip' || minCartAutoshipCheck === "True") ? product?.products?.autoship_bonus_value.split()[0] : product?.products?.bonus_value} </p>
                          }
                          <ul>
                            <li>
                              <Rating
                                name="rating"
                                value={4}
                                readOnly
                                sx={{ color: "black" }}
                                emptyIcon={<Star style={{ opacity: 0.55 }} fontSize="inherit" />}
                              />
                            </li>
                            <li className="review_list" onClick={() => myRef.current.scrollIntoView()}>
                              <a href="#" style={{ pointerEvents: "none" }}>{reviewData?.length} reviews</a>
                            </li>
                            {props.isLogin || Logintoken !== null &&
                              <>
                                <li><span className="breaker">|</span><button onClick={() => setOpenReview(true)} style={{ background: "none", padding: 0 }}>Write a review</button></li>
                                <ReviewComponent productId={id} productName={name?.replaceAll('_', " ")} section="product_page" open={openReview} handleClose={setOpenReview} setOpen={setOpen} reviewData={reviewData} setReviewData={setReviewData} />
                              </>
                            }
                          </ul>
                        </div>
                        {productDetailContent?.supplement_facts && productDetailContent?.id !== 19 &&
                          <>
                            <div className="supplement-fact">
                              <Button className="subpplement_button" onClick={() => handleClickOpen()}>Supplement Facts</Button>
                              {/*{props.isLogin === true &&
                            <Button id={product?.products?.id} onClick={(e) => addToWishList(e)}>Add to WishList</Button>
                          }*/}
                              <SupplimentPopUp handleClose={handleClose} open={open} image={productDetailContent?.supplement_facts} />
                            </div>
                            <div className="Kosher">
                              <a style={{ cursor: 'pointer' }} onClick={() => setOpenKosher(true)} className="par-logo">  <img src="KosherLogo-ai.png" /><p><span>?</span>
                                what is Kosher</p>
                              </a>
                            </div>
                            <DialogComponent opend={openKosher} handleClose={() => setOpenKosher(false)} title="Description" classFor="order_details">
                              <DialogSectionCss>
                                <div className="head_kosher">
                                  <h3>What is Kosher & What Does It Mean?</h3>
                                </div>

                                <p> According to the kldbkosher.org website, “Kosher is a Hebrew word that means fit, proper, or correct.
                                  Nowadays, it is mostly used to describe food and drink that complies with Jewish religious dietary law.”</p>

                                <div className="certified">
                                  <h3> What is Kosher Certified?</h3>
                                </div>
                                <p> For a product to be kosher certified, and to qualify for a kosher certificate, each ingredient, food additive,
                                  and processing aid used in its production must also be kosher. Additionally, to be kosher certified,
                                  the production process must be suitable for kosher requirements and therefore it must be approved by a kosher auditor.
                                  Products may be rendered non-kosher if their production lines and equipment are also used to manufacture non-kosher products.</p>

                              </DialogSectionCss>
                            </DialogComponent>
                          </>
                        }


                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="kaire_live_longer">
              <div className="container">
                <div className="row">
                  <div className="col-md-12">
                    <div className='product-desc text-center'>
                      <h3>{productDetailContent?.product_detail_heading}</h3>
                      <p>{productDetailContent?.product_detail_content}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="second_vedio_section">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-12">
                    {productDetailContent?.section2_bgVedioUrl &&
                      <>
                        {/*  <Parallax y={[-20, 5]} tagOuter="figure" className="custom-class">0*/}
                        <div className='our-unique'>
                          <VedioPlayer url={productDetailContent?.section2_bgVedioUrl} />
                        </div>
                        {/*  </Parallax>*/}
                      </>
                    }
                    {(productDetailContent?.section2_title !== "" && productDetailContent?.section2_content !== "") &&
                      <UniqueDeliveryComp
                        proven_button_link={productDetailContent?.section2_button_link}
                        image={product?.products?.product_images?.length > 0 && product?.products?.product_images[0]?.image}
                        heading={productDetailContent?.section2_title}
                        content={productDetailContent?.section2_content}
                        productName={product?.products?.name && product?.products?.name}
                        seeMoreFunction={() => seeRef.current.scrollIntoView()}
                      >
                        <div className={`btn-sec Add_to_cart ${product?.products?.is_stock_available === 'True' ? '' : 'out-of-stock'}`} >
                          <button
                            disabled={product?.products?.is_stock_available === 'True' ? false : true}
                            type="button" id={product?.products?.id}
                            onClick={(e) => addToCart(id, productqty)}>
                            {product?.products?.is_stock_available === 'True' ? 'Add to Cart' : 'Sold Out'}
                          </button>
                        </div>

                      </UniqueDeliveryComp>
                    }
                  </div>
                </div>
              </div>
            </div>
            <div className="accordian_section clearfix">
              <div className="container">
                <div className="row">
                  <div className="col-md-12">
                    <AccordionSec product={product} ref={seeRef} />
                  </div>
                </div>
              </div>
            </div>
            <div className="frequently_product_section">
              <div className="container">
                <div className="row">
                  <div className="col-md-12">
                    {relatedData?.length > 0 &&
                      <FrequentlyProducts
                        data={{
                          smartShipCheck,
                          minCartAutoshipCheck,
                          addToCart,
                          relatedData,
                          costtype, setcosttype,
                        }}
                        cartNotify={cartNotify} />
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* {third section} */}

          {/* {section 4} */}
          <div className="customer-review-now" ref={myRef}>
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="review-customer">
                    <h5>What are people saying about this product?</h5>
                    <h2>Customer Reviews</h2>
                  </div>
                </div>
              </div>
              <div id="review">
                <ReviewSection ForRatingStar={ForRatingStar} reviewData={reviewData} setReviewData={setReviewData} />
              </div>
            </div>
            <div className="main-qus">
              <div className="container">
                <div className="row">
                  <div className="col-md-12">
                    <div className="question-text">
                      <div className="stay-touch-part">
                        <h5>Stay in Touch</h5>
                      </div>
                      <div className="form-subscribe">
                        <form className="form-inline">
                          <div className="form-group">
                            <label className="col-form-label">Subscribe</label>
                            <input type="text" readOnly className="form-control-plaintext" id="staticEmail" value="*Your email address " />
                          </div>
                          <button type="submit" className="btn btn-primary">Subscribe</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="footer-text-sec">
              <div className="container">
                <div className="box-text-bnner">
                  <p>Pregnant or nursing mothers, children under 18, and individuals with a known medical condition should consult
                    a physician before using this or any dietary supplement.
                  </p>
                  <h4>*Individual results may vary and are not guaranteed</h4>
                </div>
              </div>
            </div>
          </div>
          <SvgAnimationBackground />
          <ProductPageFooter>
            <div className="price_section_footer">
              <div className='price-sec'>
                <div className="price-inner-sec">
                  <SecondSection data={{
                    smartShipCheck,
                    costtype,
                    setcosttype,
                    id: id,
                    minCartAutoshipCheck,
                    productCost: product?.products.cost_price,
                    productSmartShip: product?.products.autoship_cost_price
                  }} />
                  {/* {(customerCheck > 1) && */}
                  {props.isLogin &&
                    <h5 className="bonus_value">BV: {(_.find(costtype, { id: id })?.value === 'AutoShip' || minCartAutoshipCheck === "True") ? product?.products?.autoship_bonus_value.split()[0] : product?.products?.bonus_value} </h5>
                  }
                  {/* } */}
                </div>
                <div className="footer-merge">
                  <div className="quality">
                    <div className="main-qty-sec">
                      <div className="box">
                        <div id="qty">
                          <button type="button" id="sub" className="sub" onClick={(e) => {
                            setQty(productqty > 1 ? productqty - 1 : productqty)
                          }}>-</button>
                          <input type="text" value={productqty} name="qty" onChange={(e) => {
                            setQty(e.target.value)
                          }} min="1" max={product?.products?.quantity} readOnly />
                          <button type="button" id="add" className="add" onClick={(e) => {
                            setQty(productqty + 1);
                          }}>+</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="cart-add">
                    {/* {operation === 'showpopup' && <Popups message={addtocartmessage} />} */}
                    <div className="add_button_div">
                      <div className={`btn-sec Add_to_cart ${product?.products?.is_stock_available === 'True' ? '' : 'out-of-stock'}`} >
                        <button className="add_margin" disabled={product?.products?.is_stock_available === 'True' ? false : true}
                          type="button" id={product?.products?.id}
                          onClick={(e) => addToCart(id, productqty)}>
                          {AddCardButtonText(product?.products?.is_stock_available, costtype, id)}
                        </button>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </ProductPageFooter>
        </ProductPageDesign>
      }
    </>
  )
}

export const getStaticPaths = async ({ id }) => {
  let paths;
  const defaultValues = {
    paths: [],
    fallback: true,
  };

  try {
    // const allSlugs = ["us/product/Test2%20Product/12/", "us/product/Test3%20Product/13/"];
    const res = await fetch(`${process.env.getAllProduct}?slug=us`)
    let data_page = await res?.json();
    data_page = data_page?.products
    paths = data_page?.map((c) => {
      let names = c?.name.replace('25%', '')
      let catname = names.replace('\t', '')
      catname = catname.replace(/\s/g, '_');
      return (`/us/product/${catname}/${c?.id}`)
    });    // paths = allSlugs.map((slug) => "/" + slug);

  } catch (e) { return defaultValues; }
  return {
    paths,
    fallback: true,
  };
};

export const getStaticProps = async ({ params: { page, id } }) => {
  const res = await fetch(`${process.env.getAllProduct}?slug=us`)
  let data_page = await res.json();
  // data_page = data_page//?.products
  return {
    props: {
      id, data_page
    },
    revalidate: 60,
  };
};