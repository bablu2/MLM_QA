import MaritimePrimeSection from '@Components/Common/MaritimePrimeSection';
import SvgAnimationBackground from '@Components/Common/SvgAnimationBackground';
import VedioPlayer from '@Components/Common/VedioPlayer'
import React, { useEffect, useState, useMemo, useCallback } from 'react'
import styled from 'styled-components';
import { Parallax, Background } from "react-parallax";
import api from '@api/Apis';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import parse from 'html-react-parser';

const LagendStyle = styled.div`
&
.lagend_banner_vedio{
    width: 100%;
    height: 650px;
    overflow: hidden;
    background:${props => props?.bannerImage ? `url(${props?.bannerImage})` : `url("/images/legend.png")`};
    position: relative;
    background-repeat: no-repeat;
    background-size: 100%;
    margin-top: 90px;
    iframe {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    width: 130vw;
    height: 130%;
    @media (max-width: 767px){
        width: 200vw;
    }
    }
    @media (max-width: 1366px){
    position: relative;
    background-repeat: no-repeat;
    background-size: cover;
    margin-top: 7vh;
    height: 540px;
    /* padding-bottom: 84%; */
    }
    @media (max-width: 991px){
    height: auto;
    padding-bottom: 50%;
    } 
    @media (max-width: 767px){ 
    padding-bottom: 80%;margin-top: 75px;
    } 
}
.section2.legend_section {
    /* background:url(/images/Untitled-2.jpg); */
    background-size: cover;
    padding: 100px 0px;
    background-attachment: fixed;
    position: relative;
    @media (max-width: 991px){
        padding: 50px 0px;
    }
}
 .section2.legend_section .container{
    z-index: 0;
    position: relative;
} 

.legend_section {
    h2 {
        font-size: 31px;
        text-align: left;
    }
    p {
        color: #000000;
        font-size: 17px;
    }


}

@media (max-width: 767px){
    .section2.legend_section {
        padding: 30px 0px;
    }
}
@media (max-width: 567px){
    .martin_prime_section img {
        width: 230px;
    }
    .martin_prime_section h2{
        font-size: 35px !important;
    }
    .martin_prime_section {
        max-width: 340px;
    }
}
`;


export default function LagendPage({ setInnerLoader }) {
    const { page } = useRouter();
    const [state, setState] = useState({});
    const [bgImage, setBgImage] = useState("");
    useEffect(() => {
        getLegendPageContent();
    }, [])

    //Legend page content api call

    const getLegendPageContent = async function () {
        setInnerLoader(true)
        await api.getLegendPageFunction(page).then(res => {
            const { code, content, message } = res?.data;
            if (code === 1) {
                setState(content[0]);
                setBgImage(content[0]?.legend_bgImage)
                setInnerLoader(false)
            } else if (code === 0) {
                setState(message);
                setInnerLoader(false)
            }

        }).catch(err => {
            // toast.error("Somthing went wrong")
        })

    }
    const parallacContent = useCallback(() => {
        let backgroundImage = bgImage;

        return (
            <Parallax bgImage={backgroundImage} strength={-100}>
                <div className="section2 legend_section">
                    <div className="container">
                        {state?.legend_heading ? parse(state?.legend_heading) : <h2>"The Legend"</h2>}
                        {state?.legend_content && parse(state?.legend_content)}
                    </div>
                    <MaritimePrimeSection
                        content={{
                            product_image: process.env.MEDIA_URL + state?.legend_product_image,
                            productName: parse(String(state?.legend_productName)),
                            btnText: state?.legend_btnText,
                            btnLink: state?.legend_btnLink
                        }}
                    />
                </div>
            </Parallax>
        )
    }, [bgImage])
    return (
        <LagendStyle bannerImage={`${process.env.API_URL + state?.banner_image}`}>
            <div className="productPage_banner lagend_banner_vedio">
                <VedioPlayer url={state?.legend_bgURL} />
            </div>
            {parallacContent()}
            <SvgAnimationBackground />
        </LagendStyle>
    )
}
