import Head from "next/head";
import { useEffect, useState } from 'react';
import { useRouter } from "next/router";
import React from "react";
import { useForm } from "react-hook-form";
import { Link } from '@material-ui/core';
import 'react-toastify/dist/ReactToastify.css';
import LoginStyle from "./LoginStyle";
import SvgAnimationBackground from '@Components/Common/SvgAnimationBackground';
import LoginHtml from "./LoginHtml";
import { AiFillEye } from "react-icons/ai";
import { Visibility, VisibilityOff } from "@material-ui/icons";

const Login = props => {
    const router = useRouter()
    const { register, handleSubmit, errors, setError } = useForm({
        mode: "all"
    });
    const [passwordShow, setPasswordShow] = useState(false);
    // login user
    useEffect(() => {
        if (props?.isLogin === true) {
            router.push(`/`);
        }
    }, [])
    // on click signup link
    const handleOpen = (e) => {
        router.push(`/${router?.query?.page}/signup`)
    };

    function _callFunction(data) {
        props?.LoginSubmit(data, setError)
    }
    function showPassword() {
        setPasswordShow(!passwordShow)
    }
    function onBlurChange(e) {
        // console.log(e.target.value)
    }
    return (
        <>
            <Head>
                <title>Login</title>
            </Head>
            <LoginStyle>
                <div className="container">
                    {props?.isLogin !== true ?
                        <form className="signupform main-sign-frm" onSubmit={handleSubmit(_callFunction)}>
                            <>
                                <h1 className="title">Login</h1>
                                <div className="mb-3 field-class">
                                    <label htmlFor="exampleInputEmail1" className="form-label"> Username or Email Address</label>
                                    <input type="text" className="form-control" onBlur={(e) => onBlurChange(e)}
                                        ref={register({
                                            required: "Please enter your email",
                                            pattern: {
                                                value: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
                                                message: "Invalid Email Address"
                                            },
                                        })} name="email" id="exampleInputEmail1" aria-describedby="emailHelp" />
                                    {errors.email?.message && <span className="error">{errors.email.message}</span>}

                                </div>

                                <div className="mb-3 field-class">
                                    <label htmlFor="exampleInputPassword1" className="form-label">Password</label>
                                    <input type={passwordShow ? "text" : "password"} className="form-control" name="password" id="exampleInputPassword1" ref={register({ required: true })} />
                                    {
                                        passwordShow ?
                                            <Visibility onClick={() => showPassword()} /> : <VisibilityOff onClick={() => showPassword()} />
                                    }

                                    {errors.password && <span className="error">This field is required</span>}
                                </div>

                                <div className="sign-up-bnt">
                                    <button type="submit" className="btn btn-primary login">Login</button>
                                </div>

                                <div className="signupandforget">
                                    <>
                                        {/*button type="button" className="btn btn-primary sigup" onClick={(e) => { router.push(`/${router?.query?.page}/signup`) }}>Signup</button>*/}
                                        <div className="forgot password">
                                            <Link className="nav-link" exact href={`/${router?.query?.page}/reset-password`} >
                                                <a>Forgot Password</a>
                                            </Link>
                                        </div>
                                    </>

                                </div></>
                        </form>
                        :
                        <div className="empty_div_section"></div>
                    }
                </div>
            </LoginStyle>
            <SvgAnimationBackground />
        </>);
}

export default Login;