import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import api from "@api/Apis";

const EmailForm = props => {
    const router = useRouter();
    const [successMsg, setSuccessMsg] = useState('')
    const { register, errors, handleSubmit, setError, getValues } = useForm({
        mode: "all",
    });

    useEffect(() => {
        if (router?.query?.token) {
            setState(router?.query?.token);
        }
    }, [router?.query?.token]);

    // for reset password
    const onSubmit = async data => {
        await api.resetPassword(data, null).then(res => {
            if (res?.status === 200 && res?.data?.status === "OK") {
                setSuccessMsg("Further detail sent to your email address", { hideProgressBar: true });
                setTimeout(() => {
                    setSuccessMsg('')
                }, 4000);
                localStorage.setItem("resetemail", data?.email);
            }
            else {
                setError("email", { type: "manual", message: res?.data?.email[0] });
            }
        })
    }

    return (
        <form className="signupform main-sign-frm password" onSubmit={handleSubmit(onSubmit)}>
            <h1 className="title">Reset your password</h1>
            <div className="mb-3 input_section">
                <label htmlFor="exampleInputEmail1" className="form-label">Email Address</label>
                <input type="email" className="form-control" ref={register({
                    required: "Please enter your email",
                    pattern: {
                        value: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
                        message: "Please enter valid email"
                    }
                })}
                    name="email" id="exampleInputEmail1" />
                <span className="error">{errors.email?.message && errors.email.message}</span>
                <div className="sign-up-bnt">
                    <button type="submit" className="btn btn-primary" >Reset</button>
                    <button type="button" className="btn btn-primary sigup" onClick={(e) => { router.push(`/${router?.query?.page}/signup`) }} >signup</button>
                </div>
                {successMsg && <span className="error" style={{ color: 'green' }}>{successMsg}</span>}
            </div>
        </form>
    );
}

export default EmailForm;