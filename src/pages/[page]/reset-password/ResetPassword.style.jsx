import styled from 'styled-components';

const ResetPasswordStyle = styled.div`
padding-top: 110px;
  height: 100vh;
  min-height: 700px;
  display: flex;
  align-items: center;
  background: url("/images/login-image.png") no-repeat; 
  background-size: cover;
  margin-bottom: -13px;
  @media (max-width: 991px){
  height: auto;
  margin-bottom: -10px;
  background-position: center;
}
&& 
form.signupform.main-sign-frm.password { 
    margin: 0;
    width: 100%;
    max-width: 420px;
	 background: rgb(182,145,230);
	background: -moz-linear-gradient(173deg, rgba(182,145,230,1) 0%, rgba(110,139,222,1) 100%);
	background: -webkit-linear-gradient(173deg, rgba(182,145,230,1) 0%, rgba(110,139,222,1) 100%);
	background: linear-gradient(173deg, rgba(182,145,230,1) 0%, rgba(110,139,222,1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#b691e6",endColorstr="#6e8bde",GradientType=1); 
	border-radius: 25px;
	overflow: hidden;
	position: absolute;
	right: 70px;
    padding: 25px;    
    left: auto;  
    top: 180px;
    @media (max-width: 991px){
      flex-wrap: wrap; 
                h1,.input_section{
                    flex: 100%;
                    width: 100%;
                }
    }
    @media (max-width: 767px){
      position: unset;
      max-width: 100%;
    }
    label{
        font-size: 16px;font-family: var(--common-font);font-weight: normal;  color: #fff;
            }
    .sign-up-bnt {
        display: flex;
        justify-content: space-between; 
        align-items: center;
        button.btn.btn-primary.sigup {
                text-align: right;
                width:100%;
                color: #fff;
                text-transform: capitalize;
            }
        button[type="submit"] {
            &:hover {
                background: var(--blue);color:#fff;
            }
            &:focus {
                outline: none;
            }
            width: 100%;
            max-width: 130px; 
            line-height: normal;
            height: 40px; 
            display: flex;
            -webkit-box-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            align-items: center;
            transition: all 0.3s linear 0s;
            font-size: 16px;
            font-family: var(--common-font);
            margin: 0px;
            background: #fff;
            text-transform: uppercase;
            color:#000;
            border-radius: 25px;
            transition: 0.3s ease all;
            border: 2px solid #fff;
            line-height: 28px;
        }
    }
    h1 {
      text-align: center;
      text-transform: capitalize;
      margin: 0 0 25px;
      font-size: 30px;
      color: #fff;
      font-family: var(--common-font-bd);
      @media (max-width: 991px){
          font-size: 26px;
      }
    }
    .input_section { 
        input {
            &:focus { 
                border: none;
            }
            border: 0;
            box-shadow: unset;
            border-bottom: 1px solid #e6e6e6;
            border-radius: 9px;
            height: 51px;
            padding: 10px;
            margin-bottom: 15px;
            &+span.error {
                border-color: red;
                margin-bottom: 20px;
            }
        }
           
    }
    span.error:not(:empty) {
    background: #fff;
    display: block;
    margin-top: 10px;
    padding: 5px 10px;
    border-radius: 5px;
    border: 1px solid green;
    text-align: center;
}

}

`;

export default ResetPasswordStyle;