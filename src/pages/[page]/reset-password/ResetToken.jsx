import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Link from 'next/link';
import { useRouter } from "next/router";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { toast, Slide } from 'react-toastify';
import api from "@api/Apis";
import { Button } from "@material-ui/core";
import DialogComponent from "@Components/Common/DialogComponent";
import { DialogSectionCss } from "@PagesComponent/ActiveSmartShip/ActiveStyleComp";

// Reset password component
const ResetToken = (props) => {
    const router = useRouter();
    const [showSuccess, setShowSuccess] = useState('')
    const [errorPassword, setErrorPassWord] = useState('')
    const [open, setOpen] = useState(false)
    const [emailData, setEmailData] = useState()
    const [userToken, setUserToken] = useState()
    const [newPassword, setNewPassword] = useState()
    const [afterReset, setAfterReset] = useState(false);
    const {
        register,
        errors,
        handleSubmit,
        setError,
        getValues
    } = useForm({
        mode: "all",
        //resolver: yupResolver(ResetPassSchema),
    });
    useEffect(() => {
        const EMAIL = localStorage.getItem('resetemail')
        setEmailData(EMAIL)
    }, [])

    const onSubmit = async (data) => {
        var DATAPASSWORD = data?.password
        setNewPassword(DATAPASSWORD)
        const token = router?.query?.token;
        const FormData = {
            password: data.password,
            token: token,
        }
        await api.passwordResetConfirm(FormData).then(res => {
            if (res?.status === 200) {
                setShowSuccess('Password is successfully changed')
                setTimeout(() => {
                    setShowSuccess('')
                }, 3000);
                // toast("Password is successfully changed", { duration: 5, type: "success", });
                setAfterReset(true);
                setOpen(true)
                // router.push('/login');
            }
            else {
                setOpen(false)
                setErrorPassWord('Password is already changed')
                setTimeout(() => {
                    setErrorPassWord('')
                }, 3000);
                // setError("password", { type: "manual", message: res?.data?.password[0] });
            }
        })
    };

    function _ViewMyaccount() {
        const data_sign = { password: newPassword, email: emailData };
        api.loginUser(data_sign).then((res) => {
            if (res?.data?.code === 1) {
                setUserToken(res.data.token);
                localStorage.setItem("Token", res.data.token);
                router.push('/us/user/dashboard')
                localStorage.removeItem('resetemail')
            }
        });
    }
    return (
        <div className="resetPassword_form">
            {props?.isLogin !== true &&
                <form onSubmit={handleSubmit(onSubmit)} className="signupform main-sign-frm password">
                    <div className="mb-3 input_section">
                        <h1 className="title"> Reset your password</h1>
                        <label htmlFor="InputConfirmPassword" className="form-label">Password</label>
                        <input
                            className="form-control"
                            ref={register({
                                required: "Please enter new password",
                                minLength: {
                                    value: 6,
                                    message: "Passwords must be at least 6 characters "
                                }
                            })}
                            name="password"
                            placeholder="Password"
                            type="password"
                            disabled={afterReset}
                        />
                        <span className="error">{errors.password && errors.password.message}</span>
                        <br />
                        <label htmlFor="InputConfirmPassword" className="form-label">Confirm Password</label>
                        <input
                            className="form-control"
                            ref={register({
                                required: "Please enter confirm password",
                                minLength: {
                                    value: 6,
                                    message: "Passwords must be at least 6 characters "
                                },
                                validate: value => (getValues('password') === value) || "Password and confirm password are not same"
                            })}
                            name="confirmPassword"
                            placeholder="Confirm Password"
                            type="password"
                            disabled={afterReset}
                        />
                        <span className="error">{errors.confirmPassword && errors.confirmPassword.message}</span>
                        <div className="sign-up-bnt">
                            <button type="submit" className="btn btn-primary" style={{ maxWidth: 'none' }}>RESET PASSWORD</button>
                        </div>
                        {showSuccess && <span className="error" style={{ color: 'green' }}>{showSuccess}</span>}
                        {errorPassword && <span className="error">{errorPassword}</span>}
                    </div>
                </form>
            }
            <DialogComponent opend={open} handleClose={() => setOpen(false)} title="Password changed" classFor="order_details">
                <DialogSectionCss>
                    <div className="popupinfo">
                        <div className="rest-pass">
                            <p>Your password has been reset!</p>
                        </div>
                        <div className="would-para">
                            <p>Would you like to:</p>
                        </div>
                        <div className="view-btn d-flex" style={{ color: '#fff', gap: '40px' }}>
                            <button className="btn.btn-primary" onClick={() => _ViewMyaccount()}> View My Account</button>
                            <button className="btn.btn-primary" onClick={() => router.push('/')}>Shop Now</button>
                        </div>
                    </div>
                </DialogSectionCss>
            </DialogComponent>
        </div >
    );
};
export default ResetToken;
