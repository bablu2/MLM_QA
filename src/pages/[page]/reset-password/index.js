import Head from "next/head";
import { useRouter } from "next/router";
import ResetPasswordStyle from "./ResetPassword.style";
import EmailForm from "./EmailForm";
import ResetToken from "./ResetToken";
import SvgAnimationBackground from '@Components/Common/SvgAnimationBackground';

const ResetPasswordComp = props => {
    const router = useRouter();
    return (
        <>
            <Head>
                <title>Reset Password</title>
            </Head>
            <ResetPasswordStyle>
                <div className="container">
                    {props?.isLogin !== true &&
                        router?.query?.token ? <ResetToken /> : <EmailForm />
                    }
                </div>
            </ResetPasswordStyle>
            <SvgAnimationBackground />
        </>);
}

export default ResetPasswordComp;