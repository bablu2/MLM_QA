import SvgAnimationBackground from '@Components/Common/SvgAnimationBackground';
import React from 'react';
import ReturnPolicyCss from './ReturnPolicy.style';

const ReturnPolicy = () => {
    return (
        <>
            <ReturnPolicyCss>
                <div className="proven_banifits_section_banner">
                    <img src="/images/return_refund_banner.jpg" />
                </div>
                <div className="container return">
                    <h1>RETURN POLICY</h1>
                    <h2>KAIRE 365 DAY SATISFACTION GUARANTEE</h2>

                    <p>
                        Over 3 Million Bottles Sold - with Quality Comes Confidence!
                </p>
                    <p>
                        Over 20 years in business, and 3 million plus bottles of product sold, is a result of one thing, and that one thing is a quality product line.
                </p>
                    <p>
                        Just like the many thousands of customers world wide before you who have enjoyed the amazing life changing health benefits, we are extremely confident that you will to.
                </p>
                    <p>
                        So confident that we are offering you an unprecedented 365-Day Unconditional Money Back Guarantee!
            </p>
                    <p>
                        Now, Let’s Be Clear On This…
             </p>
                    <p>
                        No Matter What The Reason, You Have 365 Days From The Date of Purchase to Return The Product … No Questions Asked!
                </p>
                    <p>
                        All you have to do is Contact Customer Support telling them that you wish to return your product… send us the original containers, and pay the postage… It Is as Easy As That!
                </p>
                    <p>
                        You have absolutely nothing to lose and a healthier you to gain.
                </p>
                    <p>
                        Order Now!
                </p>
                </div>
            </ReturnPolicyCss>
            <SvgAnimationBackground />
        </>
    )
}

export default ReturnPolicy
