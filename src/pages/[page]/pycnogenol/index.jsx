import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react'
import api from '@api/Apis';
import { toast } from 'react-toastify';
import VedioPlayer from '@Components/Common/VedioPlayer';
import parse from 'html-react-parser';
import pycnogenolstyle from './pycnogenol.style';

export default function Pycnogenol({ setInnerLoader }) {
    const { page } = useRouter();
    const [state, setState] = useState({});
    const [bgImage, setBgImage] = useState("");

    useEffect(() => {
        getPycnogenolPageContent();
    }, [])

    const getPycnogenolPageContent = async function () {
        setInnerLoader(true)
        await api.getPycnogenolFunction(page).then(res => {
            if (res?.data?.code === 1) {
                setState(res?.data)
                setInnerLoader(false)
            } else if (code === 0) {
                setState(message);
                setInnerLoader(false)
            }
        }).catch(err => {
            // toast.error("Somthing went wrong")
        })

    }

    return (
        <>
            {
                state?.content?.map((values) => {
                    return (
                        <>
                            <pycnogenolstyle bannerImg={`${process.env.API_URL + values?.banner_image}`}>
                                <div className="banner-video">
                                    <VedioPlayer url={values?.section1_vedio_link} />
                                </div>
                                <div className="container">
                                    <div className="container-main">
                                        {values?.main_content ? parse(values?.main_content) : <h2> content....</h2>}
                                    </div>
                                </div>
                            </pycnogenolstyle>
                        </>
                    )
                })
            }
        </>

    )
}

